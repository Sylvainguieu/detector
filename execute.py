import re
from . import config
if config.debug:
    print "execute.py"
images_data_keys = ["signal", "sigma2","sigma3", "sigma4","signalerror", "sigma2error",
                    "signalvariance","sigma2variance"                    
                ]
ptc_data_key    = ["gain","flux","noise", "noise2","ptcchi2", "linchi2"]
images_key_keys = ["dit","polar", "illumination"]

_var_find = re.compile("([^a-zA-Z_]|^)([a-zA-Z_][a-zA-Z_0-9]*)([^a-zA-Z_]|$)")
def transform_exp(exp,imname="images",ptcname="ptc"):
    offset = 0
    out = exp
    for gr in _var_find.finditer(exp):
        pref, var, suff = gr.groups()
        if var in images_data_keys:
            vname = "%s.get%s(**kwargs)"%(imname,var.capitalize())
        elif var in ptc_data_key :
            vname = "%s.get%s(**kwargs)"%(ptcname,var.capitalize())
        elif var in images_key_keys:
            vname = "%s.get%s2data(**kwargs)"%(imname,var.capitalize())
        else:
            vname = var        
        out = out[0:gr.start(2)+offset]+vname+out[gr.end(2)+offset:]
        offset = len(vname)-len(var)
    return out

def explore(exp, images,ptc, **kwargs):
    exp = transform_exp(exp,imname="images",ptcname="ptc")
    exec "__value__ = "+exp
    return __value__

class _ExecuteCapabilities_(object):
    def _transform_exp(self,exp):
        offset = 0
        out = exp
        for gr in _var_find.finditer(exp):
            pref, var, suff = gr.groups()
            tmp_vname = "get%s"%(var.capitalize())
            if hasattr(self, tmp_vname ):
                vname = "self."+tmp_vname+"(**kwargs)"
            else:
                tmp_vname = "get%s2data(**kwargs)"%(var.capitalize())
                if hasattr(self, tmp_vname ):
                    vname = "self."+tmp_vname+"(**kwargs)"
                else:
                    self.say("%s var '%s' is unknown for this object left for globals"%(tmp_vname, var),vtype=config.WARNING)
                    vname = var        
            out = out[0:gr.start(2)+offset]+vname+out[gr.end(2)+offset:]
            offset = len(vname)-len(var)
        return out
    
    def execute(self, exp, **kwargs):
        exp = self._transform_exp(exp)
        exec "__value__ = "+exp
        return __value__
