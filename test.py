#!/anaconda/bin/python
from detector import rapid as r
from detector import fitsopener as opener
from detector import carac as c
import numpy as np

fls3d  = c.Carac(opener.open_glob( "/Users/guieu/DETDATA/Pionier3d/PIONIER_FLAT_F*_069_0005.fits",  opener=c.Combined.open))
darks  = c.Carac(opener.open_glob('/Users/guieu/DETDATA/Pionier3d/PIONIER_FLAT_F-10_069_0014.fits', opener=c.Combined.open))
for f in fls3d:
    f.setOffset( -darks.get_dit.choose(f.get_dit()).get_illumination.choose(-10).getSignal(expo_reduce=np.mean,nooffset=True))
flats3d = fls3d.withIllumination( (-9,100))
darks3d = fls3d.withIllumination( -10)


flats3d.withDit(0.001).getPtc(signalrange=(0,1000))
flats3d.withDit(0.001).getPtc(signalrange=(0,30000))
print "----------------------------------"
print flats3d.withDit(0.001).getPtc(signalrange=(0,1000)).getGain(pixel=67448)
print flats3d.withDit(0.001).getPtc(signalrange=(0,30000)).getGain(pixel=67448)
print flats3d.withDit(0.001).plots.ptc(pixel=67448).fit(xrange=(0,1000)).result
print flats3d.withDit(0.001).plots.ptc(pixel=67448).fit(xrange=(0,30000)).result
print "----------------------------------"

N = 67400
print "----------------------------------"
print flats3d.withDit(0.001).getPtc(signalrange=(0,1000)).getGain(pixel=N)
print flats3d.withDit(0.001).getPtc(signalrange=(0,30000)).getGain(pixel=N)
print flats3d.withDit(0.001).plots.ptc(pixel=N).fit(xrange=(0,1000)).result
print flats3d.withDit(0.001).plots.ptc(pixel=N).fit(xrange=(0,30000)).result
print "----------------------------------"

