import numpy as np
from ..wrapper import BaseWrapper, ListWrapper, on2iterable

class ArrayWrapper(BaseWrapper):
    """ An numpy array is ignitialized and created """
    minstrlen = 80
    def init(self, obj, on, args, kwargs):
        self.args   = list(args)
        self.kwargs = kwargs

        self.on =  self.get_iter(obj, on)
        self.obj = self.get_obj(obj)
        self.size = len(self.on)
        ##
        # empty array, it will be initialized after the first add
        self.A = np.array([])
        return self

    def initarray(self, first):
        if isinstance(first, np.ndarray):
            shape = (self.size,)+first.shape
            dtype = first.dtype
        else:
            shape = (self.size,)
            dtype = np.dtype(type(first))
            if dtype.kind=="S":
                dtype = "S%d"%max(len(first), self.minstrlen)
        self.A = np.ndarray(shape, dtype)

    def add(self, key, value):
        if not self.index:
            self.initarray(value)
        self.A[self.index] = value

    def end(self):
        A = self.A
        del self.A
        del self.obj
        return A


class ArrayLightWrapper(ListWrapper):
    """ just return np.array(of-the-created-list) """
    def end(self):
        return np.array(self.output)
