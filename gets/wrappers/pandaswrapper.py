import pandas
from ..wrapper import ListWrapper, on2iterable



class PandasWrapper(ListWrapper):
    """
    A panda wrapper.

    if the the attribute columns is None  that supose that the fget function
    contains an undefined number of args and that args correspond
    to the column name.
    """
    columns = None
    def __init__(self, columns=None):
        self.columns = columns if columns is not None else self.columns

    def get_columns(self,obj, args, kwargs):
        if self.columns is None:
            if len(args):
                return list(args)
            else:
                return None

        if self.columns is False:
            return None
        return list(self.columns)


    def init(self, obj, on, args, kwargs):
        self.args   = list(args)
        self.kwargs = kwargs


        self.on =  self.get_iter(obj, on)
        self.obj = self.get_obj(obj)

        self.size = len(self.on)

        self.cols = self.get_columns(obj, args, kwargs)

        self.data  = []
        self.indexes = []

        return self

    def add(self, key, value):
        self.data.append(value)
        self.indexes.append(key)

    def end(self):
        pd = pandas.DataFrame( self.data, index=self.indexes,
                                columns = self.cols
                                )
        return pd

    def __call__(self, columns=None):
        columns = columns if columns is not None else self.columns
        return self.__class__(columns)

