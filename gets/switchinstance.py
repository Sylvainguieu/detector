from .baseinstance import BaseInstance, BaseNumerical

class SwitchInstance(BaseInstance):
    on = 0
    _attrs_ = ["on"]
    def get(self, *args, **kwargs):
        on = self.on
        obj = self.obj
        fget = self.fget
        found = False
        if isinstance(on, set):
            for i in on:
                try:
                    o = obj[i]
                    found = True
                    break
                except:
                    continue
            if not found:
                raise KeyError("Cannot find target item in any of %s indexes"%on)
        else:
            o = obj[on]

        return fget(o, *args, **kwargs)

        if on is False:
            if hasattr(obj, self.on_default_attr):
                on = getattr(obj, self.on_default_attr)
            else:
                raise Exception("cannot find target to swicth")
        return self.exec_func(obj, self.fget, on, args, kwargs)

    def map(self, mapfunc, *args, **kwargs):
        """ map exist here for compatibility with ListInstance
        i.map( mapfunc, *args, **kwargs)
        return mapfunc( i.get(*args, **kwargs))
        """
        return mapfunc(self.get(*args, **kwargs))


class SwitchNumerical(BaseNumerical):
    pass
