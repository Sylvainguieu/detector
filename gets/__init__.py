"""
gets module aims to facilitate the biuld of methods of objects having
sub-object (like list of data container).
It also provide additional capabilities to get functions.

The getmethod decorator as keywords 'fget' and on' (in that order) thet
do has follow:
A call of getmethod return an other getmethod instance with new default
attributes :  e.g.: getdata = getmethod(on=True, wrapper=np.ndarray) ....
                    getdata.on #is True
                    getdata.wrapper # is np.ndarray

 * if on is None:
      the function fget is applied to the object itself, the first element
      of the fget function is the class instance (usually named self)
      It does nothing special but is useful to build parent classes (see bellow)

 * if on is True or All:
      the function is called on each elements of the object and returned in
      a list or in something returned by a wrapper function (see bellow for wrapper)

      Therefore:
            class A(list):
                @getmethod(on=True)
                def get_data(child, scale=1.0, offset=0.0):
                    return child.get_data(scale=scale, offset=offset)

      #is equivalent to:
            class A(list):
                def get_data(self, scale=1.0, offset=0.0)
                    return [child.get_data(scale=scale, offset=offset) for child in self]
      #an other way to do it:
            class A(list):
                get_data = getmethod("get_data", on=True)

 * if on is anything else that can be used as index:
      the fget function is called on the item of self[on]

            getdata = getmethod(on=0)
            class Switch(list):
                @getdata
                def get_data(child, scale=1.0, offset=0.0):
                    return child.get_data(scale=scale, offset=offset)
        #is equivalent to
            class Switch(list):
                def get_data(self,  scale=1.0, offset=0.0):
                    return self[0].get_data(scale=scale, offset=offset)

        #also
            class Switch(list):
                get_data = getdata("get_data")

The example can looks a bit ridiculus but the getmethod can be very
usefull when there is a large number of function to declare.
Also if at some point you decide that a list of array must be returned
in a np.array for instance just do: getmethod = getmethod(wrapper=np.array)
before class definition and you are set.

As an example:

class ListArray(list):
    pass
from gets import get_call_attrs, build_getmethod_string, getmethod
build_getmethod_string(ListArray, get_call_attrs(np.ndarray),
                       getmethod(on=True)
                       )
# Will bring all the methods os numpy ndarray to ListArray
test = ListArray( np.ones((10,20) )*i for i in range(10) )
test.flatten() # flatten all element of the list
#.. etc ..

An other example, a class for a list of fits file (used in astronomy)

from astropy.io import fits
datamethod   = getmethod(on=True, wrapper=np.array)
scalarmethod = getmethod(on=0)
def get_keyword(obj,key):
    return obj.header[key]

fits.HDUList.keyword = scalarmethod(get_keyword, on=0,
                                        doc="return the keyword on the first hdu")


class FitsList(list):
    @datamethod
    def get_data(obj):
        return obj[0].data
    @datamethod
    def get_mean_data(obj):
        return obj[0].data.mean()

    keyword  = scalarmethod("keyword", doc="return first keyword in the list")
    keywords = datamethod("keyword", doc="return all keys in an array")

fls = FitsList( fits.open(f) for f in list_of_files )

the  getmethod of a list (on=True) provide usefull function:
biases = fls.keywords.select( 0.0 , "EXPOSURE")
return a FitsList instance with only the fits file with the keyword
       "EXPOSURE" equal to 0.0

the getnum_method say explicitely that the returned individual value are numerical
and more attribute of the getmethod will be available, like minis, maxes, means, choose, etc...

An other way to do that is to use filter:
biases = fls.keywords.filter(lambda v:v==0.0 , "EXPOSURE")

is euivalent to:

    biases = FitsList(f for f in fls if f[0].header["EXPOSURE"] == 0.0)

fls.keywords.set("EXPOSURE") # return an ordered list of set of "EXPOSURE"

also
fls.keyword.iter("EXPOSURE") # will send an iterator object that will iter
on the set of exposures and return a FitsList having the same EXPOSURE at each
iteration.

e.g.:
#------
for sub in fls.keywors.iter("EXPOSURE"):
    print sub.keyword("EXPOSURE"), sub.get_data().mean()
#------
The iterator has also some magic attribute usefull for ploting for instance

#----
it = fls.keywors.iter("TARGET")
for sub in it:
    plt.plot( sub.keywords("EXPOSURE"), sub.get_mean_data(), marker=it.marker,
             legend='TARGET = %s'%it.value, color=it.color)
#----


An other cool thing:

datamethod   = getmethod(on=True, wrapper=DictWrapper)
scalarmethod = getmethod(on=0)

class FitsDict(dict):
    @datamethod
    def get_data(obj):
        return obj[0].data
    @datamethod
    def get_mean_data(obj):
        return obj[0].data.mean()

    keyword  = scalarmethod("keyword", on="bias")
    keywords = datamethod("keyword", doc="return all keys in an array")

fld = FitsDict( bias=fits.open(..), flat=fits.open(..) )
fld.get_data() # return a dictionary {
                                      "bias":**DATA**,
                                      "flat":**DATA**
                                     }


getmethod can also be usefull to make a class definition inside
an other class to act like a normal function does:

class A(object):
    def __init__(self, b, *args, **kwargs):
        self.b = b
        # do somethig

class B(object):
   a = getmethod(A)

B().a().b  # <__main__.B at 0x105942350>

"""


if True: #debug
    import wrapper
    reload(wrapper)
    import baseinstance
    reload(baseinstance)
    import listinstance
    reload(listinstance)
    import datainstance
    reload(datainstance)
    import switchinstance
    reload(switchinstance)
    import base
    reload(base)
    from wrappers import numerical, pandaswrapper
    reload(numerical)
    reload(pandaswrapper)


from base import (BaseGet, BaseNumerical, BaseLoop,
                  build_getmethod, getmethod_dict,
                  build_getmethod_string, get_call_attrs,
                  build_doc
                  )
from listinstance import ListInstance, ListNumerical
from datainstance import DataInstance, DataNumerical
from switchinstance import SwitchInstance, SwitchNumerical
from wrapper import (BaseWrapper, ListWrapper, DictWrapper, ArgsWrapper)

import config
if getattr(config, "use_numpy", True):
    from wrappers.numerical import ArrayWrapper, ArrayLightWrapper
if getattr(config, "use_pandas", False):
    from wrappers.pandaswrapper import PandasWrapper


class Get(BaseGet):
    """
    getmethod instance of Get is a decorator aimed to simplify parsing of method from
    child to parent.
    e.g.:

    class ChildData(object):
        def __init__(self, data):
            self.data = data
        @getmethod
        def get_data(self, scale=1.0, offset=0.0):
            return self.data*scale-offset

    class ChildKeyword(dict):
        @getmethod
        def get_keyword(self, k, default=None):
            return self.get(k, default)

    class Switch(dict):
        get_data = getmethod("get_data", on="data")
        get_keyword = getmethod("get_keyword", on="keywords")


    class List(list):
        pass
    # automaticaly bring the Switch methods into switch
    build_getmethod(List, [(True,Switch)])

    """
    DataBaseInstance   = DataInstance # must be class instance
    SwitchBaseInstance = SwitchInstance
    ListBaseInstance   = ListInstance

    DataInstances  =  tuple()
    SwitchInstances = tuple()
    ListInstances   = tuple()
    def _init_vtype(self):
        vtype = self.vtype
        if vtype in [None,False]: return
        if vtype in ["numerical", True]:
            self.DataInstances   += (DataNumerical,)
            self.SwitchInstances += (SwitchNumerical,)
            self.ListInstances   += (ListNumerical,)

class GetNum(Get):
    DataInstances  =  (DataNumerical,)
    SwitchInstances = (SwitchNumerical,)
    ListInstances   = (ListNumerical,)

class LoopArgs(BaseLoop):
    pass


getmethod = Get()
loopargsmethod = LoopArgs()

getnum_method = getmethod(vtype="numerical")



def _test_():
    global test, test2, test3
    import numpy as np
    class DataTest(object):
        def __init__(self, data):
            self.data = data
        @getmethod
        def get_data(self):
            return self.data
        @getmethod(name="datatime", wrapper=np.array)
        def get_datatime(self, factor):
            return self.get_data()*factor
        @getmethod( wrapper=np.array)
        def get_mean(self):
            return np.mean(self.get_data())


    class Test(list):
        get_data = Get("get_data", on=0)
        get_datatime = Get("get_datatime", on=[1,0])
        get_all1 = Get("get_data", on=True, wrapper=ArrayWrapper())
        get_all2 = Get("get_data", on=True, wrapper=np.array)
        @getmethod(on=0)
        def toto(obj):
            return obj.get_data()

    class Test2(list):
        object
    build_getmethod(Test2, [(0,DataTest)])
    class Test3(list):
        object
    build_getmethod(Test3, [(True,DataTest)])

    datal = [ DataTest(np.ones((30,40))*i) for i in range(2000)]

    test = Test(datal)
    test2 = Test2( [DataTest(np.ones((30,40)) ), DataTest(np.ones((30,40))*2) ])
    test3 = Test3( [DataTest(np.ones((30,40)) ), DataTest(np.ones((30,40))*2) ])
    print test.get_datatime(10).mean()
    print test.get_all1()
    print test.toto()

    print "test2", test2.get_datatime(10)
    print "test3", test3.get_datatime(20)


