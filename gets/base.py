from .wrapper import iswrapperinstance, ArgsWrapper
from .baseinstance import BaseInstance, BaseNumerical
from .listinstance import listclasses

functype = type(lambda x:x)

class BaseGet(object):
    Instance = None
    name = None
    fget = None
    on = None
    doc = None
    description = None

    fkey = None

    DataBaseInstance   = None # must be class instance
    SwitchBaseInstance = None
    ListBaseInstance   = None

    DataInstances   = None # must be class instance
    SwitchInstances = None
    ListInstances   = None

    wrapper = None
    selfwrapper = None
    items  = None
    isdict = None
    iterable = False
    vtype = None

    _parent = None

    property = False

    _attrs_ = ["fget", "on", "fkey", "name",
                "doc", "description", "wrapper",  "selfwrapper", "items",
                "isdict", "iterable", "vtype", "property",
                "_parent"
               ]

    def __init__(self, fget=None, on=None, fkey=None,
                 name=None, doc=None, description=None, wrapper=None,
                 selfwrapper=None, items=None,
                 isdict=None, iterable=None, vtype=None,
                 property=None, _parent=None
                ):
        if fget is not None:
            self.fget = fget
        fget = self.fget

        if fkey is not None:
            self.fkey = fkey

        if wrapper is not None:
            self.wrapper = wrapper

        if name is not None:
            self.name = name
        else:
            self.name = self.name # make a copy

        if doc is not None:
            self.doc = doc

        if self.doc is None:
            if not isinstance(fget, basestring):                
                self.doc = getattr(fget,"__doc__", None)
                if self.doc is None: # is this is an instance function 
                    self.doc = getattr(fget,"__func__", None)

        if description is not None:
            self.description = description

        if wrapper is not None:
            if iswrapperinstance(wrapper):
                wrapper = wrapper()

            self.wrapper = wrapper

        if selfwrapper is not None:
            self.selfwrapper = selfwrapper

        if _parent is not None:
            self._parent = _parent


        if items is not None:
            self.items = items

        if isdict is not None:
            self.isdict = isdict

        if iterable is not None:
            self.iterable = iterable

        if vtype is not None:
            self.vtype = vtype

        if property is not None:
            self.property = property


        #####
        # init the vtype (e.i. add more subclasses  if needed)
        self._init_vtype()

        on = True if on is all else on
        self.on = on
        ###
        # ignitialise the Instance class
        self.init()

    def init(self):
        on = self.on
        if on is None or on is False:
            self._data_init()
        elif (not hasattr(on, "__iter__") \
                  or isinstance(on, set)\
              ) and on is not True and not isinstance(on, slice):
            self._switch_init()
        else:
            self._list_init()


    def _init_vtype(self):
        """ ignitialisation of the vtype (add more subclasses) """
        # leave it to the parent class
        raise NotImplemented()


    def local(self, *args, **kwargs):
        ## keep always the same parent
        kwargs["_parent"] = self.parent()
        return self(*args, **kwargs)

    def parent(self, *args, **kwargs):
        if self._parent:
            return self._parent( *args, **kwargs)
        return self(*args, **kwargs)

    def switcher(self, fget=None, on=0, **kwargs):
        if isinstance(name_or_func, basestring):
            return self(fget=fget, on=on, **kwargs)


    def lister(self, fget=None, on=True, **kwargs):
        return self(fget=fget, on=on, **kwargs)


    def getter(self, *args, **kwargs):
        return self(*args, **kwargs)

    def loopargs(self, fget=None, loopargs=None, lenargs=None,
                    flen=None, mode=None, **kwargs):

        kwargs["on"] = True
        getmethod = self.local(fget=fget,**kwargs)

        wrapper = ArgsWrapper(loopargs=loopargs, lenargs=lenargs,
                              flen=flen, mode=mode,
                              wrapper = getmethod.wrapper,
                              items = getmethod.items
                              )
        return getmethod(wrapper=wrapper)

    def derive(self, *args, **kwargs):
        Nargs = len(args)
        for i, value in enumerate(args):
            attr = self._attrs_[i]
            if attr in kwargs:
                raise TypeError("getmethod() got multiple values for keyword argument '%s'"%attr)
            kwargs[attr] = value


        for k in self._attrs_:
            if kwargs.get(k,None) is None:
                kwargs[k] = getattr(self,k)

        # clean up the None values
        kwargs = { k:v for k,v in kwargs.iteritems() if v is not None }
        return self.__class__(**kwargs)

    def _new_instance(self, instance):
        """ create a new instance of this object
        a new instance is created from the __get__ method
        """
        return self.Instance(instance)


    def __getandreturn__(self,instance, *args, **kwargs):
        return self.Instance(instance)(*args, **kwargs)

    def __get__(self, instance, ctype=None):

        if self.property:
            if instance is None:
                #####
                # when called from class the @property decorator
                # return a property instance, so i do the same
                # here
                return self
            return self._new_instance(instance)()
        ######
        # not completly obvious to know what to do
        # when it is called from a class type object
        # maybe the best is to return self.__getandreturn__
        # so it can act like a function called from its class
        #
        if instance is None:
            return self.__getandreturn__
        return self._new_instance(instance)


    def __call__(self, *args, **kwargs):
        return self.derive(*args, **kwargs)




    def _mutual_dict(self, _dict_):

        if self.name is not None:
            _dict_["name"] = self.name

    def _data_dict(self):

        _dict_ = dict()
        self._mutual_dict(_dict_)

        if isinstance(self.fget, basestring):
            if self.fget[0] == ".":
                #this is an attribute, not a function
                attr = self.fget[1:]
                def fget(obj):
                    return getattr(obj, attr)
                fget.__doc__ = "Alias of '%s' attr"%self.fget
            else:
                def fget(obj, *args, **kwargs):
                    return getattr(obj, self.fget)(*args, **kwargs)
                fget.__doc__ = "Alias of %s()"%self.fget
        else:
            fget = self.fget

        _dict_["fget"] = staticmethod(fget)
        if self.doc is None:
            _dict_["__doc__"] = fget.__doc__
        else:
            _dict_["__doc__"] = self.doc

        return _dict_


    def _data_init(self):
        subclasses = self.DataInstances+(self.DataBaseInstance,)
        self.Instance = type("_DataInstance", subclasses,
                             self._data_dict()
                             )


    def _switch_dict(self):

        _dict_ = dict(on=self.on)
        self._mutual_dict(_dict_)

        if isinstance(self.fget, basestring):
            if self.fget[0] == ".":
                #this is an attribute, not a function
                attr = self.fget[1:]
                def fget(obj):
                    return getattr(obj, attr)
                fget.__doc__ = "call the '%s' attr of the child "%self.fget
            else:
                def fget(obj, *args, **kwargs):
                    return getattr(obj, self.fget)(*args, **kwargs)
                fget.__doc__ = "call %s() of the child "%self.fget
        else:
            fget = self.fget

        _dict_["__doc__"] = """
        On the first found item of: {self.on}
        ------------------------------------
        {doc}
        """.format(self=self,
                   doc=self.doc if self.doc is not None else fget.__doc__)

        _dict_["fget"] = staticmethod(fget)
        return _dict_

    def _switch_init(self):
        subclasses = self.SwitchInstances+(self.SwitchBaseInstance,)
        self.Instance = type("_SwitchInstance", subclasses,
                             self._switch_dict()
                             )

    def _list_get_subclasses(self):


        wrapper = self.wrapper


        if iswrapperinstance(wrapper):
            ###
            # if is a Wrapped Instance iteration and returned object
            # are handled by it
            wt = "lst" # does not matter if wrapped shoudl be the same classe
            won = "wrapped"

        else:
            if self.items is None:
                if wrapper is None:
                    # do not know anything about the wrapper
                    # we need to guess it from the instance.obj
                    wt = "any"
                elif hasattr(wrapper, "iteritems"):
                    ###
                    # looks like it is a dictionary so we need
                    # to return a item list
                    wt = "items"
                else:
                   ####
                   # otherwhise return in a list listinstance.value
                   ####
                   wt = "lst"

            elif self.items:
                wt = "items"
            else:
                wt = "lst"



            if self.on is True:
                won = "true"
            else:
                won = "lst"
        ##############################
        # guess the selfwrapper
        # if isdict is given, it is excplicit
        # otherwhise return the any submethods
        ########################
        if self.isdict is None:
            ws = "any"
        elif self.isdict:
            ws = "items"
        else:
            ws = "lst"

        subclasses = (listclasses["sub"][won][ws],
                      listclasses["get"][won][wt]
                     )
        if self.iterable:
            subclasses += (istclasses["iterable"],)
        return subclasses

    def _list_dict(self):
        _dict_ = dict()
        self._mutual_dict(_dict_)

        if isinstance(self.fget, basestring):
            if self.fget[0] == ".":
                #this is an attribute, not a function
                attr = self.fget[1:]
                def fget(obj):
                    return getattr(obj, attr)
                fget.__doc__ = "call '%s' attr on childs"%self.fget
            else:
                def fget(obj, *args, **kwargs):
                    return getattr(obj, self.fget)(*args, **kwargs)
                fget.__doc__ = "call %s() on childs"%self.fget
        else:
            fget = self.fget

        _dict_["__doc__"] = """
        On items {on}
        -------------
        {doc}
        """.format(on="'all'" if self.on is True else self.on,
                   doc=self.doc if self.doc is not None else fget.__doc__
                   )


        if isinstance(self.fkey, basestring):
            if self.fkey[0] == ".":
                #this is an attribute, not a function
                attr = self.fkey[1:]
                def _fkey(obj, key):
                    return getattr(obj, attr)
            else:
                def _fkey(obj, key):
                    return getattr(obj, self.fkey)(obj)

            _fkey.__doc__ = "return key from %s()"%self.fkey
        elif self.fkey in [None,False]:
            def _fkey(obj, key):
                return key
        else:
            def _fkey(obj, key):
                return self.fkey(obj)


        _dict_["fget"]  = staticmethod(fget)
        _dict_["_fkey"] = staticmethod(_fkey)




        wrapper = self.wrapper

        if self.items is not None and wrapper is None:
        ###
        # if items is set but the wrapper is not
        # return a dummy wrapper
            wrapper = lambda lst:lst

        if type(wrapper)==functype:
            _dict_["wrapper"] = staticmethod(wrapper)
        else:
            _dict_["wrapper"] = wrapper

        if self.selfwrapper is not None:
            _dict_["selfwrapper"] = self.selfwrapper

        _dict_["on"] = self.on
        return _dict_

    def _list_init(self):

        subclasses = self.ListInstances+self._list_get_subclasses()\
                         +(self.ListBaseInstance,)

        self.Instance = type("_ListInstance", subclasses,
                             self._list_dict()
                            )

class LoopInstance(object):
    def __init__(self, func , argswrapper):
        self.func = func
        self.argswrapper = argswrapper

    def __call__(self, *args, **kwargs):
        func = self.func
        if func is None:
            raise ValueError("func not set")
        wrapper = self.argswrapper
        iterator = wrapper.init(None, True, args, kwargs)
        if iterator is None:
            return func(*args, **kwargs)

        for key, dummy, args, kwargs in iterator:
            wrapper.add( key, func(*args, **kwargs))
        return wrapper.end()

class BaseLoop(object):


    _attrs_ =["loopargs", "lenargs", "wrapper", "items",
               "flen", "mode"
               ]
    argswrapper = ArgsWrapper
    def __init__(self, func=None, loopargs=None, lenargs=None, wrapper=None,
                 items=None, flen=None, mode=None):

        self.func = func
        self.argswrapper = self.argswrapper(
                                   loopargs=loopargs,
                                   lenargs=lenargs,
                                   wrapper=wrapper,
                                   items=items,
                                   flen=flen,
                                   mode=mode
                                   )
        if func:
            self.instance = LoopInstance(func, self.argswrapper)


    def __call__(self, func, *args, **kwargs):
        self = self.derive(func, *args, **kwargs)
        return self.instance

    def derive(self, func=None, *args, **kwargs):
        Nargs = len(args)
        for i, value in enumerate(args):
            attr = self._attrs_[i]
            if attr in kwargs:
                raise TypeError("getmethod() got multiple values for keyword argument '%s'"%attr)
            kwargs[attr] = value


        for k in self._attrs_:
            if kwargs.get(k,None) is None:
                kwargs[k] = getattr(self.argswrapper,k)

        # clean up the None values
        kwargs = { k:v for k,v in kwargs.iteritems() if v is not None }
        return self.__class__(func, **kwargs)





def build_getmethod(cl_to, isubclasses, recursive=False):
    """
    build_getmethod(class, listsubclasses_or_attrname, recursive=False)

    build the class method from a list of (on,subclass) tuple.

    If instead of a list a string is provided take the list
    from the class attribute of that string.
    if recursive is True, build also the subclasses, it works only
    if the the attr name is provided.




    """
    if isinstance(isubclasses, basestring):
        subclasses = getattr(cl_to, isubclasses)
    else:
        subclasses = isubclasses
        recursive = False

    for on, subclass in subclasses:
        if recursive:
            build_getmethod(subclass, isubclasses, True)
        copy_getmethod(subclass, cl_to, on)


def copy_getmethod(cl_from, cl_to, on=None):
    """ Copy all get method from cl_from to cl_to if the attribute
    in cl_to does not exists.
    on keywords gives the type of copy.


    on = False, the fget method is copied as it is
    on = True, the fget method will become fget(getattr(obj,attr), ...) where
         obj are all item of the object of class cl_from
    on = scalar or list the fget method will become fget(getattr(obj,attr), ...)
         where obj is the item found in any of the on list (first one served)

    """
    for sub in cl_from.__mro__:
        for attr, getmethod in sub.__dict__.iteritems():
            if hasattr(cl_to,attr) or not isinstance(getmethod, BaseGet):
                continue
            name = getmethod.name or attr

            if (on is False) or (on is None):
                if getmethod.fget== attr:
                    raise ValueError("'on' is false and fget=attr would result to infinite loop for attr '%s' "%attr)

                setattr(cl_to, attr, getmethod.parent(name=name))
            else:
                ###
                # if the getmethod is coming from a child and is
                # a property, add a '.' to tell the getmethod it is
                # a child property name
                fget = "."+attr if  getmethod.property else attr
                setattr(cl_to, attr, getmethod.parent(fget=fget, on=on, name=name))





def build_getmethod_string(cl, attrlist, getmethod):
    """
    for all attribute in attrlist build the attribute with the getmethod object:
        for attr in attrlist:
            if not hasattr(cl,attr):
                setattr(cl, attr, getmethod(attr))

    example of use:

class ListArray(list):
    pass

build_getmethod_string(ListArray, ["mean", "max", "min", "median"],
                       getmethod(on=True))

test = ListArray( np.ones( (10,20) )*i for i in range(10) )
test.mean()

-or-
from gets import get_call_attrs, build_getmethod_string, getmethod
build_getmethod_string(ListArray, get_call_attrs(np.ndarray),
                       getmethod(on=True)
                       )
will bring all the method (starting with [a-zA-Z]) of numpy ndarray to ListArray class
    """

    for attr in attrlist:
        if hasattr(cl,attr):
            continue
        name = getmethod.name or attr

        if getmethod.on is False or getmethod.on is None:
            if getmethod.fget== attr:
                raise ValueError("'on' is false, would result to infinite loop for attr '%s' "%attr)

            setattr(cl, attr, getmethod(name=name))
        else:
            fget = "."+attr if  getmethod.property else attr
            setattr(cl, attr, getmethod(fget=fget, name=name, thiswrapper=False))



def getmethod_dict(cl):
    """ return a dictionary of all getmethods found in the class """
    d = {}
    for sub in cl.__mro__:
         for attrname, getter in sub.__dict__.iteritems():
             if isinstance(getter,BaseGet):
                 d.setdefault(attrname, getter)
    return d

def get_call_attrs(cl, match=lambda attrname: attrname[0]!="_"):
    """ return a list of attrname for the attributes having the call method
    additionaly the attrname should return True when parsed to the match function
    default is :  match = lambda attrname: attrname[0]!="_"
    only the attribute  without '_' at begining.
    """
    l = []
    for sub in cl.__mro__:
         for attrname, attr in sub.__dict__.iteritems():
             if hasattr(attr, "__call__") and match(attrname):
                l.append(attrname)
    return l

def build_doc(cls, prefix="",
              fmt="{prefix}{attr}{call} : {descr}\n\n"):
    methods = {}
    for cl in cls[::-1]:
        methods.update(getmethod_dict(cl))

    doc = ""
    for attr, method in methods.iteritems():
        doc += fmt.format(prefix=prefix, attr=attr,
                          call  = "" if method.property else "(..)",
                          descr = method.description if method.description \
                                  else method.doc or "no doc"
                          )
    return doc
