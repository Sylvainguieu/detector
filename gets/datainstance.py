from .baseinstance import BaseInstance, BaseNumerical

class DataInstance(BaseInstance):
    def get(self, *args, **kwargs):
        return self.fget(self.obj, *args, **kwargs)

    def map(self, mapfunc, *args, **kwargs):
        """ map exist here for compatibility with ListInstance
        i.map( mapfunc, *args, **kwargs)
        return mapfunc( i.get(*args, **kwargs))
        """
        return mapfunc(self.get(*args, **kwargs))

class DataNumerical(BaseNumerical):
    pass

