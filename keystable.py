import numpy as np
from . import config
if config.debug: print "keystable.py"
from . import engine,bases,fits


class BinTableHDU(fits.BinTableHDU, bases.TableDataEntity):
    """ a bintable HDU but with the class make method"""
    @classmethod
    def make(cls, obj, rules, axes=None, name=""):
        """ make a BinTable of this class from an object and some rules
        see make_keys_table for more help
        """        
        return make_table( obj, rules, axes=axes, name=name, cls=cls)


class KeysTableEntity(bases.TableDataEntity):
    axes = [config.EXPO]
    """ This is a data entity (=HDU) containing a recarray (=Bintable) 
    The recarray contained keywords that usualy are in the header.
    for instance the getkey method will look at the rearray instead of the header
    """
    def getkey(self, key, **kwargs):
        return self.getData(key,**kwargs)
    def getRealKey(self, key, **kwargs):
        return super(KeysTableEntity, self).getkey(key, **kwargs)
    def getFilePath(self):
        return self.getkey("FILEPATH")
    
    
class KeysTableHDU(BinTableHDU, KeysTableEntity):
    def getkey(self, key, **kwargs):
        return self.getData(key,**kwargs)
    def haskey(self, key, **kwargs):
        return key in self.data.names
    def comment( self, key):
        return ""
        
def make_table(obj,rules, axes=None, name="", cls=BinTableHDU):
    """
    make a binary table from an object (e.i. a list of data, DataList).
    The rules is a list of tuples. Each tuples contains 4 arguments:
       - the table name (name= keyword of column)        
       - a 3 len tuple with either 
           * a string a list of args a dict of kwargs 
             getattr(obj,funcname)(*args, **kwargs) is called 
           * or a function a list of args a dict of kwargs 
              func( *args, **kwargs) is called to fille the column 
       - A dictionary containing any additional keywords to pass to the pyfits Column method
    The format is automaticaly matched from the data if empty
    The return object is a BinaryTable or a bintable of the class cls (optional kwargs).
    if name is ""  default is keywords
    """
    columns = []
    
    for kwargs in rules:
        kwargs = kwargs.copy()
        
        namek = kwargs.pop("name")
        array = get_array(obj, kwargs.pop("array"))
        fmt, dim = update_format(array, kwargs.pop("format",None), kwargs.pop("dim",None))
        
        columns.append(
            
            fits.pf.Column(name=namek, format=fmt, array=array, dim=dim, **kwargs)
        )
    cd = fits.pf.ColDefs(columns)
    table = fits.pf.new_table(cd)
    fits.parseclass( table, cls)
    
    if name : table.name = name
    elif not table.name: table.name = "keywords"
    
    if axes is not None: table.axes = axes    
    return table

def make_table_of_files(files, rules,subclass=fits.HDUList, axes=None, name="", cls=BinTableHDU):
    """ Build a binary table from a list of files 
    The subclass keyword is the class for witch the opened HDUList will be parsed.
    see make_keys_table
    """
    #init the values disctionary
    names = [r["name"] for r in rules]
    values_dict = {namek:[] for namek in names}
    columns = []
    
    for f in files:
        obj = subclass(fits.pf.open(f))
        for kwargs in rules:
            kwargs = kwargs.copy()
            namek   = kwargs.pop("name")
            array = get_array(obj, kwargs.pop("array"))
            values_dict[namek].append(array)
            
    for kwargs in rules:
        kwargs = kwargs.copy()
        kwargs.pop("array") 
        array = values_dict[namek]
        fmt, dim = update_format(array, kwargs.pop("format",None), kwargs.pop("dim",None))
        namek =  kwargs.pop("name")
        columns.append(
            fits.pf.Column( name=namek, format=fmt, array=array, dim=dim,  **kwargs)
        )
    cd    = fits.pf.ColDefs(columns)
    table = fits.pf.new_table(cd)
    fits.parseclass( table, cls)
    
    if name : table.name = name
    elif not table.name: table.name = "keywords"
    if axes is not None: table.axes = axes    
    return table

def get_array( obj,gf):
    if isinstance( gf, (basestring,tuple)):
        gf, a, kw = update_getfunc(gf)
        if isinstance( gf, basestring):
            return getattr( obj, gf)(*a, **kw)
        return gf( *a, **kw)
    ##
    # gf is the array 
    return gf
        
def update_getfunc(gf):
    if isinstance(gf, basestring):
        return gf,[],{}
    if isinstance(gf, tuple):
        if len(gf)==3:
            if isinstance( gf[1], dict):
                return (gf[0],gf[2],gf[1])
            return gf            
        if len(gf)==1:
            return (gf[0],[],{})
        if len(gf)==2:
            if isinstance( gf[1], dict):
                return (gf[0],[],gf[1])
            return (gf[0],gf[1], {})
    raise ValueError("Expecting a string or tuple for array function call got %s"%gf)
        
    
def update_format(array, fmt, dim):
    if not isinstance( array, np.ndarray):
        array = np.array( array)
    shape = array.shape
    if fmt is None or fmt =="":
        fmt = dtype_convert(array.dtype)
    
    if len(shape)<2: return fmt, dim
    if fmt[0] in "123456789":
        return fmt, dim
    
    size = reduce( lambda x,y:x*y, shape[1:],1)
    fmt = "%d%s"%(size,fmt)
    dim = str(shape[1:]).replace(" ","")
    return fmt, dim

def dtype_convert(dtype):
    """
FITS format code         Description                     8-bit bytes

L                        logical (Boolean)               1
X                        bit                             *
B                        Unsigned byte                   1
I                        16-bit integer                  2
J                        32-bit integer                  4
K                        64-bit integer                  4
A                        character                       1
E                        single precision floating point 4
D                        double precision floating point 8
C                        single precision complex        8
M                        double precision complex        16
P                        array descriptor                8
Q                        array descriptor                16
    """
    if dtype==np.float64 : return "D"
    if dtype==np.float32 : return "E"
    if dtype==np.complex : return "C"
    if dtype==np.complex64 : return "M"
    if dtype==np.int16 : return "I"
    if dtype==np.int32 : return "J"
    if dtype==np.int64 : return "K"
    if dtype==np.bool  : return "L"
    if dtype.kind == "S": return "A%d"%(dtype.itemsize)
    raise ValueError( "Cannort convert dtype %s into bintable format"%dtype)



def __test__( ):
    global kt, fls, ktf
    import carac
    reload(carac)
    from detector import fitsopener as opener
    #try:
    #    fls
    #    fls = carac.Images(fls)
    #    fls._subclass_all( carac.Combined)
    #except:
    ktf = make_table_of_files( opener.glob.glob("/Users/guieu/DETDATA/274-2014-PTC-P7100/PIONIER*_0.05*.fits"), config.allcombined_keys_as_table, carac.Combined)
    #fls = carac.Images(opener.open_glob("/Users/guieu/DETDATA/274-2014-PTC-P7100/PIONIER*_0.05*.fits", opener=fits.pf.open))
    #fls._subclass_all( carac.Combined)
    #kt = KeysTableHDU.make( fls, config.allcombined_keys_as_table, name="test" )
if __name__ is "__main__":
    __test__()
