from inspect import getargspec

def _issignature(want,signature):
    return signature == want


def _getsignature(method):
    spec = getargspec(method)
    # nargs = nargs-1 if method is bounded
    return len(spec.args) - (hasattr(method, "__self__") and
                            (method.im_self is not None))


class Command(list):
    def __init__(self, iterable=[], itype=None):
        super(Command, self).__init__(iterable)
        self.itype = itype

    def trigger(self, name="", old=None,
                new=None, mset=set(),
                of_signature=None):
        output = []
        all_args = [name, old, new, self.itype]
        for method in self:
            signature = _getsignature(method)
            if of_signature is not None and signature != of_signature:
                continue
            if _issignature(0, signature):
                if method in mset:
                    continue
                output.append(method())
            elif _issignature(1, signature):
                ret = method(name)
                if ret is not None and (not isinstance(ret, (tuple,list)) or len(ret) != 1):
                    raise TypeError("binded function of signature func(name)\
                    should return None or a tuple of (name,) got %s" % ret)
                elif ret:
                    name, = ret
                output.append(ret)
            elif _issignature(2, signature):
                #######
                # With this signature the return value is expected to be new
                # or a new new !
                ret = method(name, new)
                if ret is not None and (not isinstance(ret,(tuple,list)) or len(ret)!=2):
                    raise TypeError("binded function of signature func(name,new) should return None or a tuple of (name,new) got %s"%ret)
                elif ret:
                    name, new = ret
                output.append(ret)

            elif _issignature(3, signature):
                ret = method(name, old, new)
                if ret is not None and (not isinstance(ret,(tuple,list)) or len(ret)!=3):
                    raise TypeError("binded function of signature func(name,old, new) should return None or a tuple of (name,old, new) got %s" % ret)
                elif ret:
                    name, old, new = ret
                output.append(ret)
            else:
                raise TypeError("command method should have no more than 3 arguments")

        return output
