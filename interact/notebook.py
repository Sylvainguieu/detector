from IPython.display import HTML
def codetoggle(hide=False):
	return HTML('''<script>
		code_hide={hide}; 
		function code_toggle() {{
		if (code_hide){{
		$('div.input').hide();
		}} else {{
		$('div.input').show();
		}}
		code_hide = !code_hide
		}} 
		$( document ).ready(code_toggle);
		</script>
		Toggle on/off the <a href="javascript:code_toggle()">code</a>.'''.format(hide="true" if hide else "false" ))


def interactive_plot(state=True):
	import mpld3
	if True:
		mpld3.enable_notebook()
	else:
		mpld3.disable_notebook()
	