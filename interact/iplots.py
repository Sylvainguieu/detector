from ..plots import detplots
from .misc import auto_plot_decorator
import numpy as np

def ptc(data, **kwargs):    
    plot = data.data.plots["ptc"]()

    def plot_ptc():
        if not data.params["plot_ptc"]: return
        
        #if data.params["plot_ptc"] >1: clear_output()
        #if data.params["plot_ptc"]> 1: clear_output()
        ## now it is false 
        data.update(plot_ptc=False)
        try:
            plot.setData(detector=data.get_ptc_data(), freduce=data["freduce"] or np.mean,
            	section = data["section"],
            	signalrange = data["signalrange"]
            )
            plot.all(**dict(kwargs, xrange=data["signalrange"], dim=data["dim"]))
        except Exception as e:            
            print "plot failed", e
            return
    
    data.bind(plot_ptc, "plot_ptc")
    data.bind(auto_plot_decorator(data.params,"plot_ptc",2),
              "ptc_compute_on","polar", "pixel", 
              "section", "curent_dit",  "curent_illumination",
              "polar",  "signalrange", "dim", "tel", "win", "disp"   
          )    
    return plot


def signal(data, **kwargs):
	plot = detplots.Signal()
	plot.new_axes("signal.img" , axes=(2,1), figure=kwargs.get("figure", "signal"))
	plot.new_axes("signal.hist", axes=(2,2), figure=kwargs.get("figure", "signal"))
	
	def plot_signal():
		if not data.params["plot_signal"]: return
		data.update(plot_ptc=False)
		try:
			plot.setData(detector=data.get_signal_data())
			kw = {k:v for k,v in data.params["plots"]["signal"].iteritems()}
			#kw = {}
			plot.imshow_(axes="signal.img", **kw).axes_set()
			plot.hist_(axes="signal.hist").axes_set()
		except Exception as e:   
			raise Exception(e)
			print "plot failed", e
			return
	def replot_signal():
		kw = {k:v for k,v in data.params["plots"]["signal"].iteritems()}
		plot.imshow_(axes="signal.img", **kw).axes_set()
		plot.hist_(axes="signal.hist").axes_set()

	data.bind(plot_signal, "plot_signal")
	for k in data.params["plots"]["signal"]:
		data.params["plots"]["signal"].bind(replot_signal, k)
	return plot