"""
Provide Parameters() a dictionary that watch item assignment and can triger
actions
p = Parameters( a=10, b="test")

def a_changed(name, old, new):
    print "parameter {name} changed from {old} to {new}".format(name=name,old=old,new=new)

p.bind(a_changed, "a")
p["a"] = 12
# -> parameter a changed from 12 to 14
p.block("a")
p["a"] = 20
p.release("a")
p["a"] = 10
# -> parameter a changed from 20 to 10

"""


from .parameter import Parameter, Any
from .command  import Command


class Parameters(dict):

    def __init__(self,*args, **kwargs):
        #super(Parameters, self).__init__(*args, **kwargs)
        dict.__init__(self, *args, **kwargs)
        for k,v in dict.iteritems(self):
            if not isinstance(v, Parameter):
                dict.__setitem__(self,k, Any(value=v))

    def __setitem__(self,item, value, event=None):
        if isinstance(item, tuple):
            if len(item)>2:
                raise TypeError("list indice must not exceed 2")
            item , attr = item
            if not item in self:
                raise TypeError("list indice of size 2 available only if the item exists")

            parser = super(Parameters,self).__getitem__(item)
            parser.__setattr__( attr, value)
            return
        if not item in self:
            if isinstance(value, Parameter):
                dict.__setitem__(self, item, value)
            else:
                dict.__setitem__(self, item, Any(value=value))
            return

        parser = super(Parameters,self).__getitem__(item)
        old = parser.value
        if isinstance(value, Parameter):
            dict.__setitem__(self, item, value)
            parser = value
            value = parser.value
        else:
            parser.__setattr__("value",  value)

    def __getitem__(self,item):
        if isinstance(item , tuple):
            if len(item)==1: return self._get(item[0])
            item, attr = item
            trueval = super(Parameters,self).__getitem__(item)
            return getattr(trueval,attr)

        trueval = super(Parameters,self).__getitem__(item)
        if isinstance(trueval, Parameter):
            return trueval.value
        return trueval

    def touch(self,item):
        if not isinstance(item, tuple): item = (item,"value")
        self[item[0],]._touch(item[1], prefix=item[0])


    def _get(self,item, default=None):
        default = Parameter() if default is None else default
        return dict.get(self, item, default)

    def _pop(self,*args):
        return dict.pop(self, *args)

    def _setdefault(self, *args):
        return dict.setdefault(self, *args)


    def get(self,item, default=None):
        if not item in self:
            return default
        container = self[item,]
        if not hasattr( container, "value"): return default
        return container.value

    def values(self):
        return [v.value for v in dict.values(self)]

    def items(self):
        return [(k,self[k]) for k in self]

    def iteritems(self):
        # So far iteritems is equivalent  to items
        #
        return self.items()
    def itervalues(self):
        return self.values()

    def setdefault(self, item , value):
        container = self._get(item,None)
        if container is None or not hasattr( container, "_value"):
            self[item] = value

    def block(self,item, attr="value"):
        """ block binded methods of item """
        self[item,]._block(attr)

    def release(self, item , attr="value"):
        """ release the binded methods of item
        the methods can still be blocked by its itype
        """
        self[item,]._release(attr)

    def update(self, __kw__=None, **kwargs):
        """ is the same then dictionary update except
            that any binded method will not be triggered
        """
        kw = __kw__.copy() if __kw__ is not None else {}
        kw.update(kwargs)

        for k,v in kw.iteritems():
            if k in self and not isinstance(v,Parameter):
                try:
                    parameter = self[k,]
                    parameter._block()# user method are skiped
                    parameter.value = v
                    parameter._release()
                except ValueError as e:
                    raise ValueError("for parameter '%s' : "%(k) + str(e))
            else:
                self[k] = v

    def trigger(self,*items, **kwargs):
        """
        run the binded function from a list of items as for a item assignment
        execpt that if a method has signature method(name, old, new), old will be
        equal to new because there is no way to know the old value.
        Duplicate methods are called only ones if its signature is method()
        """


        mset = kwargs.pop("mset", set()).copy()
        of_signature = kwargs.pop("of_signature", None)
        if len(kwargs):
            raise ValueError("trigger accept only to keywords 'mset' and 'of_signature'")
        ##
        # make sure everything is ok before running
        for item in items:
            if not item in self:
                raise KeyError("item %s not in the dictionary"%item)

        for item in items:
            self[item,]._trigger(of_signature=of_signature, mset=mset, prefix=item)
            mset.update( self[item,]._usercommands )

    def bind(self, method, *items):
        """
        bind(method, item=None)
        bind the item assignement to a method.
        method signature can be method(), method(name), method(name, new),
               method(name, old, new), method(name, old, new, itype)
               where name, new and old itype are respectively
               the prameter key (can be string or other) the new value, the previous
               value (or None if do not exists) and the itype.
        If item is None the method is called for all item assignement prior to other
        individual method assignement. The drawback is that there is no way to differentiate
        a real None item to a the None command so is a None item is present the method is
        called twice during assignment.
        If item is True, the binded  methods are called only after self.update


        see also attributes:
        -------
        unbind, block, release
        """

        if not len(items):
            items = self.keys()

        for item in items:
            if isinstance(item , tuple):
                if len(item)>2:
                    raise ValueError("Expecting only a tuple of dim 2 got %d"%len(item))
                item, attr = item
            else:
                item, attr = item , "value" #default binding is on value

            if not item in self:
                raise KeyError("item %s is not in the list"%item)

            self[item,]._bind(method, attr)

    def unbind(self, method, *items):
        if not len(args):
            items = [None]
        for item in items:
            if isinstance(item , tuple):
                if len(item)>2:
                    raise ValueError("Expecting only a tuple of dim 2 got %d"%len(item))
                item, attr = item
            else:
                item, attr = item , "value" #default binding is on value

            if not item in self:
                raise KeyError("item %s is not in the list"%item)
            self[item,]._unbindattr(method, attr)



def __test__():
    global p
    from . import values
    p = Parameters( a = values.BoundedInt(4, min=0, max=10),
                    b = values.Listed( 3, values=[0,1,2,3,4,5])
                )
    def test(name, old, new):
        print "I am ", name, " my old value is ", old, "my new value ", new
    p.bind(test, "a")
    p.bind(test, ("b", "values"))
    p["a"] = 10
    p["b","values"] = [0,1,2]




