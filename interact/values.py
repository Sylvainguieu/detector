import command
Command = command.Command
#from .parameters2 import Command



class ResolveError(Exception):
    pass

class SetError(ValueError):
    obj      = None
    name     = None
    attempt  = None
    resolver = None
    def __init__(self, *args):
        ValueError.__init__(self, *args)
        N = len(args)

        if N<=1:
            pass
        elif N==2:
            self.obj = args[1]
        elif N==3:
            self.obj, self.name = args[1:3]
        elif N==4:
            self.obj, self.name, self.attempt  = args[1:4]
        else:
            self.obj, self.name, self.attempt, self.resolver = args[1:5]
        self.canresolve = N>4

    def resolve(self):
        if self.canresolve:
            self.resolver()


class Any(object):
    _systemcommands = None
    _usercommands   = None
    def __init__(self,**kwargs):
        #######
        # set the command definitions
        # there are to style of commands one for system wich cannot be blocked
        # others are users command
        self._systemcommands = {}
        self._usercommands   = {}
        self._blocked = set()
        ###########################################################################
        # bind any attributes not starting by '_' with the method named _{attr}_change if present
        #
        for name in set(self.__dict__.keys()+self.__class__.__dict__.keys()+
                        list(self._set_order)+kwargs.keys()):
            if name[0] =="_": continue
            ch_name =  "_"+name+"_changed"
            if hasattr(self, ch_name):
                if not name in self._systemcommands:
                    self._systemcommands[name] = Command()
                self._systemcommands[name].append(getattr(self,ch_name))

        ############################################
        # set first the attribute in self._set_order
        for k in self._set_order:
            if k in kwargs:
                setattr(self, k, kwargs.pop(k))

        for k,v in kwargs.iteritems():
            if k[0] =="_":
                raise KeyError("keywords cannot start with '_' ")
            setattr(self, k, v)

    def __setattr__(self,attr,value, prefix="", event=None):
        """
        event is for future tk event handler
        """
        if attr[0] == "_": return object.__setattr__( self, attr, value)

        old = getattr(self, attr) if hasattr(self, attr) else None
        if old is value:
            return
        self.__set__(attr,value)
        name = ".".join([prefix,attr]) if prefix else attr
        ####
        # the new value can have change so we check it again from self
        try:
            if attr in self._usercommands and not attr in self._blocked and not True in self._blocked:
                self._usercommands[attr].trigger(name,old,getattr(self,attr))
            if None in self._usercommands and not None in self._blocked and not True in self._blocked:

                self._usercommands[None].trigger(name,old,getattr(self,attr))
        except SetError as e:

            object.__setattr__(self, attr, old)
            raise SetError(e)
            if self._is_value_error_exception():
                raise self.set_error(e)
            else:
                self.set_error(e)


    def __set__(self, attr, value, prefix=""):
        """ same as __setattr__ but execute only the user commands """
        old = getattr( self, attr) if hasattr(self, attr) else None
        super(Any, self).__setattr__(attr, value)
        name = ".".join([prefix,attr]) if prefix else attr

        try:
            if attr in self._systemcommands:
                self._systemcommands[attr].trigger(name,old,value)
            if None in self._systemcommands:
                self._systemcommands[None].trigger(name,old,value)
        except SetError as e:
            object.__setattr__(self, attr, old)
            if self._is_value_error_exception():
                raise self.set_error(e)
            else:
                self.set_error(e)

    def __repr__(self):
        str_args = [ "{0}={1!r}".format(k,getattr(self,k)) for k in self.__dict__ if k[0]!="_"]
        return self.__class__.__name__ +"("+",".join(str_args)+")"

    def _bind(self,method, attr, insert=None):
        if not attr in self._usercommands:
            self._usercommands[attr] = Command()
        if insert is None:
            self._usercommands[attr].append(method)
        else:
            self._usercommands[attr].insert(insert,method)
    def _unbindattr(self,method,attr):
        self._usercommands[attr].remove( method )

    def _block(self, attr=None):
        if attr in self._usercommands:
            self._blocked.add(attr)
    def _release(self, attr=None):
        if attr in self._blocked:
            self._blocked.remove(attr)
    def _touch(self, attr=None, prefix=""):
        if attr is None:
            for attr in self.__dict__:
                if attr[0] != "_": self._touch(attr)
            return
        if not hasattr(self, attr):
            return

        old = getattr( self, attr) if hasattr(self, attr) else None
        value = old
        name = ".".join([prefix,attr]) if prefix else attr
        if attr in self._usercommands and not attr in self._blocked and not True in self._blocked:
            self._usercommands[attr].trigger(name, old,value)

        if None in self._usercommands and not None in self._blocked and not True in self._blocked:
            self._usercommands[None].trigger(name,old,value)



    def _trigger(self,attr=None, of_signature=None, mset=set(), prefix=""):
        """
        Triger all the user defined method method if signature() are called only ones
        mset keyword is a set of method to not triger
        if of_signature is a int between 0,3 trigger only the methods with the
        coresponding signature
        """
        if attr in self._usercommands:
            value = getattr(self,attr)
            self._usercommands[attr].trigger(".".join([prefix,attr]), value,value, of_signature=of_signature, mset=mset)

    def _is_value_error_exception(self):
        return isinstance( self.set_error, type) and issubclass(self.set_error, Exception)
    Error     = SetError
    set_error = ValueError

class Value(Any):
    value    = None
    disabled = False
    _set_order = ("value","disabled")
    value_error = ValueError

    def __init__(self, *args, **kwargs):
        if len(args)>1: raise ValueError("takes only one  argument")
        if len(args):
            if "value" in kwargs:
                raise KeyError("duplicate keyword value")
            value = args[0]
            kwargs["value"] = value
        Any.__init__(self, **kwargs)

    def __call__(self, value):
        return self._parse(value)

    def _parse_type(self, value):
        return value
    _parse = _parse_type



    def _set_value(self,value, prefix=""):
        """ set the value but skip the users method binded to parameter 'value' """
        self.__set__("value",value, prefix=prefix)

    def _value_changed(self,name,old,new):
        value = self._parse(new)
        object.__setattr__(self,name, value)


    def __str__(self):
        self.formated
    @property
    def formated(self):
        if hasattr(self, "fmt"):
            return self.fmt%self.value
        return str(self.value)

def _intNone_(val):
    if val is None or val is "": return None
    return int(val)
def _floatNone_(val):
    if val is None or val is "": return None
    return float(val)

class StrList(Value):
    dtype = None
    separator = ","
    sizemax = None
    sizemin = None
    size = None
    _set_order = ("separator", "size","sizemin", "sizemax",  "value","disabled")
    def _parse(self,value):
        if isinstance( value, basestring):
            values = value.split(self.separator)
        else:
            values = values
        N = len(values)
        if self.sizemax is not None:
            if N>self.sizemax:
                raise SetError("List must have no more than %d element"%self.sizemax, self, "value", value)
        if self.sizemin is not None:
            if N<self.sizemin:
                raise SetError("List must have at least %d elements"%self.sizemin, self, "value", value)
        if self.size is not None:
            if N!= self.size:
                raise SetError("List must have exactly %d elements"%self.size, self, "value", value)

        parser = self.dtype if self.dtype is not None else lambda x:x
        try:
            return tuple( parser(v) for v in values )
        except (TypeError,ValueError) as e:
            raise SetError(str(e), self, "value", value)

    @property
    def formated(self):
        fmt = getattr(self, "fmt", "%s")
        return self.separator.join( [fmt%v for v in self.value] )

class _NoneList_(object):
    @property
    def formated(self):
        fmt = getattr(self, "fmt", "%s")
        return self.separator.join( [ fmt%v if v is not None else "" for v in self.value ] )


class IntList(StrList):
    dtype = int

class IntNoneList(_NoneList_,IntList):
    dtype = staticmethod(_intNone_)

class FloatList(StrList):
    dtype = float

class FloatNoneList(_NoneList_,StrList):
    dtype = staticmethod(_floatNone_)




class Int(Value):
    """ Int value """
    def _parse_type(self, value):
        try:
            return  int(value)
        except (TypeError,ValueError) as e:
            def resolver():
                self.value = int()
            raise SetError(str(e), self, "value", value, resolver)

    _parse = _parse_type

class Bool(Value):
    """ Bool value """
    def _parse_type(self, value):
        try:
            return  bool(value)
        except (TypeError,ValueError) as e:
            def resolver():
                self.value = bool()
            raise SetError(str(e), self, "value", value, resolver)

    _parse = _parse_type

class Float(Value):
    """ Float Value """
    def _parse_type(self, value):
        try:
            return  float(value)
        except (TypeError,ValueError) as e:
            def resolver():
                self.value = float()
            raise SetError(str(e), self, "value", value, resolver)
    _parse = _parse_type

class _None_(object):
    def _parse_type(self, value):
        if value is None or value == "": return None
        return super(_None_,self)._parse_type(value)
    _parse = _parse_type
    @property
    def formated(self):
        if self.value is None: return ""
        if hasattr(self, "fmt"):
            return self.fmt%self.value
        return str(self.value)

class IntNone(_None_, Int):
    """ Int value that can take None """
    pass
class FloatNone(_None_, Float):
    """ Float value that can take None """
    pass

class Bounded(Value):
    """ Value bounded between min and max """
    min = None
    max = None
    _message = "value must be between {self.min} and {self.max}"
    _set_order = ("min","max","value", "disabled")
    outoflimit_to_origin = False
    def _parse(self, value ):
        value = self._parse_type(value)
        if value is None: return value

        if self.min is not None and value<self.min:
            if self.outoflimit_to_origin:
                def resolver():
                    pass # normaly it is back to origin
                raise ResolveError(self._message.format(self=self), self, "value", value, resolver)
            else:
                def resolver():
                    try:
                        self.value = self.min
                    except SetError: # send it back without resolve method
                        raise ResolveError(self._message.format(self=self), self, "value", value)

                raise SetError(self._message.format(self=self), self, "value", value, resolver)

        if self.max is not None and value>self.max:
            if self.outoflimit_to_origin:
                def resolver():
                    pass
                raise SetError(self._message.format(self=self), self, "value", value, resolver)
            else:
                def resolver():
                    try:
                        self.value = self.max
                    except SetError:
                        raise ResolveError(self._message.format(self=self), self, "value", value)

                raise SetError(self._message.format(self=self), self, "value", value, resolver)

        return value

    def _max_changed(self,name, old, new):
        if new is None: return
        new = self._parse_type(new) #insure that min and max are the same type than value
        if hasattr(self, "value") and self.value is not None:
            if self.value>new:
                object.__setattr__(self, "value", new)

    def _min_changed(self,name, old, new):
        if new is None: return
        new = self._parse_type(new) #insure that min and max are the same type than value
        if hasattr(self, "value") and self.value is not None:
            if self.value<new:
                object.__setattr__(self, "value", new)


class BoundedInt(Bounded,Int):
    """ Int bounded betwin min and max """
    _message = "value must be a integer between {self.min} and {self.max}"


class BoundedFloat(Bounded,Float):
    """ Float bounded betwin min and max """
    _message = "value must be a float between {self.min} and {self.max}"

class BoundedIntNone(Bounded,IntNone):
    """ Int bounded betwin min and max with None allowed """
    _message = "value must be a integer between {self.min} and {self.max}"

class BoundedFloatNone(Bounded,FloatNone):
    """ Float bounded betwin min and max with None allowed """
    _message = "value must be a float between {self.min} and {self.max}"

class Listed(Value):
    """ Value listed in 'values' """
    values   = None
    _message = "Value must be one of {self.values}"
    _set_order = ("values", "value", "disabled")
    def _parse(self, value):
        if isinstance(self.values, dict):
            values = self.values.values()
        else:
            values = self.values

        if not value in values:
            def resolver():
                try:
                    self.value = values[0]
                except SetError:
                    # raise it back without resolver
                    raise ResolveError(self._message.format(self=self), self, "value", value)

            raise SetError(self._message.format(self=self), self, "value", value, resolver)

        return value
    def _values_changed(self,name, old, new):

        if isinstance( new, dict):
            values = new.values()
        else:
            values = new

        value = self.value
        if not value in values and len(values):
            self.value = values[0]


