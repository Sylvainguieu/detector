import link
import ptc
from .widgets import DataWidgets

from .. import values
from .. import parameters
values.Value._widget = link.new_widget 
parameters.Parameters.widget = link.new_widget_p
