from IPython.html.widgets import interact, interactive, fixed
from IPython.html import widgets
from IPython.display import clear_output, display, HTML
from collections import OrderedDict
from IPython.utils.py3compat import unicode_type

ILLUMINATION = 0
DIT = 1
BOTH = 2

i = 0

class Container(widgets.ContainerWidget):
    def __init__(self, *args, **kwargs):
        super( Container,self).__init__(*args, **kwargs)
        if hasattr(self, "init_classes"):
            self.on_displayed(self.init_classes)

class HContainer(Container):
    def init_classes(self, obj):
        self.remove_class('vbox')
        self.add_class("hbox")
        
class DetBase(object):
    _params_key = None
    default_kwargs   = {}
    default_css      = {}
    kwargs2attribute = []
    
    def refresh_value(self):
        if self._params_key and self._params_key in self.params:
            self.value = self.params[self._params_key]
    def refresh(self):
        self.refresh_value()
    
    def refresh_values(self):
        if self._params_key and self._params_key in self.choices:
            choices = self.params[self._params_key, "values"]
            if not isinstance(choices,dict):
                choices = OrderedDict((unicode_type(v), v) for v in choices)
            self.values = choices
            
    def hide_me(self):
        self.visible = False
    def show_me(self):
        self.visible = True
    def init_binding(self):
        if self._params_key:
            self.params.bind(self.refresh, self._params_key)
    def init_kwargs(self, kwargs):
        for k,v in self.default_kwargs.iteritems():
            kwargs.setdefault(k,v)

    def link(self, attr, param, attrparser=lambda x:x, paramparser=lambda x:x):
        def wd2param():
            self.params[param] = attrparser(getattr(self,attr))
        def param2wd():
            self.params.block( *param )
            try:
                setattr( self, attr, paramparser(self.params[param]))
            except:
                raise Exception("failure")
            finally:
                self.params.release( *param )
                
        self.params.bind(param2wd, param)
        self.on_trait_change(wd2param, attr)
        
    def linkall(self, param, attrparsers={}, paramparsers={}):
        def wd2param(name,old, new):
            
            print "wd2param",name, old, new 
            if hasattr(self.params[param,], name):
                print "ok"
                self.params[param, name] = attrparsers.get(name,lambda x:x)(new)
        def param2wd(name,old, new):
            name = name.split(".")[-1]
            print "param2wd",name, old, new 
            if hasattr(self, name):
                print "ok"
                setattr(self, name, paramparsers.get(name,lambda x:x)(new))
            
        self.params[param,]._bind(param2wd, None)
        self.on_trait_change(wd2param, None)
        
    
def init_decorator(origin_class): 
    def init(self, params,  _key_=False, wdtypes=None, **kwargs):
        
        self.params  = params               
        self.wdtypes = wdtypes
        for k in self.kwargs2attribute:            
            setattr(self, k, kwargs.pop(k, getattr(self,k) ))
        
        self._params_key = self._params_key if _key_ is False else _key_
        self.init_kwargs(kwargs)     
        origin_class.__init__(self, **kwargs)
        self.set_css( self.default_css)
        
        self.on_displayed(self.init_backend)
    return init
    
class Menu(widgets.DropdownWidget, DetBase):
    
    def init_kwargs(self, kwargs):        
        for k,v in self.default_kwargs.iteritems():
            kwargs.setdefault(k,v)
        if self._params_key:
            kwargs.setdefault( "values",
                               self.params[self._params_key,"values"]
                           )
    __init__ = init_decorator(widgets.DropdownWidget)

    def init_binding(self):
        if self._params_key:
            self.params.bind(  self.refresh_values, (self._params_key, "values"))
    
    def init_backend(self, obj):
        self.refresh()        
        if self._params_key:
            self.init_binding()
            self.on_trait_change( self.value_changed, "value" )
    
    def value_changed(self, name, old, new):    
        if True: #self.value_lock.acquire(False):            
            try:        
                # Reverse dictionary lookup for the value name                
                for k,v in self.values.items():
                    if new == v:
                        # set the selected value name
                        self.value_name = k
                        if self._params_key:
                            self.params[self._params_key] = new
                        return
                # undo the change, and raise KeyError
                #self.value = old
                raise KeyError(new)
            finally:
                pass
                #self.value_lock.release()
      

            
class TypeText(widgets.TextWidget, DetBase):
    dtype = str
    fmt   = "%s"
    allowNone = False
    kwargs2attribute = ["dtype", "fmt", "allowNone"]
    
    default_kwargs = {}
    default_css    = {}
    __init__ = init_decorator(widgets.TextWidget)
    
    def _submited(self,obj):
        if self._params_key:
            try:
                if self.allowNone and self.value.strip() == "":
                    svalue = None
                else:
                    svalue = self.dtype(self.value)
            except:
                print "'%s' Cannot be converted to type %s"%(self.value, self.dtype)
                self.refresh()
                return 
            finally:
                self.params[self._params_key] = svalue

    def refresh_value(self):
        if self._params_key and self._params_key in self.params:
            newvalue = self.params[self._params_key]
            if self.allowNone and newvalue is None:
                self.value = ""
            else:
                self.value = self.fmt%(newvalue)
            
    def refresh(self):
        self.refresh_value()
        
    def init_backend(self, obj):
        self.refresh()        
        if self._params_key:
            self.init_binding()
            self.on_submit(self._submited)


class Button(widgets.ButtonWidget, DetBase):
    default_kwargs = {"value":True}
    default_css = {}
    __init__ = init_decorator(widgets.ButtonWidget)
    def refresh(self):
        if self._params_key and self._params_key in self.params:
            self.disabled = self.params[self._params_key]
    
    def init_backend(self, obj):
        self.refresh()        
        if self._params_key:
            self.init_binding()
            self.on_click(self._submited)
    def _submited(self,obj):
        if self._params_key and self._params_key in self.params:
            self.params[self._params_key] = self.value
            
class Checkbox(widgets.CheckboxWidget, DetBase):
    __init__ = init_decorator(widgets.CheckboxWidget)
    def init_backend(self, obj):
        self.refresh()        
        if self._params_key:
            self.init_binding()
            self.on_trait_change(self._submited, "value")
        
    def _submited(self,obj):
        if self._params_key and self._params_key in self.params:
            self.params[self._params_key] = self.value
                
