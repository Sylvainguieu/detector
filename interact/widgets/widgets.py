from IPython.html import widgets
from IPython.display import clear_output, display, HTML

class Container(widgets.ContainerWidget):
    pass
class HContainer(Container):
    def __init__(self, *args, **kwargs):
        super( Container,self).__init__(*args, **kwargs)
        self.on_displayed(self.init_classes)
    def init_classes(self,obj):
    	self.remove_class("vbox")
    	self.add_class("hbox")







class DataWidgets(object):
	""" A class containing all the widget for data interaction """

	def __init__(self, params):
		self.params = params
		self.polar               = widgets.DropdownWidget()
		self.curent_dit          = widgets.DropdownWidget()
		self.curent_illumination = widgets.DropdownWidget()
		self.ptc_on              = widgets.DropdownWidget()
		self.section_func        = widgets.DropdownWidget()
		self.freduce             = widgets.DropdownWidget()
		self.pixel               = widgets.TextWidget()
		self.pixel.set_css( {"width":30} )
		self.nextpixel           = widgets.ButtonWidget(value=1,  description=">")
		self.nextpixel.set_css({"width":15})
		self.prevpixel           = widgets.ButtonWidget(value=-1,  description="<")
		self.prevpixel.set_css({"width":15})

		self.section_x = widgets.TextWidget()
		self.section_y = widgets.TextWidget()
		map( lambda w:w.set_css({"width":30}), [self.section_x,self.section_y])

		self.win  = widgets.DropdownWidget()
		self.tel  = widgets.DropdownWidget()
		self.base = widgets.DropdownWidget()
		self.disp = widgets.DropdownWidget()


		# link all the widget to the parameters
		params.widget("polar", self.polar)
		params.widget("curent_dit", self.curent_dit)
		params.widget("curent_illumination", self.curent_illumination)
		params.widget("ptc_compute_on", self.ptc_on)
		params.widget("section_func", self.section_func)
		params.widget("freduce", self.freduce)
		params.widget("pixel", self.pixel)
		params.widget("pixel",self.nextpixel, attrs=[], offset=True)
		params.widget("pixel",self.prevpixel, attrs=[], offset=True)
		params.widget("section_x",self.section_x)
		params.widget("section_y",self.section_y)
		params.widget("win", self.win )
		params.widget("tel", self.tel )
		params.widget("base",self.base)
		params.widget("disp",self.disp)

		self.auto_plot = params.widget("auto_plot", "check")
	def clear(self, name, old, new):
			if new>1: clear_output()
	def ptc(self):
		self.plot_ptc_button = widgets.ButtonWidget(value=2, description="Plot Ptc")
		self.plot_ptc_button.set_css( {"align":"left"} )

		self.params.widget("plot_ptc",  self.plot_ptc_button)


		self.params.bind(self.clear, "plot_ptc")
		return Container( children = [
			HContainer( children=[self.polar,self.ptc_on,self.curent_dit,self.curent_illumination]),
			HContainer( children=[self.section_func, self.section_y, self.section_x]),
			HContainer( children=[self.win, self.tel, self.base, self.disp] ),
			HContainer( children=[self.freduce,  self.pixel, self.prevpixel, self.nextpixel]),#+[self.smin,self.smax,self.fdim] ),
			HContainer( children=[self.auto_plot, self.plot_ptc_button] )
    		]
    		)
	def signal(self):
		self.plot_signal_button = widgets.ButtonWidget(value=2, description="Plot Signal")
		self.plot_signal_button.set_css( {"align":"left"} )
		self.params.bind(self.clear, "plot_signal")
		self.params.widget("plot_signal",  self.plot_signal_button)

		self.signal_vmin = self.params["plots"]["signal"].widget("vmin", "text")
		self.signal_vmax = self.params["plots"]["signal"].widget("vmax", "text")
		return Container( children=[
			HContainer( children=[self.polar, self.curent_dit,self.curent_illumination, self.plot_signal_button ] ),
			HContainer( children=[self.signal_vmin, self.signal_vmax])
			])






