
from . import base
from IPython.html import widgets
from IPython.display import clear_output, display, HTML
import numpy as np
from ..misc import auto_plot_decorator



PTC = "ptc"
FIT = "fit"


class _PtcWidgets(base.Container):
    def __init__(self, params,  **kwargs):
        self.params  = params
        wdtypes = [PTC]
        self.polar    = params.widget("polar", "menu")
        self.dit      = params.widget("curent_dit", "menu")
        self.illumination = params.widget("curent_illumination", "menu")
        self.ptc_on    = params.widget("ptc_compute_on", "menu")
        self.section   = params.widget("section_func", "menu")
        self.freduce   = params.widget("freduce", "menu")
        self.pixel     = params.widget("pixel", "text", css={"width":30}, on="change")

        self.nextpixel = params.widget("pixel", widgets.ButtonWidget(value=1,  description=">"),  attrs=[], offset=True,  css={"width":15})
        self.prevpixel = params.widget("pixel", widgets.ButtonWidget(value=-1, description="<"),  attrs=[], offset=True,  css={"width":15})


        css = {} #{"width":100}
        self.section_y = params.widget("section_y", "text", css={"width":30, "padding-right":2})
        self.section_x = params.widget("section_x", "text", css={"width":30})
        
        self.win      = params.widget("win", "menu", css=css)
        self.tel      = params.widget("tel", "menu", css=css)
        #self.tel      = params["tel",].wd_menu(base.widgets.DropdownWidget())
        self.base     = params.widget("base", "menu", css=css)
        self.disp     = params.widget("disp", "menu", css=css)
        
        self.smin = params.widget("signal_min", "text",css={"width":30})
        self.smax = params.widget("signal_max", "text",css={"width":30})
        self.fdim = params.widget("dim", "text", css={"width":30})
        
        self.plot_button   = params.widget("plot_ptc",  widgets.ButtonWidget(value=2, description="Plot Ptc"))
        self.auto_plot     = params.widget("auto_plot", "check")

        
        self.plot_button.set_css( {"align":"left"} )


        ##
        # make sure that value in pixel is up to date
        #def plot_ptc():
        

        
        #params.bind(self.pixel._submited, "plot_ptc", insert=0)
        #params.bind(self.smin._submited,  "plot_ptc", insert=0)
        #params.bind(self.smax._submited,  "plot_ptc", insert=0)
        #params.bind(self.fdim._submited,  "plot_ptc", insert=0)
        
        
        
        self.hlines  = [base.HContainer( children=[self.polar,self.ptc_on,self.dit,self.illumination]),
                        base.HContainer( children=[self.section,self.section_y, self.section_x]),
                        base.HContainer( children=[self.win, self.tel, self.base, self.disp] ),
                        base.HContainer( children=[self.freduce,  self.pixel, self.prevpixel, self.nextpixel]+[self.smin,self.smax,self.fdim] ),
                        base.HContainer( children=[self.auto_plot, self.plot_button] )
                    ]
        
        base.Container.__init__(self, children=self.hlines)

                

def iptc(data, **kwargs):    
    
    def plot_ptc():
        if not data.params["plot_ptc"]: return
        
        #if data.params["plot_ptc"] >1: clear_output()
        if data.params["plot_ptc"] : clear_output()
        ## now it is false 
        data.update(plot_ptc=False)
        try:
            
            data.get_ptc_data().plots.ptc(freduce=data["freduce"] or np.mean,
                                          section=data["section"],
                                          signalrange=data["signalrange"]
                                          
            ).all(**dict(kwargs, xrange=data["signalrange"], dim=data["dim"]))
        except Exception as e:            
            print "plot failed", e
            return
    
    
    data.bind(plot_ptc, "plot_ptc")
    data.bind(auto_plot_decorator(data.params,"plot_ptc",2),
              "ptc_compute_on",
              "section", "curent_dit",  "curent_illumination",
              "polar",  "signalrange", "dim", "tel", "win", "disp"
              
          )
    
    return PtcWidgets(data.params)
    

    
