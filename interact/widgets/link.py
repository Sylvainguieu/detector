from collections import OrderedDict
from IPython.utils.py3compat import unicode_type
from IPython.html import widgets



SUBMIT = "submit"
CHANGE = "change"

def link(value, widget, value2widget, widget2value,
         attrs=None, touch=True, **kwargs):
    if attrs is None:
        value._bind(value2widget, None)
        widget.on_trait_change(widget2value, None)
    else:
        if isinstance(attrs, dict):
            value_attrs, widget_attrs = attrs.keys(), attrs.values()
        else:
            value_attrs, widget_attrs = attrs, attrs
        for kv,kw in zip( value_attrs, widget_attrs):
            value._bind(value2widget, kv)
            widget.on_trait_change(widget2value, kw)

            if touch: value._touch(kw)

    widget.set_css( kwargs.pop("css", {}) )

def text_field(value, widget, on=SUBMIT,
               attrs=["value","description", "disabled"], **kwargs):
    def submit2value(obj):
        value.value = widget.value

    def widget2value(name, old, new):
        if on==SUBMIT and name=="value":
            return
        elif on==CHANGE:
            ## wait for a successfull parse before setting the value
            try:
                value._parse(new)
            except:
                return
        if hasattr(value,name):
            setattr( value, name, getattr(widget, name))
    def value2widget(name, old, new):
        name = name.split(".")[-1]
        attr_value, attr_widget = name, name

        if  attr_value == "value":
            value._block("value")
            try:
                widget.value = value.formated
            finally:
                value._release("value")
            return

        if hasattr( widget, name):
            value._block(attr_value)
            try:
                setattr(widget, attr_widget , getattr(value, attr_value))
            finally:
                value._release(attr_value)


    link(value, widget, value2widget, widget2value,attrs=attrs,
         touch=True, **kwargs)
    widget.on_submit(submit2value)
    return widget

def menu(value, widget, attrs=["values", "value", "disabled", "description"], **kwargs):
    def widget2value(name, old, new):
        if hasattr(value,name):
            setattr( value, name, getattr(widget, name))
    ### END of widget2value
    def value2widget(name, old, new):
        name = name.split(".")[-1]
        if name=="values":
            if not isinstance(value.values, dict):
                value._block("values")
                try:
                    widget.values = OrderedDict((unicode_type(v), v) for v in value.values)
                finally:
                    value._release("values")
                return
        #if hasattr( widget, name):
        value._block(name)
        try:
            setattr(widget, name , getattr(value, name))
        finally:
            value._release(name)

    ### End of value2widget

    link(value, widget, value2widget, widget2value, attrs=attrs, touch=True,
         **kwargs)
    return widget

def default_widget(value, widget, attrs=None, **kwargs):
    def widget2value(name, old, new):
        if hasattr(value,name):
            setattr( value, name, getattr(widget, name))

    def value2widget(name, old, new):
        name = name.split(".")[-1]

        value._block(name)
        try:
            setattr(widget, name , getattr(value, name))
        finally:
            value._release(name)

    link(value, widget, value2widget, widget2value, attrs=attrs, **kwargs)
    return widget

def button(value, widget, offset=False, attrs=["description"], **kwargs):
    def click(obj):
        # if widget has value push it into the Value object
        # For button .value attributes are not linked
        if hasattr(widget, "value"):
            if offset:
                value.value = value.value + widget.value
            else:
                value.value = widget.value
        else:
            if offset:
                value.value = value.value + 1
            else:
                value.value = not value.value
    default_widget(value, widget, attrs=attrs, **kwargs)
    widget.on_click(click)
    return widget

def check(value, widget, attrs=["value","description"], **kwargs):
    default_widget(value, widget, attrs=attrs, **kwargs)
    return widget

def get_widget_str(name, **kwargs):
    name = name.lower()
    if name == "text": return widgets.TextWidget(**kwargs)
    if name == "menu": return widgets.DropdownWidget(**kwargs)
    if name == "button": return widgets.ButtonWidget(**kwargs)
    if name == "check" : return widgets.CheckboxWidget(**kwargs)

def get_linker_str(name):
    name = name.lower()
    if name == "text": return text_field
    if name == "menu": return menu
    if name == "button": return button
    if name == "check":  return check

def get_linker_widget(widget):
    if isinstance( widget, widgets.TextWidget):     return text_field
    if isinstance( widget, widgets.DropdownWidget): return menu
    if isinstance( widget, widgets.ButtonWidget):   return button
    if isinstance( widget, widgets.CheckBoxWidget):   return check

def get_widget_link(widget_or_str,**kwargs):
    if isinstance( widget_or_str, basestring):
        return get_widget_str(widget_or_str, **kwargs), get_linker_str(widget_or_str)
    else:
        return widget_or_str, get_linker_widget(widget_or_str)

def new_widget(value, widget_or_str, kw_widget=None, **kwargs):
    kw_widget = kw_widget or {}
    widget, linker = get_widget_link(widget_or_str, **kw_widget)
    return linker( value, widget, **kwargs)

def new_widget_p(params, item, widget_or_str, kw_widget=None, **kwargs):
    return new_widget(params[item,], widget_or_str, kw_widget=None, **kwargs)



