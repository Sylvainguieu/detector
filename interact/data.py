import numpy as np
from .. import pionier as rapid

#from .. import parameters
#reload(parameters)
#Parameters = parameters.Parameters
#Command = parameters.Command
from . import parameters
from .parameters import Parameters
from .command import Command
from . import values

from .values import Listed, Int, BoundedInt, Value, BoundedFloat, Float, FloatNone, IntNone, Bool, IntList, IntNoneList, StrList, FloatList, FloatNoneList
#Listed, Int, BoundedInt, Value, BoundedFloat, Float, FloatNone, IntNone, Bool = values.Listed, values.Int, values.BoundedInt, values.Value, values.BoundedFloat, values.Float, values.FloatNone, values.IntNone, values.Bool
from .parameters import Parameters



from ..plots import detplots
detplots.images.Signal.kw_imshow = Parameters( vmin=FloatNone(None), vmax=FloatNone(None) )
detplots.ptc.Ptc.kw_fit = Parameters( dim=Int(1), xrange=FloatNoneList( "-inf,inf", size=2), yrange=FloatNoneList( "-inf,inf", size=2))




ILLUMINATION = 0
DIT  = 1
BOTH = 2

grism   = (rapid.grism)
#grismAC = (rapid.grismAC)
free    = (rapid.free)
woll    = (rapid.woll)
griwoll = (rapid.griwoll)
box     = (rapid.rapid)
mapp = rapid.mapABCD

def fullframe(*args,**kwargs):
    return rapid.rapid(None,None,**kwargs)


class Data(object):

    params = {
        "freduce":Listed(np.mean, values= {"Mean":np.mean,
        "Median":np.median,
        "Pixel":None}),
        "pixel":BoundedInt(0, min=0, max=320*254, description="Pix"),

        "section_func":Listed(fullframe, values={
            "Grism" : grism,
            #"GrismAC" : grismAC,
            "Free": free,
            "Woll": woll,
            "GRI+WOLL":griwoll,
            "fullframe":fullframe}, description="Section"),
        "section_x":Int(rapid.grism.pos[1], description="x"),
        "section_y":Int(rapid.grism.pos[0], description="y"),

        "ptc_compute_on":Listed(BOTH,
            values={"Illumination":ILLUMINATION,
             "Dit":DIT,
             "Both":BOTH},
             description="PTC on"
            ),
        "section_params":{},
        "signal_min": FloatNone(), # min value for signal (in plot fit)
        "signal_max": FloatNone(),
        "win" :Listed( values=[None],description="win"  ),
        "tel" :Listed( values=[None],description="tel"  ),
        "base":Listed( values=[None],description="base" ),
        "disp":Listed( values=[None],description="disp" ),
        "dim": Int(1) , # fit dimention default
        "plot_ptc" :Bool(False, description="Plot Ptc"), #made to start a ptc plot (e.i. with button)
        "plot_signal" :Bool(False, description="Plot Signal"),
        "auto_plot":Bool(False),  #when true plot accure when parameter are changed

        "plots":Value(
            Parameters( signal=Parameters( vmin=FloatNone(None), vmax=FloatNone(None)   )  )

            )
        }

    def __init__(self,data, **kwargs):
        self.data = data
        self.params  = Parameters(self.params)

        ####
        # set data dependent variables
        dits = data.get_dit.set()
        self.params["curent_dit"] = Listed(values=list(dits),
                                           description="Dit")

        illuminations = data.get_illumination.set()
        self.params["curent_illumination"] = Listed(values=list(illuminations),
                                                    description="Illumination")
        polars = data.get_polar.set()
        self.params["polar"] =  Listed(values=list(polars), description="Polar")

        self.params["polar"] = max(polars)
        self.params.bind(self.reset_polar, "polar")

        self.params["curent_dit"] = max(dits)
        self.params["curent_illumination"] = max(illuminations)
        self.reset_ptc_on()
        self.params.bind(self.reset_ptc_on, "ptc_compute_on")

        self.reset_win()
        self.params.bind(self.reset_win, "section_func")
        self.reset_telbase()
        self.params.bind(self.reset_telbase, "section_func")
        self.params.bind(self.reset_tel, "tel" )
        self.params.bind(self.reset_base, "base")

        self.reset_disp()
        self.params.bind(self.reset_disp, "section_func")

        self.reset_pos()
        self.params.bind(self.reset_pos, "section_func")


        self.reset_section()
        self.params.bind(self.reset_section, "pixel")
        self.params.bind(self.reset_section, "section_func")

        self.params.bind(self.reset_section, "freduce")
        self.params.bind(self.reset_section, "win" )
        self.params.bind(self.reset_section, "tel" )
        self.params.bind(self.reset_section, "disp")
        self.params.bind(self.reset_section, "base")
        self.params.bind(self.reset_section, "section_x")
        self.params.bind(self.reset_section, "section_y")

        self.reset_signalrange()
        self.params.bind(self.reset_signalrange, "signal_min")
        self.params.bind(self.reset_signalrange, "signal_max")



        self.params.update(kwargs)

    def __getitem__(self, item):
        return self.params[item]

    def __setitem__(self, item , value):
        self.params[item] = value

    def __getattr__(self,attr):
        if attr[0:4] =="get_":
            return lambda : self.params[attr[4:]]
        if attr[0:4] == "set_":
            return lambda v: self.params.__setitem__(attr[4:],v)
        super(Data,self).__getattribute__(attr)

    def bind(self, method, *items, **kwargs):
        return self.params.bind(method, *items, **kwargs)

    def unbind(self,method,  *items):
        return self.params.unbind(method, *items)

    def block(self, ctype):
        return self.params.block(ctype)
    def release(self, ctype):
        return self.params.release(ctype)
    def update(self, *args, **kwargs):
        return self.params.update(*args, **kwargs)

    def reset_polar(self):
        data = self.data.get_polar.choose(self.params["polar"])
        dits = data.get_dit.set()
        illuminations = data.get_illumination.set()
        self.params["curent_dit", "values"] = dits
        self.params["curent_illumination", "values"] = illuminations

    def reset_ptc_on(self):
        pon = self.params["ptc_compute_on"]
        if pon == ILLUMINATION:
            self.params["curent_dit","disabled"] = False
            self.params["curent_illumination","disabled"] = True
        elif pon == DIT:
            self.params["curent_dit","disabled"] = True
            self.params["curent_illumination","disabled"] = False
        else:
            self.params["curent_dit","disabled"] = False
            self.params["curent_illumination","disabled"] = False

    def reset_section(self):
        kw = self.params["section_params"].copy()
        if self.params["freduce"] is None:
            kw["pixel"] = self.params["pixel"]
            self.params["pixel", "disabled"] = False
        else:
            self.params["pixel", "disabled"] = True
        section_func  = self.params["section_func"]
        if section_func != fullframe:
            for k in ["win","disp", "tel"]:
                kw[k] = self.params[k]

        #self.block("section")

        if section_func != fullframe:
            kw["pos"] = self.params["section_y"], self.params["section_x"]
        section = section_func(**kw)


        self.params["section"] = section
        if self.params["freduce"] is not None:
            if hasattr(section, "size"):
                self.params["pixel","max"] = section.size-1
                self.params["pixel","description"] = "Pix/%d"%(section.size-1)
            else:
                self.params["pixel","max"] = 999999999

        #self.release("section")


        #self.update(section_func=section_func)
    def reset_win(self):
        self.reset_telbase("win")
    def reset_tel(self):
        self.reset_telbase("tel")
    def reset_base(self):
        self.reset_telbase("base")
    def reset_pos(self):
        section_func  = self.params["section_func"]
        disabled = (not hasattr(section_func ,"slicer")) or section_func == fullframe
        self.params["section_x", "disabled"] = disabled
        self.params["section_y", "disabled"] = disabled
    #def reset_pos(self):
    #    section_func  = self.params["section_func"]

    def reset_telbase(self, frm=None):

        section_func  = self.params["section_func"]
        if hasattr(section_func ,"slicer"):
            slicer = section_func.slicer

            if hasattr(slicer,"mapping"):
                self.params["base", "disabled"] = False
                self.params["tel", "disabled"]  = False
                self.params["win", "disabled"]  = False


                mpi = mapp
                mp  = mpi.select(**{k:self.params[k] for k in ["tel","win","base"]})

                if frm is "tel":
                    self.params["tel", "values"]  = [None]+list(set(mpi.t1+mpi.t2))
                else:
                    self.params["tel", "values"]  = [None]+list(set(mp.t1+mp.t2))
                if frm is not "win":
                    self.params["win", "values"]  = [None]+mp.win
                if frm is "base":
                    self.params["base", "values"]  = [None]+list(set(mpi.base))
                else:
                    self.params["base", "values"] = [None]+list(set(mp.base))

                return

        self.params["base", "disabled"] = True
        self.params["tel", "disabled"] = True
        self.params["win", "disabled"] = True



    def reset_disp(self):
        disp = self.params["disp"]
        section_func = self.params["section_func"]
        if hasattr(section_func,"slicer"):
            slicer = section_func.slicer
            if hasattr(slicer, "dispersion"):
                 self.params["disp", "values"] = [None]+range(slicer.dispersion)
                 return
        self.params["disp", "disabled"] = True


    def set_signalrange(self, name, value):
        if value is None: value=(None,None)
        self.params.update( signalrange=value)
        self.params["signal_min"] = value[0]
        self.params["signal_max"] = value[1]

    def reset_signalrange(self):
        self.params["signalrange"] = (self.params["signal_min"], self.params["signal_max"])

    def get_signal_data(self):
        return self.data.get_polar.choose( self["polar"] ).get_dit.choose(self["curent_dit"]).get_illumination.choose(self["curent_illumination"])[0]

    def get_ptc_data(self):
        data = self.data.get_polar.choose( self["polar"] )
        ptc_on  = self.get_ptc_compute_on()
        if ptc_on == BOTH:          return data
        if ptc_on == ILLUMINATION:  return data.get_dit.choose( self.get_curent_dit() )
        if ptc_on == DIT:           return data.get_illumination.choose(self.get_curent_illumination())

