
def auto_plot_decorator(params,plot_param,plot_value):
    def auto_plot():
        if params["auto_plot"]:    
            params.update(auto_plot=False)            
            try:
                params[plot_param] = plot_value
                params.update(auto_plot=True)
            except e:
                raise Exception(e)
            finally:
                params.update(auto_plot=True)
    return auto_plot

