from ...base import get_io_class as _gic, update_io
from ...base import DATA, TABLE, SWITCH, PRIMARY, LIST
from .reader import read_from_path, read_hd5

class DataHD5IO(_gic(DATA)):
    _data_address = None
    _data = None
    def __init__(self, *args, **kwargs):
        #path = kwargs.pop("path", None)
        super(DataHD5IO, self).__init__(*args, **kwargs)
        #self._data_address = path

    @property
    def data(self):
        return self._get_the_raw_data()
    
    def _get_the_raw_data(self, copy=False):
        if self._data_address:
            return read_from_path(self._data_address)
        if copy:
            return self._data.copy()
        return self._data
    def _set_the_raw_data(self, data):
        if isinstance(data, tuple):
            self._data_address = data
            self._data = None
        else:
            self._data_address = None
            self._data = data                

class SwitchHD5IO(_gic(SWITCH)):
    @classmethod
    def open(cl, file_name, index, entity=None, subentity=None,
             instrument=None, io="hd5", 
             get_class = None, copy=False):
        if not isinstance(index, int):
            raise ValueError("expecting a int for index got '%s'"%index)    
        return read_hd5(file_name, entity=entity, subentity=subentity, 
                        instrument=instrument, io=io, get_class=get_class, 
                        gpath=[index],
                        copy=copy)
    


class ListHD5IO(_gic(LIST)):
    @classmethod
    def open(cl, file_name, indexes=None, entity=None, subentity=None,
             instrument=None, io="hd5", 
             get_class = None, copy=False):
        return read_hd5(file_name, entity=entity, subentity=subentity, 
                        instrument=instrument, io=io, get_class=get_class, 
                        copy=copy, gpath= [indexes] if indexes else None)


def get_io_class(TYPE, io="hd5"):
    if io!="hd5":
        return _gic(TYPE, io=io)
    if TYPE & DATA:
        return  DataHD5IO
    if TYPE &  SWITCH:
        return SwitchHD5IO
    if TYPE & LIST:
        return ListHD5IO           
    return _gic(TYPE, io="base")  



update_io( "hd5", get_io_class)        