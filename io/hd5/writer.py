""" function to write a set of list/switch/data inside a hd5 table """
import tables
import numpy as np
from .. import fits
default_opener = fits.get_class("combined").open


ENTITY = "ENTITY"
SUBENTITY = "SUBENTITY"
INSTRUMENT = "INSTRUMENT"

_header_string_size = 60

tables.UnicodeCol = tables.StringCol
VERBOSE = True
FILENAME = "FILENAME"

def say(msg, level=1):
    if level>VERBOSE:
        print msg

def dtype2Col(dtype):
    """ convert a type to the right table Col subclass """
    dtype = np.dtype(dtype)
    #if dtype is np.dtype(str) or dtype is np.dtype(unicode):
    #    return tables.StringCol

    str_col = dtype.name.capitalize()+"Col"
    return tables.__dict__[str_col]


def make_header_description(header, keyfilter=lambda k:True, filename=None):
    """ from a header object with the iteritems method,
    create a description dictionary for Table
    """
    description = {}
    index = 0
    for key, value in header.iteritems():
        if not key: continue
        if not keyfilter(key): continue

        if isinstance(value, tuple):
            value = value[0]  # case of a dictionary where the second tuple
                              # element is a comment
        coltype = dtype2Col(type(value))
        if issubclass(coltype, tables.StringCol):
            col = coltype(_header_string_size,pos=index)
        else:
            col = coltype(pos=index)
        description[key] = col
        index += 1
    if filename is not None:
        description[FILENAME] = tables.StringCol(max(100,len(filename)))
    return description

def make_data_description(switch, **kwargs):
    """ from a list of object with the getData method (or .data attribute),
    create a description dictionary for Table.
    The getData method should return ndarray
    additional kwargs are parsed to getData
    """
    # switch here is equivalent to HDUList
    description = {}
    for index, data_entity in enumerate(switch):
        if hasattr(data_entity, "getData"):
            data = data_entity.getData(**kwargs)
        else:
            data = data_entity.data

        if isinstance(data, np.recarray):
            raise TypeError("Can only create table of images no reacarray")
        coltype = dtype2Col(data.dtype)
        shape = data.shape
        name = getattr(data_entity, "name", "")
        if not name:
            name = "DATA%d"%index
        description[name] = coltype(shape, pos=index)
    return description

def populate_header_row(header, row, filename=None):
    """ take a table row and a header (with iteritems method)
    and populate the row with the header.
    the row should have the right description
    """
    if not isinstance( row, tables.tableextension.Row):
        raise TypeError("expecting a table row object as 1st argument got %s"%row)
    for key, value in header.iteritems():
        if isinstance(value, tuple):
            value = value[0]
        try:
            row[key] = value
        except KeyError:
            say("The key '%s' is not defined in table and will be ignored"%key)
            continue
    if filename is not None:
        row[FILENAME] = filename

    row.append()

def populate_data_row(switch, row, **kwargs):
    """
    take a table row and a list/switch of object having the getData
    method (or .data attribute) and populate the row with the data
    additional **kwargs are sent to getData
    """
    if not isinstance( row, tables.tableextension.Row):
        raise TypeError("expecting a table row object as 1st argument got %s"%row)
    for index, data_entity in enumerate(switch):
        if hasattr(data_entity, "getData"):
            data = data_entity.getData(**kwargs)
        else:
            data = data_entity.data
        name = getattr(data_entity, "name", "")
        if not name:
            name = "DATA%d"%index
        row[name] = np.asarray(data)
    row.append()

def make_header_table(header,  group, name="headers", description="", filename=None):
    return tables.Table(group, name, make_header_description(header, filename=filename))

def make_data_table(switch, group, name="data", description="", **kwargs):
    tbl =  tables.Table(group, name, make_data_description(switch, **kwargs))
    return tbl

def get_header(switch):
    """ return .getheader if switch has the method else return
    the first header in the list
    """
    #if hasattr( switch , "getheader"):
    #    return switch.getheader()
    first = switch[0]
    if hasattr( first , "getheader"):
        return first.getheader()
    return first.header

def get_filename(switch):
    if hasattr(switch, "file_name"):
        return switch.file_name.get()
    first = switch[0]
    if hasattr(first, "fileinfo"):
        finfo = first.fileinfo()
        return finfo['file'].name
    return ""

def add_header_table(lst, group, name="headers", description=""):
    if not len(lst):
        raise TypeError("the first list element has no len cannot guerr the data srtucture")
    tbl = make_header_table(get_header(lst[0]), group, name=name, description=description,
                            filename=get_filename(switch)
                            )

    row = tbl.row
    for switch in lst:
        populate_header_row(get_header(switch), row, **kwargs)
    return tbl


def add_data_table(lst, group, name="data", description="",
                   entity=None, instrument=None, subentity=None,
                   **kwargs):
    if not len(lst):
        raise TypeError("the first list element has no len cannot guerr the data srtucture")
    tbl = make_data_table(lst[0], group, name=name, description=description, **kwargs)

    row = tbl.row
    for switch in lst:
        populate_data_row(switch, row, filename=get_filename(switch))

    set_table_attrs(tbd, lst, entity=entity, subentity=subentity,
        instrument=instrument
    )

    return tbl

def set_table_attrs(tbd, lst, entity=None, subentity=None, instrument=None):
    if entity is None:
        if hasattr(lst, "_entity_name_"):
            tbd.attrs[ENTITY] = lst._entity_name_
    else:
        tbd.attrs[ENTITY] = entity

    if instrument is None:
        if hasattr(lst, "_get_instrument_"):
            tbd.attrs[INSTRUMENT] = lst._get_instrument_()
    else:
        tbd.attrs[INSTRUMENT] = instrument

    if subentity is None:
        if len(lst) and  hasattr(lst[0], "_entity_name_"):
               tbd.attrs[SUBENTITY] = lst._entity_name_
    else:
        tbd.attrs[SUBENTITY] = subentity


def make_table(switch, group, header_name="headers", data_name="data",
                 header_description="", data_description="",
                 opener=default_opener,
                 **kwargs
                 ):
    if isinstance(switch, basestring):
        switch = opener(switch)

    tbh = make_header_table(get_header(switch), group, name=header_name,
                     description=header_description,
                     filename=get_filename(switch)
                     )
    tbd = make_data_table(switch, group, name=data_name,
                     description=data_description)


    return tbh, tbd


def populate_table(switch, header_row, data_row, opener=default_opener, **kwargs):
    if isinstance(switch, basestring):
        switch = opener(switch)
    populate_header_row(get_header(switch), header_row,
                        filename=get_filename(switch))
    populate_data_row(switch, data_row, **kwargs)

def add_table(lst, group, header_name="headers", data_name="data",
              header_description="", data_description="",
              opener=default_opener, entity=None, instrument=None,
              subentity=None,
              **kwargs):

    tbh, tbd = make_table(lst[0], group, header_name=header_name,
                      data_name=data_name,
                      header_description=header_description,
                      data_description=data_description,
                      opener=opener,
                      **kwargs
                      )
    for switch in lst:
        populate_table(switch, tbh.row, tbd.row, opener=opener, **kwargs)


    set_table_attrs(tbd, lst,
        entity=entity, subentity=subentity,
        instrument=instrument
    )


def record_table(lst, group, header_name="headers", data_name="data",
                 header_description="", data_description="", **kwargs
                 ):
    add_header_table(lst, group, name=header_name,
                     description=header_description)
    add_data_table(lst, group, name=data_name,
                     description=data_description)



def record_group(lst, h5file, group="/", name="detector", title="", **kwargs):
    gr = h5file.create_group(group, name, title=title)
    add_table(lst, gr, **kwargs)

def write_hd5(lst, file_path, title="", group_name="detector",
              group_title="",
              **kwargs):
    with tables.open_file(file_path, mode = "w",
                   title = title) as fl:
        record_group(lst, fl, name=group_name, title=group_title, **kwargs)












