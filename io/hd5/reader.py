import tables
from ...base import get_class as _def_get_class

open_file = tables.open_file
ENTITY = "ENTITY"
SUBENTITY = "SUBENTITY"
INSTRUMENT = "INSTRUMENT"

get_class = _def_get_class
_copy_ = True
_io_ = "hd5"

def row_to_switch(row_header, row_data, keys_header=None,
                  keys_data=None,  entity="switch", get_class=get_class, rpath=None, copy=_copy_, gpath=None):
    keys_data = keys_data or row_data.table.colnames
    keys_header = keys_header or row_header.table.colnames
    rpath = rpath or []
    lst = []
    first = True

    isscalar = False
    if gpath and len(gpath):
        if isinstance(gpath[0], (basestring, int)):
            names = [gpath[0]]
            isscalar = True
        else:
            names = gpath[0]    
    else:
        names = keys_data        

    for name in names:
        if first:
            header = cell_to_dict(row_header, keys_header)
        else:
            header = None

        lst.append(
                   cell_to_entity(row_data[name], name=name, header=header,
                                  get_class=get_class, rpath=rpath+[name], copy=copy)
                  )
    if isscalar:
        return lst[0]        
    return get_class(entity)(lst)

def cell_to_dict(cell, keys):
    return {k:cell[k] for k in keys}

def cell_to_entity(cell, name="", header=None, entity="data", get_class=get_class, rpath=None, copy=_copy_):
    rpath = rpath or []
    if copy: 
        obj = get_class(entity)(cell, name=name)
        obj.rpath = rpath 
    else:
        obj = get_class(entity)(tuple(rpath), name=name)


    if header:
        obj.setheader(obj.newheader(header))
    return obj

def table_to_list(table_header, table_data, entity="list", subentity="switch",
                  get_class=get_class, rpath=None, copy=_copy_, gpath=None):
    rpath = rpath or []

    N = len(table_header)
    if N!=len(table_data):
        raise Exception("table_header and table_data must have the same dimention")
    lst = []
    keys_header = table_header.colnames
    keys_data   = table_data.colnames

    isscalar = False
    if gpath and len(gpath):
        if isinstance(gpath[0], int):
            indexes = [gpath[0]]
            isscalar = True
        else:
            indexes = gpath[0]
    else:
        indexes = range(N)            
    for index in indexes:
        lst.append(
                   row_to_switch(table_header[index], table_data[index],
                                keys_header=keys_header, keys_data=keys_data,
                                entity=subentity, get_class=get_class, rpath=rpath+[index], copy=copy, 
                                gpath= None if gpath is None else gpath[1:]
                                )
                   )
        if isscalar:
            return lst[0]
    return get_class(entity)(lst)


def group_to_list(group, entity=None, subentity=None,
                  instrument=None, io=_io_,
                  get_class=None, rpath=None, copy=_copy_, gpath=None
                  ):
    if entity is None:
        if ENTITY in group.data.attrs:
            entity = group.data.attrs[ENTITY]
        else:
            entity = "list"
    if subentity is None:
        if SUBENTITY in group.data.attrs:
            subentity = group.data.attrs[SUBENTITY]
        else:
            subentity = "switch"
    if instrument is None:
        if INSTRUMENT in group.data.attrs:
            instrument = group.data.attrs[INSTRUMENT]
        else:
            instrument = "base"

    def _get_class(entity):
        return _def_get_class(entity, io=io, instrument=instrument)

    get_class = _get_class if get_class is None else get_class
    return table_to_list(group.headers, group.data,
                         entity=entity, subentity=subentity,
                         get_class=get_class, rpath=rpath, copy=copy, gpath=gpath
                        )


def read_hd5(file_name, entity=None, subentity=None,
              instrument=None, io=_io_,
              get_class = None, copy=_copy_, gpath=None
             ):
    f = tables.open_file(file_name, "r")
    lst = group_to_list(f.root.detector, entity=entity,
                  subentity=subentity,
                  instrument=instrument,
                  io = "base" if copy else "hd5",
                  get_class=get_class, rpath=[f, "/detector"], copy=copy, gpath=gpath)
    lst.hd5 = f
    return lst


def read_from_path(path):
    path = list(path)
    f = path[0]
    g = f.root
    path.pop(0)
    for p in list(path):
        if isinstance(p,basestring) and p[0] == "/":
            g = getattr(g, p[1:])
            path.pop(0)
        else:
            break

    data = g.data
    for p in path:
        data = data[p]
    return data 
                    






