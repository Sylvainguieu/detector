try:
    import pyfits as pf
except:
    from astropy.io import fits as pf

import numpy as np
from ...base import DataIO
from ...datax import DataX, dataxconcat, size,size_of_shape, isdatax
from ...base import get_class, getarray


def new_header(*args, **kwargs):
    kwargs = dict(*args, **kwargs)
    header = pf.Header()
    for k,v in kwargs.iteritems():
        header[k] = v
    return header


class BaseHDU(DataIO):
    """
    Some additional function added to every kind of HDU
    """

    _new_header = staticmethod(new_header)

    @getarray
    def comment(self, k=None):
        if k is None:
            return self.header["COMMENT"]
        return self.header.comments[k]

    def _get_header(self):
        return self.header

    def getshape(self):
        N = self.header["NAXIS"]
        return tuple(self.header["NAXIS%d"%(N-i)] for i in range(N) )
        
    def _get_file_path(self):
        finfo = self.fileinfo()
        if finfo is None: return ""
        return finfo['file'].name


def _get_data_axes( data):
    if isdatax(data): return np.asarray(data), data.axes
    return data, None

class PrimaryHDU(pf.PrimaryHDU, BaseHDU):
    """ Primary HDU. see pyfits """
    def __init__(self,data, *args, **kwargs):
        data, axes = _get_data_axes(data)
        axes = kwargs.pop("axes", axes)
        _name = getattr(self, "_default_name_", "")

        data = self._parse_data(data, axes)
        pf.PrimaryHDU.__init__(self, data, *args, **kwargs)
        # if no name or it is a PRIMARY set the one comming from 
        # the entity with _default_name_ if any  
        if self.name == "" or (self.name=="PRIMARY" and _name):
            self.name = _name


class ImageHDU(pf.ImageHDU, BaseHDU):
    """ Image HDU. see pyfits """
    def __init__(self,data, *args, **kwargs):
        data,axes = _get_data_axes(data)
        axes = kwargs.pop("axes", axes)
        _name = getattr(self, "_default_name_", "")

        data = self._parse_data(data, axes)
        pf.ImageHDU.__init__(self, data, *args, **kwargs)

        # if anonymous set name from _default_name_
        self.name = self.name or _name


class BinTableHDU(pf.BinTableHDU, BaseHDU):
    """ Binary table HDU. see pyfits """
    def __init__(self,data, *args, **kwargs):
        axes = kwargs.pop("axes", None)
        _name = getattr(self, "_default_name_", "")

        data = self._parse_data(data, axes)
        pf.BinTableHDU.__init__(self, data, *args, **kwargs)
        if axes:
            self.axes = axes
        # if anonymous set name from _default_name_    
        self.name = self.name or _name
