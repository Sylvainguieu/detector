try:
    import pyfits as pf
except:
    from astropy.io import fits as pf

from hdulist import HDUList, HDUSwitch, FitsList
from hdus import PrimaryHDU, ImageHDU, BinTableHDU
from ...base import DATA, SWITCH, LIST, TABLE, PRIMARY
from ...base import update_io
from ...base import (get_class as _get_class,
                     get_io_class as _get_io_class
                     )

open = HDUList.open

from .opener import local

def get_io_class(TYPE, io="fits"):
    if io!="fits":
        return _get_io_class(TYPE, io=io)

    if (TYPE & PRIMARY) and (TYPE & TABLE):
        raise ValueError("embigous i/0 TABLE cannot be PRIMARY")
    if (TYPE & DATA) and (TYPE & TABLE):        
        raise ValueError("embigous i/0 TABLE cannot be DATA")

    if ((TYPE & DATA) or\
        (TYPE & TABLE) or\
        (TYPE & PRIMARY)) and ((TYPE & SWITCH) or (TYPE & LIST)):
        raise ValueError("embigous i/0")

    if TYPE & PRIMARY:
        return PrimaryHDU

    elif TYPE & DATA:
        return ImageHDU

    elif TYPE & TABLE:
        return BinTableHDU

    elif TYPE & SWITCH:
        return HDUSwitch

    elif TYPE & LIST:
        return FitsList
    else:
        raise TypeError("entity class case %s not understood"%TYPE)


update_io("fits", get_io_class)


def get_class(entity_name, io="fits", **kwargs):
    if io is None:
        io = "fits"
    return _get_class(entity_name, io=io, **kwargs)


def _test_():
    fh = HDUList.open("/Users/guieu/DETDATA/2014-12-09/PIONIER_DARK-TEMPVAR-SO_343_0022.fits")
    print "shape", fh.getData().shape

