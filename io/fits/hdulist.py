try:
    import pyfits as pf
except:
    from astropy.io import fits as pf

import numpy as np
from ...base import SwitchIO, ListIO
from . import opener as io
from .hdus import  new_header

from detector.base import SwitchMutual

def write_to_fits(switch, file_name, **kwargs):
    cl = switch._get_class(switch._entity_name_, io="fits")
    ##
    # Here cl is needed and should reclass the childs
    ##
    switch = pf.HDUList(cl(switch))
    switch.writeto(file_name, **kwargs)

def switch_to_fits(switch):
    cl = switch._get_class(switch._entity_name_, io="fits")
    return cl(switch)

SwitchMutual.write_to_fits = write_to_fits
SwitchMutual.to_fits = switch_to_fits


class _base_(object):

    @classmethod
    def open(cls, name, mode='readonly', memmap=None, save_backup=False, **kwargs):
        hdus = pf.open(name, mode=mode, memmap=memmap,
                    save_backup=save_backup, **kwargs)
        return cls(hdus)
    _new_header = staticmethod(new_header)

class HDUList(pf.HDUList, ListIO, _base_):
    pass

class HDUSwitch(_base_, pf.HDUList, SwitchIO):
    """ This is handled like  pyfits HDUList class """
    def __init__(self, lst=[], *args, **kwargs):
        SwitchIO.__init__(self, lst, axes=kwargs.pop("axes",None))
        pf.HDUList.__init__(self, lst, *args, **kwargs)
        return


class FitsList(ListIO):
    """ Handle a list of fits file
    The open method allows to open several fits files  in a FitsList class
    """
    _new_header = staticmethod(new_header)
    _io_opener = None  
    @classmethod
    def open(cls, strglob, subdir=None, ffilter=None,
             opener=pf.open, verbose=True, wrapper=None, **kwargs):
        """open a list of file in a FitsList object

           Attribute
           ---------
             strglob =  a string glo expression (e.g "*.fits")
             subdir  = the subdirectory is not explicit in strglob
             open   = the fits opener  default is the pyfits open function
             verbose = True/False toggle on/off the verbose

        """
        wrapper = wrapper if wrapper else getattr(cls, "fromlist", cls)
        if isinstance(wrapper, basestring):
            wrapper = cls._get_class(wrapper)

        if isinstance(opener, basestring):
            opener = cls._get_class(opener).open

        io_opener = getattr(cls, "_io_opener", None)
        if io_opener is None:
            io_opener = io.local   

        return io_opener.open(strglob, subdir=subdir, ffilter=ffilter,
                             opener=opener, wrapper=wrapper,verbose=verbose,
                             **kwargs)        

