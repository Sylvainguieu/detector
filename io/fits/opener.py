from .. import opener
from ..opener import *
from .hdus import pf

default_openeur = pf.open

class ImgListOpeneur(opener.Opener):
    path = "*.fits"
    opener = pf.open
    def biases( self, *args, **kwargs):
        return self.open( kwargs.pop("path", "*BIAS*.fits"), *args, **kwargs)
    def flats( self, directory="./", **kwargs):
        return self.open( kwargs.pop("path", "*FLAT*.fits"), *args, **kwargs)
    def darks( self, directory="./", **kwargs):
        return self.open( kwargs.pop("path", "*DARK*.fits"), *args, **kwargs)

local = ImgListOpeneur()

