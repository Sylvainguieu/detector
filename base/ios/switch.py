from ..engine import get_class
from .header import HeaderList

def switch_to_item(obj, item):
    if isinstance(item, tuple):
        item, number = item
        if isinstance(item, int):
            raise TypeError("expecting string,int has item got %s,%s "%(item, number))
    else:
        number = 0

    if not isinstance(item, int):
        nfound = 0
        for sub in obj:
            if hasattr(sub,"_is"):
                if sub._is(item):
                    if nfound==number:                            
                        return sub
                    nfound += 1                                         
        raise ValueError("data entity '%s',%d not found"%(item,number))
    
    else:
        for i,sub in enumerate(obj):
            if i<item: continue
            return sub
    raise ValueError("index %d out of range"%item)        




class SwitchIO(list, HeaderList):
    """
    A Switch is a list of DataEntity (or other)
    it aims is to dispatch command to the right DataEntity.
    For instance :  s.getImage() will call s[1].getImage() while
                    s.getSpectrum() will call s[2].getSpectrum()
                    s.key("DIT")  call s[0].key("DIT") etc ....
    """
    def __init__(self, lst, *args, **kwargs):
        axes = kwargs.pop("axes", None)
        list.__init__(self, lst,*args, **kwargs)
        self._subclass_list()
        if axes is not None: 
            self.axes = list(axes)

    def __getitem__(self, item):
        if isinstance(item, int):
            return list.__getitem__(self, item)
        return switch_to_item(self,item)

