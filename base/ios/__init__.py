from ..engine import DATA, SWITCH, LIST, TABLE, PRIMARY
from ..engine import update_io

from .data import DataIO
from .datalist import ListIO
from .switch  import SwitchIO
from .table   import TableIO
from .primary import PrimaryIO
from ..engine import get_class as _get_class

def get_io_class(entity_case, **kwargs):

    if entity_case & DATA:
        return DataIO
    elif entity_case & PRIMARY:
        return PrimaryIO
    elif entity_case & TABLE:
        return TableIO

    elif entity_case & SWITCH:
        return SwitchIO
        
    elif entity_case & LIST:
        return ListIO
    else:
        raise TypeError("entity class case %s not understood"%entity_case)


def get_class(entity_name, io="base", instrument=None, **kwargs):
    return _get_class(entity_name, io=io, instrument=instrument, **kwargs)

update_io("base", get_io_class)
