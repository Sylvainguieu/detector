from ..engine import get_class
from .header import HeaderList
from ..engine import subclass_list
import numpy as np
class ListIO(list, HeaderList):
    def __init__(self, *args, **kwargs):
        axis = kwargs.pop("axis", None)
        subclass = kwargs.pop("subclass", None)
        list.__init__(self,*args, **kwargs)

        if axis is not None: self.axes = [axis]


    def idx(self, items):
        """
        getitem( lst, item)
        mimic fancy numpy index for detector lists
        """
        if not isinstance( items, slice) and not hasattr( items, "__iter__"):
            return list.__getitem__(self, items)


        if isinstance( items, slice):
            return  self.__class__( list.__getitem__(self, items) )

        if isinstance( items, np.ndarray):
            items = np.arange( len(self))[ items].flat
        else:
            items = np.arange( len(self))[ np.r_[items] ].flat
        return self.__class__( [self[i] for i in items] )

    def __getitem__(self, items):
        return self.idx(items)

    def __getslice__(self, i, j):
        return self.idx(slice(i,j))



