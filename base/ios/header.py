class Header(object):
    def _get_header(self, copy=False):
        """
        obj.getheader( )
        Return the header of the curent Data
        """
        if self.header is not None and copy and hasattr(self.header, "copy"):
            return self.header.copy()
        else:
            return self.header

    def _set_header(self, header):
        self.header = header

    def _new_header(self, *args, **kwargs):
        """
        obj.newheader( )
        obj.newheader( dictionary , key1=val1, key2=val2, ....)
        Return a new empty Header filled with the key/vals pairs
        """
        return dict(*args, **kwargs)

    def _get_file_path(self):
        return ""
        #raise Exception("This data is not linked to any file")

class HeaderList(object):
    def _new_header(self, *args, **kwargs):
        """
        obj.newheader( )
        obj.newheader( dictionary , key1=val1, key2=val2, ....)
        Return a new empty Header filled with the key/vals pairs
        """
        return dict(*args, **kwargs)
