from ..engine import get_class
from .header import Header
from ...datax import DataX, dataxconcat, size, size_of_shape, isdatax
import numpy as np
from ... import config


class DataIO(Header):
    def _get_the_raw_data(self, copy=False):
        if self.data is not None and copy:
            return self.data.copy()
        return self.data

    def _set_the_raw_data(self,data):
        self.data = data

    def __init__(self, data=None, header=None, axes=None, name=None, **kwargs):
        header = header or {}
        header.update(kwargs)

        self._set_header(header)
        #self._set_the_raw_data(data)
        self.name = name if name is not None else getattr(self, "_default_name_", "")

        self._set_the_raw_data(self._parse_data(data, axes))

