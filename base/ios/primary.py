from ..engine import get_class
from .header import Header
import numpy as np

class PrimaryIO(Header):
    """ primary handler has not get/set data method only headers method """
    pass