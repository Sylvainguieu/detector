import numpy as np
from ...datax import DataX, dataxconcat, size,size_of_shape, isdatax
from ... import config


def get_item_exec_method(obj, funcname, item , args, kwargs):
    return getattr(obj[item], funcname)(*args, **kwargs)

def get_item_exec_func(obj, func, item , args, kwargs):
    return func(obj[item],*args, **kwargs)

len_func = len

def parseclass(obj, cls):
    obj.__class__ = cls



def list_decorator_prepare(obj, onsection, kwargs, lenfunc=len, inaxis=None,
                            on_default_attr="_on_default"):
        section = None
        freduce = None
        axes = None


        if onsection is False:
            if hasattr(obj, on_default_attr):
                onsection = getattr(obj, on_default_attr)
            else:
                onsection = None


        if inaxis is not None:
            axes = [inaxis]
        elif hasattr(obj ,"axes"):
            axes =  obj.axes

        if axes is not None:
            if axes and len(axes):
                if axes[0] in config.idxkeys: # check is the short/lazy keyword is accepted
                    section = kwargs.pop(axes[0], None)
                if str(axes[0])+"_idx" in kwargs:
                    if section is not None:
                        say("conflict between '%s_section' and '%s' keyword, '%s' is retained"%(axes[0], axes[0], axes[0]))
                    section = kwargs.pop(str(axes[0])+"_idx", None)

            ######
            # If section is anything else than a slice, a int None or array
            # convert it to an array
            if (section is not None) and (not isinstance(section,slice)) and \
            (not isinstance(section,(int,long))) and \
            (not isinstance(section,np.ndarray)):
                section = np.array(section)
            ####
            # if section is boolean convert it to an array of index
            if isinstance(section, np.ndarray) and section.dtype == np.bool:
                section = np.where(section.flat)[0]
            ###
            # if section is a datax takes its axes
            if isdatax(section):
                axes = section.axes
            else:
                axes = axes[0:1]

            freduce = { ax+"_reduce":kwargs.pop(ax+"_reduce") for ax in axes if ax+"_reduce" in kwargs}
        if isinstance(onsection, slice):
            onsection = np.r_[0:lenfunc(obj)][onsection]

        # ## ## ## ## ##
        # if onsection is a scalar, section and freduce should be None !
        # ## ## ## ## ##
        if onsection is not None and not hasattr(onsection, "__len__"):
            if section is not None:
                raise IndexError( " %s_idx is %s on an index with no lens "%(axes[0],section))
            #if freduce is not None:
            #    raise IndexError( " %s is %s on an index with no lens "%(axes[0]+"_reduce",freduce))

        if onsection is None and section is not None:
            index = np.r_[0:lenfunc(obj)][section]
        elif section is not None:
            index = np.r_[0:len(onsection)][section]
        else:
            index = onsection

        if index is None:
            index = range(lenfunc(obj))
        if isinstance(index, np.ndarray): index = index.flat
        return index, section, freduce



def get_axis(self, axis=None):
    if axis is not None:
        return [axis]
    return self.axes

def finalise(self, data, inarray=True, inaxis=None):
    """ Key function to finalise a list of returned elements
    if inarray is true return in a DataX if list has axes or in np.array otherwhise
    if inarray is a string look at the coresponging class with the _get_class method
    and return the object in this class
    inaxis keyword force force the returned data to have the inaxis axis for firs axis
    """
    if inarray is True:
        return self.inarray(data, axes=self._get_axis(inaxis))
    if isinstance(inarray, basestring):
        return self._get_class(inarray)(data)
    if inarray not in [None,False]:
        return inarray(data)
    return data

def inarray(data, axes=None):
    if axes and len(axes):
        return dataxconcat(data, axes[0])
    return np.asarray(data)


def isarray(data):
    return isinstance( data, np.ndarray)
