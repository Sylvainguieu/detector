from ... import config
from .mutual import get_mutual_subclasses

from ...gets import build_getmethod, build_doc

from ...config import (DATA, SWITCH, LIST, TABLE, PRIMARY)

####
# BaseGet is here to avoid recursive include

def retype(get_class_method, classes, *args, **kwargs):
    docs = []
    out_classes = []
    names = []
    for cl in classes:
        if isinstance(cl, basestring):
            name, cl = get_class_method(cl, *args, **kwargs)
        else:
            name = cl.__name__    
        if cl.__doc__:
            docs.append(cl.__doc__)
        out_classes.append(cl)
        names.append(name)
    names = "_".join(names)        
    return names, type(names, tuple(out_classes), dict(__doc__="\n".join(docs)))

def update_io(name, get_class_func):
    config.ios[name] = get_class_func

def get_io_class(io_name, TYPE, **kwargs):
    if io_name is None:
        io_name = "base"
    return config.ios[io_name](TYPE,  **kwargs)


def update_instrument_space(instrument, get_class_func, subinstruments=["base"]):
    config.instruments[instrument] = get_class_func
    if not instrument in config.class_entity_dictionary:
        config.class_entity_dictionary[instrument] = {}
    if instrument in subinstruments:
        raise TypeError("instrument '%s' in subinstruments "%instrument)    
    config.class_entity_dictionary[instrument]["__from__"] = list(subinstruments)


def get_instrument_subclasses(name, TYPE, **kwargs):
    if name is None:
        name = "base"     
    return config.instruments[name](TYPE, **kwargs)


def get_entity_class(name, instrument="base"):
    try:
        inst = config.class_entity_dictionary[instrument]
    except KeyError:
        raise TypeError("instrument '%s' not defined"%instrument)

    try: 
        cl = inst[name]
    except KeyError:
        _from_ = inst.get("__from__", [])       
        for f in _from_:
            try:
                cl = get_entity_class(name, instrument=f)
            except:
                continue
            else:
                return cl
        raise TypeError("Cannot find  '%s' in instrument '%s' "%(name,instrument))                    
    return cl                        
def update_entities(_instrument_="base", _kw_=None, **classes):
    """ record new entities classes 

    Args:
    -----
        _instrument_ (Optional[string]) : instrument space name, default is 'base'
        _kw_ (Optional[string]) : a dictionary of name/class pair 
        **kwargs :  name/ class pairs 

    Each item should be type (classes), this is not checked !
    """
  
    nd = {}   
    nd.update(_kw_ or {} , **classes)
    if not _instrument_ in config.class_entity_dictionary:
        config.class_entity_dictionary[_instrument_] = {}

    config.class_entity_dictionary[_instrument_].update(nd)

        

def save_get_entity_class(name, instrument="base"):
    try:
        cl = config.class_entity_dictionary[instrument][name]   
    except:
        if instrument!="base":
            try:
                cl = config.class_entity_dictionary["base"][name]
            except KeyError:
                raise ValueError("Cannot find entity of name '%s' in instrument '%s' neither in base"%(name,instrument))
        else:
            raise ValueError("cannot find entity name '%s' in base"%name)
    return cl


def reset_class(*clnames):
    out = [ ]
    for clname in clnames:
        for k in config.class_dictionary.keys():
            spk = k.split("_")
            if spk[1] == clname:
                out.append(config.class_dictionary.pop(k))
    return out


def _get_class_from_class(cls, entity_name=None, io=None,
                           instrument=None, **kwargs):
    """ return the class from stored default in class """
    return get_class(entity_name or cls._get_entity_(), 
                     io=io or cls._get_io_(),
                     instrument= instrument or cls._get_instrument_(),
                      **kwargs)




def get_class(entity_name, io="base", instrument="base", **kwargs):

    if isinstance(entity_name, basestring):
        entity_name = (entity_name,)
    e_names = []
    entity_cl = []
    TYPE = 0
    for name in entity_name:
        ecl = get_entity_class(name, instrument=instrument)        
        e_names.append(name)
        
        if not TYPE & ecl._entity_type_:
            TYPE += ecl._entity_type_
        entity_cl.append(ecl)

    entity_cl = tuple(entity_cl)
    e_names = tuple(e_names)

    entity_name = "_".join(e_names)

    
    io_cl = get_io_class(io,TYPE, **kwargs)                                                                               

    instrument_cls = get_instrument_subclasses(instrument, TYPE, 
                                                 **kwargs)
    mutual_cl = get_mutual_subclasses(TYPE, **kwargs)


    cname = "{instrument}_{io}_{entity}".format(io=io,
                                                instrument=instrument,
                                                entity=entity_name)
       
    if cname in config.class_dictionary:
        return config.class_dictionary[cname]

    allsubclasses = instrument_cls + (io_cl,)  + entity_cl + mutual_cl
    mdoc = build_doc(allsubclasses, "   ")

    doc = """
    Data Type = {ename}:
    ---------
    {edoc}

    Instrument = {iname}:
    ----------
    {idoc}

    IO:
    ---
    {iodoc}

    Methods
    -------
    {mdoc}
    """.format(ename= entity_name, iname=instrument, io=io,
               edoc = "\n".join( [c.__doc__ for c in entity_cl if c.__doc__]), 
               idoc = "\n".join( [c.__doc__ for c in instrument_cls if c.__doc__]), 
               iodoc= io_cl.__doc__ or "",
               mdoc = mdoc
              )


    _dict_ = dict(
     _entity_name_ = entity_name,
     _get_io_ = staticmethod(lambda : io),
     _get_instrument_ = staticmethod(lambda : instrument),
     _get_entity_ = staticmethod(lambda : e_names),
     _get_class = classmethod(_get_class_from_class),
     __doc__ = doc
    )


    TMPCLASS = type(cname,
                    allsubclasses,
                    _dict_
                    )

    build_get(TMPCLASS)
    config.class_dictionary[cname] = TMPCLASS
    return TMPCLASS






def build_get(cl_to, subclasses=None):
    if subclasses is None:
        subclasses = getattr(cl_to, "subclasses", [])    
    out_classes = []
    for  on,cl in subclasses:
        if isinstance(cl, (basestring, tuple, list)):
            out_classes.append((on, cl_to._get_class(cl)))                   
        else:
            out_classes.append((on, cl))

    return build_getmethod(cl_to, out_classes)

def _rename_class(clsname):
    return clsname.lower()


def copy_attrs(obj, cls, attrs, **kwargs):
    for attr in attrs:
        val = kwargs.get(attr, None)
        if val is None:
            val = getattr(cls, attr, None)
        if val is not None:
            setattr(obj, attr, val)


def parse_data_class(obj, cls):
    ##
    # if the _io_name attribute has changed, we need to copy the
    # data.
    #
    copy_data = False

    if hasattr(obj, "_io_name") and \
        (getattr(obj, "_io_name", True)!=getattr(cls, "_io_name", False)):
        
        copy_data = True
        data   = obj._get_the_raw_data(True)
        header = obj._get_header(True)

    obj.__class__ = cls



    copy_attrs(obj, cls, ["axes","_default_name_"])

    ###
    # overwrite the object name with the class name
    if not getattr(obj, "name", "") and hasattr(obj, "_default_name_"):
        obj.name = obj._default_name_

    if copy_data:
        obj._set_the_raw_data(data)
        obj._set_header( obj._new_header(header) )

def parse_switch_class(obj, cls):
    obj.__class__ = cls

    copy_attrs(obj, cls,  ["axes", "subclasses"])
    obj._subclass_list()

def parse_list_class(obj, cls):
    obj.__class__ = cls
    copy_attrs(obj, cls,  ["axes"])

def parse_class(obj, cls):
    TYPE = getattr(cls, "_entity_type_", None)
    obj_entity_type_ = getattr(obj, "_entity_type_", None)
    if TYPE and obj_entity_type_ and TYPE != obj_entity_type_:
        if any( (TYPE & C) for C in  [DATA, TABLE, PRIMARY] ) and\
           not any( (obj_entity_type_ & C) for C in  [DATA, TABLE, PRIMARY] ):
            raise TypeError("Cannot change class, obj and class should have a compatible _entity_type_ attribute")   
            
    if TYPE & SWITCH:
        return parse_switch_class(obj, cls)
    if TYPE & LIST:
        return parse_list_class(obj, cls)
    if any( (TYPE & C) for C in  [DATA, TABLE, PRIMARY]):
        return parse_data_class(obj, cls)
    raise ValueError("Class type %s not understood"%TYPE)

def parse_class_by_name(obj, cls_name):
    return parse_class(obj, obj._get_class(cls_name))



def subclass_list(obj,  subclasses, io=None, instrument=None):
    """ a Switch object can have rules to reclass the childs
    This function is aimed to reclass the init list according
    to the subclasses attribute
    """
    if not subclasses: 
        return

    names = [getattr(child,"name", None) for child in obj]

    for ons, cl in subclasses:

        if ons is True:
            return subclass_childs(obj, cl, io=io, instrument=instrument)

        if not hasattr(ons, "__iter__"):
            ons = [ons]
        for on in ons:
            if isinstance(on, basestring):                    
                if on in names:
                    child = obj[names.index(on)]
                    break    
            elif isinstance(on, int):
                if on<len(obj):
                    child = obj[on]            
                    break
        else:
            continue
        parse_class(child, obj._get_class(cl, io=io, instrument=instrument))       


def subclass_childs(obj, subclasses, io=None, instrument=None):

    if isinstance(subclasses, tuple):
        subclass, subclasses = subclasses[0], subclasses[1:]
    else:
        subclass, subclasses = subclasses, tuple()

    if isinstance(subclass, basestring):
        subclass = obj._get_class(subclass, io=io, instrument=instrument)

    for child in obj:
        parse_class(child, subclass)
        if len(subclasses):
            subclass_childs(obj, subclasses, io=io, instrument=instrument)




def parseclass(obj, cls):
    obj.__class__ = cls
    return
    #TYPE = getattr(cls, "_entity_type_", None)
    #if TYPE == SWITCH or TYPE == LIST:
    #    return cls(obj)
    #else:
    #    obj.__class__ = cls
    #return obj












