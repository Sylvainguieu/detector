
""" These Mutual class are dynamicaly added to the handler, entity, instrument classes
when get_class is called.
The goal is to provide a way to add writting function for io handler
e.g. for fits file one can add

from detector.base import MutualSwitch

def write_to_fits(switch, file_name, **kwargs):
    cl = switch._get_class(switch._get_entity_(), io="fits")
    switch = switch(cl)
    switch.writeto(file_name, **kwargs)

MutualSwitch.write_to_fits = write_to_fits
"""
from ...config import DATA, SWITCH, LIST, TABLE, PRIMARY

class DataMutual(object):
    pass
class SwitchMutual(object):
    pass
class ListMutual(object):
    pass
class TableMutual(object):
    pass
class PrimaryMutual(object):
    pass

def get_mutual_subclasses(TYPE, **kwargs):
    lookup = {
        DATA:   ("D", DataMutual), 
        PRIMARY:("P", PrimaryMutual),
        TABLE:  ("T", TableMutual),
        SWITCH: ("S", SwitchMutual), 
        LIST:   ("L", ListMutual)
    }
    cls = []
    pref = ""
    for case, (p,cl) in lookup.iteritems():
        if TYPE & case:
            pref+=p
            cls.append(cl)
    return tuple(cls)
