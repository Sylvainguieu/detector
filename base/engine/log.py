from ... import config


class Log(object):
    def say(self, txt, level=1, vtype=""):
        verbose = getattr(self, "verbose", config.verbose)
        if verbose>level:
            if vtype:
                print "{vtype}: {txt}".format(vtype=vtype,txt=txt)
            else:
                print txt
