from class_handler import DATA, SWITCH, LIST, TABLE
from ... import config
from ...datax import DataX, isdatax
from ... import gets
from ...gets.wrappers.pandaswrapper import PandasWrapper
import numpy as np
from ... import plots

class DetBaseInstance(object):
    encaps_class = "data"
    newheader = None
    name = ""
    _attrs_ = ["encaps_class" , "newheader", "name"]

    def asdata(self, *args, **kwargs):
        obj = self.obj
        shape = obj.getshape()
        if len(shape)==2:
            inside = "image"
        elif len(shape)==3:
            inside = "cube"
        else:
            inside = "data"
        inside = kwargs.pop("inside", inside)

        value = self.get(*args, **kwargs)

        if hasattr(value, "__iter__"):
            raise ValueError("get result must be a scalar")

        A = np.ndarray( shape, type(value))
        A[...] = value

        return obj._get_class(inside, io="base")(A)

    def reshaped(self, newshape, *args, **kwargs):
        def reshaped(value):
            if hasattr(value, "__iter__"):
                raise ValueError("get result must be a scalar")
            A = np.ndarray(newshape, type(value))
            A[...] = value
            return A
        return self.map(reshaped, *args, **kwargs)


    def datashaped(self, *args, **kwargs):
        obj = self.obj
        def reshape(obj, *args, **kwargs):
            value = self.fget(obj, *args, **kwargs)
            newshape = obj.getshape()
            if hasattr(value, "__iter__"):
                raise ValueError("get result must be a scalar")
            A = np.ndarray(newshape, type(value))
            A[...] = value
            axes = obj.getaxes()
            if axes:
                return DataX(A, axes)
            return A
        kwargs["__fget__"] = reshape
        return self.get(*args, **kwargs)


    def encaps(self, *args, **kwargs):
        obj = self.obj

        header = kwargs.pop("header", {})
        axes = kwargs.pop("axes", None)
        name = kwargs.pop("name", None)
        inside = kwargs.pop("inside", self.encaps_class)
        cl_kw = { k:kwargs[k] for k in ["instrument", "io"] if k in kwargs }


        if self.newheader:
            header = self.newheader(obj, header)
        else:
            header = obj.newheader(header)


        return self.obj._get_class(inside, **cl_kw)(self.get(*args, **kwargs),
                                                    header, axes=axes, name=name
                                                   )

    def hist(self, bins=None, kw=None, **kwargs):
        kw = kw or {}
        data = self.get(**kwargs)
        if bins is None:
            bins = np.linspace(np.min(data), np.max(data), 50)
        return plots.hist(data.flatten(), bins, **kw)

    def plot(self, kw=None, **kwargs):
        kw = kw or {}
        data = self.items(**kwargs)
        return plots.plot(*zip(*data))





class DataInstance(DetBaseInstance, gets.DataInstance):
    _attrs_ = DetBaseInstance._attrs_+gets.DataInstance._attrs_


class SwitchInstance(DetBaseInstance, gets.SwitchInstance):
    on = False
    _attrs_ = DetBaseInstance._attrs_+gets.SwitchInstance._attrs_


def get_item_exec_func(obj, func, item , args, kwargs):
    return func(obj[item],*args, **kwargs)
len_func = len

def _dummy_(val):
    return val


def _list_decorator_prepare(obj, on, kwargs, lenfunc=len, inaxis=None):
        section = None
        freduce = None
        axes = None
        onsection = None

        if inaxis is not None:
            axes = [inaxis]
        elif hasattr(obj ,"axes"):
            axes =  obj.axes

        if axes is not None:
            if axes and len(axes):
                if axes[0] in config.idxkeys: # check is the short/lazy keyword is accepted
                    section = kwargs.pop(axes[0], None)
                if str(axes[0])+"_idx" in kwargs:
                    if section is not None:
                        say("conflict between '%s_section' and '%s' keyword, '%s' is retained"%(axes[0], axes[0], axes[0]))
                    section = kwargs.pop(str(axes[0])+"_idx", None)

            ######
            # If section is anything else than a slice, a int None or array
            # convert it to an array
            if (section is not None) and \
                (not isinstance(section,slice)) and \
                (not isinstance(section,(int,long))) and \
                (not isinstance(section,np.ndarray)):
                section = np.array(section)
            ####
            # if section is boolean convert it to an array of index
            if isinstance(section, np.ndarray) and section.dtype == np.bool:
                section = np.where(section.flat)[0]
            ###
            # if section is a datax takes its axes
            if isdatax(section):
                axes = section.axes
            else:
                axes = axes[0:1]

            freduce = { ax+"_reduce":kwargs.pop(ax+"_reduce") for ax in axes if ax+"_reduce" in kwargs}

        # ## ## ## ## ##
        # if onsection is a scalar, section and freduce should be None !
        # ## ## ## ## ##
        if onsection is not None and not hasattr(onsection, "__len__"):
            if section is not None:
                raise IndexError( " %s_idx is %s on an index with no lens "%(axes[0],section))
            #if freduce is not None:
            #    raise IndexError( " %s is %s on an index with no lens "%(axes[0]+"_reduce",freduce))

        if section is not None:
            if on is True:
                index = np.r_[0:lenfunc(obj)][section]
            else:
                index = [on[i] for i in section]
        else:
            if on is True:
                index = range(lenfunc(obj))
            else:
                index = on

        if isinstance(index, np.ndarray): index = index.flat
        return index, section, freduce

def _return_list_array_decorator(A, section, freduce):
    ##
    # At this point A should be flat
    # we need to return A to reflect the section attribute if this one
    # was a N>2 array dimention.
    if isinstance( A, np.ndarray):
        if isinstance( section,  np.ndarray):
            index = np.arange(section.size).reshape(section.shape)
            if isdatax( section):
                index = DataX( index, section.axes )

            A = A[index]

    if isdatax( A) and freduce is not None and len(freduce):
        A = A.apply( **freduce)
    return A

class DataXWrapper(gets.ArrayWrapper):
    _attrs_ = ["axis"]
    axis = None
    def __init__(self, axis=None):
        if axis is not None:
            self.axis = axis
    def init(self, obj, on, args, kwargs):
        self.args = list(args)
        self.kwargs = kwargs

        on, self.section, self.freduce = _list_decorator_prepare(
               obj, on, kwargs, len_func, inaxis = None
        )
        self.obj  = obj
        if not hasattr(on, "__iter__"):
            self.size = 1
            self.scalar = True
            self.on = [on]
        else:
            self.size = len(on)
            self.scalar = False
            self.on = on
        self.index = 0
        self.A = np.array([])
        return self
    def initarray(self, value):
        if self.scalar:
            self.A = [value]
            return
        else:
            gets.ArrayWrapper.initarray(self, value)

        if self.axis is not None:
            if isinstance(value, DataX):
                axes = [self.axis]+value.axes
            else:
                axes = [self.axis]
            self.A = DataX(self.A, axes)
        else:
            axes = getattr(self.obj, "axes", None)
            if axes and isinstance(value, DataX):
                axes =  [axes[0]]+value.axes
            if axes:
                self.A = DataX(self.A, axes)

    def end(self):
        if self.scalar:
            return self.A[0]

        #axes = self.obj.getaxes()
        #if axes:
        #    self.A = DataX(self.A, axes)
        return _return_list_array_decorator(self.A,
                                            self.section,
                                            self.freduce)


class ListInstance(DetBaseInstance, gets.ListInstance):
    pass

def aflen(value):
    if isinstance(value, (list,np.ndarray)):
        return len(value)
    return 0

class GetData(gets.Get):

    Instance = None
    newheader = None

    inarray = True
    encaps = None

    vtype = "numerical"

    wrapper = DataXWrapper

    selfwrapper = False # false is the object class

    DataBaseInstance    =  DataInstance
    SwitchBaseInstance  =  SwitchInstance
    ListBaseInstance    =  ListInstance
    axis = None
    _attrs_ = gets.Get._attrs_+["newheader", "axis"]
    def __init__(self, *args, **kwargs
                ):
        newheader = kwargs.pop("newheader", None)
        axis = kwargs.pop('axis', None)

        if newheader is not None:
            self.newheader = newheader
        gets.Get.__init__(self,*args, **kwargs)

        wrapper = self.wrapper
        if isinstance(wrapper, DataXWrapper) and axis is not None:
            self.wrapper = wrapper(axis=axis)


    def switcher(self, fget=None, on=0, **kwargs):
        return self(fget=fget, on=on, **kwargs)

    def lister(self, fget=None, on=True, **kwargs):
        return self(fget=fget, on=on, **kwargs)

    def getter(self, *args, **kwargs):
        return self(*args, **kwargs)

    def aloopargs(self, *args, **kwargs):
        kwargs["flen"] = aflen
        return self.loopargs(*args,**kwargs)

    def key(self, keyname,  **kwargs):
        def fget(obj, *args, **kwarg):
            return obj.key.get(keyname, *args, **kwarg)
        fget.__doc__ = """Return the header key '%s'"""%keyname
        kwargs["fget"] = fget
        return self(**kwargs)

    def data(self, fget="getData", **kwargs):
        return self(fget, **kwargs)


    def pandas(self, fget=None, on=None, columns=None, **kwargs):
        if not "wrapper" in kwargs:
            kwargs["wrapper"] = PandasWrapper(columns)
            kwargs["items"] = True
        return self(fget=fget, on=on, **kwargs)

    def _mutual_dict(self, _dict_):
        if isinstance(self.newheader, basestring):
            def newheader(obj, *args, **kwargs):
                return  getattr(obj, self.newheader)(*args, **kwargs)

        elif self.newheader:
             newheader = self.newheader
        else:
            def newheader(obj, *args, **kwargs):
                return  obj.newheader(*args, **kwargs)
        _dict_["newheader"] = staticmethod(self.newheader)

        if self.name is not None:
            _dict_["name"] = self.name


        if self.encaps:
            _dict_["encaps_class"] = self.encaps


getarray   = GetData()
getlist  = GetData()

getlist.DataInstances = tuple()
getlist.SwitchInstances = tuple()
getlist.ListInstances = tuple()
getlist.wrapper = list








