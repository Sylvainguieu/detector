from ... import config
if config.debug: print "dateEntity.py"
import numpy as np
import os

from ..engine import update_entities, DATA, PRIMARY
from ..engine import Log, getarray, getlist
from ...datax import DataX, dataxconcat,size, size_of_shape, isdatax


def _map( func, iterable):
    """ same as map but return in a datax if iterble is a datax """
    if not hasattr(iterable, "__iter__"):
        return func(iterable)
    output = map(func, iterable)
    if isdatax(iterable):
        return DataX( output, iterable.axes)
    return type(iterable)(output)


class KeysCapabilities(object):
    """ Keyword capabilities of Data
    """
################################
#
# Consern mostly keys function
#
################################

    ####
    # Following methods must be implemented function to the
    # type of io


    def _get_file_path(self):
        raise NotImplementedError()

    def _get_header(self):
        raise NotImplementedError()

    def _set_header(self, header):
        raise NotImplementedError()

    def _new_header(self):
        raise NotImplementedError()

    ################################
    # Derived method

    @getlist
    def getheader(self, copy=False):
        h = self._get_header()
        if copy:
            if not hasattr(h, "copy"):
                raise TypeError("header has no copy method")
            return h.copy()
        return h                    

    @getlist    
    def getheadercopy(self, *args, **kwargs):
        kwargs = dict(*args, **kwargs)        
        header = self.getheader(copy=True)
        for k,v in kwargs.iteritems():
            header[k] = v
        return header

    def setheader(self,header):
        return self._set_header(header)

    def newheader(self, *args, **kwargs):
        getinfo = kwargs.pop("__get__", {})
        return self._new_header(*args, **kwargs)

    @getarray(name="key")
    def key(self, k):
        """ return the key value, raise KeyError if not in header """
      
        v = self.getheader()[k]
        if isinstance(v, tuple):
            v = v[0]
        return v

    @getarray
    def getkey(self, k, default=None):
        """ return the key value or default 

        the default for *default* is None
        """
        try:
            v = self.key(k)
        except KeyError:
            v = default
        return v         
    
    def setkey(self,k, v):
        """
        obj.setkey(key,val)
        Set the key/value pair
        """
        self.getheader()[k] = v

    def haskey(self,k):
        """ Return True is the object has the key k, False otherwise """
        return k in self.getheader()

    @getarray    
    def comment(self, k=None):
        """ return comment on keyword k, if k is None return obj comments if any """

        if k is None:
            return "" # base has no general comment so far
        kc = self.getheader().get(k)
        if isinstance(kc, tuple):
            return kc[1]
        return ""

    @getarray
    def getcomment(self, k, default=""):
        """ return the key comment  or default if key not in header 

        the default for *default* is '' (empty string)
        """
        try:
            v = self.comment(k)
        except KeyError:
            v = default
        return v                 

    @getarray
    def file_path(self):
        return self._get_file_path()

    @getarray
    def file_directory(self):
        """ Return the directory where file has been open """
        path = self.file_path()
        return _map( lambda p:os.path.split(p)[0], path )

    @getarray
    def file_name(self):
        """ Return the file name (without directory) """
        for key in config.FILENAMEKEYS:
            if self.haskey(key):
                fname = self.getkey(key)
                if fname:
                    return fname


        path = self.file_path()
        return _map( lambda p: os.path.split(p)[1], path)

    @getarray
    def directory(self, rootdir=None):
        """ Same has ile_directory but substract a rootdir to the path"""
        dirs = self.file_directory()
        return _map( lambda d: _get_directory(d,rootdir=rootdir), dirs)

    @getlist
    def getattr(self, attr):
        """ obj.getattr("attr") ->  obj.attr """
        return self.__getattribute__(attr)

    @getlist
    def callattr(self, attr, *args, **kwargs):
        """ obj.callattr( "attr", *args, **kwargs) ->  obj.attr(*args, **kwargs) """
        return self.getattr(attr)( *args, **kwargs)


class DataCapabilities(object):
    data_params = None
    #####
    #  Method to be implemented

    def _get_the_raw_data(self):
        raise NotImplementedError()
    def _set_the_raw_data(self):
        raise NotImplementedError()

    @staticmethod
    def _update_data_kwargs(kwargs):
        """ a  method used to update the kwargs for getData call"""
        return

    def _return_array_data_and_scale(self, data, idx, kwargs, axes=None):
        """
        Return the data  but offset it and/or scale it if needed
        """
        noscale  = kwargs.pop("noscale", False)
        nooffset = kwargs.pop("nooffset", False)
        data =  self._return_array_data(data, idx, kwargs, axes=axes)
        if not nooffset:
            data =  self.offsetData(data, idx, **kwargs)
        if not noscale:
            data =  self.scaleData(data, idx, **kwargs)
        return data

    def _return_array_data(self, data, idx, kwargs, axes=None):
        axes = self.axes if (axes is None) and hasattr(self,"axes") else axes if isinstance(data, np.ndarray) else []        
        if axes:
            self._update_data_kwargs(kwargs)
            if len(kwargs): self.say("reducing the data with %s"%kwargs, 3)
            if idx is not None:
                data = DataX(data, axes)[idx]
                if isdatax(data) and len(kwargs):
                    data = data.apply(**kwargs)
            else:    
                data = DataX(data, axes).apply(**kwargs)
        else:        
            if len(kwargs):
                ###
                # Decide or not to put an error here
                self.say( "entity has no axes name, data is not a DataX. *_reduce and *_idx and section kwargs will be ignored ", vtype=config.WARNING)
            if idx is not None:
                data = data[idx]
        return data

    def _parse_data(self, data, axes):
        """ parse the data to be ready """
        if axes is not None:
            self.axes = list(axes)

        if isdatax(data):
            # if it is a datax and self.axes is set, transpose
            # the new data to fit the self.axes
            # if self.axes is not set, take it from the datax
            if self.axes is not None and self.axes != data.axes:
                data = data.transpose(self.axes, add_unknown=True,
                                      drop_missing=True)
            else:
                self.axes = data.axes
        elif isinstance(data, np.ndarray) and self.axes is not None:
            if len(self.axes) != len(data.shape):
                raise ValueError("Expecting a data with %d dimention, got %d"(len(self.axes),len(data.shape)))
        return data


    @getarray
    def getData(self, idx=None, **kwargs):
        """
        obj.getData( **kwargs)

        Return the data inside a DataX object if obj as the axes attribute else in a nparray.
         - Keywords argument are eventually applied to the data before returned.
         - The data is offsetted and/or scale if obj has offset array (or scalar) or
           scale array

        Example:
           if obj contain a data of dimention  N*Ny*Nx and axes ["time", "y", "x"]
        > obj.getData( time_reduce=np.mean)
          will return a Ny*Nx image where the time axis has been averaged
        > obj.getData(  x_idx=slice(10,21), y_idx=slice(30,41) )
          will return a N*10*10 cube
        > obj.getData(  x_idx=slice(10,21), y_idx=slice(30,41), x_reduce=np.mean, y_reduce=np.mean )
          is   obj.getData()[:,10:21,30:41].mean( axis=["x","y"])

        """
        copy = kwargs.pop("copy", False)
        if copy:
            return self._return_array_data_and_scale(self._get_the_raw_data().copy(), idx, kwargs)
        else:
            return self._return_array_data_and_scale(self._get_the_raw_data(), idx, kwargs)

    
    ##
    # getsize, getshape and getaxes are not decorated by getarray 
    # they must be redefined in LIST or SWITCH 
    def getsize(self, axis=None):
        """ Return the data size along the given axis
        if axis is None return the full data size
        axis can be a list of axis.
        """
        return size_of_shape(self.getshape(), self.getaxes(), axis=axis)
    _getsizes = getarray("getsize")    
    
    def getshape(self):
        """
        Return the shape of data has for a nparray
        """
        return self._get_the_raw_data().shape
    _getshapes = getarray("getshape")    
    
    def getaxes(self):
        return list(self.axes)
    _getaxes = getarray("getaxes") # the array version of getaxes 


    def offsetData(self, data, idx, offset=None,**kwargs):
        """
        Substract an offset to the reduced data
        if keyword offset is present use it otherwhise look at
        the hdu.offset attribute.
        If all of them are None data is reutrned as it is silently
        """
        data_params = self._get_data_params()
        if data_params.get("offset",None) is None and offset is None: return data

        self.say( "Substract offset to the data")
        return data + self.getOffset(offset=offset, idx=idx, **kwargs)

    def _get_data_params(self):
        """ return the dictionary containing data params """
        if self.data_params is None:
            self.data_params  = {}
        return self.data_params

    @getarray
    def getOffset(self, offset=None, idx=None, **kwargs):
        """
        Return the offset array or value if any, else return 0.0
        """
        data_params = self._get_data_params()
        offset = data_params.get("offset",None) if offset is None else offset

        if offset is None: return 0.0
        if idx is not None:
            offset = offset[idx]
        if isdatax(offset):
            return offset.apply(**kwargs)
        elif len(kwargs):
            ###
            # Decide or not to put an error here
            if isinstance(offset, np.ndarray) and offset.size:
                self.say( "offset is not a DataX. reduce and\
                section kwargs will be ignored", vtype=config.WARNING)
        return offset
    @getarray
    def getScale(self, scale=None, idx=None, **kwargs):
        """
        Return the scale data or value if any else return 1.0
        """
        data_params = self._get_data_params()
        scale = data_params.get("scale",None) if scale is None else scale
        if scale is None: return 1.0
        if idx is not None:
            scale = scale[idx]
        if isdatax(scale):
            return scale.apply(**kwargs)
        elif len(kwargs):
            if isinstance(scale, np.ndarray) and scale.size:
                self.say( "scale is not a DataX. reduce and\
                section kwargs will be ignored", vtype=config.WARNING)

        return scale

    def scaleData(self, data, idx=None, scale=None, **kwargs):
        """
        Multiply by a scale factor the data
        if keyword scale is present use it otherwhise look at
        the hdu.scale attribute.
        If all of them are None data is reutrned as it is silently
        """
        data_params = self._get_data_params()
        if data_params.get("scale",None) is None and scale is None: return data
        self.say( "Rescaling the data ")
        return data*self.getScale(scale=scale, idx=idx, **kwargs)

    @getarray
    def setOffset(self, offset):
        """ Set the offset to the HDU

        offset must be compatible with the data and can be a DataX object
        """
        data_params = self._get_data_params()
        data_params['offset'] = offset

    @getarray
    def setScale(self, scale):
        """ Set the scale to the HDU

        scale must be compatible with the data and can be a DataX object
        """
        data_params = self._get_data_params()
        data_params['scale'] = scale

    @getarray
    def delOffset(self):
        """ delete curent offset if any """
        data_params = self._get_data_params()
        data_params.pop("offset", None)

    @getarray
    def delScale(self):
        """ delete the curent scale is any """
        data_params = self._get_data_params()
        data_params.pop("scale", None)

class PrimaryEntity(KeysCapabilities, Log):
    _entity_type_ = PRIMARY    
    axes = None    
    def _get_class(self,name):
        raise NotImplementedError()

    def _is(self, id):
        """ from id (string) return True if match the object False otherwhise """
        return self.name.upper() == id.upper()


class DataEntity(KeysCapabilities,
                 DataCapabilities,
                 Log):
    _entity_type_ = DATA
    axes = None
    
    def _get_class(self,name):
        raise NotImplementedError()

    def _is(self, id):
        """ from id (string) return True if match the object False otherwhise """
        return self.name.upper() == id.upper()

    #_a_data = True, "data"
    #_a_key  = False

#DataEntity._add_scalar_data_capability("data")
#DataEntity._add_key_to_data_capability("key")

update_entities(data = DataEntity, 
                primary = PrimaryEntity)







