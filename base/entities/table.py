from .data import DataEntity
from .datalist import ListEntity
from ..engine.shared import get_axis

from ..engine import update_entities, getarray, TABLE

class TableEntity(DataEntity):
    """ This is a data entity where the data is a recarray
    the offset and scale have effect only if the method
    isOffsetable(name) return True
    """

    inarray = staticmethod(ListEntity.inarray)
    isarray = staticmethod(ListEntity.isarray)

    _get_axis = get_axis

    _entity_type_ = TABLE

    def finalise(self, data, inarray=True, inaxis=None):
        if inarray is True:
            return self.inarray(data, axes=self._get_axis(inaxis))
        if isinstance(inarray, basestring):
            return self._get_class(inarray)(data)
        return self.__class__(data)

    @getarray
    def getData(self, idx=None,  **kwargs):
        """ Return the data from a recarray 

        Args:
        -----
            idx (Optional): The table column id if s string, a table row if a int, rows if slice, idx can also 
                combine both :  ("COL1", 2) is the row 2 of column "COL1" 
                                ("COL2", slice(0,10)) ... etc ...  
            **kwargs :  Any other keyword argument applied to the data axes before it is returned.
                Notes that idx takes effect before axes keyword are applied.               

        Returns:
        --------
            - A DataX (array with name axes) if the table row has axes
            -or- a numpy array  (if no axes)
                   
           The data is offsetted and/or scale if obj has offset array (or scalar) or
           scale array and if the method isOffsetable(column_name) return True (default is false)

        """
        copy = kwargs.pop("copy", False)

        
        if hasattr(idx, "__iter__"):
            if len(idx) and isinstance(idx[0], basestring):
                name, idx = idx[0], idx[1:]
            else:
                name, idx = None, idx                                        
        else:
            if isinstance(idx, basestring):
                name, idx = idx, None    
            else:
                name, idx = None, idx

        axes = self.getaxes(name)        
        if name is None:
            if copy:
                return self._return_array_data( self._get_the_raw_data().copy(), idx,  kwargs, axes=axes)
            else:
                return self._return_array_data( self._get_the_raw_data(), idx, kwargs, axes=axes)

        if self.isOffsetable(name):
            if copy: 
                return self._return_array_data_and_scale( self._get_the_raw_data()[name].copy(), idx, kwargs, axes=axes)
            else:
                return self._return_array_data_and_scale( self._get_the_raw_data()[name], idx, kwargs, axes=axes)

        
        if copy:
            return self._return_array_data( self._get_the_raw_data()[name].copy(), idx, kwargs, axes=axes)
        else:
            return self._return_array_data( self._get_the_raw_data()[name], idx, kwargs, axes=axes)

    def getDataInArray(self, columns=None, inaxis="column", **kwargs):
        column = column or []
        lst = [self.getData(name, **kwargs) for name in columns]
        return self.finalise(lst, inaxis=inaxis)

    def getaxes(self, column=None):
        """ Return the axes list according to the column name of the recarry """

        if column is None: return self.axes
        return self.axes

    
    def getshape(self, column=None):
        """ return the data shape, a optional argument is the column of data
        inside the recarray
        """
        if column is None:
            return self._get_the_raw_data().shape

        return self._get_the_raw_data()[column].shape


    def isOffsetable(self,column):
        """
        isOffsetable(column)
        Return True if the data with column NAME inside the recarray is offsetable (and scalable)
        """
        return False

update_entities(table = TableEntity)

