
from ... import config

from ...datax import DataX, dataxconcat, size, size_of_shape, isdatax
from ..engine.shared import get_axis, inarray, isarray
from ..engine import Log, getarray, getlist
from ..engine.class_handler import subclass_list

from ..engine import update_entities, LIST


import numpy as np

class KeyListCapability(object):
    pass

class DataListCapability(object):
    inarray = staticmethod(inarray)
    isarray = staticmethod(isarray)
    _get_axis = get_axis

    def getshape( self, *idx):
        if not len(self): return tuple()
        dshape = self[0].getshape(*idx)

        #if (dshape != shapes).sum():
        #    raise Exception("Seems that individual data have not the same shape")
        return tuple( [len(self)]+list(dshape) )

    def getaxes( self):
        if hasattr(self, "data_axes"): return self.data_axes
        if not len(self): return self.axes
        return self.axes+self[0].getaxes()

    def getsize(self):
        return self._getsizes().sum()

    def order(self, method, *args, **kwargs):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        values = method(*args, **kwargs)
        indexes = np.argsort(values)
        return self.__class__ (  [self[i] for i in indexes] )

    def filter(self, method):
        if issubclass( type(method), str):
            if not method in self.__dict__:
                raise ValueError("Method %s does not exist in this class"%method)
            method = getattr(self, method)
        return self.__class__ ( [fts for fts in self if method(fts)] )

    def newheader(self, *args, **kwargs):
        return self._new_header(*args, **kwargs)


    getheader    = getlist.lister("getheader")

    getData      = getarray.lister("getData")

    setOffset    = getlist.lister("setOffset")
    delOffset    = getlist.lister("delOffset")

    setScale     = getlist.lister("setScale")
    delScale     = getlist.lister("delScale")


class ListEntity(KeyListCapability, DataListCapability, Log):    
    _entity_type_ = LIST
    @classmethod
    def fromlist(cl, lst):
        lst = cl(lst)
        subclasses = getattr(lst, "subclasses",  None)
        if subclasses:
            subclass_list(lst, subclasses, io=lst._get_io_(), instrument=lst._get_instrument_())
        return lst     

update_entities(list = ListEntity)

