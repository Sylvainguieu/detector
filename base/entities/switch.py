
from ..engine import update_entities, DATA, SWITCH, LIST, TABLE
from ...datax import DataX, dataxconcat, size, size_of_shape, isdatax

from ..engine import Log, getarray, getlist
from ..engine.class_handler import (
                                    parse_class, subclass_list
                                    )
from ... import config


class KeySwitchCapability(object):
    pass


class DataSwitchCapability(object):
    def getshape(self,*idx):
        """
        Return the DataShape -> obj.getData().shape
        """
        if not len(self): return tuple()
        return self[self._on_data_default_].getshape(*idx) #idx for recarray

    def getsize(self, *idx, **kwargs):
        """
        Return the DataSize (size of array returned by getData)

        Args:
            column (Optional) : if 
        """
        if len(idx)>1:
            if "axis" in kwargs:
                raise KeyError("duplicate value for keyword axis")
            kwargs["axis"] = idx[1]
            idx.pop(1)

        axis = kwargs.pop("axis", None)
        if len(kwargs):
            raise KeyError("getsize accept only one keyword argument axis=")
        # idx is for recarrays, will return an error if not
        return size_of_shape( self.getshape(*idx), self.getaxes(), axis=axis)

    def getaxes(self):
        if hasattr(self, "data_axes"): return self.data_axes
        if not len(self): return []
        if hasattr(self, "_on_data_default_") and self._on_data_default_ is not None:
            return self[self._on_data_default_].getaxes()
        return self.axes+self[0].getaxes()

    def newheader(self, *args, **kwargs):
        return self._new_header(*args, **kwargs)


    getattr       = getlist("getattr"  , on=True)
    callattr      = getlist("callattr" , on=True)
    getheader     = getlist("getheader", on=True)



class SwitchEntity(DataSwitchCapability, KeySwitchCapability, Log):
    _entity_type_ = SWITCH
    ##
    # empty sublcasses by default all subclass will be data
    subclasses = []
    _on_data_default_ = 0

    def _subclass_list(self, io=None, instrument=None):
        """ a Switch object can have rules to reclass the childs
        This function is aimed to reclass the init list according
        to the subclasses attribute
        """
        if not hasattr(self, "subclasses"): return
        return subclass_list(self, self.subclasses, io=io, instrument=instrument)        
 
update_entities(switch = SwitchEntity)

