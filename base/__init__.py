from .entities import DataEntity, TableEntity, SwitchEntity, ListEntity, PrimaryEntity


from .ios import DataIO, TableIO, SwitchIO, ListIO, PrimaryIO
from .instruments import DataInstrument, PrimaryInstrument, TableInstrument, SwitchInstrument, ListInstrument
from .engine.mutual import DataMutual, TableMutual, SwitchMutual, ListMutual, PrimaryMutual

from .engine import (update_io, update_instrument_space,
                     update_entities, get_entity_class, 
                     getarray, getlist,
                     DATA, SWITCH, LIST, TABLE, PRIMARY)
from .engine import (get_class as _get_class,
                     get_io_class as _get_io_class,
                     get_instrument_subclasses as _get_instrument_subclasses, 
                     get_mutual_subclasses
                     )



def get_class(entity_name, instrument="base", io="base"):
    """ return a dynamic detector class definition 
              
    Args:
    -----
        entity_name (string, or tuple): is the class entity_name or tuple of name
        instrument (Optional[string]) : is the instrument string definition, default is "base"
        io (Opional[string]) : the input/output string definition (e.g. "fits") default is "base"
    
    Returns:
    --------
        A dynamicaly created class for its entity/instrument/io            

    """
    return _get_class(entity_name, instrument=instrument, io=io)

type_lookup = {
    "entity": {
        DATA: DataEntity,
        TABLE: TableEntity,
        PRIMARY: PrimaryEntity,
        SWITCH: SwitchEntity,
        LIST: ListEntity
    }, 
    "instrument": {
        DATA: DataInstrument,
        TABLE: TableInstrument,
        PRIMARY: PrimaryInstrument,
        SWITCH: SwitchInstrument,
        LIST: ListInstrument        
    }, 
    "io": {
        DATA: DataIO,
        TABLE: TableIO,
        PRIMARY: PrimaryIO,
        SWITCH: SwitchIO,
        LIST: ListIO        
    },
    "mutual": {
        DATA: DataMutual,
        TABLE: TableMutual,
        PRIMARY: PrimaryMutual,
        SWITCH: SwitchMutual,
        LIST: ListMutual                
    }        
} 


def get_type(TYPE, kind="entity"):
    """ Return the built-in default class for its TYPE and kind

    Args:
    -----
        TYPE (integer) : The class type intger, one of DATA, TABLE, PRIMARY, SWITCH, LIST
        kind (Optiona[string]) : the kind of class, one of "entity", "instrumnet", "io", "mutual"
    """
    if not kind in type_lookup:
        raise ValueError("kind should be one of 'entity', 'instrument', 'io' or 'mutual' got %s"%kind)
    try:
        cl = type_lookup[kind][TYPE]
    except KeyError:
        raise ValueError("class type %s is not recognized "%TYPE)    
    return cl    

def get_entity_type(TYPE):
    """ return a root entity class for a given TYPE

     Args:
    -----
        TYPE (integer) : The class type intger, one of DATA, TABLE, PRIMARY, SWITCH, LIST
    """
    return get_type(TYPE, "entity")


def get_instrument_type(TYPE):
    """ return a root instrument class for a given TYPE

     Args:
    -----
        TYPE (integer) : The class type intger, one of DATA, TABLE, PRIMARY, SWITCH, LIST
    """
    return get_type(TYPE, "instrument")

def get_io_type(TYPE):
    """ return a root io class for a given TYPE

     Args:
    -----
        TYPE (integer) : The class type intger, one of DATA, TABLE, PRIMARY, SWITCH, LIST
    """
    return get_type(TYPE, "io")

def get_mutual_type(TYPE):
    """ return a root mutual class for a given TYPE

     Args:
    -----
        TYPE (integer) : The class type intger, one of DATA, TABLE, PRIMARY, SWITCH, LIST
    """
    return get_type(TYPE, "mutual")

def get_io_class(TYPE, io="base"):
    return _get_io_class(io, TYPE)

def get_instrument_subclasses(TYPE, instrument="base"):
    return _get_instrument_subclasses(instrument, TYPE)

