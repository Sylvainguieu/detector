from .data import DataInstrument
from .switch import SwitchInstrument
from .datalist import ListInstrument
from .table import TableInstrument
from .primary import PrimaryInstrument

from ..engine import DATA, SWITCH, LIST, TABLE, PRIMARY
from ..engine import update_instrument_space
from ..engine import get_class as _get_class


def get_instrument_subclasses(entity_case, **kwargs):
    
    lookup = {DATA:DataInstrument, 
              PRIMARY:PrimaryInstrument, 
              TABLE:TableInstrument,
              SWITCH:SwitchInstrument,
              LIST:ListInstrument}
              
    cls = []
    for case,cl in lookup.iteritems():
        if entity_case & case:
            cls.append(cl)
    return tuple(cls)

def get_class(entity_name, io="base", instrument="base", **kwargs):
    return _get_class(entity_name, io=handler, instrument=instrument, **kwargs)

update_instrument_space("base", get_instrument_subclasses, [])
