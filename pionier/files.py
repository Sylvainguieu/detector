from ..io.fits.opener import ImgListOpeneur
try:
    from .config import localrootdir
except:
    localrootdir = "./"

from detector.io import fits
from detector.base import get_class as _get_class


def get_class(entity_name, io="fits", instrument="pionier", **kwargs):
    return _get_class(entity_name, io=io, instrument=instrument, **kwargs)


def auto_wrapper(lst):
    if not len(lst):
        return get_class("list")(lst)
    entity = getattr( lst[0], "_entity_name_", "")
    if entity=="combined" or "cubeswitch":
        imgs = get_class("images")(lst)
        if hasattr(imgs, "get_polar") and len(imgs.get_polar.set())>1:
            imgs = get_class("carac")(imgs)
        return imgs
    return get_class("list")(lst)


def auto_open(filename, opener=fits.pf.open):
    """
    object = auto_open(default_openeur(file_name))
    Automatlycaly assign the right obbject for a given pyfits hdulist
    """
    fh =  opener(filename)

    if len(fh)>=4 and (fh[1].name == "M2" or fh[1].name=="SIGMA2"):
        return get_class("combined")(fh)

    if len(fh)==1 and fh[0].header['NAXIS'] >2:
        return get_class("cubeswitch")(fh)
    if "IMAGING_DATA" in  [hdu.name for hdu in fh]:
        return get_class("fringes")(fh)
    return get_class("switch")(fh)



class PionierImgListOpeneur(ImgListOpeneur):
    combined_file_name = "all_combined.fits"
    opener = staticmethod(auto_open)
    wrapper = staticmethod(auto_wrapper)
    subdir = ""

    def biases( self,  **kwargs):
        return self.open(  kwargs.pop("path","PIONIER*BIAS*combined.fits"), subdir, **kwargs)
    def flats( self,  **kwargs):
        return self.open( kwargs.pop("path","PIONIER*FLAT*combined.fits"), subdir, **kwargs)
    def darks( self,  **kwargs):
        return self.open( kwargs.pop("path","PIONIER*DARK*combined.fits"), subdir, **kwargs)
    def cubes(self,  **kwargs):
        return self.open(  kwargs.pop("path","PIONIER*FLAT*cube.fits") , subdir, **kwargs)

local = PionierImgListOpeneur(
                            rootdir = localrootdir
                         )



dmz = local.new(
    distant  = True,
    distdir  = "/data/pionier/",
    host     = "dmz98.obs.ujf-grenoble.fr",
    user     = "guieus",
    password = "Cham1308"
)

# opener for ftp conection on beti DETDATA
betidetdata = local.new(
    distant  = True,
    distdir  = "/insroot/PIONIER/SYSTEM/DETDATA/",
    host     = "wbeti.obs.ujf-grenoble.fr",
    user     = "pnr",
    password = "Bnice2me",
)

# opener for ftp conection on beti /data/PIONIER
betidata = betidetdata.new(
    distdir  ="/data/PIONIER/",
)

