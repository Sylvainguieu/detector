
from detector import *

from detector.io.hd5 import read_hd5, write_hd5
from detector.vlt import (get_instrument_subclasses as _get_instrument_subclasses, 
                          get_class as _get_class
                          )

from .instruments import (PionierDataInstrument, PionierPrimaryInstrument,
                          PionierSwitchInstrument, PionierListInstrument, 
                          PionierTableInstrument  
                         ) 

from .mappings import mapABCD, mapABCD_pol
from .slicers  import rapid, picnic, ioc, vioc

from .files import local, dmz, betidata, betidetdata

grism   = ioc("grism")
free    = ioc("free")
griwoll = ioc("gri+wol")
woll =  ioc("woll")

def get_instrument_subclasses(TYPE, instrument="pionier"):
    if instrument!="pionier":   
        return _get_instrument_subclasses(TYPE, instrument=instrument)
    lookup = {
              DATA: PionierDataInstrument, 
              PRIMARY: PionierPrimaryInstrument, 
              TABLE: PionierTableInstrument,
              SWITCH: PionierSwitchInstrument,
              LIST: PionierListInstrument
              }
              
    cls = []
    for case,cl in lookup.iteritems():
        if TYPE & case:
            cls.append(cl)
    return tuple(cls)  
    
update_instrument_space("pionier", get_instrument_subclasses, ["vlt"])

def get_class(entity_name, io="fits", instrument="pionier", **kwargs):
    if instrument is None:
        instrument = "pionier"
    if io is None:
        io = "fits"                
    return _get_class(entity_name, io=io, instrument=instrument, **kwargs)

def write_hd5_carac(lst, file_name):
    write_hd5(lst, file_name, entity="carac",
              instrument="pionier", subentity="combined")




Image = get_class("image")
Combined = get_class("combined")
Images = get_class("images")
Carac = get_class("carac")
Cube = get_class("cube")



