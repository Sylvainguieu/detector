from .mappings import mapABCD, mapABCD_pol
from ..slicers import OutputSlicer, IOCSlicer
try:
    from . config import ioc_type
except:
    ioc_type = "ABCD"


###
# picnic detector slicer only one output
picnic = OutputSlicer(imgdim=(128,128), noutput=(1,1) )
###
# the RAPID detector
rapid  = OutputSlicer(imgdim=(255,320), noutput=(1,8) )


####
# ABCD Integrated optic Optical componant with the RAPID configuration
# 24 outputs separated by 8 pixels
free  = IOCSlicer(  pos=(58,248), outputs=24,
                    dispersion=1,
                    wollastons=1 , doutputs=8,
                    imgdim=(255,320),
                    ref = "bl",
                    mapping=mapABCD
)


###
# position of the dark windows
freeDark  = IOCSlicer(pos=(21,179),
                      outputs=2,
                      dispersion=1,
                      wollastons=1 ,
                      doutputs=160,
                      disp_ref=[0,0],
                      imgdim=(255,320),
                      ref = "bl"
                      )

grismDark = freeDark.new(dispersion=6)


####
# 6 canals dispersions
grism = free.new(dispersion=6,  disp_ref=[0,0])

woll  = free.new(dispersion=1, wollastons=2, dwollastons=[8,0],
                 woll_ref = [0,-5],
                 disp_ref = [0,0],
                 mapping=mapABCD_pol)

griwoll = woll.new( dispersion=6, disp_ref=[0,0],
                    wollastons=2, dwollastons=[-4,10],
                    woll_ref=[+2,-5]
                )

####
# AC componant
freeAC  = free.new( pos=(free.pos[0]+(8*5), free.pos[1]), outputs=12)
grismAC = freeAC.new(  dispersion=7, disp_ref=[0,0])

wollAC  = freeAC.new( dispersion=1 ,wollastons=2 , dwollastons=[10,0], woll_ref=[0,-5])

griwollAC = wollAC.new(
                       dispersion=6, disp_ref=[0,0],
                       wollastons=2, dwollastons=[-4,10],
                       woll_ref=[+2,-5]
                       )




######
# virual integrated optic where all the windows are separated by one pixel
# use when windows are Collapsed in a cube
#
# Windows are in the order from 0 to 47 with 0->23 one Pdown polar 24->47 Pup polar
#
freeVirtual  = IOCSlicer(pos=(0,0), outputs=24,
                         dispersion=1,
                         wollastons=1 , doutputs=1,
                         imgdim=(24,1),
                         ref="bl",
                         mapping=mapABCD
)
grismVirtual = freeVirtual.new( dispersion=6,  disp_ref=[0,0])
wollVirtual = freeVirtual.new(dispersion=1 , wollastons=2, dwollastons=[24,0],
                              woll_ref=[0,0],
                              mapping=mapABCD_pol
                              )

griwollVirtual = wollVirtual.new(dispersion=6, disp_ref=[3,0],
                                 wollastons=2, dwollastons=[24,0],
                                 woll_ref=[0,0]
                                )

####
#  define the ioc type and instrument configuration in upper case
ioc_dict  = {"ABCD":{"FREE":free, "WOLL":woll, "GRI+WOL":griwoll, "GRISM":grism},
             "AC":{"FREE":freeAC, "WOLL":wollAC,"GRI+WOL":griwollAC,"GRISM":grismAC},
             "DARK":{"FREE":freeDark, "GRISM":grismDark, "WOLL":freeDark, "GRI+WOL":grismDark},
             "VIRTUAL":{"FREE":freeVirtual, "WOLL":wollVirtual,
                        "GRI+WOL":griwollVirtual, "GRISM":grismVirtual}
            }

def ioc(name, ioctype=ioc_type):
    """ ioc(name, *args, **kwargs)
    return the instrument integrated optic componant image slicer of the ioc
    of name NAME and type ioctype (default abcd)
    e.g:
      > ioc("grism")(34,120)
      return the indexes (y,x) of the pixels if the reference pixel of
      the componant is (34,120) in y,x coordinates
      > ioc("grism")(base=4, disp=2)
      return the pixel indexes of base 4 and dispersion chanel number 2
      (for the default position)

    """
    return ioc_dict[ioctype.upper()][name.upper()]

def vioc(name, ioctype="VIRTUAL"):
    return ioc(name, ioctype)
