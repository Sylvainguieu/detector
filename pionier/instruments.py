from detector.vlt import get_instrument_type as git
from detector.vlt import get_entity_class as gec


#(VltDataInstrument, VltTableInstrument, VltPrimaryInstrument, 
# VltSwitchInstrument, VltListInstrument)	
from .. import config 
from detector import getarray, DATA, TABLE, PRIMARY, SWITCH, LIST, update_entities
import re


class Fringes(gec("fringes")):
    toto = "yahoooooo"
update_entities("pionier", fringes=Fringes)


class _AllPionier(object):
    """ Shared methods for all types """
    pass
    
class PionierDataInstrument(_AllPionier, git(DATA)):
    """ basic methods related to Pionier instrument """
    pass


class PionierPrimaryInstrument(_AllPionier, git(PRIMARY)):
    """ basic methods related to Pionier instrument """

    dit = getarray("get_dit", name="dit")    
    ndit = getarray("get_ndit", name="ndit")
    polar = getarray("get_polar", name="polar")

    @getarray(name="polar")
    def get_polar(self):
        """ return [HIERRARH ESO DET POLAR] if found else [POLAR]
        else try to gess with the file name (for old BETI files)
        """
        k = "ESO DET POLAR"
        if self.haskey(k):
            return self.key(k)
        if self.haskey("POLAR"):
            return self.key("POLAR")
        # if no polar is found try to gess it with file name
        # (this is for old files on BETI)
        fname = self.file_name()

        tc = re.compile( "P([0-9][0-9][0-9][0-9])[^0-9]" )
        f = tc.findall( fname)
        if not len (f):
            raise Exception("cannot find polar information in file name")
        return int(f[0])

    illumination = getarray("get_illumination", name="illumination")

    @getarray(name="illumination")
    def get_illumination(self):
        """ If any return the illumination value """
        k = "ILLUMIN"
        if self.haskey(k):
            return self.key(k)
        ###
        # look into the file name to find information
        s = re.search(".*F([-0-9]+)", self.file_name())
        if s:
            return int(s.groups()[0])
        #PIONIER_FLAT_F5_069_0009.fits
        return 0.0


    get_cycletime = getarray.key("ESO DET READOUT CYCLETIME")

    @getarray(name="Frate")
    def get_framerate(self):
        return 1./self.get_cycletime()
    
    if hasattr(getarray, "pandas"):
        get_config= getarray.pandas("_get_config", columns=["DIT","POLAR","ILLUM"],
                                   fkey="get_dateobs")
    else:
        get_config= getlist("_get_config")
    def _get_config(self):
        """ return a list containing pairs of key/value for instrument config """
        return (self.get_dit(),
                self.get_polar(),
                self.get_illumination()
                )

    get_disp = getarray.key("ESO INS OPTI2 NAME")

    @getarray
    def get_shutters(self):
        """ return the shuter sequence of PIONIER """
        pass


class PionierSwitchInstrument(_AllPionier, git(SWITCH)):
    """ basic methods related to Pionier instrument """
    pass

class PionierListInstrument(_AllPionier, git(LIST)):
    """basic methods related to the pionier instrument """    
    def keytable(self, *keys, **kwargs):
        fkey  = kwargs.pop("fkey", "get_dateobs")

        namedkeys = [ tuple(k.strip() for k in key.split("=",1)) \
                          if "=" in key \
                          else (key,key) for key in keys]

        def fcall(obj, **kwargs):
            return tuple(getattr(obj,key[1:])(**kwargs) if key[0] == "." else \
                           obj.getkey(key, **kwargs) for name, key in namedkeys)
        gm = getarray.pandas(fcall, columns=zip(*namedkeys)[0], fkey=fkey, on=True)
        return gm.__get__(self)(**kwargs)


class PionierTableInstrument(_AllPionier, git(TABLE)):
    pass




