import os
from detector.config import mainkeys

####
# rootdit for pionier data
host = os.getenv("HOST")
localrootdir = "/Users/guieu/DETDATA"
machines = { "wbeti":"/insroot/SYSTEM/DETDATA/",
             "wpnr" :"/insroot/SYSTEM/DETDATA/",
             "wgoff":"/data-vlti/raw/",
             "dmz98":"/data/pionier/"
        }
if host in machines:
    localrootdir =  machines[host]


#####
# default componant type   ABCD, AC
ioc_type = "ABCD"
