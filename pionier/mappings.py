from ..misc.mapping import newMapping
from numpy import pi


# bellow is the copy of pndrScope.i
# mapABCD_H.pol = "Pnat";
#
# mapABCD_H.win  = indgen(24);
# mapABCD_H.t1   = [1,1,1,1,2,2,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3];
# mapABCD_H.t2   = [2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4];
# mapABCD_H.base = [1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,2,2,6,6,6,6];
# mapABCD_H.vis  = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
#
# mapABCD_H.phi= [0,      1,   1.6,   0.6,
#                 0,      1,
#                 0,      1,   1.6,   0.6,
#                 0,      1,  1.83,   0.83,
#                 0,      1,   1.6,   0.6,
#                              1.6,   0.6,
#                 0,      1,   1.6,   0.6] *(pi);


mapABCD = newMapping( [1,1,1,1,2,2,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3],
                      [2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4],
                      [1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,2,2,6,6,6,6],
                      [t*pi for t in [0, 1,   1.6,   0.6,
                                         0, 1,
                                         0, 1,   1.6,   0.6,
                                         0, 1,  1.83,   0.83,
                                         0, 1,   1.6,   0.6,
                                         1.6,   0.6 ,
                                         0,      1,   1.6,   0.6]],
                       ["A", "C", "D", "B",
                        "A", "C",
                        "A", "C", "D", "B",
                        "A", "C", "D", "B",
                        "A", "C", "D", "B",
                        "D", "B",
                        "A", "C", "D", "B"],
                      description="""In tegrated optic componant of
beti and pionier. This is an ABCD
                      """
                  )

mapABCD_pol =  newMapping( mapABCD.t1+mapABCD.t1,
                           mapABCD.t2+mapABCD.t2,
                           mapABCD.base+[b+6 for b in mapABCD.base],
                           mapABCD.phi+mapABCD.phi,
                           mapABCD.phin+mapABCD.phin,
                           ["Pdown"]*24+["Pup"]*24,
                           description=mapABCD.description
 )
