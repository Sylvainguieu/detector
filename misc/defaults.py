"""
created by S.Guieu

dictionary with hierarchical structure.
The child dictionary contains key/value pair of itself and
of the parent dictionary.
"""

import UserDict
class DefaulIterator(object):
    def __init__(self,d):
        self.obj = d
        self.i = -1
    def next(self):
        self.i += 1
        if self.i>=len(self.obj):
            raise StopIteration
        return self.obj.allkeys()[i]

class Default(UserDict.UserDict, object):
    #__metaclass__ = ABCMeta
    parent = None # parent will be set at init

    def items(self):
        """
        D.keys() -> list of ALL D's (key, value) pairs, as 2-tuples
        """
        return self.allitems()

    def iteritems(self):
        return self.alliteritems()

    def keys(self):
        return self.allkeys()

    def __iter__(self):
        return DefaulIterator(self)
        #return iter(self.allkeys())

    def __len__(self):
        return super(Default,self).__len__()+len(self.parent)
        #return len(self.allkeys())

    def __contains__(self, item):
        return super(Default,self).__contains__(item) or item in self.parent

    def has_key(self, key):
        return key in self

    #@classmethod
    #def __subclasshook__(cls, c):
    #    if issubclass(c, dict):
    #        return True
    #    return NotImplemented

    def __init__(self, *args, **kwargs):
        """
        Default() create a dict like derived object which suport hierarchicaly
        structured data.
        for instance:
           > parent = Default( color="red", linestyle="-")
           > child = parent.child( marker="+")
           > child.keys()
           ['marker']
           > child.allkeys()
           ['marker', 'color', 'linestyle']
           > child['color']
           "red"
           > parent.color = "blue"
           child['color']
           "blue"
           > child['color'] = "yellow"
           > child['color']
           yellow
           > parent['color']
           blue
           > child.parent is parent
           True

        Default() works as dict() except that it accepts a second argument, which is
        the parent object. e.g.:
           > child = Default( {"linestyle":"-"}, parent, color="red")
         Is equivalent to :
           > child = parent.child( linestyle="-", color="red")


        Methods  are the same than dict plus

        parse( kwargs) : fill up kwargs dictionary with all defaults keyword/values pairs
                           find in d and not in kwargs same as kwargs.update(d)

        set({"k0":vo, ...},  k1=v1, k2=v2, ...) : same as update, set a list of keywords but
                                                    return the object itself.

        child( somedict, k1=v1, k2=v2 , ...) : create a child object with new values set
                                                 in the child object
        derive is synonym to d.child
        allkeys(), d.keys      : return all the keys found in the hierarchical structure tree
        allitems(), d.allitems : return all (kwd,value) pair found in the hierarchical structure tree
        allvalues(), d.allvalues: return all values found in the hierarchical structure tree
        alliteritem(), d.iteritems : return an iterator on all the value  found in the tree
        all(), dict(d) :  return a dictionary containing all items found in the tree

        """
        if len(args) == 2:
            args = list(args)
            parent = args.pop()
            if not issubclass(type(parent), dict) and  not issubclass(type(parent), UserDict.UserDict):
                raise ValueError("Expecting a subclass of dictionary for second argument")
            self.parent = parent
        elif len(args)>2:
            raise ValueError("expecting none one or two arguments, got %d"%(len(args)))
        else:
            self.parent = {}
        #dict.__init__(self, *args, **kwargs)

        #UserDict.UserDict.__init__(self, *args, **kwargs)
        super(Default, self).__init__(*args, **kwargs)

    def parse( self, kwargs):
        """
        Parse method of a Default object
        usage:
         d.parse( kwargs )

        Where kwargs is a dictionary like object. All the keyword/value pairs found in the
        default structure hierarchy will be set in the kwargs dictionary if not present in the
        former.
        It is equivalent to do  kwargs.update(d)  except that parse return the kwargs

        example:
        ------------
        from defaults import Default
        myplotdefault = Default( linestyle="--", color="red")
        def myplot(*args, **kwargs):
            myplotdefault.parse( kwargs)
            return plot(*args, **kwargs)

        -------------
        In the former example the keywork color will be "red" unless redefined in the myplot call
          e.i myplot( color="blue")
        """
        for k,v in self.all().iteritems():
            kwargs.setdefault(k, v)
        return kwargs

    def set(self, *args, **kwargs):
        """
        set method of a Default object
        usages :
           d.set( {'k1':v1, 'k2':v2, ..., 'kn':vn } )
           d.set( k1=v1, k2=v2, ..., kn=vn )
         -or- a mixt of two:
           d.set( {'k1':v1, 'k2':v2, ..., 'kn':vn } , kk1=vv1, kk2=vv2, ..., kkn=vvn )

        This is equivalent to the dict update method except that it will return the object itself
        """
        self.update(*args, **kwargs)
        return self

    def change(self, *args, **kwargs):
        """
        D.change( {'k1':v1, 'k2':v2, ..., 'kn':vn } , kk1=vv1, kk2=vv2, ..., kkn=vvn )
        Change the keyords values where they have been found.
        If the key has not been found it raise a KeyError, use change_or_set if you want
        to set keyword if not found.

        example:
          > d = Default(color="red")
          > dc = d.child()
          > dc.set(color="blue")
          > dc['color']
          "blue"
          > d['color']
          "red"      ## The parent value is not modified

          > dc2 = d.child()
          > dc2.change( color="blue")
          > d['color']
          "blue"     ## The parent default has been modified

        See also change_or_set
        """
        if len(args)>1:
            raise ValueError("change take one or non positional argument")
        if len(args)==1:
            kwargs.update(args[0])
        for k,v in kwargs.iteritems():
            self.changeone(k,v)

    def change_or_set(self, *args, **kwargs):
        """
        Same has change, but create the key/value pair in case the key is not found
        """
        if len(args)>1:
            raise ValueError("change take one or non positional argument")
        if len(args)==1:
            kwargs.update(args[0])
        for k,v in kwargs.iteritems():
            self.changeone_or_set(k,v)

    def changeone_or_set(self, key, newvalue):
        try:
            return self.changeone(key, newvalue)
        except KeyError:
            self[key] = newvalue
            return newvalue

    def changeone(self, key, newvalue):
        if key in self.this():
            oldvalue = self[key]
            self[key] = newvalue
            return oldvalue
        if hasattr(self.parent, "changeone"):
            return self.parent.changeone(key, newvalue)
        else:
            if key in self.parent:
                oldvalue = self.parent[key]
                self.parent[key] = newvalue
                return oldvalue
        raise KeyError("Cannot find key '%s'"%(key))

    def derive(self, *args,  **kwargs):
        """
        derive method is a synonym of child method
        """
        return self.child( *args, **kwargs)

    def child(self, *args, **kwargs):
        """
        child method of a Default object
        usages:
           parent.chil( )
           parent.child( {'k1':v1, 'k2':v2, ..., 'kn':vn } )
           parent.child( k1=v1, k2=v2, ..., kn=vn )
         -or- a mixt of two:
           parent.child( {'k1':v1, 'k2':v2, ..., 'kn':vn } , kk1=vv1, kk2=vv2, ..., kkn=vvn )

        child will create a child object of parent.
        ----------------
        example:
            from default import Default
            stars = Default( color="black", marker="+")
            massivestars = stars.child( color="blue")
            lowmassstars = stars.child( color="red", merker="*")

            plot( x1, y1, **massivestars.a )
            plot( x1, y1, **lowmasstars.a  )
        ----------------

        """
        if len(args)>1:
            raise ValueError("child accept none or one argument")
        elif len(args)==1:
            d = args[0]
            if not issubclass( type(d), dict):
                raise TypeError("expecting a dict as first argument got %s"%type(d))
            d.update(kwargs)
        else:
            d = kwargs
        return self.__class__(d, self)

    def copy(self, *args, **kwargs):
        if len(args)>1:
            raise ValueError("child accept none or one argument")
        elif len(args)==1:
            d = args[0]
            if not issubclass( type(d), dict):
                raise TypeError("expecting a dict as first argument got %s"%type(d))
            d.update(kwargs)
        else:
            d = kwargs
        cp = dict(self) # this create a copy of self
        cp.update(d)
        return self.__class__(cp , self.parent)

    def get(self, key, default=None):
        #if super(Default, self).__contains__(key):
        if not key in self:
            try:
                return self.parent.get(key, default)
            except KeyError,e:
                raise KeyError(e)

        return super(Default,self).get(key, default)


    def __getitem__(self, key):
        #if super(Default, self).__contains__(key):
        if not super(Default,self).__contains__(key):
            # key in self
            try:
                return self.parent[key]
            except KeyError,e:
                raise KeyError(e)
        return super(Default,self).__getitem__(key)

    def __str__(self):
        #sr = "Default("+super(Default, self).__str__()
        sr = "Default("+self.this().__str__()
        sr += ","+self.parent.__str__()+")"

        return sr

    def __repr__(self):
        return self.__str__()
        sr = "Default("+super(Default,self).__str__()
        sr += ","+self.parent.__str__()+")"
        return sr

    def this(self):
        return self.data

    def thiskeys(self):
        return super(Default, self).keys()
    def thisitems(self):
        return super(Default, self).items()
    def thisiteritems(self):
        return super(Default, self).iteritems()
    def thisvalues(self):
        return super(Default, self).values()

    def allkeys(self):
        allkeys = super(Default, self).keys()
        if hasattr( self.parent, "allkeys"):
            allkeys += self.parent.allkeys()
        else:
            allkeys += self.parent.keys()
        return allkeys
    def all(self):
        return  {k:self[k] for k in self.allkeys()}
    def allitems(self):
        return [(k,self[k]) for k in self.allkeys()]
    def alliteritems(self):
        # A bit stupid since it is longer to execute then allitems
        return self.all().iteritems()
    def allvalues(self):
        return [self[k] for k in self.allkeys()]
