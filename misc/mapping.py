
"""
Classes to define a integrating optic componant mapping
"""
try:
    from .. import config
    WIN = config.WIN
except:
    WIN = "win"

import numpy as np
from .. import datax

################################################################################
#
#   Class for Mapping
#
################################################################################
def deprecated(msg):
    print "Deprecated: %s"%msg


def newMapping(t1, t2, base, phi, phin=None, polar="Pnat", win=None,
               id=None, description=""):
    """
    Return a Mapping list
    input arguments are
    t1 : telescope 1 number  (dim N)
    t2 : telescope 2 number  (dim N)
    base : base number       (dim N)
    phi : output phases      (dim N)
    phin : output phases name (dim N) e.g. ["A","B","C", ...]

    optional args:
    polar :  string defininf the polar state 'Pnat' (default) 'Pup' 'Pup' (dim N or scalar)
    win : window numbers default is range(N)
    id  : is of the integrated optic componant
    desciption : string description of the integrated optic
    """
    N = len(t1)
    if win is None:
        win = range(N)

    if phin is None:
        phin =[""]*N

    if not isinstance( polar, (list,np.ndarray)):
        polar = [polar]*N

    mapping =  Mappings([Mapping(t1[i], t2[i], base[i], phi[i],\
                         polar[i], win[i], phin[i]) for i in range(N)])
    mapping.id = id
    mapping.description = ""
    return mapping

class Mappings(list):
    """
    Mappings( [Mapping(..), Mapping(..), etc...])
    list of mapping for integrated optic componant
    """
    #def __init__(self, lst):
    # This is a list of mapping
    #   super(Mappings,self).__init__(lst)
    id = None
    def __call__(self, tel=None, base=None, polar=None, win=None, phin=None):
        if win is None:
            if tel   is not None: self =self.with_tel(tel)
            if not len(self):
                raise ValueError("Nothing with tel %s"%tel)
            if base  is not None: self =self.with_base(base)
            if not len(self):
                raise ValueError("Nothing with tel %s, base %s"%(tel,base))
            if polar is not None: self =self.with_polar(polar)
            if not len(self):
                raise ValueError("Nothing with tel %s, base %s polar %s"%(tel,base, polar))
            if phin is not None: self =self.with_phin(phin)
            if not len(self):
                raise ValueError("Nothing with tel %s, base %s, polar %s, phin %s"%(tel,base, polar, phin))
        elif win is not None:
            if (tel is not None) or (polar is not None) or (base is not None):
                raise ValueError( "if win= keyword is not None, tel,base,polar keywords should be None")
            self = self.with_wins(win)
        return self.get_idx()

    def __getitem__(self, item):
        r = super(Mappings, self).__getitem__(item)
        if isinstance(r, list):
            return Mappings(r)
        return r 
    def __getslice__(self, i,j):
        r = super(Mappings, self).__getslice__(i,j)
        if isinstance(r, list):
            return Mappings(r)
        return r 


    def select(self, tel=None, base=None, polar=None, win=None):
        return self.with_base(base).with_tel(tel).with_polar(polar).with_win(win)

    def withTel(self, tel):
        deprecated("withTel will be deprecated use with_tel instead")
        return self.with_tel(tel)
    def with_tel(self, tel):
        if tel is None: return self
        return self.__class__([m for m in self if m.t1==tel or m.t2==tel])

    def withBase(self, base):
         deprecated("withBase will be deprecated use with_base instead")
         return self.with_base(base)
    def with_base(self, base):
        if base is None: return self
        return self.__class__([m for m in self if m.base==base])

    def withPolar(self, polar):
        deprecated("withPolar will be deprecated use with_polar instead")
        return self.with_polar(polar)
    def with_polar(self, polar):
        if polar is None: return self
        return self.__class__([m for m in self if m.polar==polar])

    def with_phin(self, phin):
        if phin is None: return self
        if isinstance(phin, basestring):
            phin = phin.upper()
        return self.__class__([m for m in self if m.phin==phin])

    def withWins(self, wins):
        deprecated("withWins will be deprecated use with_wins instead")
        return self.with_wins(wins)
    def with_wins(self, wins):
        if wins is None: return self
        return self.__class__([m for m in self if m.win in wins])

    def withWin(self, win):
        deprecated("withWin will be deprecated use with_win instead")
        return self.with_win(win)

    def with_win(self, win):
        if win is None: return self
        return self.__class__([m for m in self if m.win==win])
        for m in self:
            if m.win == win:
                return m
        raise ValueError("the window %d does not exists")

    def getT1(self):
        deprecated("%s will be deprecated use %s instead"%("getT1","get_t1"))
        return self.get_t1()
    def get_t1(self):
        return [m.t1 for m in self]

    def getT2(self):
        deprecated("%s will be deprecated use %s instead"%("getT2","get_t2"))
        return self.get_t2()
    def get_t2(self):
        return [m.t2 for m in self]

    def getWin(self):
        deprecated("%s will be deprecated use %s instead"%("getWin","get_win"))
        return self.get_win()
    def get_win(self):
        return [m.win for m in self]

    def getBase(self):
        deprecated("%s will be deprecated use %s instead"%("getBase","get_base"))
        return self.get_base()
    def get_base(self):
        return [m.base for m in self]

    def getPhi(self):
        deprecated("%s will be deprecated use %s instead"%("getPhi","get_phi"))
        return self.get_phi()
    def get_phi(self):
        return [m.phi for m in self]

    def get_polar(self):
        deprecated("%s will be deprecated use %s instead"%("get_polar","get_polar"))
        return self.get_polar()
    def get_polar(self):
        return [m.polar for m in self]

    def get_phin(self):
        return [m.phin for m in self]


    def getIdx(self):
        deprecated("%s will be deprecated use %s instead"%("getIdx","get_idx"))
        return self.get_idx()
    def get_idx(self):
        return datax.DataX(self.get_win(), axes=[WIN])

    t1    = property(get_t1)
    t2    = property(get_t2)
    win   = property(get_win)
    base  = property(get_base)
    phi   = property(get_phi)
    polar = property(get_polar)
    idx   = property(get_idx)
    phin  = property(get_phin)

class Mapping(object):
    def __init__(self, t1=0,t2=0,base=0,phi=0.0, polar="pNat", win=0, phin=""):
        """
        define a mapping object providing integreted optic output caracteristics
        """
        self.t1 = t1
        self.t2 = t2
        self.phi = phi
        self.polar = polar
        self.win = win
        self.base = base
        self.phin =  phin.upper() if isinstance(phin, basestring) else phin

    def __repr__(self):
        return "(tels=%d-%d  base=%d  win=%d  phi=%3.2f  phin='%s'  polar='%s')"%(self.t1, self.t2, self.base, self.win,self.phi,
                                                                         self.phin, self.polar)



mapABCD = newMapping( [1,1,1,1,2,2,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3],
                      [2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4],
                      [1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,2,2,6,6,6,6],
                      [t*np.pi for t in [0, 1,   1.6,   0.6,
                                         0, 1,
                                         0, 1,   1.6,   0.6,
                                         0, 1,  1.83,   0.83,
                                         0, 1,   1.6,   0.6,
                                         1.6,   0.6 ,
                                         0,      1,   1.6,   0.6]],
                       ["A", "C", "D", "B",
                        "A", "C",
                        "A", "C", "D", "B",
                        "A", "C", "D", "B",
                        "A", "C", "D", "B",
                        "D", "B",
                        "A", "C", "D", "B"]
                  )

mapABCD_pol =  newMapping( mapABCD.t1+mapABCD.t1,
                           mapABCD.t2+mapABCD.t2,
                           mapABCD.base+[b+6 for b in mapABCD.base],
                           mapABCD.phi+mapABCD.phi,
                           mapABCD.phin+mapABCD.phin,
                           ["Pdown"]*24+["Pup"]*24
)

