from detector import getarray, get_instrument_type, DATA, TABLE, PRIMARY, SWITCH, LIST


def reform_key(k):
    """ reform a key like DET.DIT to HIERARCH ESO DET DIT """
    k = " ".join(k.split("."))
    if " " in k and k[0:13]!="HIERARCH ESO ":
        if k[:4] != "ESO ":
            k = "HIERARCH ESO "+k
    return k


class VltHeader(object):
    @getarray
    def key(self, k):
        """ return the header keyword  k"""
        return super(VltHeader, self).key(reform_key(k))
    @getarray    
    def haskey(self, k):
        #return bases.DataEntity.haskey(self,reform_key(k))
        return super(VltHeader , self).haskey( reform_key(k))
    @getarray    
    def comment(self,k=None):
        #return bases.DataEntity.comment(self,reform_key(k))
        return super(VltHeader, self).comment(reform_key(k) if k else k)
        

class VltPrimaryInstrument(VltHeader, get_instrument_type(PRIMARY)):    
    get_dit  = getarray.key("ESO DET DIT", name="dit")
    get_ndit = getarray.key("ESO DET NDIT", name="ndit")    
    get_date = getarray.key("DATE", name="date")
    get_telescope = getarray.key("TELESCOPE", name="telescope")
    get_instrument = getarray.key("INSTRUME", name="instrument")
    get_object = getarray.key("OBJECT", name="object")
    get_exptime = getarray.key("EXPTIME", name="exptime")
    get_mjdobs  = getarray.key("MJD-OBS", name="mjdobs")
    get_dateobs = getarray.key("DATE-OBS", name="dateobs")            
    
class VltDataInstrument(VltHeader, get_instrument_type(DATA)):
    pass

class VltTableInstrument(VltHeader, get_instrument_type(TABLE)):
    pass

class VltSwitchInstrument(get_instrument_type(SWITCH)):
    pass

class VltListInstrument(get_instrument_type(LIST)):
    pass

