from detector import config, update_instrument_space, DATA, TABLE, PRIMARY, SWITCH, LIST
from detector import (get_entity_type, 
                      get_class as _get_class,
                      get_entity_class as _get_entity_class, 
                      get_instrument_subclasses as _get_instrument_subclasses)

from .instruments import (VltPrimaryInstrument, VltDataInstrument, 
						  VltSwitchInstrument, VltListInstrument, 
						  VltTableInstrument)

type_lookup = {
              DATA: VltDataInstrument, 
              PRIMARY: VltPrimaryInstrument, 
              TABLE: VltTableInstrument,
              SWITCH: VltSwitchInstrument,
              LIST: VltListInstrument
             }
def get_instrument_type(TYPE):
    try:
        cl = type_lookup[TYPE]
    except KeyError:
        raise ValueError("unknown type %s"%TYPE)
    return cl       

def get_instrument_subclasses(TYPE, instrument="vlt"):
    if instrument!="vlt":
        return _get_instrument_subclasses(TYPE, instrument=instrument)               
    cls = []
    for case, cl in type_lookup.iteritems():
        if TYPE & case:
            cls.append(cl)
    return tuple(cls)  

def get_entity_class(name, instrument="vlt"):
    return _get_entity_class(name, instrument=instrument)    

def get_class(entity_name, io="fits", instrument="vlt", **kwargs):
    if instrument is None:
        instrument = "vlt"
    if io is None:
        io = "fits" 
    return _get_class(entity_name, io=io, instrument=instrument, **kwargs)

update_instrument_space("vlt", get_instrument_subclasses)