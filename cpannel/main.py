try:
    import Tkinter as tk
except:
    import tkinter as tk
import ttk
from .data import Data

from .dataselection import SelecFrame
from .section import SectionFrame
from .plots.flat import FlatFrame
from .plots.gain import GainFrame
from .plots.ptc  import PtcFrame,swapPtcFrameClass
from .plots.chi2  import Chi2Frame
from .ptcconfig import PtcConfigFrame

class QuitButton(tk.Button):
    def __init__(self, parent, **kwargs):
        kwargs.setdefault("text","QUIT")
        kwargs.setdefault("command",self._quit)
        tk.Button.__init__(self, parent, **kwargs)
        self.parent = parent
    def _quit(self):
        self.parent.quit()
        self.parent.destroy()

class Panel(tk.Tk):
    def __init__(self,  carac , *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)
        self.carac = carac

        self.mainFrame  =  MainFrame(self, carac)
        self.quitButton =  QuitButton(self)
        self.mainFrame.pack(side=tk.TOP, padx=8)
        self.quitButton.pack(side=tk.TOP, padx=8)

class MainFrame(tk.Frame):
    def __init__(self, parent, carac, **kwargs):
        self.data = Data(carac)

        tk.Frame.__init__(self, parent, **kwargs)
        self.selecFrame  = SelecFrame(self, self.data)
        self.sectionFrame= SectionFrame(self, self.data)
        self.ptcConfig = PtcConfigFrame(self, self.data)

        #self.chooseImageFrame = ChooseImageFrame(self)
        self.mainTabs = MainTabs(self, self.data)

        #################################################
        #self.chooseImageFrame.pack(side=tk.TOP, fill=tk.BOTH)
        self.selecFrame.pack(side=tk.TOP)
        self.sectionFrame.pack(side=tk.TOP)
        self.ptcConfig.pack(side=tk.TOP)

        self.mainTabs.pack(side=tk.TOP)

        ##########################
        # refresh all plots
        #self.data.refreshFlatPlot()
        #self.data.refreshGainPlot()
        #self.data.refreshPtcPlot()


class MainTabs(ttk.Notebook):
    def __init__(self, parent ,data,  **kwargs):
        ttk.Notebook.__init__(self, parent, **kwargs)
        self.data = data
        self.flatTab    = ttk.Frame(self)
        self.gainTab    = ttk.Frame(self)
        self.ptcTab    = ttk.Frame(self)
        self.chi2Tab   = ttk.Frame(self)
    #self.ptcTab         = PtcTab(self)

        self.flatFrame = FlatFrame(self.flatTab, self.data)
        self.gainFrame = GainFrame(self.gainTab, self.data)
        self.ptcFrame =  swapPtcFrameClass(data)(self.ptcTab,  self.data)
        self.chi2Frame = Chi2Frame(self.chi2Tab, self.data)
        #################################################
        self.add(self.flatTab  , text="Flat")
        self.add(self.gainTab , text="Gain")
        self.add(self.ptcTab , text="PTC")
        self.add(self.chi2Tab , text="Chi2")

         ##########################
        # refresh all plots
        self.flatFrame.refresh()
        self.gainFrame.refresh()
        self.chi2Frame.refresh()
        self.ptcFrame.refresh()

        #self.data.refreshGainPlot()
        #self.data.refreshPtcPlot()


        self.flatFrame.pack(side=tk.TOP)
        self.gainFrame.pack(side=tk.TOP)
        self.chi2Frame.pack(side=tk.TOP)
        self.ptcFrame.pack(side=tk.TOP)
        self.all_frames = [self.flatFrame, self.gainFrame,self.chi2Frame,self.ptcFrame]

        #self.bind_all('<<NotebookTabChanged>>', self.on_tab_changed)
    def on_tab_changed(self,event):
        """ test not in use """
        self.data.config["selected_frame"] = self.tab(self.select(), "text").lower()
        for f in self.all_frames:
            f.refresh()

