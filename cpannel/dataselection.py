from . import base
tk = base.tk


class PolarMenu(base.BaseMenu):
    def __init__(self, parent, data, selected=0, command=None, **kwargs):
        self.data = data
        values = list(data.carac.get_polar.set())
        
        base.BaseMenu.__init__(self,parent, data.carac.get_polar.set(),
                               selected=self.getSelected(data.polar, values, selected),
                               command=command or self.on_change, **kwargs)
    def on_change(self,polar_str):
        polar = float( polar_str )
        self.data.selectPolar(polar)
    
    def refresh(self):
        # Reset var and delete all old options
        return self._refresh(list(self.data.carac.get_polar.set()), self.data.polar)
        
class DitMenu(base.BaseMenu):
    def __init__(self, parent, data, selected=0, command=None, **kwargs):
        self.data = data
        values = list(data.carac_polar_illumination.get_dit.set())
        
        base.BaseMenu.__init__(self,parent, values,
                          selected=self.getSelected(data.dit,values, selected),
                          command=command or self.on_change, **kwargs)
    
    def on_change(self,dit_str):
        dit = float( dit_str )
        self.data.selectDit(dit)
    
    def refresh(self):
        return self._refresh( list(self.data.carac_polar_illumination.get_dit.set()), self.data.dit) 

class IlluminationMenu(base.BaseMenu):
    def __init__(self, parent, data, selected=0, command=None, **kwargs):
        self.data = data
        values = list(data.carac_polar_dit.get_illumination.set())        
        base.BaseMenu.__init__(self,parent, values,
                          selected=self.getSelected(data.illumination,values, selected),
                          command=command or self.on_change, **kwargs)
    def on_change(self,illumination_str):
        illumination = int( illumination_str )
        self.data.selectIllumination(illumination)
    
    def refresh(self):
        # Reset var and delete all old options
        return self._refresh( list(self.data.carac_polar_dit.get_illuminationSet()), self.data.illumination)
    
        
class SelecFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data    = data

        self.polarLabel = base.BaseLabel(self, text="Polar: ")
        self.polarMenu = PolarMenu( self, data)
        
        self.ditLabel = base.BaseLabel(self, text="DIT: ")
        self.ditMenu = DitMenu( self, data)
        
        self.illuminationLabel = base.BaseLabel(self, text="Illum: ")
        self.illuminationMenu = IlluminationMenu( self, data)
        

        self.polarLabel.pack(side=tk.LEFT)
        self.polarMenu.pack(side = tk.LEFT)
        
        self.ditLabel.pack(side=tk.LEFT)
        self.ditMenu.pack(side = tk.LEFT)
        self.illuminationLabel.pack(side=tk.LEFT)
        self.illuminationMenu.pack(side = tk.LEFT)
        
        self.data.on_carac_polar_illumination_change.append( self.ditMenu.refresh)
        self.data.on_carac_polar_dit_change.append(self.illuminationMenu.refresh)
        self.data.on_carac_polar_change.extend([self.ditMenu.refresh,self.illuminationMenu.refresh])
           

