from .. import rapid
import numpy as np
from .plots.flat import FlatPlot
from .plots.gain import GainPlot
from .plots.ptc  import Ptc
from .plots.chi2  import Chi2Plot

class Command(list):
    def __call__(self):
        return [func() for func in self]

def fullframe():
    #return r.rapid(None,None)
    return (slice(0,None), slice(0,None))

class Data(object):
    on_image_change = Command()
    on_carac_polar_change = Command()
    on_carac_polar_illumination_change = Command()
    on_carac_polar_dit_change = Command()
    on_ptc_config_change = Command()
    on_polar_change = Command()
    on_section_change = Command()
    on_freduce_change = Command()
    on_pixel_change = Command()

    on_refresh_ptcPlot = Command()
    on_refresh_flatPlot = Command()
    on_refresh_gainPlot = Command()
    on_refresh_chi2Plot = Command()
    dit   = None
    polar = None
    illumination = None
    flatPlot = None
    gainPlot = None
    ptcPlot  = None
    config = {
        "selected_frame":"flat",
        "ptc_on":"illumination",
        "signal_min":None,
        "signal_max":None,
        "freduce":"mean",
        "pixel":0,
        "sectionfunc":"fullframe",
        "output":None,
        "section_origin":"center",
        "size":10,
        "yx":(0,0),
        "yx_io":rapid.grism.pos
    }
    choices = {
        "ptc_on":["illumination","dit","both"],
        "freduce":["mean", "median", "pixel"],
        #"sectionfunc":[fullframe, rapid.rapid, rapid.grism],
        #"sectionfuncargs":{ rapid.rapid:["output","size", "yx"] },
        "sectionfunc":["fullframe", "box", "grism"],
        "section_origin":["center","bottomleft", "bottomright", "topleft", "topright",
                          "topcenter", "bottomcenter", "centerleft", "centerright"],
        "output":[None]+list(range(8))
    }
    def __init__(self, carac, config=None):
        self.carac = carac
        self.selectPolar(np.max(carac.get_polar()))
        config = config or {}
        self.config = dict(self.config)
        self.config.update(config)
        if not "ptc_on" in config:
            # guess the default
            if len( self.carac.get_illumination.set() )<3:
                self.config["ptc_on"] = "dit"
            if len( self.carac.get_dit.set() )<3:
                self.config["ptc_on"] = "illumination"
        self.setFreduce( self.config["freduce"])
        self.setSection()
        #self.initFlatPlot()
        #self.initGainPlot()
        #self.initPtcPlot()
        #self.initChi2Plot()

    def selectPolar(self, polar):
        self.carac_polar = self.carac.get_polar.choose(polar)
        self.polar = polar

        if self.dit is None:
            self.dit = np.min(self.carac_polar.get_dit())
        else:
            dits = self.carac_polar.get_dit()
            self.dit = dits[np.argsort( np.abs(dits-self.dit) )[0]]
        self.selectDit(self.dit)

        if self.illumination is None:
            self.illumination = np.min(self.carac_polar.get_illumination())
        else:
            illums = self.carac_polar.get_illumination()
            self.illumination = illums[np.argsort(np.abs(illums-self.illumination))[0]]

        self.selectIllumination( self.illumination )

        self.on_carac_polar_change()
        self.on_polar_change()

    def selectDit(self, dit):
        self.carac_polar_dit = self.carac_polar.get_dit.choose(dit)
        if self.illumination is not None:
            # reset polar to the closest one
            illums = self.carac_polar_dit.get_illumination()
            self.illumination = illums[np.argsort(np.abs(illums-self.illumination))[0]]


        if hasattr(self, "carac_polar_illumination"):
            self.image = self.carac_polar_illumination.get_dit.choose(dit)[0]
        else:
            self.image = self.carac_polar_dit[0]

        self.dit = dit

        self.on_carac_polar_dit_change()
        self.on_image_change()


    def selectIllumination(self, illumination):
        self.carac_polar_illumination = self.carac_polar.withIllumination(illumination)
        if self.dit is not None:
             # reset polar to the closest one
            dits = self.carac_polar_illumination.get_dit()
            self.dit = dits[np.argsort( np.abs(dits-self.dit) )[0]]

        self.image = self.carac_polar_dit.withIllumination(illumination)[0]
        self.illumination = illumination

        self.on_carac_polar_illumination_change()
        self.on_image_change()
    def getDetCarac(self):
        kw = {"signalrange":self.getSignalRange()}

        if self.config["ptc_on"] == "dit":
            return self.carac_polar_illumination.getDetCarac(**kw)
        if self.config["ptc_on"] == "illumination":
            return self.carac_polar_dit.getDetCarac(**kw)
        if self.config["ptc_on"] == "both":
            return self.carac_polar.getDetCarac(**kw)
        raise KeyError("ptc_on should be one of 'dit','illumination' or 'both' got %s"%self.config["ptc_on"] )
    def set_ptc_on(self,val):
        if not val in self.choices["ptc_on"]:
            raise KeyError("ptc_on should be one of %s got %s"%(self.choices["ptc_on"], val))
        self.config["ptc_on"] = val
        self.on_ptc_config_change()

    def get_ptc_on(self):
        return self.config["ptc_on"]
    def setSignalRange(self,mn, mx):
        self.config["signal_min"] = mn
        self.config["signal_max"] = mx
        self.on_ptc_config_change()

    def getSignalRange(self):
        return (self.config["signal_min"], self.config["signal_max"])

    def setSection(self):
        self.section= self.getSection( self.config["sectionfunc"])
        if self.getPixel()>=self.getSectionSize():
            self.setPixel(0)
        #self.refreshSectionPlot()

        self.on_section_change()

    def setSectionFunc(self, sectionfunc):
        self.config["sectionfunc"] = sectionfunc
        self.setSection()

    def setSectionOrigin(self, origin):
        self.config["section_origin"] = origin
        self.setSection()

    def getSectionSize(self):
        if hasattr(self.section, "size"):
            return self.section.size
        # return the full frame size
        return self.image.getsize()
        return reduce( lambda x,y: x*y, self.image.getshape())

    def setFreduce(self,freduce):
        fred = freduce.lower()

        if fred == "mean":
            self.freduce = np.mean

        elif fred == "median":
            self.freduce = np.median

        elif fred == "pixel":
            self.freduce = None
        else:
            raise ValueError("freduce should be one of %s"%self.choices["freduce"])
        self.config["freduce"] = freduce

        self.on_freduce_change()
    def setPixel(self, pixel):
        self.config["pixel"] = max( min(pixel, self.getSectionSize()-1) , 0)
        self.on_pixel_change()

    def getPixel(self):
        return self.config["pixel"]

    def getSection(self, sectionfunc):
        sf = sectionfunc.lower()
        if sf == "box":
            return rapid.rapid( self.config["output"], self.config["size"],
                                self.config["yx"], ref=self.config["section_origin"])
        if sf == "grism":
            return rapid.grism( self.config["yx_io"] )
        if sf == "free":
            return rapid.free( self.config["yx_io"] )

        return fullframe()


    def setSectionOutput(self, output):
        self.config["output"] = output
        self.setSection()
    def setSectionIOPos( self, yx):
        y,x = yx
        self.config["yx_io"]     = int(y),int(x)
        self.setSection()
    def setSectionBox(self, size, yx):
        y,x = yx
        self.config["yx"]     = int(y),int(x)
        self.config["size"]   = size
        self.setSection()
