try:
    import Tkinter as tk
except:
    import tkinter as tk
import ttk


class Command(list):
    def __call__(self):
        return [func() for func in self]


class BaseFrame(tk.Frame):
    pass 

class BaseLabel(tk.Label):
    pass

class BaseEntry(tk.Entry):
    pass

class TypeEntry(BaseEntry):
    """ 
    An entry class that take the data type in consideration 
    and format it corectly.
    """
    def __init__(self, parent, value=None, dtype=str,
                 fmt="%s", allowNone=False,
                 on_return=None, **kwargs):
        
        tk.Entry.__init__(self, parent, **kwargs)

        self.dtype = dtype
        self.allowNone = allowNone
        if on_return:
            self.bind("<Return>", on_return)
        
        self.fmt = fmt
        if value is None:
            if allowNone: 
                svalue = ""
            else:
                svalue = fmt%dtype()
        else:
            svalue =  fmt%value
        
        self.value = value        
        self.delete(0,tk.END)        
        self.insert(0,svalue)
        
    def setVal(self, val):
        self.delete(0, tk.END) 
        if val is None:
            if self.allowNone:
                self.insert(0,"")        
            else: 
                self.insert(0,self.fmt%self.dtype())
        else:
            self.insert(0,self.fmt%self.dtype(val))
            
    def getVal(self):        
        val = self.get()
        if not len(val): 
            if self.allowNone:
                return None
            else:
                return self.dtype()
        
        return self.dtype(val)



class BaseMenu(tk.OptionMenu):
    def __init__(self, parent, values, selected=0, var=None, command=None, **kwargs):
        if var is None:
            self.var = tk.StringVar(parent)
            self.var.set( values[selected] )
        else:
            self.var = var
        tk.OptionMenu.__init__(self, parent, self.var, *values, command=command, **kwargs)
    def _refresh(self,new_choices, value=None):
        if value is None:
            self.var.set('')
        else:
            self.var.set( new_choices[ self.getSelected(value,new_choices)  ] )
            
        self['menu'].delete(0, 'end')        
                
        for choice in new_choices:
            self['menu'].add_command(label=choice, command=tk._setit(self.var, choice))

    def getSelected(self, value, values, selected=0):
        if value in values:
            return values.index(value)
        return selected    

class LabelEntry(BaseFrame):
    def __init__(self, parent, Label, kw_label, Entry, kw_entry, inside=None, pack=None, **kwargs):
        
        BaseFrame.__init__(self, parent, **kwargs)
        inside = inside or self
        self.label = Label(inside,  **kw_label)
        self.entry = Entry(inside,  **kw_entry)
        
        pack = pack or self.pack_horizontal
        pack(self)
    @staticmethod
    def pack_horizontal(obj):
        obj.label.pack( side=tk.LEFT)
        obj.entry.pack( side=tk.LEFT)
    
    def setVal(self, val):
        return self.entry.setVal(val)
    def getVal(self):
        return self.entry.getVal()
    
class BaseButton(tk.Button):
    pass

class EditFrame(BaseFrame):
    rules = {}
    order = []
    def __init__(self, parent, kw, onparamschange=None,                 
                 pack=None, inside=None, **kwargs):
        BaseFrame.__init__(self, parent, **kwargs)
        self.kw = kw
        self.onparamschange = onparamschange
        self.init_parameters (self.rules, inside)
        self.pushValues()
        pack = pack or self.pack_params_horizontal
        pack(self)

    def pack_all(self, **kwargs):
        for k in self.order:
            p = self.parameters[k]        
            p.pack(side=tk.LEFT)
    
    @staticmethod
    def pack_params_horizontal(self):
        self.pack_all(side=tk.LEFT)
    def pack_params_vertical(self):
        self.pack_all(side=tk.TOP)
    
    def set_kw(self, kw):
        self.kw = kw
    
    def init_parameters(self, rules, inside=None, default_entry=None, default_label=None):
        inside = inside or self
        default_entry = default_entry or {"on_return": lambda event: self.pullValues()}
        default_label = default_label or {}
        self.parameters = {k:LabelEntry(inside, Label, dict(default_label,**kw_label),
                                        Entry, dict( default_entry,**kw_entry)) for
                           k,(Label,kw_label, Entry, kw_entry) in rules.iteritems() if k in self.order}
    
    def pullValues(self):
        for k,obj in self.parameters.iteritems():
            self.kw[k] = obj.getVal()
        if self.onparamschange:
            self.onparamschange()
    
    def pushValues(self):
        for k,obj in self.parameters.iteritems():
            if k in self.kw:
                obj.setVal(self.kw[k])
        


    
