from . import base
tk = base.tk

class SectionFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):
        base.BaseFrame.__init__( self, parent , **kwargs)
        self.data = data
        self.sectionFuncMenu = SectionFuncMenu(self, data, command=self.func_change)
        
        self.sectionBoxFrame = SectionBoxFrame(self, data)
        self.sectionIOFrame  = SectionIOFrame(self, data)
        
        self.sectionFuncMenu.pack(side=tk.LEFT)        
        self.swap()
        
    def func_change(self,sectionfunc):
        self.data.setSectionFunc(sectionfunc)
        self.swap()
    
    def swap(self):
        case = self.sectionFuncMenu.getCase()        
        if case==CASE_OUTPUT:
            self.sectionBoxFrame.pack(side=tk.LEFT)
            self.sectionIOFrame.pack_forget()
        elif case==CASE_IOFRAME:
            self.sectionIOFrame.pack(side=tk.LEFT)
            self.sectionBoxFrame.pack_forget()
        else:
            self.sectionBoxFrame.pack_forget()
            self.sectionIOFrame.pack_forget()

CASE_IOFRAME = 2
CASE_OUTPUT  = 1
CASE_OTHER   = 0 
class SectionFuncMenu(base.BaseMenu):
    def __init__(self, parent, data,command = None,  **kwargs):
        self.data = data
        values = data.choices["sectionfunc"]
        self.var = tk.StringVar()
        self.var.set( data.config["sectionfunc"] )
        
        base.BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs) 
    def on_change(self,sectionfunc):
        self.data.setSectionFunc(sectionfunc)

    def getCase(self):
        f = self.var.get().lower()        
        if f=="box":
            return CASE_OUTPUT
        if f=="grism" or f=="free":    
            return CASE_IOFRAME
        return CASE_OTHER
    
class SectionOriginMenu(base.BaseMenu):
    def __init__(self, parent, data,command = None,  **kwargs):
        self.data = data
        values = data.choices["section_origin"]
        self.var = tk.StringVar()
        self.var.set( data.config["section_origin"] )
        
        base.BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs) 
    def on_change(self,sectionfunc):
        self.data.setSectionOrigin(sectionfunc)
        

class SectionBoxFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):        
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        y,x = self.data.config["yx"]
        
        self.x = base.LabelEntry( self, base.BaseLabel, {"text":"X"},
                                  base.TypeEntry, {"dtype":int, "value":x, "width":4,
                                         "on_return":self.on_change} )
        self.y = base.LabelEntry( self, base.BaseLabel, {"text":"Y"},
                                  base.TypeEntry, {"dtype":int, "value":y, "width":4,
                                                   "on_return":self.on_change} )
        
        self.output = OutputMenu(self, data)
        self.origin = SectionOriginMenu(self, data)
        self.originLabel = base.BaseLabel(self, text="Origin: ")
        self.size = base.LabelEntry( self, base.BaseLabel, {"text":"size"}, base.TypeEntry, {"dtype":str, "value":"20,20", "on_return":self.on_change, "width":5, "allowNone":False} )
        
        self.output.pack( side=tk.LEFT )
        self.size.pack( side=tk.LEFT)
        self.x.pack( side=tk.LEFT)
        self.y.pack( side=tk.LEFT)
        self.originLabel.pack( side=tk.LEFT)
        self.origin.pack( side=tk.LEFT)
        
    def on_change(self, event):        
        self.data.setSectionBox(self.getsize(), (self.y.getVal(), self.x.getVal()))
    def getsize(self):
        ssize = self.size.getVal()
        sizes = ssize.split(",")[0:2]
        return [None if s.strip() is "" else int(s) for s in sizes]

class SectionIOFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):            
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        y,x = self.data.config["yx_io"]
        self.x = base.LabelEntry( self, base.BaseLabel, {"text":"X"},
                                  base.TypeEntry, {"dtype":int, "value":x,  "width":4,
                                         "on_return":self.on_change} )
        self.y = base.LabelEntry( self, base.BaseLabel, {"text":"Y"},
                                  base.TypeEntry, {"dtype":int, "value":y, "width":4,
                                         "on_return":self.on_change} )
        
        self.x.pack( side=tk.LEFT)
        self.y.pack( side=tk.LEFT)
    def on_change(self,event):
        self.data.setSectionIOPos((self.y.getVal(), self.x.getVal()))
        

    
        
class OutputMenu(base.BaseMenu):
    def __init__( self, parent, data, command=None, **kwargs):
        self.data = data
        values = data.choices["output"]
        self.var = tk.Variable()
        self.var.set( data.config["output"] )
        base.BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs)
        
    def on_change(self, output):
        self.data.setSectionOutput(output)
