from . import base
tk = base.tk

class PtcOnMenu(base.BaseMenu):
    def __init__(self, parent, data,command = None,  **kwargs):
        self.data = data
        values = data.choices["ptc_on"]
        self.var = tk.StringVar()
        self.var.set( data.config["ptc_on"] )
        
        base.BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs)
    def on_change(self, ptcon):
        self.data.set_ptc_on(ptcon)

class PtcConfigFrame(base.BaseFrame):
    def __init__(self, parent, data,pack=None, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        self.ptcOnLabel = base.BaseLabel(self, text="PTC fit on ")
        self.ptcOn = PtcOnMenu( self, data)
        self.signalMin = base.LabelEntry( self, base.BaseLabel, {"text":"Fit Signal Min"},
                                          base.TypeEntry, {"dtype":float,
                                                           "value":data.config["signal_min"],
                                                           "width":4,"allowNone":True, 
                                                           "on_return":self.on_change} )
        self.signalMax = base.LabelEntry( self, base.BaseLabel, {"text":"Max"},
                                          base.TypeEntry, {"dtype":float,
                                                           "value":data.config["signal_max"],
                                                           "width":4,"allowNone":True, 
                                                           "on_return":self.on_change} )
        pack = pack or self.pack_horizontal
        pack(self)
    @staticmethod
    def pack_horizontal(self):
        self.ptcOnLabel.pack(side=tk.LEFT)
        self.ptcOn.pack(side=tk.LEFT)
        self.signalMin.pack( side=tk.LEFT)
        self.signalMax.pack( side=tk.LEFT)        
    
    def on_change(self, event):
        return self.data.setSignalRange( self.signalMin.getVal(),  self.signalMax.getVal() )
    
