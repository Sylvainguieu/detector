from  .. import base
tk = base.tk
from  .image import DetImgPlot, ImgHistFrame


class Chi2Plot(DetImgPlot):
    kw_allaxes = { "hist":{"figure":"chi2_hist"},
                    "img":{"figure":"chi2_imshow"}
                }
    
    def initData(self, data):
        self.on_refresh = base.Command()
        self.on_refresh_section = base.Command()
        self.data = data
        data.on_polar_change.append(self.refresh)
        data.on_ptc_config_change.append(self.refresh)
        
    def refresh_section(self):
        self.sectionplot = self.data.section
        self.on_refresh_section()
    def refresh(self):
        data = self.data
        
        ptc = data.getDetCarac()
        self.setData( ptc.Ptcchi2(), sectionplot=data.section)
        
        dits   = list(set(ptc[-1].getData("DET DIT")))
        illums = list(set(ptc[-1].getData("ILLUMINATION")))
        if len(dits)>1:
            ditsleg = "%.4f->%.4f"%(min(dits), max(dits))
        else:
            ditsleg = "%.4f"%dits[0]
        if len(illums)>1:
            illumsleg = "%.4f->%.4f"%(min(illums), max(illums))
        else:
            illumsleg = "%s"%illums[0]
        
        self.kw["allaxes.img"]["title"] = "p=%d, DIT=%s, I=%s"%(data.polar, ditsleg, illumsleg)
        self.on_refresh()
        
class Chi2Frame(base.BaseFrame):
    def __init__(self, parent, data, pack=None, kw=None, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        plot = Chi2Plot()
        plot.initData(data)
        self.plot = plot
        self.plotFrame = ImgHistFrame(self, plot)
        plot.on_refresh.append(self.replot)
        plot.on_refresh_section.append(self.replot)
        data.on_section_change.append(self.plot.refresh_section)
        
        self.plotFrame.pack(side=tk.TOP)
    def refresh(self):
        self.plot.refresh()
    def replot(self):
        self.plotFrame.replot()
    
