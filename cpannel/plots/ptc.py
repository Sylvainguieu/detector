from .. import base
from .baseplot import BasePlotFrame
from .linearity import LinearityPlotFrame

tk = base.tk
from detector.plots import detplots as plots

class Ptc(plots.Ptc):
    kw_errorbar = {"axes":"ptc"}
    kw_plotfit  = {"axes":"ptc"}
    kw_plot     = {"axes":"ptc"}
    kw_allaxes  = {"ptc":{"figure":"ptc"}}
    def initData(self, data):
        self.on_refresh = base.Command()
        self.data = data
                
        #data.on_polar_change.append(self.refresh)
        data.on_carac_polar_dit_change.append(self.refresh)
        data.on_carac_polar_illumination_change.append(self.refresh)
         
        data.on_section_change.append(self.refresh)
        data.on_freduce_change.append(self.refresh)
        data.on_pixel_change.append(self.refresh)    
        data.on_ptc_config_change.append(self.refresh)
        
    def allptc(self):
        axes = self.get_axes("ptc")
        axes.clear()
        self.all(xrange  = self.data.getSignalRange())
        axes.figure.canvas.draw()
    def refresh(self):
        data = self.data
        kw = dict(            
            section = data.section,
            xrange  = data.getSignalRange()
        )
        if data.freduce is None:
            kw["pixel"] = data.config["pixel"]
        else:
            kw["freduce"] = data.freduce
        
        if data.config["ptc_on"] == "dit":
            ptcdata = data.carac_polar_illumination
        elif data.config["ptc_on"] == "illumination":
            ptcdata = data.carac_polar_dit
        elif data.config["ptc_on"] == "both":
            ptcdata = data.carac_polar        
        self.setData(ptcdata, **kw)
        self.on_refresh()
        
        
class PtcPlotFrame(BasePlotFrame):
    def __init__(self, parent, data, **kwargs):
        self.data = data
        
        plot = Ptc()
        plot.initData(data)
        self.plot = plot
        BasePlotFrame.__init__(self, parent, plot.allptc, plot.get_axes("ptc").figure)
        
        plot.on_refresh.append(self.replot)
        
    def refresh(self):
        return self.plot.refresh()
        

class PtcFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        
        self.plotFrame    = PtcPlotFrame(self, data)        
        self.freduceFrame = FreduceFrame(self, data)
        
        self.plotFrame.pack(side=tk.TOP)
        self.freduceFrame.pack(side=tk.TOP)
    def refresh(self):
        return self.plotFrame.refresh()
    def replot(self):
        return self.plotFrame.replot()

class PtcLinearityFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        
        self.ptcPlotFrame    = PtcPlotFrame(self, data)
        self.linearityPlotFrame = LinearityPlotFrame(self, data)
        self.freduceFrame = FreduceFrame(self, data)
        
        self.ptcPlotFrame.grid( row=0, column=0)
        self.linearityPlotFrame.grid( row=0, column=1)
        self.freduceFrame.grid(row=1, column=0)
    def refresh(self):
        self.ptcPlotFrame.refresh()
        self.linearityPlotFrame.refresh()
    def replot(self):
        self.ptcPlotFrame.replot()
        self.linearityPlotFrame.replot()

def swapPtcFrameClass(data):
    
    if len( data.carac.get_dit.set() )>1:
        return PtcLinearityFrame
    return PtcFrame
    

class FreduceMenu(base.BaseMenu):
    def __init__( self, parent, data, command=None, **kwargs):
        self.data = data
        values = data.choices["freduce"]
        self.var = tk.StringVar()
        self.var.set( data.config["freduce"] )
        base.BaseMenu.__init__(self, parent, values, var=self.var, command=command or self.on_change, **kwargs)
        
    def on_change(self, freduce):
        self.data.setFreduce(freduce)

class PixelFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        self.freducePixel = base.TypeEntry(self, dtype=int, width=4, value=data.getPixel(),
                                           on_return=self.set_pixel
                                  )
        self.prevButton = base.BaseButton(self, text="<", command=self.prev_pixel)
        self.nextButton = base.BaseButton(self, text=">", command=self.next_pixel)
        self.freducePixel.pack(side=tk.LEFT)
        self.prevButton.pack(side=tk.LEFT)
        self.nextButton.pack(side=tk.LEFT)
                
        self.state_button( self.data.getPixel(),  0, self.data.getSectionSize()-1)
        self.data.on_pixel_change.append(self.refresh_pixel_val)
        
    def refresh_pixel_val(self):
        pixel = self.data.getPixel()
        self.freducePixel.setVal(pixel)
        self.state_button(pixel, 0, self.data.getSectionSize()-1)
    
    def set_pixel(self,event):
        self.data.setPixel(self.freducePixel.getVal())
            
    def state_button(self,pixel, mn, mx):
        self.state_prev_button(pixel, mn)
        self.state_next_button(pixel, mx)
        
    def state_prev_button(self, pixel, mn):
        if pixel<(mn+1):
            self.prevButton.config(state='disable')
        else:
            self.prevButton.config(state='normal')
            
    def state_next_button(self, pixel, mx):
        if pixel>(mx-1):
            self.nextButton.config(state='disable')
        else:
            self.nextButton.config(state='normal')
        
    def prev_pixel(self):
        self.data.setPixel( self.data.getPixel()-1)
    
    def next_pixel(self):
        self.data.setPixel( self.data.getPixel()+1)

    
class FreduceFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        self.freduceLabel = base.BaseLabel(self, text="Freduce")
        self.freduceMenu  = FreduceMenu(self, data, command=self.on_freduce_change)
        self.freducePixel = PixelFrame(self, data)
        
        self.freduceMenu.pack(side=tk.LEFT)
        self.swap()
    
    def on_freduce_change(self, freduce):
        self.data.setFreduce(freduce)
        self.swap()
    
    def swap(self):
        freduce = self.data.config["freduce"].lower()

        if freduce=="pixel":
            self.freducePixel.pack(side=tk.LEFT)
        else:            
            self.freducePixel.pack_forget()
