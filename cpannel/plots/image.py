from . import baseplot
from .. import base
tk = base.tk
import numpy as np
from detector.plots import detplots as plots

rules ={
    "imshow.vmin":(base.BaseLabel,{"text":"vmin :"},
                   base.TypeEntry,dict(dtype=float, width=5, allowNone=True)),
    "imshow.vmax":(base.BaseLabel,{"text":"vmax :"},
                   base.TypeEntry,dict(dtype=float, width=5, allowNone=True)),
    "hist.nbins":(base.BaseLabel,{"text":"N Bins :"},
                  base.TypeEntry,dict(dtype=int, width=5, value=100))
    }
rules["hist.vmin"] = rules["imshow.vmin"]
rules["hist.vmax"] = rules["imshow.vmax"]


    
class DetImgPlot(plots.DetImgPlot):
    kw_imshow = {"axes":"img", "origin":"lower" }
    kw_hist   = {"axes":"hist", "color":"black", "normed":True}
    ckw_histsection = {"axes":"hist","color":"red", "alpha":0.8, "normed":True}
    ckw_image_vlines = {}
    kw_image_vlines = {"axes":"hist","selected":-1,  "linestyle":'dashed',
                       "color":"k", "colorselected":"r"
                   }
    kw_allaxes = { "hist":{},"img":{} }

    def __init__(self, *args, **kwargs):
        super(DetImgPlot,self).__init__(*args, **kwargs)
        self.on_plot_histogram = base.Command()
        self.on_plot_imshow    = base.Command()
    
    def allhistogram(self):

        axes = self.get_axes("hist")
        print "PRINT ALLHISTOGRAM " , axes.figure.number
        axes.clear()
        self.hist()
        self.image_vlines()
        self.axes_set(axes="hist")
        axes.figure.canvas.draw()
        
        self.on_plot_histogram()
        
    def allimage(self):

        axes = self.get_axes("img")
        print "PRINT ALLIMAGE", axes.figure.number
        axes.clear()
        self.imshow()
        self.axes_set(axes="img")        
        axes.figure.canvas.draw()   
        self.on_plot_imshow()
        
    def setImg(self, **kwargs):
        img = self.detector.getData(**kwargs)
        self.img = np.ma.masked_array( img, np.isinf(img)+np.isnan(img))
        self.imgmax = np.max(self.img)
        self.imgmin = np.min(self.img)
        self.kw_hist.setdefault("vmin", self.imgmin)
        self.kw_hist.setdefault("vmax", self.imgmax)
        self.kw_imshow.setdefault("vmin", self.imgmin)
        self.kw_imshow.setdefault("vmin", self.imgmin)
        
    def on_hist_click(self,event):
        print "CLICK"
        kw = self.kw_image_vlines
        kwimg = self.kw_imshow
        self.update_vmin_vmax(kwimg)
        vmin, vmax = kwimg["vmin"], kwimg["vmax"]
        
        if event.inaxes != self.get_axes("hist"):
            
            if kw['selected']>-1:
                kw["selected"] = -1
                self.image_vlines()
                self.figure.canvas.draw()                
            return
        
        x,y = event.xdata, event.ydata
        self.allhistogram()
        
                    
        if kw["selected"]<0:
            kw["selected"] = np.abs(np.array([vmin,vmax])-x).argmin()
            self.image_vlines()
            self.get_axes("hist").figure.canvas.draw()
            
        else:
            selected = kw["selected"]
            kw["selected"] = -1
            

            if selected: vmax = x
            else: vmin = x
            
            if vmin>vmax: vmax,vmin = vmin,vmax
            kwimg.update( vmin=vmin, vmax=vmax)
            print "vmin", vmin, "vmax", vmax
            self.allhistogram()
            self.allimage()

    def init_figures(self):
        print "INIT FIGURE", self.get_axes("hist").figure._label
        
        self.get_axes("hist").figure.canvas.mpl_connect("button_press_event",
                                                        self.on_hist_click)
    
    def update_vmin_vmax(self,kwargs):
        vmin, vmax = kwargs.get("vmin", None), kwargs.get("vmax",None)
        if vmin is None: vmin = self.imgmin
        if vmax is None: vmax = self.imgmax
        kwargs.update( vmin=vmin, vmax=vmax )
        
    def image_vlines(self, **kwargs):
        self._parsekwargs_(kwargs, "image_vlines")
        axes = self.getKwAxes(kwargs)
        
        kw =  self.kw_imshow
        self.update_vmin_vmax(kw)        
        vmin, vmax = kw["vmin"], kw["vmax"]
        
        
        selected = kwargs.pop("selected", -1)
        cselected = kwargs.pop("colorselected", "r")
        color = kwargs.pop("color", "k")
        out = [] 
        for i,x in enumerate([vmin,vmax]):            
            out.append( plots.axvline(x, color=cselected if selected==i else  color,**kwargs)
                    )
        return out
    
    def hist(self, **kwargs):
        self._parsekwargs_(kwargs, "hist")
        self.update_vmin_vmax(kwargs)
        vmin, vmax, nbins = kwargs.pop("vmin",None), kwargs.pop("vmax",None), kwargs.pop("nbins",100)
        self.kw_allaxes["hist"]["xlim"] = (vmin,vmax) 
        
        kwargs.pop("bins", None)
        
        bins = np.linspace(vmin, vmax, nbins)
        axes = self.getKwAxes(kwargs)
        x    = self.substituteX(kwargs)
        masknan = kwargs.pop("masknan", False)
        
        h = axes.hist(x, bins=bins,**kwargs)
        self.histsection(bins=bins)        
        self.kw_hist.update( vmin=vmin, vmax=vmax, nbins=nbins)
        return h
    def histsection(self, **kwargs):
        if not hasattr(self, "sectionplot"):
            return None
        self._parsekwargs_(kwargs, "histsection")
        img = self.substituteImg(kwargs)[self.sectionplot]
        x = img.flatten()
        axes = self.getKwAxes(kwargs)
        return axes.hist(x, **kwargs)




class EditImgShowParams(base.EditFrame):
    rules = rules
    order = ["imshow.vmin", "imshow.vmax"]
    def __init__(self, parent, kw, pack=None, inside=None,onparamschange=None,  **kwargs):

        base.EditFrame.__init__(self, parent, kw, pack=pack,inside=inside, onparamschange=onparamschange,  **kwargs)
        
        inside = inside or self
        
        self.setButton = base.BaseButton(inside, text="set", command=self.pullValues)
        
        pack =  pack or self.pack_horizontal
        pack(self)
    @staticmethod
    def pack_horizontal(self):        
        self.setButton.pack(side=tk.LEFT)


class EditHistParams(base.EditFrame):
    rules = rules
    order = ["hist.vmin", "hist.vmax","hist.nbins"]

    def __init__(self, parent,  kw, pack=None, inside=None,onparamschange=None, **kwargs):
        base.EditFrame.__init__(self, parent,kw, pack=pack,inside=inside,
                                onparamschange=onparamschange,**kwargs)
        
        inside = self                
        self.setButton = base.BaseButton(inside, text="set", command=self.pullValues)
        pack = pack or self.pack_horizontal
        pack(self)
    
    @staticmethod
    def pack_horizontal(self):
        self.setButton.pack(side=tk.LEFT)



        
class HistFrame(baseplot.EditAndPlotFrame):
    def __init__(self, parent, plot, axes="hist", pack=None, inside=None, connections=None, onparamschange=None, **kwargs):
        baseplot.EditAndPlotFrame.__init__(self, parent, plot, axes, **kwargs)
                
        figure  = self.plot.get_axes(self.axes).figure

        self.plotFrame =  baseplot.BasePlotFrame( self, self.plot2plotfunc(),
                                                  figure, connections=connections
                                              )
        
        onparamschange = onparamschange or self.plotFrame.replot
        self.editFrame = EditHistParams( self, self.plot2kw(),
                                         onparamschange=onparamschange )

        self.plot.on_plot_histogram.append( self.editFrame.pushValues )
        pack = pack or self.pack_verticaly
        pack(self)
            
    def plot2plotfunc(self):
        return self.plot.allhistogram
    
    def plot2kw(self):
        return self.plot.kw
    
    
        
class ImgShowFrame(baseplot.EditAndPlotFrame):
    def __init__(self, parent, plot, axes="imshow", pack=None, inside=None, connections=None, onparamschange=None, **kwargs):        
        baseplot.EditAndPlotFrame.__init__(self, parent, plot, axes, **kwargs)
        
        figure  = self.plot.get_axes(self.axes).figure
        print "FIGURE NUMBER HIST", figure.number
        self.plotFrame =  baseplot.BasePlotFrame( self, self.plot2plotfunc(),
                                                  figure, connections=connections,
                                                  navigation=True
                                              )
        onparamschange = onparamschange or self.plotFrame.replot
        self.editFrame = EditImgShowParams( self, self.plot2kw(),
                                            onparamschange=onparamschange )

        self.plot.on_plot_imshow.append(self.editFrame.pushValues)
        pack = pack or self.pack_verticaly
        pack(self)

    def plot2plotfunc(self):
        return self.plot.allimage
    def plot2kw(self):        
        return self.plot.kw
        
class ImgHistFrame(base.BaseFrame):
    """ Image and Histogram on the same frame """
    def __init__(self, parent, plot, pack=None, inside=None, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        
        self.plot = plot
        
        
        inside = inside or self
        
        self.imgFrame  =  ImgShowFrame(inside, plot,   "img")
        self.histFrame =   HistFrame(inside,   plot,  "hist")
        
        self.imgFrame.editFrame.onparamschange = self.replot
        plot.init_figures()                        
        pack = pack or self.pack_horizontal_grid
        pack(self)
    @staticmethod
    def pack_horizontal(self):
        self.imgFrame.pack(side=tk.LEFT)
        self.histFrame.pack(side=tk.LEFT)
    @staticmethod
    def pack_horizontal_grid(self):
        self.imgFrame.grid( row=0, column=0, stick=tk.NW)
        self.histFrame.grid(row=0, column=1, stick=tk.NE)
         
    def refresh(self):
        self.imgFrame.refresh()
        self.histFrame.refresh()
        
    def replot(self):
        self.imgFrame.replot()
        self.histFrame.replot()
        
