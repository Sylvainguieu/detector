from .. import base
tk = base.tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
import numpy as np
from matplotlib.pylab import figure
from detector import plots

def add_figure( master, figure):
    print "ADDDD FIGURE ",figure.number, figure._label
    
    canvas = FigureCanvasTkAgg(figure, master=master)
    widget = canvas.get_tk_widget()
    return canvas, widget

def add_navigation(master, canvas):
    return NavigationToolbar2TkAgg(canvas, master)

class BaseFigureFrame(base.BaseFrame):
    def __init__(self, parent, figure, pack=None, connections=None, navigation=False, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.figure = figure
                
        self.figureCanvas, self.figureWidget = add_figure(self, figure)
        
        self.figureCanvas.show()
        self.navigation = navigation
        
        if navigation:
            self.navigationTool = add_navigation(self, self.figureCanvas)
        
        if connections:
            for action, func in connections.iteritems():
                figure.canvas.mpl_connect(action, func)
        
        pack = pack or self.pack_default
        pack(self)
    @staticmethod
    def pack_default(self):
        if self.navigation:
            self.navigationTool.pack(fill=tk.BOTH,side=tk.TOP)
            
        self.figureWidget.pack(fill=tk.BOTH,side=tk.TOP)
    def redraw(self):        
        self.figure.canvas.draw()
                
        
class BasePlotFrame(BaseFigureFrame):    
    def __init__(self,  parent, plotfunc, figure,  pack=None, connections=None, navigation=False, **kwargs):
        self.plotfunc = plotfunc
        
        BaseFigureFrame.__init__(self, parent, figure,
                                 pack = lambda o:None,
                                 connections=connections,
                                 navigation=navigation, 
                                 **kwargs)
        pack = pack or self.pack_verticaly
        pack(self)
        
    @staticmethod
    def pack_verticaly(self):        
        self.figureWidget.pack(side=tk.TOP)
    def refresh(self):
        self.replot()
    def replot(self):
        self.plotfunc()
        
class EditAndPlotFrame(base.BaseFrame):
    plotFrame = None
    editFrame = None
    plot = None
    def __init__(self, parent, plot, axes, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.axes = axes
        self.plot = plot
        
    def plot2kw(self):
        return self.plot.kw    
    def plot2plotfunc(self):
        raise NotImplemented("plot2plotfunc not implemented")
    
    def refresh(self):
        self.plot.refresh()
        self.plotFrame.replot()
        self.editFrame.pushValues()
    
    def replot(self):
        return self.plotFrame.replot(  )        
    @staticmethod
    def pack_verticaly(self):
        self.plotFrame.pack(side=tk.TOP)
        self.editFrame.pack(side=tk.TOP)
