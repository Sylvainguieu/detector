from .. import base
tk = base.tk
from  .image import DetImgPlot, ImgHistFrame

class GainPlot(DetImgPlot):
    #kw_hist = {"vmin":0, "vmax":5},
    kw_allaxes = { "hist":{"figure":"gain_hist"},
                    "img":{"figure":"gain_imshow"}
    }
    def initData(self, data):
        self.on_refresh = base.Command()
        self.on_refresh_section = base.Command()
        self.data = data
        data.on_polar_change.append(self.refresh)
        data.on_ptc_config_change.append(self.refresh)
        
    def refresh_section(self):
        self.sectionplot = self.data.section
        self.on_refresh_section()    
    def refresh(self):
        data = self.data
        ptc = data.getDetCarac()
        self.setData( ptc.Gain(), sectionplot=data.section)
                        
        
        dits   = list(set(ptc[-1].getData("DET DIT")))
        illums = list(set(ptc[-1].getData("ILLUMINATION")))
        if len(dits)>1:
            ditsleg = "%.4f->%.4f"%(min(dits), max(dits))
        else:
            ditsleg = "%.4f"%dits[0]
        if len(illums)>1:
            illumsleg = "%.4f->%.4f"%(min(illums), max(illums))
        else:
            illumsleg = "%s"%illums[0]
        
        self.kw["allaxes.img"]["title"] = "p=%d, DIT=%s, I=%s"%(data.polar, ditsleg, illumsleg)
        self.on_refresh()
        

class GainFrame(base.BaseFrame):
    def __init__(self, parent, data, pack=None, kw=None, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        plot = GainPlot()
        plot.initData(data)
        self.plot = plot
        self.plotFrame = ImgHistFrame(self, plot)
        plot.on_refresh.append( self.replot)
        plot.on_refresh_section.append(self.replot)
        data.on_section_change.append(self.plot.refresh_section)
        
        
        self.plotFrame.pack(side=tk.TOP)
    def is_selected(self):
        return True
        #return self.data.config["selected_frame"] == "gain"
    def refresh(self):
        if self.is_selected():
            #self.plot.refresh()
            self.plotFrame.refresh()
    def replot(self):
        if self.is_selected():
            self.plotFrame.replot()
        
