from .. import base
from .baseplot import BasePlotFrame
tk = base.tk
from detector.plots import detplots as plots

class Linearity(plots.Linearity):
    kw_errorbar = {"axes":"linearity"}
    kw_plotfit  = {"axes":"linearity"}
    kw_plot     = {"axes":"linearity"}
    kw_allaxes  = {"linearity":{"figure":"linearity"}}
    def initData(self, data):
        self.on_refresh = base.Command()
        self.data = data
                
        data.on_carac_polar_illumination_change.append(self.refresh)
        data.on_section_change.append(self.refresh)
        data.on_freduce_change.append(self.refresh)
        data.on_pixel_change.append(self.refresh)    
    
    def alllinearity(self):
        axes = self.get_axes("linearity")
        axes.clear()
        self.all(yrange  = self.data.getSignalRange())
        axes.figure.canvas.draw()
    def refresh(self):
        data = self.data
        kw = dict(            
            section = data.section
        )
        if data.freduce is None:
            kw["pixel"] = data.config["pixel"]
        else:
            kw["freduce"] = data.freduce
        
        self.setData(data.carac_polar_illumination, **kw)
        self.on_refresh()
        
        
class LinearityPlotFrame(BasePlotFrame):
    def __init__(self, parent, data, **kwargs):
        self.data = data
        
        plot = Linearity()
        plot.initData(data)
        self.plot = plot
        BasePlotFrame.__init__(self, parent, plot.alllinearity, plot.get_axes("linearity").figure)
        
        plot.on_refresh.append(self.replot)
        
    def refresh(self):
        return self.plot.refresh()
        

class LinearityFrame(base.BaseFrame):
    def __init__(self, parent, data, **kwargs):
        base.BaseFrame.__init__(self, parent, **kwargs)
        self.data = data
        
        self.plotFrame = LinearityPlotFrame(self, data)
        
        self.freduceFrame = FreduceFrame(self, data)

        self.plotFrame.pack(side=tk.TOP)
        self.freduceFrame.pack(side=tk.TOP)
        
    def refresh(self):
        return self.plotFrame.refresh()
    def replot(self):
        return self.plotFrame.replot()
    
