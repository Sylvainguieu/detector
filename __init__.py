import config
from base import (get_class,
                  get_entity_type, get_instrument_type, 
                  get_entity_class,
                  get_io_class, get_instrument_subclasses, 
                  get_mutual_subclasses, 
                  get_io_type, get_mutual_type, 
                  getarray, getlist,
                  DATA, SWITCH, LIST, TABLE, PRIMARY,                  
                  update_instrument_space,
                  update_entities
                  )
from plots import detplots2 as plots
from .io import opener
from .io import fits
import entities

