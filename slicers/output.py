from .grid import *

class OutputSlicer(BaseSlicer, GridSliceUtilities):
    """ slicer for detector with several outputs
    attributes are
        outputnum = int or None, the number of the output starting from 0
                    if None refer to the full frame
        size = 2xTuple The size of the box inside the output
        pos  = 2xTuple position of the box inside the output

        ref  = string, box pixel string reference 'bl', 'tr', 'center' etc
               gives the position of the bos inside the output.
               The position can be than adjusted with pos
        inslice = bool, (default True) return result in slice
        noerror = if True do not return OutsideError if the box is
                  outside the output.
    """
    imgdim = TupleProperty("_imgdim", (255,320), doc="image dimension")
    noutput = TupleProperty("_noutput",(1,8))
    outputnum = IntProperty("_outputnum",None)
    size = TupleProperty("_size", None)
    pos  = TupleProperty("_pos",(0,0))
    ref  = RefProperty("_ref", "cc", doc="String, position of the referencepixel")
    inslice = BoolProperty("_inslice", True, doc="if inslice is True return the index in a tuple of slice. win must be set")
    noerror = BoolProperty("_noerror", False)

    def _get_attrs(self):
        return ["outputnum", "size", "pos", "ref", "inslice",
        "imgdim", "noutput", "noerror"]

    def get_index(self):
        (outputnum, size, pos, ref,
         inslice, imgdim, noutput, noerror) = self._get_default()

        inside = None
        if outputnum is None:
            if size is None:
                size = imgdim
            if not noerror:
                inside = ( (0,imgdim[0]), (0,imgdim[1]) )

            refpix = ref2offset(ref, imgdim)
            refpix = pos[0]-refpix[0], pos[1]-refpix[1]
            return GridSlicer(
                        grid=(1,1), win=0, inslice=inslice,
                        size=size, refpix=refpix, ref=ref,
                        inside=inside
                        )()
        else:
            step = imgdim[0]/noutput[0], imgdim[1]/noutput[1]
            if size is None:
                size = step
            if not noerror:
                inside = GridSlicer(grid=noutput, win=outputnum,
                       inslice=inslice, step=step,
                       size=step, refpix=(0,0),
                       ref="bl")
                inside = [(inside[i].start,inside[i].stop) for i in range(2)]

            refpix = ref2offset(ref, step)
            refpix = pos[0]-refpix[0], pos[1]-refpix[1]

            return GridSlicer(grid=noutput, win=outputnum,
                       inslice=inslice, step=step,
                       size=size, refpix=refpix,
                       ref=ref, inside=inside
                       )()

    def outputs(self):
        (outputnum, size, pos, ref,
         inslice, imgdim, noutput, noerror) = self._get_default()
        step = imgdim[0]/noutput[0], imgdim[1]/noutput[1]
        size = step
        return  GridSlicer(
                          grid=noutput,
                          step=step,
                          size=size, refpix=(0,0),
                          ref="bl", inside=inside
                       )

