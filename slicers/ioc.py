#debug
#from . import grid
#reload(grid)
import numpy as np

from ..datax import DataX
from .grid import (GridSliceUtilities, GridSlicer,
                   SliceProperty
                   )
from .base import (BaseSlicer, ref2offset,
                   TupleProperty, StringProperty,
                   IntProperty, FloatProperty, BoolProperty,
                   RebuildProperty, RefProperty,
                   BaseSlicer, IntNoneProperty, StringNoneProperty)


def with_decorator(prop):
    def _with_property(self, value):
        kw = {prop:value}
        return self.new(**kw)
    return _with_property

class IOCSlicer(BaseSlicer, GridSliceUtilities):
    """
    Integrated Optic Componant Slicer.
    It returns y,x coordinates of an integrated outputs.

    Any change of the following attribute will rebuild the slicer in place if
    necessary.

    Use the  ioc.new( attr1=val1, attr2=val2 , ...)

    Attributes are:
    ---------------
        outputs = (int) number of componant output (e.g. 12,24)

        dispersion = (int) number dispersion chanels

        wollaston  = (int) number of wollaston window separation

        pos or refpixel = (y,x tuple of int) position of the reference pixel

        ref = (string) Determine the relative position of the refpixel inside the
                       detector. Default is "lb" for left-bottom
                       value can be e.g.: "rt"="tr="righttop"="topright"
                                          "ct"="tc"="centertop"="topcenter"
                                          "cc"="c"="center"="centercenter"
                                          etc .....

        disp_ref = (y,x tuple of int) position of the zero dispertion pixel,
                   relative to the the refpixel pos

        woll_ref = (y,x tuple of int) position of the left polarisation relative
                   to the refpixel pos.

        doutputs = (int) number of pixel separations of outputs

        dwollastons = (dy,dx tuple of int) pixel separation of wollaston in y and x

        imgdim = (y,x tuple of int) image (detector) dimention

        axis = (0 or 1) if 1 the IOC is in vertical direction
                       (outputs in y direction, dispersion in x)
                        0 is the contrary

        win = (int) limit the window number
        base = (int) limit to that base number
        tel = (int) limit to that telescope number
        disp = (int) limit to that dispersion chanel
        polar = (string) limit to that polar

    More Attribute:
    ---------------
        mapping = a Mapping instance witch define the mapping of
                  the IOC componant.

    Methods:
    --------
        new(attr1=val1, ..) : Return a new instance of the slicer but modified
                              attributes.

        pixel(pix) : return the (y,x) tuple coordinate of the pix'em pixel

        envelop() : return a box slicer containing the ioc

        rectangles() : return a list of [(x,y), width, height] ready to be
                       parsed in mathplotlib Rectangle object
                       e.g.:
                           import matplotlib.pyplot as plt
                       [plt.Rectangle(*r, fill=fill, **kwargs) for r in ioc.rectangle()]

        with_base(base), with_win(win), with_tel(tel), with_disp(disp),
        with_polar(polar)  equivalent of new(base=base), new(win=win), etc ....


    Example
    -------
    ## the FREE  and GRISM slicer for the PIONIER instrument
    from detector.misc.mapping import newMapping
    from detecto.slicers.ioc import IOCSlicer

    mapABCD = newMapping(
                      [1,1,1,1,2,2,1,1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3],
                      [2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4],
                      [1,1,1,1,2,2,3,3,3,3,4,4,4,4,5,5,5,5,2,2,6,6,6,6],
                      [t*np.pi for t in [0, 1,   1.6,   0.6,
                                         0, 1,
                                         0, 1,  1.6 ,   0.6,
                                         0, 1,  1.83,   0.83,
                                         0, 1,   1.6,   0.6,
                                         1.6,   0.6 ,
                                         0,      1,   1.6,   0.6]]
                  )

    free  = IOCSlicer( pos=(58,248), outputs=24,
                        dispersion=1,
                        wollastons=1 , doutputs=8,
                        imgdim=(255,320),
                        ref = "bl",
                        mapping=mapABCD
            )
    grism = free.new(dispersion=6)

    img = np.random.random((255,320))
    img[grism] # return the value of the grism ioc position on the detector

    """

    outputs    = IntProperty("_outputs",24)
    dispersion = IntProperty("_dispersion",6)
    wollastons  = IntProperty("_wollastons",2)
    pos = TupleProperty("_pos", (0,0))
    refpixel = TupleProperty("_pos", (0,0))

    disp_ref = TupleProperty("_disp_ref", (0,0))
    woll_ref = TupleProperty("_woll_ref", (0,0))
    doutputs = IntProperty("_doutputs", 8)
    dwollastons = TupleProperty("_dwollastons", (10,0))

    imgdin   = TupleProperty("_imgdim", (255,320))

    axis     = IntProperty( "_axis", 1)

    win  = IntNoneProperty("_win", None)
    base = IntNoneProperty("_base", None)
    polar = StringNoneProperty("_polar", None)
    tel   = IntNoneProperty("_tel", None)
    disp = IntNoneProperty("_disp", None)
    phin = RebuildProperty("_phin", None)

    axes = RebuildProperty("_axes",["win", "disp"])
    mapping = RebuildProperty("_mapping", None)
    ref = RefProperty("_ref", "bl")
    axes_img = ["disp"]

    def _get_attrs(self):
        return ["pos", "win", "disp", "base", "polar", "tel", "phin", "outputs", "dispersion",
                "wollastons", "disp_ref",
                "woll_ref", "doutputs", "dwollastons",
                "imgdim",  "axis", "base", "polar", "tel", "mapping", "ref"]

    def get_index(self):
        base, polar, tel, disp, win, phin = self._get_default("base", "polar", "tel",
                                                        "disp", "win", "phin")

        if (base is not None) or (polar is not None) \
          or (tel is not None) or (phin is not None):
            if self.mapping is None:
                return KeyError("slicer has no mapping for key, 'base','polar' or 'tel'")

            #if pixel is not None:
            #    return KeyError("Keyword conflict pixel not compatible with one of 'base','polar' or 'tel'")
            #win     = win_idx
            win_idx = self.mapping(tel=tel, base=base, polar=polar, phin=phin)
            if win is not None:
                for w in win:
                    if not win in win_idx:
                        return KeyError("Keyword win=%s not compatible with tel=%s base=%s"%(win,tel, base))
                win_idx = win
        else:
            win_idx = win



        disp_ref =  list(self.disp_ref)
        dispersion = self.dispersion

        ref = self.ref
        nwin = self.wollastons*self.outputs
        grid = (self.outputs, self.wollastons)
        size = (1, dispersion)

        if self.axis:
            refpix = [self.pos[i]+disp_ref[i]+self.woll_ref[i] for i in range(2)]
        else:
            refpix = [self.pos[i]+disp_ref[not i]+self.woll_ref[not i] for i in range(2)]

        step  = (self.doutputs, self.dwollastons[1])
        delta = (self.dwollastons[0], 0)

        if not self.axis:
            #### need to inverse directions
            grid = grid[::-1]
            step = step[::-1]
            size = size[::-1]
            delta = delta[::-1]

        gr =  GridSlicer(
                          grid=grid, step=step, size=size,
                          refpix=refpix, delta=delta, ref=ref
                          )
        y,x = gr

        tr = [tuple(["win"]+gr.axes_win), ("disp",)+tuple(gr.axes_img)]
        y, x = [v.transform(tr) for v in (y,x)]

        if win_idx is not None:
            y, x  = tuple(v.section(win_idx, "win") for v in [y,x])
        if disp is not None:
            y, x  = tuple(v.section(disp , "disp") for v in [y,x])

        return y ,x


    def rotate(self, **kwargs):
        """ create a new slicer Instance with the IOC rotate to 90 degrees """
        kwargs["axis"] = not self.axis
        return self.new( **kwargs )

    with_base = with_decorator("base")
    with_tel = with_decorator("tel")
    with_disp = with_decorator("disp")
    with_polar = with_decorator("polar")
    with_win = with_decorator("win")
    with_phin = with_decorator("phin")



