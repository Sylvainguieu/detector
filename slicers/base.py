import numpy as np

class SliceProperty(object):
    """ A property for Slicer object
    value of the attribute (attr) of the instance Slicer is getted
    and setted by this object instance.
    """
    attr = None
    doc = ""
    rebuild = False
    @staticmethod
    def parser(obj,value):
        return value

    def __init__(self, attr, default, parser=None, doc=None, rebuild=None):
        self.attr = attr
        self.default = default
        if rebuild is not None:
            self.rebuild = rebuild

        if parser is not None:
            self.parser = parser
        if doc:
            self.__doc__ = doc

    def __get__(self, instance, ctype=None):
        return getattr(instance, self.attr, self.default)

    def __set__(self, instance, value):
        value = self.parser(instance,value)

        old_value = getattr(instance, self.attr, self.default)
        setattr(instance, self.attr, value)
        if self.rebuild and not instance._is_builder_stoped():
            try:
                instance._build()
            except Exception as e:
                setattr(instance, self.attr, old_value)
                raise e



class RebuildProperty(SliceProperty):
    """
    A Slicer property that trigger a instance._build() by default
    """
    rebuild = True

class TupleProperty(RebuildProperty):
    """
    A Slicer property that trigger a instance._build().
    Plus is the value is a scalar, it is converted to a the tuple value,value
    """
    @staticmethod
    def parser(obj,value):
        if not hasattr(value, "__iter__"):
            return value, value
        return value

class StringProperty(RebuildProperty):
    """
    A Slicer property that trigger a instance._build().
    value is converted to string
    """
    @staticmethod
    def parser(obj,value):
        return str(value)

class StringNoneProperty(RebuildProperty):
    """
    A Slicer property that trigger a instance._build().
    value is converted to string
    """
    @staticmethod
    def parser(obj,value):
        if value is None:
            return None
        return str(value)


class IntProperty(RebuildProperty):
    """
    A Slicer property that trigger a instance._build().
    value must be int
    """
    def parser(self, obj,value):
        if not isinstance(value, int):
            raise ValueError("%s must be an integer"%self.attr)
        return value

class IntNoneProperty(RebuildProperty):
    """
    A Slicer property that trigger a instance._build().
    value must be int
    """
    def parser(self, obj,value):
        if (not isinstance(value, int)) and (value is not None):
            raise ValueError("%s must be an integer"%self.attr)
        return value



class FloatProperty(RebuildProperty):
    """
    A Slicer property that trigger a instance._build().
    value is converted to float
    """
    @staticmethod
    def parser(obj,value):
        return float(value)

class BoolProperty(RebuildProperty):
    """
    A Slicer property that trigger a instance._build().
    value is converted to bool
    """
    @staticmethod
    def parser(obj,value):
        return bool(value)



class BaseSlicer(list):
    """ Base class for slicer
        methods _get_attrs and get_index should be implemented
    """


    def _get_attrs(self):
        """ return a list of slicer attribute """
        raise NotImplemented()

    attrs = property(fget=_get_attrs)

    def get_index(self):
        raise NotImplemented()

    def _build(self):
        for i,idx in enumerate(self.get_index()):
            self[i] = idx

    def _stop_builder(self):
        """ stop to build the object during attr asignment """
        self._builder_stoped = True

    def _release_builder(self):
        """ release an eventual stop triggered by _stop_builder"""
        self._builder_stoped = False

    def _is_builder_stoped(self):
        """ check if the building is stoped """
        return self._builder_stoped

    def _rebuild_kwargs(self, *args, **kwargs):
        """
        from args and kwargs
        rebuild and return the kwargs with positional argument taken
        in the order of the list returned by _get_attrs()
        """
        attrs = self._get_attrs()
        if len(args)>len(attrs):
            TypeError("Takes only %d attribute (%d given)"%(len(attrs),len(args)))
        for i,arg in enumerate(args):
            if attrs[i] in kwargs:
                raise TypeError("got multiple values for keyword argument '%s'"%attrs[i])
            kwargs[attrs[i]] = arg
        return kwargs

    def _set_attrs(self, *args, **kwargs):
        """
        stop object building and set the attribute from positional and
        kwargs argument.
        """
        kw = self._rebuild_kwargs(*args, **kwargs)
        attrs = self._get_attrs()

        rebuild = False


        for attr in kw:
            if not attr in attrs:
                raise TypeError("got an unexpected keyword argument '%s'"%attr)

        self._stop_builder()
        try:
            for attr in attrs:
                if attr in kw and kw[attr] is not None:
                    value = kw.pop(attr)
                    try:
                        setattr(self, attr, value)
                    except ValueError as e:
                        self._release_builder()
                        e.message = "setting attr '%s' : "%attr + e.message
                        raise e
                    rebuild = True
        finally:
            self._release_builder()
        return rebuild

    def _get_default(self, *args, **kwargs):
        """ return a list of attribute value in the _get_attrs()
        list order if no positional argument are given.
        optional **kwargs overwrite the object attributes.

        example of use:
        grid, size = self._defaul("grid", "size", grid=(2,2))
        """
        values = []
        keys = args if len(args) else self._get_attrs()
        for k in keys:
            if not k in kwargs or kwargs[k] is None:
                values.append(getattr(self,k))
            else:
                values.append(kwargs[k])
        return values


    def __call__(self, *args, **kwargs):
        """ return the tuple of this object or a copy of this object
        if any new args or kwarg are given
        """
        if not len(args) and not len(kwargs):
            return tuple(self)

        return self.new(*args, **kwargs).get_index()

    def __init__(self, *args, **kwargs):
        list.__init__(self,  [slice(0,None), slice(0,None)] )
        if not self.set(*args, **kwargs):
            self._build()

    def set(self, *args, **kwargs):
        """
        set attributes and then rebuild the object.
        return True if build has been triggered
        """
        if self._set_attrs(*args, **kwargs):
            self._build()
            return True
        return False

    def pixel(self, pix):
        """ return the pixel coordinate of the pix'em pixel found in the indexes
        """
        first = self[0]
        if isinstance(first, slice):
            raise Exception("pixel not available yet when elements are slice")
        return self[0].flat[pix], self[1].flat[pix]



    def new(self, *args, **kwargs):
        """ create a new instance of this object.
        all attribute are copied but can be overwriten by keyword
        arguments.
        """
        kwargs = self._rebuild_kwargs(*args, **kwargs)
        for k in self._get_attrs():
            kwargs.setdefault(k, getattr(self, k))
        return self.__class__(**kwargs)



BL = ["bl", "bottomleft", "lb", "leftbottom"]
BR = ["br", "bottomright", "rb", "rightbottom"]
TL = ["tl", "topleft", "lt", "lefttop"]
TR = ["tr", "topright", "rt", "righttop"]
TC = ["tc", "topcenter", "ct", "centertop"]
BC = ["bc", "bottomcenter", "cb", "centerbottom"]
CL = ["cl", "centerleft", "lc", "leftcenter"]
CR = ["cr", "centerright", "rc", "rightcenter"]
CC = ["c", "center", "cc", "centercenter"]
VALIDS = [BL,BR,TL,TR,TC,BC,CL,CR,CC]

def ref2offset(ref, size):
    """ return the offset position of a box of size SIZE, given a string
    refering the position of the reference pixel.
    The offset is 0,0 if ref is 'bl' or "bottomleft"

    Valid values for ref are:
    {0}
    """

    offset = (0,0)
    delta = (size[0]/2, size[1]/2)

    if ref in BL:
        return 0, 0
    if ref in BR:
        return 0, -size[1]

    if ref in TL:
        return -size[0], 0

    if ref in TR:
        return -size[0], -size[1]

    if ref in TC:
        return -size[0], -int(size[1]/2)

    if ref in BC:
        return 0, -int(size[1]/2)

    if ref in CL:
        return -int(size[0]/2), 0

    if ref in CR:
        return -int(size[0]/2), -size[1]

    if ref in CC:
        return -int(size[0]/2), -int(size[1]/2)

    elif isinstance(ref, (tuple,list)) and len(ref)==2:
        return -ref[0], -ref[1]
    else:
        raise KeyError("ref should be on of %s or a 2 element list or tuple"%(VALIDS,))
ref2offset.__doc__ = ref2offset.__doc__.format( "\n    ".join([" or ".join(v) for v in VALIDS]))

class RefProperty(StringProperty):
    valid = reduce( lambda x,y: x+y, VALIDS)
    def parser(self, slicer, ref):
        if not ref in self.valid:
            raise ValueError("should be one of %s"%(self.valid,))
        return ref


