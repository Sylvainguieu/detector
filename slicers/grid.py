#debug
#from . import base
#reload(base)

from ..datax import DataX
from .base import (BaseSlicer, ref2offset, SliceProperty,
                   TupleProperty, StringProperty,
                   IntProperty, FloatProperty, BoolProperty,
                   RebuildProperty, RefProperty, IntNoneProperty
                   )

import numpy as np

def cell_indexes(dim, shape, sls):
    if dim:
        return np.tile( np.r_[sls] , shape[not dim] ).reshape(shape)
    else:
        return np.repeat( np.r_[sls], shape[not dim] ).reshape(shape)

def get_cell_slice(i, cell, refpix, size, step, space, delta, offset, inside=None):
    true_step = size[i]+space[i] if step[i] is None else step[i]

    start = refpix[i]+cell[i]*true_step+offset[i]+delta[i]*cell[not i]
    stop  = start+size[i]
    if inside:
        if start<inside[i][0] or stop>inside[i][1]:
            raise OutsideError("cell %s -> %d,%d outside %s"%(cell,start,stop, inside[i]))

    return slice(start,stop)

def get_cell_indexes(i, cell, refpix, size, step, space, delta, offset, inside=None):
    sls = get_cell_slice(i, cell, refpix, size, step, space, delta, offset, inside)
    return cell_indexes(i, size, sls)

def parse_win(slicer, win):
    if isinstance(win,int):
        return np.unravel_index(win, slicer.grid)
    return win

class WinProperty(RebuildProperty):
    @staticmethod
    def parser(slicer, win):
        if isinstance(win, int):
            return np.unravel_index(win, slicer.grid)
        return win
class InsideProperty(RebuildProperty):
    @staticmethod
    def parser(slicer, inside):
        try:
            N = len(inside)
        except:
            ValueError("inside has no len")

        if N!=2:
            raise ValueError("inside should be of sie 2x2")
        for i in inside:

            try:
                N = len(i)
            except:
                ValueError("inside should be of sie 2x2")
            if N!=2:
                raise ValueError("inside should be of sie 2x2")

        return inside

class GridSliceUtilities(object):
    def get_min_max(self, indice):
        """Return min,max tuple of numpy array of the grid """
        i = self[indice]
        if isinstance(i, slice):
            if i.step and i.step<0:
                return np.array([i.stop]), np.array([i.start-1])
            return np.array([i.start]), np.array([i.stop-1])

        if isinstance(i, int):
            return np.array([i]),np.array([i])
        axs = [ax for ax in self.axes_img if ax in i.axes]
        if axs:
            return i.reduce(np.min, axs), i.reduce(np.max, axs)
        return i, i


    def rectangles(self, offset=(0,0), scale=(1,1)):
        """ return a list of list of rectangle each elements are
        ready to be parsed inside myplotlib Rectangle function

        Properties
        ----------
        offset = (0,0) eventual offset of the boxes
        scale = (1,1)  rescale of boxes
        """
        xmin, xmax = (v.flat for v in self.get_min_max(1))
        ymin, ymax = (v.flat for v in self.get_min_max(0))
        N = len(xmin)
        return [[(xmin[i]+offset[1], ymin[i]+offset[0]), \
                 (xmax[i]-xmin[i]+1)*scale[1], \
                 (ymax[i]-ymin[i]+1)*scale[0]] for i in range(N)]

    def envelop(self, inslice=True, cl=None):
        """ return the cell enveloping the actual slicer object in a new
            GridSlicer or any object of class defined with the cl keyword.

            Property:
            ---------
              inslice = True  if True return the box in a tuple of slice
                              if false in array pixel coordinates
        """
        xmin, xmax = self.get_min_max(1)
        ymin, ymax = self.get_min_max(0)

        xmin, xmax = xmin.min(), xmax.max()
        ymin, ymax = ymin.min(), ymax.max()
        cl = cl or GridSlicer
        return cl(
                  refpix=(ymin,xmin), grid=(1,1), ref="bl",
                  size=(ymax-ymin, xmax-xmin), flat=True,
                  inslice=inslice, win=0
                  )

    def patches(self, offset=(0,0), scale=(1,1), fill=False, **kwargs):
        """ return mathplotlib patches of the grid contours """
        global plt
        try:
            plt
        except:
            import matplotlib.pyplot as plt
        rect = self.rectangles(offset=offset, scale=scale)
        return [ plt.Rectangle( *r, fill=fill, **kwargs) for r in rect ]

    def plot(self, offset=(0,0), scale=(1,1), fill=False, axes=None,  **kwargs):
        """ Plot the rectangle contours with the mathplotlib rectange function
        """
        patches = self.patches(offset=offset,scale=scale, fill=fill, **kwargs)
        if axes is None:
            axes = plt.gca()
        return [axes.add_patch(p) for p in patches]

    def pixel(self, pix):
        """ return the pixel coordinate of the pix'em pixel found in the indexes
        """
        first = self[0]
        if isinstance(first, slice):
            return self.new(inslice=False).pixel(pix)


        return self[0].flat[pix], self[1].flat[pix]


class GridSlicer(BaseSlicer, GridSliceUtilities):
    """ An image slicer that define a regular grid on a 2d image
    attributes are (in this order):

      refpix = The reference pixel coordinate default (0,0)
      grid   = The grid sizes default (1,1).
               If one of the grid size is 0, flat is set to True

      step   = The step sizes, distance between each cells of the grid
      size   = Size of each cells default (1,1)
      space  = space separation between cell used only
               if step is None in that dimention
      delta  = addiiotnal offset between columns or lines:
                the column #3 will have an offset of 3*delta[0] compare
                to the first one.
                e.g:  GridSlicer( delta=(1,1), grid=(5,5), size=(1,1) )
                      gives a diagonale.
      ref    = string that define the refpixel posision e.g. "tl", "cc", "cl"
               default is "bl" for bottom left
      win    = if not None but int return only the cell of number win
      inslice = if win is set, return a tuple of slices intead of tuple of array
      axes_img = a list of 2 string defining the axes name of returned indexes
                  for cells default is  ["winy", "winx"]
      axes_win = a list of 2 string that define axis name of image portion
                 default is "y", "x" of course
      axes_flat = a string that define the axis name to replace axes_win in case
                  flat is True or one of the grid number is 0
      flat = if True return the window axes in a flat vector
              result will have shape of (grid[0]*grid[1], size[0], size[1])
              instead of grid[0], grid[1], size[0], size[1]
      inside = ( (y0,y1), (x0,x1) ) 2x2 boundary tuple for the grid. If any cell
               is outside, return OutsideError (subclass of ValueError)
    """
    refpix = TupleProperty("_refpix", (0,0), doc="The reference pixel coordinate")
    grid = TupleProperty("_grid", (1,1), doc="(Ny,Nx) grid size")
    step = TupleProperty("_step", (None,None), doc="grid step (separation between cells same corner)")
    size = TupleProperty("_size", (1,1), doc="size of cells")
    space = TupleProperty("_space", (0,0), doc="space separation between cell used only if step is None in that dimention")
    delta = TupleProperty("_delta", (0,0), doc="addiiotnal offset between columns or lines")
    ref  = RefProperty("_ref", "bl", doc="String, position of the referencepixel")
    axes_img = RebuildProperty("_axes_img", ["y", "x"])
    axes_win = RebuildProperty("_axes_win", ["winy", "winx"])
    axis_flat = StringProperty("_axis_flat", "win")
    flat = BoolProperty("_flat", False, doc="coordinate in a Nx,Ny grid if not flat else in Nx*Ny vector")

    win  = WinProperty("_win", None, doc="return only the window(=cell) of number win")
    inslice = BoolProperty("_inslice", False, doc="if inslice is True return the index in a tuple of slice. win must be set")
    inside = InsideProperty("_inside", None , doc="boundary of the grid default is None")

    def _get_attrs(self):
        return ["refpix","grid", "step", "size", "space", "delta",
                "ref", "axes_img", "axes_win", "axis_flat",
                "flat", "win", "inslice", "inside"]

    def get_index(self):
        (
         refpix, grid, step, size, space, delta,
         ref , axes_img, axes_win, axis_flat,
         flat , win, inslice, inside
         ) = self._get_default()

        offset = ref2offset(ref,size)
        if win is not None:
            if inslice:
                return tuple(get_cell_slice(i, win ,refpix,size,
                                         step,space, delta, offset, inside) for i in range(2))
            else:
                return tuple(DataX(get_cell_indexes(i, win ,refpix,size,
                                                     step, space, delta, offset,
                                                     inside), axes_img) for i in range(2))


        if inslice:
            raise Exception("inslice can be true only if win is not None")

        if grid[0] is 0:
            grid = 1, grid[1]
            flat = True
        elif grid[1] is 0:
            grid = grid[0], 1
            flat = True
        elif grid is (0,0):
            raise ValueError("grid cannot be 0,0")

        indexes = tuple(
                    DataX(np.ndarray(grid+size, dtype=int), axes_win+axes_img) for i in range(2)
                  )

        for j in range(grid[0]):
            for k in range(grid[1]):
                cell = [j,k]
                for i in range(2):
                    indexes[i][j,k]= get_cell_indexes(i, cell,refpix,size,
                                                      step,space, delta, offset, inside)

        if flat:
            return tuple(
                    indexes[i].transform([(axis_flat, axes_win[0], axes_win[1]),None] ) for i in range(2)
                   )
        return indexes

class OutsideError(ValueError):
    pass




