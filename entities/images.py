from ..base import ListEntity, update_entities, getarray
from ..capabilities import DetcaracCapability
from ..capabilities.fft import FFTCapability
from ..plots.baseplot import Images as ImagesPlot
from ..plots.baseplot import Carac as CaracPlot

class ImagesEntity(ListEntity, DetcaracCapability, FFTCapability, ImagesPlot):
    """ list of 'combined' object (average signal and variance).
    """
    axes = ["expo"]
    subclasses = [ (True,"combined") ]



##
# need to set it here for carac
#for k in ["signal", "sigma", "sigma2", "sigma3", "sigma4", "number",
#          "signalerror", "sigma2error", "signalvariance", "sigma2variance"
#          ]:
#    ImagesEntity._add_data_capability(k, clsname=k, add_scalar=True)
#
#ImagesEntity._add_data_capability("centroid", clsname="jitter")
#
#for k in ["dit", "ndit", "polar", "illumination", "framerate"]:
#    ImagesEntity._add_key_capability(k)

def polarwrapper(instance, lst):
    lst = instance.obj.__class__(lst)
    if len(lst.get_polar.set())<2:
            return instance.obj._get_class("images")(lst)
    return lst


getpolar = getarray("get_polar", on=True,
                        selfwrapper=polarwrapper,
                        name="polar"
                       )

class CaracEntity(ImagesEntity, CaracPlot):
    """ list of 'combined' object (average signal and variance).
        same as 'images' except that it can have detector data with
        different polarisation.
        therefore, getPtc, getLinearity, getDetCarac methods will return
        a list of det carac.
    """
    get_polar = getpolar
    polar = getpolar



    def getPtc(self, **kwargs):
        """ Return the ptcHDUList for each polar of the set """
        return self._get_class("ptclist")(
            [self.get_polar.choose(p).getPtc(**kwargs) for p in self.get_polar.set()]
        )
    def getLinearity(self, **kwargs):
        """ Return the ptcHDUList for each polar of the set """
        return self._get_class("linearitylist")(
            [self.get_polar.choose(p).getLinearity(**kwargs) for p in self.get_polar.set()]
        )
    def getDetCarac(self, **kwargs):
        return self._get_class("detcaraclist")(
            [self.get_polar.choose(p).getDetCarac(**kwargs) for p in self.get_polar.set()]
        )
    def getMinDitSigma(self, **kwargs):
        out = []
        for p in self.get_polar.set():
            out += self.get_polar.choose(p).get_dit.choose(np.min)

        return self._get_class("carac")(
            out
        ).getSigma(**kwargs)



update_entities(images=ImagesEntity, carac=CaracEntity)
