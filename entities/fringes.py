from ..base.entities import SwitchEntity, ListEntity
from .image import ImageEntity
from ..base import update_entities, getarray
from .. import computing
from .imagingdata import ImagingDataEntity
import numpy as np


class FringesEntity(SwitchEntity):
    """ ESO style Interferometric fits file """
    subclasses = [
                  ({"IMAGING_DATA","IMAGING_DATA_SC"}, "imagingdata"),
                  ("IMAGING_DATA_SC", "imagingdatasc"),
                  ("IMAGING_DATA_FT", "imagingdataft"),
                  (0, "primary")
                 ]
    #_subclasses_model = ("IMAGING_DATA",)
    @getarray
    def Windows(self, *args, **kwargs):
        data = self.getWindows(*args,**kwargs)
        if ("window" in data.axes) and ("y" in data.axes):
            data = data.transform([("y","window","y"), None])
        if ("x" in data.axes) and ("y" in data.axes):
            data = data.transform([None, "y", "x"])

        return self._get_class("cubetime")(data,
                                      self[0].header.copy()
                                      )

class FringesListEntity(ListEntity):
    @getarray
    def Windows(self, *args, **kwargs):
        return self._get_class("images")(
                           [f.Windows(*args, **kwargs) for f in self]
                         )


update_entities(fringes=FringesEntity, fringeslist=FringesListEntity)
