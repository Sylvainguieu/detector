"""
Entity for derived carac product (gain, noise, etc ... )
"""

import numpy as np

from .. import config
from .image import ImageEntity

from ..base import update_entities, TableEntity, getarray, PrimaryEntity, getarray

class PtcPrimary(PrimaryEntity):
    get_polar = getarray.key("POLAR", name="polar")
    get_illumination = getarray.key("ILLUM", name="illumination")

class GainEntity(ImageEntity):
    """ Image of system gain computed by ptc fit in [e-/ADU] """
    _default_name_ = "GAIN"
    getGain = getarray.data()


class MGainEntity(ImageEntity):
    """ Multiplicative Gain [without unit] HDU Image"""
    _default_name_ = "MGAIN"
    getMgain = getarray.data()

class Noise2Entity(ImageEntity):
    """ Fits HDU containing the square of the noise [ADU^2] computed from intercept of PTC fit """
    _default_name_ = "NOISE2"
    getNoise2 = getarray.data()


class NoiseEntity(ImageEntity):
    """ Image of the detector noise in ADU, e.i. the standart deviation of a cube
    it is taken from the cube/image with the minimum DIT and/or minimum flux in a sery
    of exposure.
    """
    _default_name_ = "NOISE"
    getNoise = getarray.data()

class ENoiseEntity(ImageEntity):
    """
    Image of the detector Noise in e-, e.i. the standart deviation of a cube
    it is taken from the cube/image with the minimum DIT and/or minimum flux in a sery
    of exposure.
    """
    _default_name_ = "ENOISE"
    getEnoise = getarray.data()

class PTCNumberEntity(ImageEntity):
    """ Image of the number of point used to compute the PTC """
    _default_name_ = "PTCNUMBER"
    getNumber    = getarray.data()
    getPtcnumber = getarray.data()

class FluxEntity(ImageEntity):
    _default_name_ = "FLUX"
    """
    Image of the incoming flux in ADU/s calculated from a linear fit of
    signal = flux*dit + bias
    """
    getFlux = getarray.data()

class EFluxEntity(ImageEntity):
    _default_name_ = "EFLUX"
    """
    Image of the incoming flux in e-/s calculated from a linear fit of
    signal = flux*dit + bias
    """
    getEflux = getarray.data()

class BiasEntity(ImageEntity):
    _default_name_ = "BIAS"
    """
    Image of the Bias  in ADU  calculated from a linear fit of
    signal = flux*dit + bias
    """
    getBias = getarray.data()

class EBiasEntity(ImageEntity):
    _default_name_ = "EBIAS"
    """
    Image of the Bias  in ADU  calculated from a linear fit of
    signal = flux*dit + bias
    """
    getEbias = getarray.data()

class MfEntity(ImageEntity):
    _default_name_ = "MF"
    """
    Image of the multiplicative Gain * excess noise
    calculated from the fraction of
    system gain for different polars.
    """
    getMf = getarray.data()


class ExcessnoiseEntity(ImageEntity):
    _default_name_ = "EXCESSNOISE"
    """
    Image of the Excess noise: fraction of MF computed
    from ratio of system gain with different polar and M computed
    from the ratio Flux.
    """
    getExcessnoise = getarray.data()

class LinNumberEntity(ImageEntity):
    """ Image of the number of point used to compute the PTC """
    _default_name_ = "LINNUMBER"
    getNumber = getarray.data()
    getLinnumber = getarray.data()

class PtcChi2Entity(ImageEntity):
    """ Chi2 map of the ptc fit """
    _default_name_ = "PTCCHI2"
    getChi2 = getarray.data()
    getPtchi2 = getarray.data()

class LinChi2Entity(ImageEntity):
    """ Chi2 map of the linear flux fit"""
    _default_name_ = "LINCHI2"
    getChi2 = getarray.data()
    getLinchi2 = getarray.data()

class KeysTableEntity(TableEntity):
    _default_name_ = "KEYWORDS"

classes = dict(gain=GainEntity, mgain=MGainEntity,
               ptcprimary=PtcPrimary, 
               noise2=Noise2Entity, noise=NoiseEntity,
               ptcnumber=PTCNumberEntity, flux=FluxEntity,
               bias=BiasEntity, ebias=EBiasEntity,
               mf=MfEntity, excessnoise=ExcessnoiseEntity,
               linnumber=LinNumberEntity, ptcchi2=PtcChi2Entity,
               linchi2=LinChi2Entity, keystable=KeysTableEntity
               )


update_entities(**classes)
