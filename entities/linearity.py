from .. import config
from ..plots.baseplot import Linearity as LinearityPlot
from ..plots.baseplot import LinearityList as LinearityListPlot
if config.debug: print "linearity.py"
import numpy as np
from ..base import SwitchEntity, ListEntity, update_entities, getarray

class LinearityEntity(SwitchEntity, LinearityPlot):
    """
    Detector Linearity caracterisation, contains:
     flux : slope of signal vs exptime in ADU/s
     bias : y intercept of signal vs exptime in ADU
     linchi2: chi2 of the linearity fit
     linnumber: number of point used by the fit for each pixel
    """

    subclasses = [
        ("FLUX",     ("flux","ptcprimary")),
        ("BIAS",     "bias"),
        ("LINCHI2",  "linchi2"),
        ("LINNUMBER", "linnumber"),
        ("KEYWORDS", "keystable")
    ]

class LinearityListEntity(ListEntity, LinearityListPlot):
    """
     A list of linearity caracterisation object
    """
    axes = ["polar"]
    subclasses  = [(True, "linearity")]
    @getarray
    def getGainm(self, zero_polar=np.min, **kwargs):
        """ Return the multiplicative gain
            zero_polar is the reference polar. by default take the minimun
        """
        polars = self.get_polar()
        zeros = self.get_polar.choose(zero_polar)

        if not len(zeros):
            raise ValueError("Cannot find data for polar %s"%zero_polar)
        return self.getFlux(**kwargs)/zeros[0].getFlux(**kwargs)





update_entities(linearity=LinearityEntity,
                         linearitylist=LinearityListEntity)

