
from .cube import CubeEntity
from ..base import update_entities, getarray

class FftEntity(CubeEntity):
    """ Cube of a image fft result"""
    _default_name_ = "fft"

class PsdEntity(CubeEntity):
    """Cube of a image powerspectral density result of fft """
    _default_name_ = "psd"
update_entities(fft=FftEntity,psd=PsdEntity)

