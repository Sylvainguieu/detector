from .. import config
import numpy as np
from ..base import DataEntity, update_entities
from ..capabilities.fft import FFTCapability
from ..plots.baseplot import Jitter as JitterPlot


class JitterEntity(DataEntity, FFTCapability, JitterPlot):
    """ Nx2 array of a jitter position """
    axes = ["time", "yx"]

update_entities(jitter=JitterEntity)
