from ..base.entities import SwitchEntity, ListEntity
from .image import ImageEntity
from ..base import update_entities, getarray
from .. import computing
import numpy as np

#####################################################
#
#  Misc Functions
#
####################################################
def _update_number_kwargs_for_error(Nkwargs):
    for k in Nkwargs:
        if k[-6:]=="reduce":
            if Nkwargs[k] in [np.mean, np.median]:
                Nkwargs[k] = np.sum
            elif Nkwargs[k] in [np.nanmean,  np.nanmedian]:
                Nkwargs[k] = np.nansum
            else: raise ValueError("computing error: wrong reduce function in %k expecting mean or median to comvert to sum got %s"%(k,Nkwargs[k]))




class SignalEntity(ImageEntity):
    """ Image of a time averaged signal """
    _default_name_= "SIGNAL"
    getSignal = getarray.data()


class SigmaEntity(ImageEntity):
    """ Standart deviation Image of a cube """
    _default_name_= "SIGMA"
    @getarray
    def getSigma2(self, **kwargs):
        sigma = self.getSigma(**kwargs)
        return sigma*sigma
    getSigma = getarray.data()


class Sigma2Entity(ImageEntity):
    """ Moment of order 2 Image of a cube """
    _default_name_= "SIGMA2"
    @getarray
    def getSigma(self, **kwargs):
        sigma2 = self.getSigma2(**kwargs)
        return np.sqrt(sigma2)
    getSigma2 = getarray.data()


class Sigma3Entity(ImageEntity):
    """ Moment of order 3 Image of a cube """
    _default_name_= "SIGMA3"
    getSigma3 = getarray.data()



class Sigma4Entity(ImageEntity):
    """  Moment of order 4 Image of a cube """
    _default_name_= "SIGMA4"
    getSigma4 = getarray.data()


class NumberEntity(ImageEntity):
    """Image of the number of point used to combine cube """
    _default_name_= "NUMBER"
    getNumber = getarray.data()


class CombinedEntity(SwitchEntity):
    """
    Contain a set of data used to compute detector caracterisation:
       - signal (averaged image)
       - sigma2 (variance = moment of order 2)
       - sigma3 (moment of order 3)
       - sigma4 (moment of order 4) the two latest are usualy used
          to compute error bars on sigma2.
       - number an image of the number of point used to compute the latest
         if not present look at the header the NDIT or equivalent

    """
    subclasses = [        
        ({"SIGNAL","PRIMARY",0}, ("signal", "primary")),
        ({"SIGMA2","M2"},"sigma2"),
        ({"SIGMA3","M3"},"sigma3"),
        ({"SIGMA4","M4"},"sigma4"),
        ({"NUMBER","N"} ,"number")
    ]

    @getarray
    def getSigma(self, **kwargs):
        return self["SIGMA2"].getSigma(**kwargs)
    @getarray
    def getSigma2variance(self, **kwargs):
        return self.getSigma2error(**kwargs)**2
    @getarray
    def getSignalerror(self, **kwargs):
        """ Return the Signal Error bar of a previously combined images
            std / sqrt(N)
            std is the standart deviation and the number of combined points
        """
        # Replace silently all the mean by sum
        Nkwargs = kwargs.copy()
        _update_number_kwargs_for_error(Nkwargs)
        return self.getSigma(**kwargs) / np.sqrt(self.getNumber(**Nkwargs))
    @getarray
    def getSignalvariance(self, **kwargs):
        return self.getSignalerror(**kwargs)**2

    @getarray
    def getSigma2error(self, **kwargs):
        """ Return the error on the moment of order 2 (variance)
        to function correctly the object must have the following method:
        getSigma2 -> Moment of order 2
        getSigma4 -> Moment of order 4
        getNumber -> Number of points used to compute moment
        """
        Nkwargs = kwargs.copy()
        _update_number_kwargs_for_error(Nkwargs)
        return computing.variance_error( self.getSigma2(**kwargs),
                                         self.getSigma4(**kwargs),
                                         self.getNumber(**Nkwargs)
                                     )
    @getarray
    def getNumber(self, **kwargs):
        """ Return a array of number of combined images
        look first at hdu "NUMBER" than "N" and if not
        built it from the keyword DET NDIT
        """
        names = [t.name for t in self]
        if "NUMBER" in names:
            return self["NUMBER"].getData(**kwargs)
        if "N" in names:
            return self["N"].getData(**kwargs)
        if hasattr(self, "get_ndit"):
            return self.get_ndit.asdata().getData(**kwargs)
        else:
            raise RuntimeError("need get_ndit attribute to compute number of frames")    
        return self.getkey.asdata("ESO DET NDIT").getData(**kwargs)



update_entities(signal=SignalEntity, sigma=SigmaEntity,
                sigma2=Sigma2Entity, sigma3=Sigma3Entity,
                sigma4=Sigma4Entity, number=NumberEntity,
                combined=CombinedEntity
                )


