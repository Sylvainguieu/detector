from .. import config
import numpy as np
from ..base import DataEntity, SwitchEntity, update_entities, getarray
from ..capabilities.fft import FFTCapability
from ..capabilities.jitter import JitterCapability
from .kwargs import ImageKwargs
from ..plots.baseplot import CubeImage as CubeImagePlot
from .. import computing



class CubeEntity(ImageKwargs, DataEntity, FFTCapability,
                 JitterCapability, CubeImagePlot):
    """cube where the two last dimention are y,x image.
       Usualy the  first dimension is time
    """
    def getSignal(self,**kwargs):
        """ Average the first axis of the cube and return data """
        return self.getData(**kwargs).mean(axis=self.axes[0])
    getSignal = getarray.data()
    getImage = getSignal
    @getarray
    def getMoment(self, moment, meandata=None, **kwargs):
        """ Compute the moment on the last dimention of the cube """
        return computing.moment(
            self.getData(**kwargs), moment,
            meandata=meandata,
            axis=self.axes[0]
        )

    def getSignalHeader(self):
        return self.getheader(copy=True)

    @getarray
    def getSignalerror(self, **kwargs):
        """ Return the Signal Error bar of a cube of  images
            std / sqrt(N)
            std is the standart deviation and N the size of the cube first dimension
        """
        shapes = self.getshape()
        return self.getData(**kwargs).std(axis=self.axes[0]) / np.sqrt(shapes[0])


    @getarray
    def getSigma(self,**kwargs):
        return self.getData(**kwargs).std(axis=self.axes[0])

    @getarray
    def getNumber(self, **kwargs):
        """ Return an image with a unique number: the axis len of the first axis dimension """
        shape = self.getshape()
        N    =  np.zeros(shape[1:], dtype=int)
        N[:] = shape[0]
        return self._return_array_data(N, kwargs, axes=self.axes[1:])

    @getarray
    def getSigma2(self, **kwargs):
        """ return sigma2, the moment of order 2 """
        return self.getMoment(2, **kwargs)

    @getarray
    def getSigma3(self, **kwargs):
        """ return sigma2, the moment of order 2 """
        return self.getMoment(3, **kwargs)

    @getarray
    def getSigma4(self, **kwargs):
        """ return sigma2, the moment of order 2 """
        return self.getMoment(4, **kwargs)

    @getarray
    def getSigma2error(self, **kwargs):
        """ Return the error on the moment of order 2 (variance)
        to function correctly the object must have the following method:
        getSigma2 -> Moment of order 2
        getSigma4 -> Moment of order 4
        getNumber -> Number of points used to compute moment
        """
        Nkwargs = kwargs.copy()
        _update_number_kwargs_for_error(Nkwargs)
        return computing.variance_error( self.getSigma2(**kwargs),
                                         self.getSigma4(**kwargs),
                                         self.getNumber(**Nkwargs)
                                     )

#########################################################
#
#
#
#########################################################


def _update_number_kwargs_for_error(Nkwargs):
    for k in Nkwargs:
        if k[-6:]=="reduce":
            if Nkwargs[k] in [np.mean, np.median]:
                Nkwargs[k] = np.sum
            else: raise ValueError("computing error: wrong reduce function in %k expecting mean or median to comvert to sum got %s"%(k,Nkwargs[k]))


def _remove_time_reduce(kwargs):
    """ Remove all the {key}_reduce keyword in kwargs with key inside config.timeaxes """
    for k in config.timeaxes:
        kwargs.pop(k,None)

def _get_time_axes(axes):
    """ Return the list of time axes contained in axes
    raise a key error if none are found
    """
    timeaxes = [ax for ax in axes if ax in config.timeaxes]
    if not len(timeaxes):
        raise KeyError("Cannot find time axis, looking for %s in %s"%(config.timeaxes, axes))
    return timeaxes

class CubeTimeEntity(CubeEntity):
    """ cube of time, y, x """
    axes = [config.TIME, config.Y, config.X]
    @getarray
    def getSignal(self,**kwargs):
        """ Average the time axes of the cube and return data """
        _remove_time_reduce(kwargs)
        timeaxes = _get_time_axes(self.axes)
        return self.getData(**kwargs).mean(axis=timeaxes)
    getImage = getSignal

    @getarray
    def getSignalerror(self, **kwargs):
        """ Return the Signal Error bar of a cube on the "time" axes
            std / sqrt(N)
            std is the standart deviation and the number of points on time axes
        """
        _remove_time_reduce(kwargs)
        timeaxes = _get_time_axes(self.axes)
        return self.getData(**kwargs).std(axis=timeaxes)/ np.sqrt(self.getsize(axis=timeaxes))
    @getarray
    def getSignalvariance(self, **kwargs):
        return self.getSignalerror(**kwargs)**2

    @getarray
    def getNumber(self, **kwargs):
        """ Return an image with a unique number: the axis len of the first axis dimension """
        _remove_time_reduce(kwargs)
        timeaxes = _get_time_axes(self.axes)

        
        axes  = self.getaxes()
        shape = self.getshape()
        notime_shape = [ sh for ax,sh in zip(axes, shape ) if ax not in timeaxes]
        notime_axes  = [ ax for ax,sh in zip(axes, shape ) if ax not in timeaxes]

        N    = np.zeros(notime_shape , dtype=int)
        N[:] = self.getsize(axis=timeaxes)

        return self._return_array_data(N, kwargs, axes=notime_axes)

    @getarray
    def getMoment(self, moment, **kwargs):
        """ Compute the moment on the dimention name 'time' of the cube """
        _remove_time_reduce(kwargs)
        timeaxes = _get_time_axes(self.axes)
        return computing.moment(
            self.getData(**kwargs), moment,
            axis=timeaxes
        )

class CubeSwitch(SwitchEntity, CubeImagePlot):
    """ cube of time, y, x in a list with one 'cubtime' child"""
    subclasses = [
       ({"PRIMARY", 0}, ("cubetime", "primary"))
    ]

update_entities(cube=CubeEntity, cubetime=CubeTimeEntity,
                         cubeswitch= CubeSwitch
                         )



