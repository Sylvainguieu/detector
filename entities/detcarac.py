from .linearity import LinearityEntity, LinearityListEntity
from .ptc import PtcEntity, PtcListEntity
from ..base import update_entities, getarray
from ..plots.baseplot import PtcLinearity as PtcLinearityPlot
from ..plots.baseplot import PtcLinearityList as PtcLinearityListPlot
import numpy as np


linsubclasses = list(LinearityEntity.subclasses)
ptcsubclasses = list(PtcEntity.subclasses)
linsubclasses[0] = ("FLUX", "flux") # remove the primary

subclasses = linsubclasses + ptcsubclasses


class DetcaracEntity(LinearityEntity, PtcEntity, PtcLinearityPlot):
    """
    Detector caraterisation contains:
     gain   : the computed gain in e/ADU
     noise2 : square of the noise (y intercept of the ptc)
     noise  : noise of the shortest exposure image of the ptc
     ptchi2 : chi2 of the ptc fit
     ptcnumber : number of point used by the ptc fit for each pixel

     flux : slope of signal vs exptime in ADU/s
     bias : y intercept of signal vs exptime in ADU
     linchi2: chi2 of the linearity fit
     linnumber: number of point used by the linearity fit for each pixel

    """
    subclasses = subclasses

    @getarray
    def getEflux(self, **kwargs):
        return self.getFlux(**kwargs)*self.getGain(**kwargs)

    @getarray
    def getEbias(self, **kwargs):
        return self.getBias(**kwargs)*self.getGain(**kwargs)


class DetcaracListEntity(LinearityListEntity, PtcListEntity, PtcLinearityListPlot):
    """ A list of 'detcarac' detector caracterisation objects """
    subclasses = [(True, "detcarac")]
    @getarray
    def getExcessnoise(self, zero_polar=np.min, **kwargs):
        """ Return the excess noise factor fraction of MF computed
        from ratio of system gain with different polar and M computed
        from the ratio Flux.
        """
        return self.getGainm(zero_polar=zero_polar,**kwargs)/self.getMf(zero_polar=zero_polar,**kwargs)

    axes = ["polar"]



update_entities(detcarac=DetcaracEntity,
                         detcaraclist=DetcaracListEntity
                         )

