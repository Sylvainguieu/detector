from .. import config
from ..base.engine import Log

log = Log()

def updateSectionKwargs(kwargs):
    """ transform section kwargs to x_idx=, y_idx= kwargs """
    KEYWORDCONFLICT = "conflict between '%s' and '%s' keyword, '%s' is retained"

    for k in config.idxkeys:
        if k in kwargs:
            kr = k+"_idx"
            if kr in kwargs:
                log.say(KEYWORDCONFLICT%(k,kr,k), vtype=config.WARNING)
            kwargs[kr] = kwargs.pop(k)

    if not "section" in kwargs: return

    section = kwargs.pop("section")
    if section is None: return
    if isinstance( section, slice):
        if config.Y+"_idx" in kwargs:
                log.say(KEYWORDCONFLICT%("section",config.Y+"_idx","section"), vtype=config.WARNING)
        kwargs[config.Y+"_idx"] = section
        return
    else:
        y_idx, x_idx = section
        if config.X+"_idx" in kwargs:
            log.say(KEYWORDCONFLICT%("section",config.X+"_idx","section[1]"), vtype=config.WARNING)
        kwargs[config.X+"_idx"] = x_idx

        if config.Y+"_idx" in kwargs:
            log.say(KEYWORDCONFLICT%("section",config.Y+"_idx","section[0]"), vtype=config.WARNING)
        kwargs[config.Y+"_idx"] = y_idx

def updateReduceKwargs(kwargs):
    """ transform freduce= kwargs to x_reduce=, y_reduce= kwargs """    #
    #  for backward compatibility
    #  freduce is equivalent to xreduce+yreduce
    if "freduce" in  kwargs:
        freduce = kwargs.pop("freduce")
        for k in config.pixelaxes:
            kwargs.setdefault(k+"_reduce",freduce)

def updateKwargs(kwargs):
    updateReduceKwargs (kwargs)
    updateSectionKwargs(kwargs)


class ImageKwargs(object):
    _update_data_kwargs = staticmethod(updateKwargs)




