from ..base import (SwitchEntity, ListEntity, DataEntity,
                    update_entities, getarray)
from ..plots.baseplot import Ptc as PtcPlot
from ..plots.baseplot import PtcList as PtcListPlot
import numpy as np




class PtcEntity(SwitchEntity, PtcPlot):
    """
    Detector PTC contains:
     gain   : the computed gain in e/ADU
     noise2 : square of the noise (y intercept of the ptc)
     noise  : noise of the shortest exposure image of the ptc
     ptchi2 : chi2 of the ptc fits
     ptcnumber : number of point used by the ptc fit for each pixel
    """
    subclasses = [
        ("GAIN"   , ("gain", "ptcprimary")),
        ("NOISE2" , "noise2"),
        ("NOISE"  , "noise"),
        ("PTCNUMBER" , "ptcnumber"),
        ("PTCCHI2",  "ptcchi2"),
        ("KEYWORDS", "keystable")
    ]
    @getarray
    def getEnoise(self, **kwargs):
         return self.getNoise(**kwargs)*self.getGain(**kwargs)



class PtcListEntity(ListEntity, PtcListPlot):
    """ A list of 'ptc' caracterisation object """
    axes = ["polar"]
    subclasses = [(True, "ptc")]
    @getarray
    def getMf(self, zero_polar=np.min, **kwargs):
        """ Return the ratio between to system gain of different polar
            zero_polar is the reference polar. by default take the minimun
        """
        polars = self.get_polar()

        zeros = self.get_polar.choose(zero_polar)
        if not len(zeros):
            raise ValueError("Cannot find data for polar %s"%zero_polar)
        return zeros[0].getGain(**kwargs)/self.getGain(**kwargs)


update_entities(ptc=PtcEntity, ptclist=PtcListEntity)

