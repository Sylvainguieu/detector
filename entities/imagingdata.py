from ..base import TableEntity, ListEntity,  update_entities
from ..base import TABLE, getarray
from ..base.engine.shared import get_axis
from .. import config
import numpy as np

from ..capabilities.fft import FFTCapability
from ..capabilities.jitter import JitterCapability
from ..plots.baseplot import CubeImage as CubeImagePlot
from .kwargs import ImageKwargs
from ..gets import ListWrapper
from ..datax import DataX, dataxconcat


class ImagingDataEntity(ImageKwargs, TableEntity, FFTCapability,
                        JitterCapability, CubeImagePlot):
    """ Entity class to handle ESO/interferometry IMAGINDATA HDU"""
    _default_name_ = "IMAGING_DATA"

    _entity_type_ = TABLE

    inarray = staticmethod(ListEntity.inarray)
    isarray = staticmethod(ListEntity.isarray)
    _get_axis = get_axis

    data_axes  = [config.TIME, config.SAMPIX, config.Y, config.X]
    opd_axes   = [config.TIME, config.TELESCOPE]
    other_axes = [config.TIME]

    @getarray.aloopargs(axis="win", loopargs=[0, "column", "window"])
    def getWindow(self, window=None, **kwargs): 
        if window is None: 
            return self.getWindow( window=list(range(self.getNumberofwindows() )), **kwargs)
            
        window = self.winIndex(window)
        return self.getData(window, **kwargs)

    def isOffsetable(self, column):
        column = self.winIndex(column)
        return column[0:4] == "DATA"

    
    def getaxes(self,column):
        column = self.winIndex(column)
        if column[0:4] == "DATA":
            return self.data_axes
        if column in ["LOCALOPD", "OPD"]:
            return self.opd_axes
        else:
            return self.other_axes

    @getarray
    def getNumberofwindows(self):
        return self.getkey("NREGION")

    @getarray
    def Windows(self, window=None, **kwargs):
        if window is None: window = range( self.getNumberofwindows() )
        else: window = np.s_[window]

        winclass = self._get_class("window")
        if not hasattr(window, "__iter__"):
            return winclass( self.getData( self.winIndex(window), **kwargs) )

        return self._get_class("windowlist")(
            [
               winclass( self.getData( self.winIndex(i), **kwargs) ) for i in window
            ]
        )

    @getarray
    def getWindows(self, window=None, **kwargs):
        if window is None: window = range( self.getNumberofwindows() )
        else: window = np.s_[window]
        if not hasattr(window, "__iter__"):
            return self.getData( self.winIndex(window), **kwargs)
        kwargs.setdefault("inaxis","window")

        return self.getDataInArray([self.winIndex(i) for i in window], **kwargs)


    def winIndex(self, i):
        """
        tranform i (integer) to the data keyword correxponding to the wanted window
        0 -> DATA1
        1 -> DATA2
        etc ...
        """
        if isinstance(i, basestring): return i
        return "DATA%d"%(i+1)


    @getarray
    def getImage(self, column=None, time_reduce=np.mean, **kwargs):
        return self.getData(column=column, time_reduce=time_reduce, **kwargs)

    @getarray
    def getOpd(self, **kwargs):
        return self.getData("OPD", **kwargs)
        
    @getarray
    def getLocalOpd(self, **kwargs):
        return self.getData("LOCALOPD", **kwargs)
    @getarray
    def getSteppingphase( self, **kwargs):
        return self.getData("STEPPING_PHASE", **kwargs)

    @getarray
    def getFramecount( self, **kwargs):
        return self.getData("FRAMECNT", **kwargs)

    @getarray
    def getExptime( self, **kwargs):
        return self.getData("EXPTIME", **kwargs)

    @getarray
    def getTime( self, **kwargs):
        return self.getData("TIME", **kwargs)
    getMjd = getTime

class ImagingData_FTEntity(ImagingDataEntity):
    _default_name_ = "IMAGING_DATA_FT"
    @getarray
    def getData(self, column="PIX", window=None, **kwargs):
        if column is not None and window is not None:
            raise KeyError("value and window are aliases choose one")
        return super(ImagingDataEntity, self).getData(column, **kwargs)

class ImagingData_SCEntity(ImagingData_FTEntity):
    _default_name_ = "IMAGING_DATA_SC"



update_entities(imagingdata = ImagingDataEntity,
                         imagingdataft = ImagingData_FTEntity,
                         imagingdatasc = ImagingData_SCEntity
                         )

