from .. import config
import numpy as np
from ..base import DataEntity, update_entities, getarray
from ..capabilities.jitter import CentroidCapability
from ..capabilities.image import ImageCapability

from ..plots.baseplot import Image as ImagePlot
from .kwargs import ImageKwargs

class ImageEntity(ImageKwargs, DataEntity, CentroidCapability, ImageCapability,
                  ImagePlot):
    """ 2d Image """
    axes = [config.Y, config.X]
    @getarray
    def getYx(self, physical=True, **kwargs):
        """
        return a grid of pixel coordinates
        axes "y" and "x" must be present in the DataEntity
        """
        shapes = self.getshape()
        if config.X in self.axes and config.Y in self.axes:
            xi = self.axes.index(config.X)
            yi = self.axes.index(config.Y)
        elif len(shapes)==2:
            yi,xi = 0,1
        else:
            raise ValueError("Need the axes name 'y' and 'x' if the data is not an image")

        if yi<xi:
            y,x = np.mgrid[0:shapes[yi],0:shapes[xi]]
        else:
            x,y = np.mgrid[0:shapes[yi],0:shapes[xi]]


        output= self.getOutput()
        if output and physical:
            if config.outputaxis == config.X:
                x += output*config.outputsize
            elif config.outputaxis == config.Y:
                y += output*config.outputsize

        y = self._return_array_data(y, kwargs, axes=[config.Y,config.X])
        x = self._return_array_data(x, kwargs, axes=[config.Y,config.X])
        return y,x
    @getarray
    def getX(self, **kwargs):
        return self.getYx(**kwargs)[1]
    @getarray
    def getY(self, **kwargs):
        return self.getYx(**kwargs)[0]
    @getarray
    def getYxpixel(self, pixel, physical=True, **kwargs):
        y, x = self.getYx(physical=physical, **kwargs)
        return y.flat[pixel], x.flat[pixel]
    @getarray
    def getImage(self, **kwargs):
        shape = self.getshape()
        if len(shape)!=2:
            raise Exception("Not an image")
        return self.getData(**kwargs)

update_entities(image = ImageEntity)

