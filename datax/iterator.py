

color_cycle  = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
marker_cycle = [',', '+', '-', '.', 'o', '*']

class DataxIterator(object):
    _index = -1
    color_cycle  = color_cycle
    marker_cycle = marker_cycle
    _obj    = None
    _key    = None
    _kwargs = None
    _size   = None
    def __init__(self, obj, key,  **kwargs):
        
        self._obj    = obj
        self._key    = key
        self._size   = len(obj)
        self._kwargs = kwargs
    
    def __iter__(self):
        self._index = -1
        return self
    def __len__(self):
        return self._size
    
    def __getitem__(self, item):
        return self._obj[item]
    
    def __getattr__(self, attr):
        if attr in self._kwargs:
            return self._get_kw(attr)
        return object.__getattribute__(self, attr)
        #if attr in object.__getattribute__(self,"_kwargs"):
        #    return object.__getattribute__(self, "_get_kw")(attr)
        raise AttributeError("This Iterator does not have attribute '%s' "%attr)
    
    def next(self):
        self._index += 1
        if self._index>=self._size:
            raise StopIteration()
        return self._obj[self._index]
    @property
    def legend(self):
        if "legend" in self._kwargs: return self._get_kw("legend")        
        return self._get_legend()
    
    _legend = "{0._key} = {0._index:}"
    def _get_legend(self):
        return self._legend.format(self)
    @property
    def color(self):
        if "color" in self._kwargs: return self._get_kw("color")
        return self.color_cycle[self._index%len(self.color_cycle)]
    @property
    def marker(self):
        if "marker" in self._kwargs: return self._get_kw("marker")
        return self.marker_cycle[self._index%len(self.marker_cycle)]
    @property
    def axes(self):
        if "axes" in self._kwargs: return self._get_kw("axes")
        return (self._size,self._index+1)
    @property
    def figure(self):
        if "figure" in self._kwargs: return self._get_kw("figure")
        return self._index+1
    def _get_kw(self, key):
        v = self._kwargs[key]
        if hasattr(v,"__iter__") and not isinstance(v,tuple):
            return v[self._index%len(v)]
        return v
    @property
    def kw(self):
        return {k:self._get_kw(k) for k in self._kwargs}
