import numpy as np
from ..datax import DataX, isdatax

from ..base import update_entities

AXIS = "time"

class FFTCapability(object):
    def getFft(self, frate=None, fmin=None, fmax=None,
               axis=AXIS,
               **kwargs):
        """
        Return the fft on the object.
        return a tuple with the frequence array and the fft
        By default the fft is made on the first axis.

        KEYWORDS:
           frate: framerate to compute the frequence array
                  if None try to execute self.getFrameRate()
           fmin, fmax: remove frequency not in this range
                       however if fmax is True the frequences
                       will be cut at the Nyquist frequence (e.i frate/2)
           axis: can be a number of a axis name. default is 'time'

        """
        data = self.getData(**kwargs)

        if isdatax(data):
            if isinstance(axis, basestring):
                axis_name = axis
                axis_num  = data.axes.index(axis)
            elif isinstance(axis, int):
                axis_name = data.axes[axis]
                axis_num  = axis
        else:
            if isinstance(axis, basestring):
                raise ValueError("get axis='%s' but data is not DataX, use an axis integer instead"%(axis))

        if axis is None:
            axis_name = "time"
            axis_num = 0

        N = data.shape[axis_num]
        fft = np.fft.fft(data, axis=axis_num)

        if frate is None:
            if not hasattr(self, "get_framerate"):
                raise KeyError("object do not have the get_framerate attribute and keyword 'frate' is None")

            frate = self.get_framerate()
            if isinstance(frate, np.ndarray):
                frate = np.mean(frate)

        freq_array = (np.arange(N) / float(N))*(frate)

        if fmax is True:
            fmax = frate/2.

        test_min = freq_array >= fmin if fmin is not None else True
        test_max = freq_array <= fmax if fmax is not None else True
        test = test_min & test_max
        test = slice(0, None) if test is True else test

        tpls = [slice(0,None)]*len(data.shape)
        tpls[axis_num] = test
        tpls = tuple(tpls)

        if isdatax(data):
            axes = list(data.axes)
            axes[axis_num] = "freq"
            return (DataX(freq_array[test], ["freq"]),
                          DataX(fft[tpls], axes))

        return freq_array[test], fft[tpls]

    def getPsd(self, dt=None, frate=None,  fmin=None, fmax=None,
               axis=AXIS, **kwargs):
        """
        Return the Psd on the object.
        return a tuple with the frequence array and the fft
        By default the fft is made on the first axis.

        KEYWORDS:
           frate: framerate to compute the frequence array
           fmin, fmax: remove frequency not in this range
                       however if fmax is True the frequences
                       will be cut at the Nyquist frequence (e.i frate/2)
           axis: can be a number of a axis name
        """
        if frate is None:
            frate = self.get_framerate()
            if isinstance(frate, np.ndarray):
                frate = np.mean(frate)
        if dt is None:
            dt = 1./frate
        f, tf = self.getFft(frate=frate,
                            fmin=fmin, fmax=fmax,
                            axis=axis,
                             **kwargs)

        if isdatax(tf):
            return f, DataX(np.abs(tf.A*tf.A.conjugate())*dt, tf.axes)
        else:
            return f, np.abs(tf*tf.conjugate())*dt

    def getDirectSpace(self, dt=None, frate=None,
                       timemin=None, timemax=None,
                     axis=None, **kwargs):

        if dt is None and frate is None:
            frate = self.get_framerate()
            if isinstance(frate, np.ndarray):
                frate = np.mean(frate)

        if dt is None:
            dt = 1./frate

        data = self.getData(**kwargs)

        if isdatax(data):
            if isinstance(axis, basestring):
                axis_name = axis
                axis_num = data.axes.index(axis)
            elif isinstance(axis, int):
                axis_name = data.axes[axis]
                axis_num = axis

        if axis is None:
            axis_num = 0
            axis_name = "time"

        N = data.shape[axis_num]
        time_array = np.arange(N)*dt

        test_min = time_array >= timemin if timemin is not None else True
        test_max = time_array <= timemax if timemax is not None else True
        test = test_min & test_max
        test = slice(0, None) if test is True else test

        if isdatax(data):
            axes = list(data.axes)
            return (DataX(time_array[test], ["time"]),
                     data.section( test, axis_name ) )

        tpls = [slice(0,None)]*len(data.shape)
        tpls[axis_num] = test
        tpls = tuple(tpls)

        return time_array[test], data[tpls]












