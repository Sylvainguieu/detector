from .. import config
from .linearfit import LinearfitCapability

if config.debug:
    try:
        lin_buffer
    except:
        lin_buffer      = []
else:
    lin_buffer      = []
lin_buffer_size = config.lin_buffer_size = 50

class LinearityCapability(LinearfitCapability):
    def _hash_lin(self, kwargs):
        return hash( ((kwargs.get("signalrange",None), kwargs.get("ditrange",None),
                       kwargs.get("chi2_signalrange",None), kwargs.get("chi2_ditrange",None)
                   ),

                      tuple( self.file_name.get())) )

    def _look_in_buffer_lin(self, **kwargs):
        h = self._hash_lin(kwargs)
        buff = dict(lin_buffer)
        return h, buff.get( h, None)
    def _add_to_buffer_lin(self, h, ptc):
        global lin_buffer, lin_buffer_size
        if len(lin_buffer)>lin_buffer_size:
            lin_buffer.pop(0)
        lin_buffer.append( (h, ptc) )

    def getLinearity(self, ditrange=None, signalrange=None,
                     chi2_ditrange=None, chi2_signalrange=None,
                     force=False, nokeytable=False, 
                     instrument="base", io=None
                     ):
        """
        Return a Linearuty HDU list result of linear fit:
         signal = flux*dit + bias

        - Flux  (ADU/s the slope of the fit)
        - Bias  (ADU  the intercept value of the fit signal = flux*dit+bias)
        - LINNUMBER   (a map of the Number of points used for the fit)
        - LINCHI2     (a chi2 map, result of the linear fit)


         KEYWORDS:
           signalrange : a tuple of (min,max) value for signal to compute the fit
           ditrange : a tuple of (min,max) value for dit to compute the fit

           chi2_signalrange : a tuple of (min,max) value for signal to compute chi2
           chi2_ditrange : a tuple of (min,max) value for dit to compute chi2
           if chi2_signalrange or chi2_ditrange, default is signalrange and
           ditrange respectively
        """


        lin = None

        h, lin = self._look_in_buffer_lin( signalrange=signalrange,ditrange=ditrange,
                                           chi2_ditrange=chi2_ditrange,
                                           chi2_signalrange = chi2_signalrange
                                       )
        if force or (lin is None):
            lin = self.compute_linearity(signalrange=signalrange, ditrange=ditrange,
                                         chi2_ditrange    = chi2_ditrange,
                                         chi2_signalrange = chi2_signalrange, 
                                         instrument="base", io=None
                                     )
            self._add_to_buffer_lin(h, lin)

        if nokeytable:
            lin = self._get_class("linearity", instrument="base", io=None)(lin)
            if "KEYWORDS" in lin:
                 lin.pop([f.name for f in ptc].index("KEYWORDS"))

        return lin

    def compute_linearity(self, ditrange=None, signalrange=None,
                          chi2_ditrange=None, chi2_signalrange=None, 
                          instrument="base", io=None, 
                           **kwargs):
        yrange = signalrange
        xrange = ditrange
        chi2_yrange = chi2_signalrange
        chi2_xrange = chi2_ditrange



        if xrange is None:
            xrange = (None, None)
        if yrange is None:
            yrange = (None, None)

        polar = 0
        illumination = 0
        try:
            polars = self.get_polar()

            if len(set(polars))>1:
                self.say("Found more than one detector polar in this list: %s"%(set(polars)), vtype=WARNING)
            polar = max(polars)

        except:
            self.say("Cannot determine detector polar", vtype=config.WARNING)
        try:
            illuminations = self.get_illumination()
            if len(set(illuminations))>1:
                self.say("Found more than one detector illumination  in this list: %s"%(set(illuminations)), vtype=config.WARNING)
            illumination  = max(illuminations)
        except:
            self.say("Cannot determine detector illumination", vtype=config.WARNING)

        self.say("Computing full frame Linearity with ditrange=%s, signalrange=%s for polar %d"%(ditrange, signalrange, polar), 1)

        coeff = self.linearfit(self.get_dit.datashaped,
                               self.getSignal,
                               variance="getSignalvariance",
                               compute_chi2=True,
                               xrange=xrange, yrange=yrange,
                               chi2_xrange=chi2_xrange,
                               chi2_yrange=chi2_yrange,
                               **kwargs)



        flux = self._get_class(("flux", "ptcprimary"), instrument=instrument, io=io)(coeff[0])
        bias = self._get_class("bias", instrument=instrument, io=io)(coeff[1])
        N    = self._get_class("linnumber", instrument=instrument, io=io)(coeff[2])
        chi2 = self._get_class("linchi2", instrument=instrument, io=io)(coeff[3])

        flux.setkey("XFITMIN", xrange[0] or "None" )
        flux.setkey("XFITMAX", xrange[1] or "None" )
        flux.setkey("YFITMIN", yrange[0] or "None" )
        flux.setkey("YFITMAX", yrange[1] or "None" )
        flux.setkey("POLAR", polar)
        flux.setkey("ILLUM", illumination)

        if hasattr( self, "make_keys_table"):
            return self._get_class("linearity", instrument=instrument, io=io)([flux,bias,N,chi2,self.make_keys_table()])
        else:
            return self._get_class("linearity", instrument=instrument, io=io)([flux,bias,N, chi2])
