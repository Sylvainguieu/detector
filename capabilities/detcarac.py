from .ptc import PtcCapability
from .linearity import LinearityCapability
########
# Make sure the detcarac entities are loaded

class DetcaracCapability(PtcCapability, LinearityCapability):
    def getDetCarac(self, signalrange=None, ditrange=None, sigma2range=None,
                    chi2_signalrange=None, chi2_sigma2range=None,
                    chi2_ditrange=None,
                    signalrange_ptc=None, signalrange_lin=None,
                    chi2_signalrange_ptc=None, chi2_signalrange_lin=None,
                    force=False, instrument="base", io=None):
        """ Return a fits Carac HDUList containing detector carac :
        - Gain system  (e-/ADU from slope of ptc fit)
        - Noise2       (ADU^2 square of noise as the intercept value of ptc fit)
        - Noise        (ADU This is the rms of the minimum dit image: the BIAS)
        - PTCNumber    (a map of the Number of points used for the fit)
        - PTCCHI2      (a chi2 map, result of the ptc fit)

        - Flux  (ADU/s the slope of the fit  signal = flux*dit+bias)
        - Bias  (ADU  the intercept value of the fit signal = flux*dit+bias)
        - LINNUMBER   (a map of the Number of points used for the fit)
        - LINCHI2     (a chi2 map, result of the linear fit)

        KEYWORDS:
           see getPtc and getLinearity function.
           - signalrange_ptc, chi2_signalrange_ptc overwrite signalrange, chi2_signalrange
                 for ptc fit
           - signalrange_lin, chi2_signalrange_lin overwrite signalrange, chi2_signalrange
                 for linearity fit

        """
        signalrange_ptc = signalrange_ptc or signalrange
        chi2_signalrange_ptc = chi2_signalrange_ptc or chi2_signalrange

        signalrange_lin      = signalrange_lin or signalrange
        chi2_signalrange_lin = chi2_signalrange_lin or chi2_signalrange

        return self._get_class("detcarac", instrument=instrument, io=io)(
            self.getPtc( signalrange=signalrange_ptc, sigma2range=sigma2range,
                         chi2_signalrange=chi2_signalrange_ptc,
                         chi2_sigma2range=chi2_sigma2range,
                         force=force, nokeytable=True)+
            self.getLinearity( signalrange=signalrange_lin, ditrange=ditrange,
                               chi2_signalrange=chi2_signalrange_lin,
                               chi2_ditrange=chi2_ditrange,

                               force=force)
        )
