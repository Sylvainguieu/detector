from .linearfit import LinearfitCapability
from .. import config
import numpy as np

if config.debug:
    try:
        ptc_buffer
    except:
        ptc_buffer      = []
else:
    ptc_buffer      = []
ptc_buffer_size = config.ptc_buffer_size

class PtcCapability(LinearfitCapability):
    def getMinDitSigma(self, **kwargs):
        return self.get_dit.choose(np.min).getSigma(**kwargs)

    def MinDitSigma(self, **kwargs):
        Sigma = self.get_dit.choose(np.min)[0].getSigma.encaps(inside="sigma",
                                                              **kwargs)
        Sigma.name = "NOISE"
        return Sigma

    def _hash_ptc(self, kwargs):
        return hash( ((kwargs.get("signalrange",None), kwargs.get("sigma2range",None),
                       kwargs.get("chi2_signalrange",None), kwargs.get("chi2_sigma2range",None)
                   ),
                      tuple( self.file_name()))
                   )

    def _look_in_buffer_ptc(self, **kwargs):
        h = self._hash_ptc(kwargs)
        buff = dict(ptc_buffer)
        return h, buff.get( h, None)

    def _add_to_buffer_ptc(self, h, ptc):
        global ptc_buffer, ptc_buffer_size
        if len(ptc_buffer)>ptc_buffer_size:
            ptc_buffer.pop(0)
        ptc_buffer.append( (h, ptc) )

    def getPtc(self, signalrange=None, sigma2range=None,
               chi2_signalrange=None, chi2_sigma2range=None,
               force=False, nokeytable=False, instrument="base", io=None):
        """
        Return a fits PTC HDUList containing result of photon transfer curve (ptc) fit :
        sigma2 = signal * alpha + beta

        - Gain system  (e-/ADU from slope of ptc fit:  1/alpha)
        - Noise2       (ADU^2 square of noise form the intercept value of ptc fit: beta*gain^2)
        - Noise        (ADU This is the rms of the minimum dit image: the BIAS)
        - PTCNumber    (a map of the Number of points used for the fit)
        - PTCCHI2      (a chi2 map, result of the ptc fit)

        KEYWORDS:
           signalrange : a tuple of (min,max) value for signal to compute the fit
           sigma2range : a tuple of (min,max) value for sigma2 to compute the fit

           chi2_signalrange : a tuple of (min,max) value for signal to compute chi2
           chi2_sigma2range : a tuple of (min,max) value for sigma2 to compute chi2
           if chi2_signalrange or chi2_sigma2range, default is signalrange and
           sigma2range respectively

        """
        ptc = None

        h, ptc = self._look_in_buffer_ptc( signalrange=signalrange, sigma2range=sigma2range,
                                           chi2_signalrange=chi2_signalrange,
                                           chi2_sigma2range=chi2_sigma2range
                                       )
        if force or (ptc is None):
            ptc = self.compute_ptc(signalrange=signalrange, sigma2range=sigma2range,
                chi2_signalrange=chi2_signalrange,
                chi2_sigma2range=chi2_sigma2range, 
                instrument=instrument, io=io
            )
            self._add_to_buffer_ptc(h, ptc)

        if nokeytable:
            ptc = self._get_class("ptc", instrument=instrument, io=io)(ptc)
            if "KEYWORDS" in ptc:
                 ptc.pop([f.name for f in ptc].index("KEYWORDS"))

        return ptc

    def compute_ptc(self, signalrange=None, sigma2range=None, chi2_signalrange=None, chi2_sigma2range=None, 
                        instrument="base", io=None, 
                        **kwargs):
        xrange = signalrange
        yrange = sigma2range
        chi2_xrange = chi2_signalrange
        chi2_yrange = chi2_sigma2range

        medbox = kwargs.pop("medbox", None)

        polar = 0
        try:
            polars = self.get_polar()
            if len(set(polars))>1:
                self.say("Found more than one detector polar in this list: %s"%(set(polars)), vtype=config.WARNING)
            polar = max(polars)
        except:
            self.say("Cannot determine detector polar", vtype=config.WARNING)

        self.say("Computing full frame PTC with signalrange=%s, sigma2range=%s for polar %d"%(signalrange, sigma2range, polar), 1)

        coeff = self.linearfit(self.getSignal, self.getSigma2,
                               variance="getSigma2variance", compute_chi2=True,
                               xrange=xrange, yrange=yrange,
                               chi2_xrange=chi2_xrange,
                               chi2_yrange=chi2_yrange, **kwargs)

        noisemin = self.MinDitSigma(**kwargs)
        k     = 1./coeff[0]
        eta2  = (coeff[1]*k**2)
        N = coeff[2]
        chi2 = coeff[3]

        if xrange is None:
            xrange = (None, None)
        if yrange is None:
            yrange = (None, None)


        k    =  self._get_class(("gain", "ptcprimary"), instrument=instrument, io=io)(k)        
        eta2 =  self._get_class("noise2", instrument=instrument, io=io)(eta2)
        N    =  self._get_class("ptcnumber", instrument=instrument, io=io)(N)
        chi2 =  self._get_class("ptcchi2", instrument=instrument, io=io)(chi2)


        k.setkey("XFITMIN", xrange[0] or "None" )
        k.setkey("XFITMAX", xrange[1] or "None" )
        k.setkey("YFITMIN", yrange[0] or "None" )
        k.setkey("YFITMAX", yrange[1] or "None" )
        k.setkey("POLAR", polar)

        if hasattr( self, "make_keys_table"):
            return self._get_class("ptc", instrument=instrument, io=io)([k, eta2, noisemin, N, chi2,self.make_keys_table()])
        else:
            return self._get_class("ptc", instrument=instrument, io=io)([k, eta2, noisemin, N, chi2])



