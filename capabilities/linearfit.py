from .. import config
if config.debug: print "linearfit.py"
import numpy as np

class LinearfitCapability(object):
    def linearfit(self, xmethod, ymethod, variance=None, xrange=None, yrange=None,
                  chi2_xrange=None, chi2_yrange=None,
                  kw_x=None, kw_y=None,
                  compute_chi2=True,
                  **kwargs):

        if issubclass( type(xmethod), basestring):
            xmethod = getattr(self,xmethod)
        if issubclass( type(ymethod), basestring):
            ymethod = getattr(self,ymethod)

        kw_x = kw_x or kwargs
        kw_y = kw_y or kwargs
        ix = xmethod(**kw_x)
        iy = ymethod(**kw_y)

        ishape = list(ix.shape)
        iaxes  = ix.axes if hasattr(ix, "axes") else range(len(ishape))
        ishape.remove(ishape[0])
        ishape = tuple(ishape)

        N = len(ix)
        x = np.asarray(ix).reshape( (N,-1)).T
        y = np.asarray(iy).reshape( (N,-1)).T
        if variance:
            if issubclass( type(variance), str):
                variance_method = self.__getattribute__(variance)
            else:
                variance_method = variance
            variance = variance_method(**kw_y)

            variance = np.asarray(variance).reshape( (N,-1)).T

        if config.vectorialized_fit:
            coeff = vectorialized_linearfit( x, y,
                                             xrange, yrange,
                                             chi2_xrange=chi2_xrange,
                                             chi2_yrange=chi2_yrange,
                                             compute_chi2=compute_chi2,
                                             variance=variance
                                         ).T
        else:
            coeff = linfitarray(x, y,
                                xrange, yrange
                            ).T

        ncoeff = 3+compute_chi2
        coeff = coeff.reshape( [ncoeff]+list(ishape))
        return tuple(coeff)

def vectorialized_linearfit(x,y, xrange=None, yrange=None,
                            compute_chi2=True, variance=None,
                            chi2_xrange=None, chi2_yrange=None
                        ):
    """
    linfitarray( x, y)

    Perform a linear fit on the first dimension of n,x,y cubes
    KEYWORDS:
      xrange : 2 val tuple
      yrange : 2 val tuple
     """
    ####
    #
    #if x.dtype == np.float64:
    #    x = np.array(x, dtype=np.float32)
    #if y.dtype == np.float64:
    #    y = np.array(y, dtype=np.float32)

    x, y = cliparray(x, y, xrange=xrange, yrange=yrange)
    if chi2_xrange or chi2_yrange:
        xchi2, ychi2 = cliparray(x, y, xrange=chi2_xrange, yrange=chi2_yrange)
    else:
        xchi2, ychi2 = x, y
    return multiple_linregress(x, y, compute_chi2=compute_chi2,
                               variance=variance,
                               xchi2=xchi2, ychi2=ychi2
                           )


def vlinreg(x,y):
    #Only compute where we have n_min unmasked values in time
    n_min = 3
    valid_idx = y.count(axis=0).filled(0) >= n_min
    #Returns 2D array of unmasked columns
    y = self.ma_stack[:, valid_idx]

    #Extract mask for axis 0 - invert, True where data is available
    mask = ~y.mask
    #Remove masks, fills with fill_value
    y = y.data
    #Independent variable is time ordinal
    x = self.date_list_o
    x = x.data

    #Prepare matrices and solve
    X = np.c_[x, np.ones_like(x)]
    a = np.swapaxes(np.dot(X.T, (X[None, :, :] * mask.T[:, :, None])), 0, 1)
    b = np.dot(X.T, (mask*y))
    r = np.linalg.solve(a, b.T)

    #Create output grid with original dimensions
    out = np.ma.masked_all_like(self.ma_stack[0])
    #Fill in the valid indices
    out[valid_idx] = r[:,0]


def multiple_linregress(x, y, compute_chi2=True, variance=None,
                        xchi2=None, ychi2=None):

    s = x.shape
    x_mean = np.mean(x, axis=1).reshape((s[0],1))
    x_norm = x - x_mean
    #y_mean = np.mean(y, axis=1, keepdims=True)
    y_mean = np.mean(y, axis=1).reshape( (s[0],1))
    y_norm = y - y_mean

    if isinstance( x, np.ma.masked_array):
        Ns =  np.sum(~x.mask, axis=1)
        # need to put 0 for all the masked array value. Do not know why.
        # probably a bug in einsum
        y_norm[y_norm.mask] =  0
        x_norm[x_norm.mask] =  0

    else:
        Ns = np.ndarray( (x.shape[0],), dtype=int )
        Ns[0:None] = x.shape[1]




    slope = (np.einsum('ij,ij->i', x_norm, y_norm) /
             np.einsum('ij,ij->i', x_norm, x_norm))


    intercept = y_mean[:, 0] - slope * x_mean[:, 0]



    if compute_chi2:
        if xchi2 is None: xchi2 = x
        if ychi2 is None: ychi2 = y

        if variance is not None:
            chi2 = ((slope*xchi2.T + intercept) - ychi2.T)**2 / variance.T
            chi2 = chi2.sum(axis=0) / 2.
        else:
            if isinstance( xchi2, np.ma.masked_array):
                Nchi2 =  np.sum(~xchi2.mask, axis=1)
            else:
                Nchi2 = np.ndarray( (xchi2.shape[0],), dtype=int )
                Nchi2[0:None] = x.shape[1]
            chi2 = ((slope*xchi2.T + intercept) - ychi2.T)**2
            chi2 = chi2.sum(axis=0)/Nchi2

        return np.column_stack((slope, intercept, Ns, chi2))
    return np.column_stack((slope, intercept, Ns))


def linfitarray( x, y, xrange=None, yrange=None):
    """
    linfitarray( x, y)

    Perform a linear fit on the first dimension of x,y cubes
    KEYWORDS:
      xrange : 2 val tuple
      yrange : 2 val tuple
    """
    if not x.shape==y.shape:
        raise Exception("x and y must have the same dimention")
    N = len(x)

    out = np.ndarray( (x.shape[0],3) )
    for i in np.arange(N):
        out[i] = linregress( x[i], y[i], xrange=xrange, yrange=yrange)
    return out

def linregress(x,y, xrange=None, yrange=None):
    """
    linregress(x,y)
    perform a linear regression on vectors a and y
    xrange and yrange define the range of fit for x and y values
    """
    if not len(x):
        raise Exception("All values are masked")

    x,y = clipfit(x,y, xrange, yrange)
    lx = len(x)

    if lx<2:
        return [-99,-99, lx]

    w = np.linalg.lstsq( np.vstack([ x, np.ones(x.shape)]).T, y)
    return list(w[0])+[lx]

def clipfit(x, y, xrange=None, yrange=None):

    if xrange is None:
        xrange = (None, None)
    if yrange is None:
        yrange = (None, None)

    xmin, xmax = xrange
    ymin, ymax = yrange
    if issubclass(type(x),np.ma.MaskedArray) or issubclass(type(y), np.ma.MaskedArray):
        mask =(x+y).mask
        x = x[~mask]
        y = y[~mask]
    if xmin is not None:
        test = x>=xmin
        x = x[test]
        y = y[test]
    if xmax is not None:
        test = x<=xmax
        x = x[test]
        y = y[test]
    if ymin is not None:
        test = y>=ymin
        x = x[test]
        y = y[test]
    if ymax is not None:
        test = y<=ymax
        x = x[test]
        y = y[test]
    return x,y

def cliparray( x,y, xrange=None, yrange=None):
    """
    cliparray( x,y, xrange=, yrange=)

    return a masked array.

    clip the x and y vectors given a tuple of len 2  of range for x and y.
    All the  x/y data outside x/yrange will be masked.
    x/yrange can be :
          None
          (None, max_val)
          (min_val, None)
          (min_val, max_val)

    """
    test = None
    if xrange is not None:
        xrange = list(xrange)
        if xrange[0] is not None:
            if test is None:
                test = (x>=xrange[0])
            else: test *= (x>=xrange[0])
        if xrange[1] is not None:
            if test is None:
                test = (x<=xrange[1])
            else: test *= (x<=xrange[1])
    if yrange is not None:
        yrange = list(yrange)
        if yrange[0] is not None:
            if test is None:
                test = (y>=yrange[0])
            else: test *= (y>=yrange[0])
        if yrange[1] is not None:
            if test is None:
                test = (y<=yrange[1])
            else: test *= (y<=yrange[1])

    if test is not None:
        x = np.ma.array( data=x, mask=~test)
        y = np.ma.array( data=y, mask=~test)
    return x, y

def __test__():
    global l,v, x, y, v2, l2
    try:
        x
    except:
        x = np.array( np.linspace(1,50,50),  dtype=np.float64).reshape((50,1)).T
        y = np.array( [1,2,3,4,5.]+[50.]*45, dtype=np.float64).reshape((50,1)).T
        #y += np.random.random((2,9))*0.01


    N = len(x)
    kw = dict(xrange=(0 ,7))

    #kw = dict(xrange=(-0.8 ,0.8), yrange=(None, None))
    v  = vectorialized_linearfit( x, y, **kw ).T
    v2 = vectorialized_linearfit( x, y).T
    l = linfitarray( x,y, **kw).T
    l2 = linfitarray( x,y ).T
    print "Number not equal", (~(l[2]==v[2])).sum()
    print "coef 0/1 diff", (l[0]-v[0]).max(), (l[1]-v[1]).max()
    print "coef 0/1 diff [np mask]", (l2[0]-v2[0]).max(), (l2[1]-v2[1]).max()

