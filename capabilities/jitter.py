from  scipy.ndimage import center_of_mass
from ..datax import DataX, isdatax
from ..base import getarray
import numpy as np
def clip(data, clipmin, clipmax):
    if clipmin is not None:
        data[data<clipmin] = clipmin
    if clipmax is not None:
        data[data>clipmax] = clipmax
    return data



class CentroidCapability(object):
    @getarray
    def getCentroid(self, clipmin=None, clipmax=None, **kwargs):
        data = self.getData(**kwargs)
        return center_of_mass(clip(data, clipmin, clipmax))
    _centroid_instrument = "base"



class JitterCapability(CentroidCapability):
    @getarray
    def getJitter(self, clipmin=None, clipmax=None, **kwargs):
        data = self.getData(**kwargs)
        if len(data.shape)<3:
            return center_of_mass(clip(data, clipmin, clipmax))

        data = clip(data, clipmin, clipmax)

        if isdatax(data):
            ndata = data.transform( [("time",None), "y", "x"] )
        else:
            ndata = data.reshape((-1,data.shape[-2],data.shape[-1]))

        jitters = np.array([center_of_mass(img) for img in ndata])
        jitters = jitters.reshape(  tuple(list(data.shape[0:-2])+[2])  )
        if isdatax(data):
            return DataX(jitters, data.axes[0:-2]+["yx"])
        return jitters

