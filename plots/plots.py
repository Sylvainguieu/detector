import matplotlib.pylab as plt
from matplotlib.pylab import *

import numpy as np


def plotdecorator(plotclass):
    def tmpplotfunc(self, *args, **kwargs):
        return plotclass.newWithData(self, *args, **kwargs)
    tmpplotfunc.__doc__ = plotclass.__doc__
    return tmpplotfunc


def plotfuncdecorator(plotfunc):
    def tmpplotfunc(self, *args, **kwargs):
        return plotfunc(self, *args, **kwargs)
    tmpplotfunc.__doc__ = plotfunc.__doc__
    return tmpplotfunc


def _make_doc(pname, plot):
    return "plot %s with Substitued data" % pname + "\n%s Doc :\n"+plot.__doc__


def _masknan(x):
    return np.ma.masked_array(x, np.isinf(x) + np.isnan(x))


class _void_:
    pass


class DictWalker(object):
    _d_ = None

    def __init__(self, d):
        self._d_ = d

    def __contains__(self, item):
        _d_ = self._d_
        if not isinstance(item, basestring):
            raise ValueError("expecting a string for item")
        while True:
            item_splited = item.split(".", 1)
            if len(item_splited) < 2:
                return item in _d_
                if not item_splited[0] in _d_:
                    return False
            _d_ = _d_[item_splited[0]]
            item = item_splited[1]

    def __getitem__(self, item):
        _d_ = self._d_
        if not isinstance(item, basestring):
            raise ValueError("expecting a string for item")
        while True:
            item_splited = item.split(".", 1)
            if len(item_splited) < 2:
                return _d_[item]
            _d_ = _d_[item_splited[0]]
            item = item_splited[1]

    def __setitem__(self, item, value):
        _d_ = self._d_
        if not isinstance(item, basestring):
            raise ValueError("expecting a string for item")
        while True:
            item_splited = item.split(".", 1)
            if len(item_splited) < 2:
                if item in _d_ and isinstance(_d_[item], dict):
                    raise ValueError("That would erase en entire dictionary")
                _d_[item] = value
                return None
            _d_ = _d_[item_splited[0]]
            item = item_splited[1]

    def __getattr__(self, attr):
        attr_splited = attr.split(".", 1)
        if len(attr_splited) < 2:
            tmp = self._d_[attr]
            if isinstance(tmp, dict):
                return self.__class__(tmp)
            return tmp
        return self.__class__(self._d_[attr_splited[0]]).__getattr__(attr_splited[1])

    def __setattr__(self, attr, value):
        if attr == "_d_":
            object.__setattr__(self, attr, value)
            return
        attr_splited = attr.split(".", 1)
        if len(attr_splited) < 2:
            self.__setitem__(attr, value)
            return None
        return self.__class__(self._d_[attr_splited[0]] ).__setattr__(attr_splited[1], value)

    def __repr__(self):
        texts = []
        _walker_rep(self._d_, "", texts)
        return "\n".join(texts)


def _walker_rep(d, pref, texts):
    if isinstance(d, dict):
        for k, v in d.iteritems():
            _walker_rep(v, pref + "." + k if pref else k, texts)
    else:
        texts.append("%s = %s" % (pref, d))


def plotsetter(plotobject):
    alldict = {}
    pnames = plotobject._getpnames_()
    for pname in pnames:
        kw_name = "kw_" + pname
        if hasattr(plotobject, kw_name):
            alldict[pname] = getattr(plotobject, kw_name)
    return DictWalker(alldict)


def return_plot_decorator(plotobject, func):
    def tmp_return_plot(*args, **kwargs):
        func(*args, **kwargs)
        return plotobject
    return tmp_return_plot


class _Plot_(object):
    ckw_axes = {}

    ckw_figure = {}
    ckw_legend = {}
    ckw_subplots_adjust = {}
    ckw_allaxes = {}
    ckw_misc = {}



    dispach_init = {}
    _stop_ckw_dependency_ = False

    _axes = None
    _figure = None
    _lastFit = None

    _kw_classes = dict  # classes of kw parameters

    def __init__(self, **kwargs):
        self._initKw_()  # set the self.kw_*
        self._setKw_(kwargs)

    def _setKw_(self, kwargs):
        # init the axes take "axes" from kwargs
        # and set it
        # in self._axes
        self._initAxes_(kwargs)
        # samething for figure
        self._initFigure_(kwargs)

        # dispatch all the floating keywords inside the kw_*
        self._initDispach_(kwargs)

        # fill the self.kw_* with the kw_* found in kwargs
        self._initUserKw_(kwargs)

    def set(self, **kwargs):
        self._setKw_(kwargs)

    def setData(self, *args, **kwargs):
        raise Exception("not implemented")

    def copy(self, **kwargs):
        """
        plot.copy( **kwargs)
        Make a copy of the plot object and update the kwargs definitions.
        Return a new object of the class of plot.
        The new object will not contains data use setData or copy with
        copyWithData method
        """
        for attr in self._getpnames_():
            kw_object = getattr(self, "kw_" + attr).copy()
            kw_object.update(kwargs.get("kw_" + attr, {}))

            kwargs["kw_" + attr] = kw_object
        return self.__class__(**kwargs)

    def copyWithData(self, *args, **kwargs):
        """
        plot.copyWithData(*args, **kwargs)
        make a new copy of plot and set the data with the setData method
        the setData will receive all the keywords left that are not
        plot keywords e.g. kw_plot, kw_axes, color, ...
        """
        new = self.copy()
        new._setKw_(kwargs)
        new.setData(*args, **kwargs)
        return new
    cd = copyWithData

    @classmethod
    def new(cls, **kwargs):
        """
        plot.new(**kwargs)
        return a brend new object of plot class with updated kwargs
        """
        return cls(**kwargs)

    @classmethod
    def newWithData(cls, *args, **kwargs):
        """
        plot.copyWithData(*args, **kwargs)
        make a new  plot class object and set the data with the setData method
        the setData will receive all the keywords left that are not
        plot keywords e.g. kw_plot, kw_axes, color, ...
        """
        new = cls.new()
        new._setKw_(kwargs)
        new.setData(*args, **kwargs)
        return new

    nd = newWithData

    def _initAxes_(self, kwargs):
        if "axes" in kwargs:
            axes = kwargs.pop("axes", None)
            if (axes is not None) and not isinstance(axes, (tuple, list, plt.Axes, int)):
                raise ValueError(("axis should be a Axis instance, "
                                  "a 3 or 2 element tuple or an integer, "
                                  "got %s: ") % (axes,))
            self._axes = axes

    def _initFigure_(self, kwargs):
        if "figure" in kwargs:
            figure = kwargs.pop("figure", None)
            if (figure is not None) and not isinstance(figure, (plt.Figure, int, str)):
                raise ValueError(("figure should be a Figure instance, "
                                  "or a integer, got %s: ") % figure)
            self._figure = figure

    def _initDispach_(self, kwargs):
        for klist, plotlist in self.dispach_init.iteritems():
            if klist is all:
                continue
            for k in klist:
                if k in kwargs:
                    value = kwargs.pop(k)
                    for plot in plotlist:
                        kwp = getattr(self, "kw_" + plot)
                        kwp[k] = value

        if all in self.dispach_init:
            allplot = self.dispach_init[all]
            for k in kwargs.keys():
                if k[0:3]!="kw_":
                    value = kwargs.pop(k)
                    for plot in allplot:
                        kwp = getattr(self, "kw_"+plot)
                        kwp[k] = value

    def _parseDispach_(self, kwargs, pname):
        if kwargs.pop("_noDispach_", False):
            return
        if hasattr(self, "dispach_"+pname):
            self._parseFromDispacher_(getattr(self, "dispach_"+pname), kwargs)

    def _warningKw_(self, kwargs):
        if len(kwargs):
            print "Warning "+(",".join(kwargs))+" are left unused"

    @staticmethod
    def _parseFromDispacher_(dispach, kwargs):
        for klist, plotlist in dispach.iteritems():
            if klist is all:
                continue
            for k in klist:
                if k in kwargs.keys():  # .keys because the dict will change
                    value = kwargs.pop(k)
                    for plot in plotlist:
                        kw_plot = "kw_"+plot
                        if kw_plot not in kwargs:
                            kwargs[kw_plot] = {}
                        kwargs[kw_plot].setdefault(k, value)
                        # setdefault to not
                        # not overwrite the keywords set in the kw_* keyword

        if all in dispach:
            allplot = dispach[all]
            for k in kwargs.keys():
                if k[0:3] != "kw_":
                    value = kwargs.pop(k)
                    for plot in allplot:
                        kw_plot = "kw_" + plot
                        if kw_plot not in kwargs:
                            kwargs[kw_plot] = {}
                        kwargs[kw_plot].setdefault(k, value)

    def _initUserKw_(self, kwargs):
        """
        fill the self.kw_SOMETHING with the kw_SOMETHING found in kwargs
        """
        for k, v in kwargs.items():
            if k[0:3] == "kw_":
                if not hasattr(self, k):
                    raise KeyError("bad keyword '%s'" % k)
                getattr(self, k).update(v)
                kwargs.pop(k)

    def _getpnames_(self):
        cls = self.__class__
        pnames = set()
        while issubclass(cls, _Plot_):
            for k in cls.__dict__:
                if k[0:4] == "ckw_":
                    pnames.add(k[4:])
            if not hasattr(cls, "__base__") or cls._stop_ckw_dependency_:
                break
            cls = cls.__base__
        return pnames

    def _initKw_(self):
        """ Look inside the class and find the ckw_SOMETHING paramters
        create kw_SOMETHING empty dictionary if does not exists or copy
        the self.kw_SOMETHING if already exists
        """
        for pname in self._getpnames_():
            if hasattr(self, "kw_"+pname):
                setattr(self, "kw_"+pname,
                        self._kw_classes(getattr(self, "kw_"+pname)))
            else:
                setattr(self, "kw_"+pname, self._kw_classes())

    def __getattr__(self, attr):
        if attr in ["axes", "figure"]:
            return super(_Plot_, self).__getattribute__("get_"+attr)()
        if attr is "kw":
            return plotsetter(self)

        if attr[-1] == "_" and hasattr(self, attr[0:-1]):
            # fancy feature for attribute terminating by '_'
            # the return object is self, one can do obj.plot_().axes_set_().etc...
            return return_plot_decorator(self, super(_Plot_, self).__getattribute__(attr[0:-1]))

        return super(_Plot_, self).__getattribute__(attr)

    def _parsekwargs_(self, kwargs, pname):
        """
        set keys in kwargs found in self.kw_PNAME or self.ckw_PNAME
        look also of ckw_PNAME in the base class and sub base class except
        if the attribute _stop_ckw_dependency_ is True
        """
        skw = "kw_"+pname
        sckw = "ckw_"+pname
        kw = getattr(self, skw)
        ckw = getattr(self, sckw)
        #
        # If save is False make a copy of kw
        # otherwhise leave it has it is
        if kwargs.pop("save", False):
            for k, v in kwargs.iteritems():
                kw[k] = v

        for k, v in kw.iteritems():
            kwargs.setdefault(k, v)
        for k, v in ckw.iteritems():
            kwargs.setdefault(k, v)

        #
        # Now look into the subclasses definition for kw
        #
        while hasattr(self.__class__, "__base__") and issubclass( self.__class__.__base__, _Plot_) and not self._stop_ckw_dependency_:
            if hasattr(self.__class__.__base__, sckw):
                for k,v in getattr(self.__class__.__base__ , sckw).iteritems():
                    kwargs.setdefault(k,v)
            self = self.__class__.__base__

    def getKwAxes(self,kw):
        """
        (kw,) -> look for 'axes' keyword in kw
                 return self.getaxes( kw.get("axes", None))
        """
        return self.getaxes( kw.pop("axes", None), kw.pop("figure",None) )

    def new_axes(self, axname, **kwargs):
        axes = kwargs.get("axes",None)
        if axes == axname:
            raise ValueError("axes cannot reference to itself")
        self.kw_allaxes[axname] = kwargs
        return axes

    def axes_set(self,  **kwargs):
        ikeys = kwargs.keys()
        if "axes" in ikeys: ikeys.remove("axes")
        self._parsekwargs_(kwargs, "axes")

        axes =  kwargs.pop("axes", None)
        if isinstance(axes,basestring):
            kwa = {}
            self._parsekwargs_(kwa, "allaxes")
            if not axes in kwa:
                raise KeyError("unknown axes '%s' "%axes)

            for k,v in kwa[axes].iteritems():
                if (not k in ikeys) or (k=="axes"): #conserve if parsed in **kwargs
                    # but eraze if comming from default
                    kwargs[k] = v

            return self.axes_set(**kwargs)

        axes = self.getKwAxes(kwargs)

        return axes.set(**kwargs)


    def figure_set(self, **kwargs):
        self._parsekwargs_(kwargs, "figure")
        figure = self.getFigure(kwargs.pop( "figure",None))
        return figure.set(**kwargs)

    def legend(self, **kwargs):
        self._parsekwargs_(kwargs, "legend")
        axes = self.getKwAxes(kwargs)
        if kwargs.pop("nolegend", False):
            return
        axes.legend(**kwargs)
    legend.__doc__ = plt.legend.__doc__

    def getaxes(self, axes=None, figure=None):
        """
        (axes,) -> if axes is not None return axes and set at default
        -> if axes return the default axes if any or plt.gca()
        """
        figure = self.getFigure(figure)

        if axes is None:
            axes = self._axes

        if axes:
            if isinstance( axes, (plt.Axes, plt.Subplot)):
                return axes
            if isinstance( axes, tuple):
                if len(axes)==2:
                    #########
                    # If axes is a (N,i) tuple, build the (nx,ny,i) tuple automaticaly
                    #########
                    nx = int(np.sqrt(axes[0]))
                    ny = int(np.ceil(axes[0]/float(nx)))
                    axes = (nx,ny,axes[1])
                elif len(axes)!=3:
                    raise ValueError("if axes is a tuple must be a (nx,ny, axenum) or (N,axenum) tuple got %s"%(axes))
                return figure.add_subplot( *axes)
            if isinstance(axes, list):
                if len(axes)!=4:
                    raise ValueError("if axes is a list, must be a  \
                    [left, bottom, width, height] 4 element list")
                return figure.add_axes(axes)

            if isinstance(axes, int):
                allaxes = figure.get_axes()
                if axes>len(allaxes):
                    raise ValueError("Ask axes %d but figure contains only %d subplot"%(axes, len(allaxes)))
                return allaxes[axes]
            if isinstance(axes,basestring):
                kwa = {}
                self._parsekwargs_(kwa, "allaxes")
                if not axes in kwa:
                    raise KeyError("unknown axes '%s' "%axes)
                return self.getaxes(kwa[axes].get("axes",None),
                                    kwa[axes].get("figure",None)
                                )



            raise ValueError("axis should be a Axis instance, a 3 element tuple or a integer, got %s: "%axes)

        return figure.gca()
    get_axes = getaxes
    def getFigure(self, figure=None):
        if isinstance( self._axes, plt.Axes):
            return self._axes.figure
        if figure is None:
            figure = self._figure

        if figure is not None:
            return plt.figure(figure)
        return plt.gcf()
    get_figure = getFigure

    def setAxes(self, axes):
        """
        (axes,) -> set the default axes
        """
        self._axes = axes

    def setDefaultAxes(self,axes):
        """
        (axes,) -> set the axes if not defined
        return axes
        """
        if not self._axes: self._axes = axes
        return axes

    def getData(self, k, value=_void_):
        """
        getData( key, value=_void_)
        return the data of key KEY if value is _void_ else return value
        """
        if value is not _void_: return value
        return getattr(self, k)

    def getInfoKw(self):
        output = []
        for kw_plot in self.__dict__:
            if kw_plot[0:3] == "kw_":
                plot = kw_plot[3:]
                kwargs = {}
                self._parsekwargs_(kwargs, plot)
                output.append( "{plot} : {kwargs}".format(plot=plot, kwargs=kwargs))
        return output
    def getInfoDispach(self):
        output = []
        for dispach_plot, dispach in self.__class__.__dict__.iteritems():

            if dispach_plot[0:8] == "dispach_":
                output.append("Plot {plot}: ".format(plot=dispach_plot[8:]))
                for keys, plots in dispach.iteritems():
                    if keys is all: continue
                    output.append( "   {keys}  ->  {plots}".format(keys=keys,plots=plots))
                if all in dispach:
                    output.append( "all others keys -> {plots}".format(plots=dispach[all]))
        return output
    def info(self):
        print " Single plots default settings : "
        print "\n".join(self.getInfoKw())
        print "\n\n Keyword dispatch for multy plot functions call :"
        print "\n".join(self.getInfoDispach())

    def go(self, plotname, *args, **kwargs):
        if not hasattr(self, plotname):
            raise AttributeError("Cannot find plot '%s'"%plotname)
        return getattr(self, plotname)(*args, **kwargs)

    def go_(self, *args, **kwargs):
        self.go(*args, **kwargs)
        return self.go_

    ###################################################################
    #
    # Some usefull copied function
    #
    ####################################################################
    def subplots_adjust(self,**kwargs):
        self._parsekwargs_(kwargs,"subplots_adjust")
        return self.figure.subplots_adjust(**kwargs)

    def show(self):
        return self.figure.show()
    def draw(self):
        return self.figure.canvas.draw()
    def savefig(self, *args, **kwargs):
        return self.figure.savefig(*args, **kwargs)
    def aclear(self):
        return self.axes.clear()
    def fclear(self):
        return self.figure.clear()

class _XPlot_(_Plot_):
    ckw_acorr    = {}
    ckw_hist     = {}
    dispach_init = {}
    x    = None
    xerr = None

    def setData(self,x=None, xerr=None):
        self.setX(x)
        self.setXerr(xerr)

    def setX(self,x):
        self.x = x
    def setXerr(self,xerr):
        self.xerr = xerr

    def getX(self,x=_void_):
        return self.getData("x", x)
    def getXerr(self,xerr=_void_):
        return self.getData("xerr",xerr)
    def getXXerr(self, x=_void_, xerr=_void_):
        return [self.getX(x), self.getXerr(xerr)]

    def substituteX(self, kw):
        return self.getX(kw.pop("x",_void_))
    def substituteXerr(self, kw):
        return self.getXerr(kw.pop("xerr",_void_))
    def substituteXXerr(self, kw):
        return [ self.substituteX(kw), self.substituteXerr(kw)]

    def acorr(self, **kwargs):
        self._parsekwargs_(kwargs, "acorr")
        axes = self.getKwAxes(kwargs)
        x = self.substituteX(kwargs)
        return axes.acorr(x, **kwargs)
    acorr.__doc__ = _make_doc("acorr", plt.acorr)

    def hist(self, **kwargs):
        self._parsekwargs_(kwargs, "hist")
        axes = self.getKwAxes(kwargs)
        x    = self.substituteX(kwargs)
        masknan = kwargs.pop("masknan", False)
        if masknan and isinstance(x, np.ndarray):
            x= _masknan(x)

        return axes.hist(x, **kwargs)
    hist.__doc__ = _make_doc("hist", plt.hist)



class _XYPlot_(_Plot_):
    ckw_plot = {}
    ckw_fit  = {}
    ckw_errorbar = {}
    ckw_scatter  = {}
    ckw_fit      = {}
    ckw_plotfit  = {}
    ckw_plotresiduals = {}
    ckw_bar      = {}

    dispach_init = {}

    def setData(self, x=None, y=None,xerr=None, yerr=None):
        self.setX(x)
        self.setY(y)
        self.setXerr(xerr)
        self.setYerr(yerr)

    def setX(self, x):
        self.x = x
    def setY(self, y):
        self.y = y
    def setXerr(self, xerr):
        self.xerr = xerr
    def setYerr(self, yerr):
        self.yerr = yerr

    def getX(self,x=_void_):
        return self.getData("x",x)
    def getY(self, y=_void_):
        return self.getData("y",y)
    def getXY(self,x=_void_,y=_void_):
        return [self.getX(x) ,self.getY(y)]
    def getXerr(self,xerr=_void_):
        return self.getData("xerr",xerr)
    def getYerr(self, yerr=_void_):
        return self.getData("yerr",yerr)
    def getXYerr(self,x=_void_,y=_void_,xerr=_void_, yerr=_void_):
        return [self.getX(x), self.getY(y), self.getXerr(xerr), self.getYerr(yerr)]

    def substituteX(self, kw):
        return self.getX(kw.pop("x",_void_))
    def substituteY(self, kw):
        return self.getY(kw.pop("y",_void_))
    def substituteXY(self, kw):
        return [self.substituteX(kw), self.substituteY(kw)]
    def substituteXerr(self, kw):
        return self.getXerr(kw.pop("xerr",_void_))
    def substituteYerr(self, kw):
        return self.getYerr(kw.pop("yerr",_void_))

    def substituteXYerr(self, kw):
        return [self.substituteX(kw), self.substituteY(kw), self.substituteXerr(kw), self.substituteYerr(kw)]

    def plot(self,*args, **kwargs):
        self._parsekwargs_(kwargs, "plot")
        axes = self.getKwAxes(kwargs)
        x, y = self.substituteXY(kwargs)
        if "fmt" in kwargs and len(args)==0: # copy the fmt keyword argument to the args argument
            args = list(args)
            args.append(kwargs.pop("fmt"))
        else:
            kwargs.pop("fmt",None)

        return axes.plot(x, y, *args, **kwargs)
    plot.__doc__ = _make_doc("plot", plt.plot)

    def errorbar(self, *args, **kwargs):
        self._parsekwargs_(kwargs, "errorbar")
        axes = self.getKwAxes(kwargs)
        x, y,xerr,yerr = self.substituteXYerr(kwargs)
        return axes.errorbar(x,y,xerr=xerr,yerr=yerr,*args ,**kwargs)
    errorbar.__doc__ = _make_doc("plot", plt.errorbar)


    def scatter(self, *args, **kwargs):
        self._parsekwargs_(kwargs, "scatter")
        axes = self.getKwAxes(kwargs)
        x, y  = self.substituteXY(kwargs)
        return axes.scatter(x,y,*args ,**kwargs)
    errorbar.__doc__ = _make_doc("scatter", plt.scatter)


    def fit(self, fitter=None, **kwargs):
        """
        plot.fit(fitter, **kwargs)
        execute fitter.fit()  and return fitter
        also fitter is set in plot._lastFit
        """
        self._parsekwargs_(kwargs, "fit")
        fitter = fitter or kwargs.pop("fitter", None)
        if fitter is None:
             raise Exception("No fitter installed")
        x, y, xerr,yerr = self.substituteXYerr(kwargs)
        self._lastFit = fitter.copy(**kwargs)
        self._lastFit.fit(x,y, xerr, yerr)
        return self._lastFit

    def getFitResult(self, fitter):
        return fitter.result
    def getFitLabel(self, fitter):
        return fitter.get_text()


    def plotfit(self,fitter=None, **kwargs):
        self._parsekwargs_(kwargs, "plotfit")
        fit = fitter or self._lastFit
        axes = self.getKwAxes(kwargs)

        if fit is None:
            raise Exception("no 'fit' in kwargs and no fit ran. use self.fit()")
        space, Npoints, plotall, labelresult = kwargs.pop("space", np.linspace), kwargs.pop("Npoints",None), kwargs.pop("plotall",True), kwargs.pop("labelresult",True)
        if "x" in kwargs:
            y = fit.get_model(kwargs.pop("x"))
        else:
            x = self.getX()
            xrange = fit.kw.get("xrange", (None,None))
            xrange = xrange or (None, None) # in case xrange is None
            xmin  = x.min() if xrange[0] is None or plotall else xrange[0]
            xmax  = x.max() if xrange[1] is None or plotall else xrange[1]

            x, y = fit.get_xmodel(xmin, xmax, Npoints, space=space)
        if labelresult:
            kwargs.setdefault( "label", self.getFitLabel(fit) ) # setdefaul label will overwrite
        p = axes.plot(x, y, **kwargs)[0]
        fit._lastLine = p
        return p

    plotfit.__doc__ =  """
        plot the fit representation
        kwargs
        ------
        fit = fit object with a result attached. If not look at the lastfit of this object
        x = an array of x point to draw the line if any Npoint, space and plotall
            is ignored
        Npoints = number of point used to draw line else default of fit is used
        space   = space  to draw line default is np.linspace
        plotall = if True draw the line from x.min() to x.max() in any case
                  if False use the xrange limits if any in the fit object

        all other keywords are plt.plot keywords. see bellow.

        """+plt.plot.__doc__ or ""



    def plotresiduals(self, **kwargs):
        """
        Plot residuals of curent fit or fit in kwargs
        accept fit as keywors all other are errorbar keywords, see below.

        """
        self._parsekwargs_(kwargs, "plotresiduals")
        fit = kwargs.pop("fit", self._lastFit)
        axes = self.getKwAxes(kwargs)
        if fit is None:
            raise Exception("no 'fit' in kwargs and no fit ran. use self.fit()")
        x, y, xerr, yerr = self.substituteXYerr(kwargs)
        y = fit.get_residuals(x,y)
        return axes.errorbar(x ,y, xerr, yerr, **kwargs)
    plotresiduals.__doc__ += plt.errorbar.__doc__

    def bar(self, **kwargs):
        self._parsekwargs_(kwargs)
        axes= self.getKwAxes(kwargs)
        x,y,xerr,yerr = self.substituteXYerr(kwargs)
        return axes.bar(x, y, xerr=xerr, yerr=yerr, **kwargs)
    bar.__doc__ = _make_doc("bar",plt.bar)

    def barh(self, **kwargs):
        self._parsekwargs_(kwargs)
        axes= self.getKwAxes(kwargs)
        x,y,xerr,yerr = self.substituteXYerr(kwargs)
        return axes.barh(y, x, xerr=xerr, yerr=yerr, **kwargs)
    barh.__doc__ = _make_doc("barh",plt.barh)





class _Fit_(object):
    kw = {"xrange":(None,None), "yrange":(None,None), "dim":1}
    result = None
    def set(self, **kwargs):
        self.kw.update( kwargs )

    def _initKw_(self, kwargs):
        self.kw.update(kwargs)

    def _init_(self, kwargs):
        self.kw = self.kw.copy() # make a copy of the class definition
        self._initKw_(kwargs)

    def __init__(self, **kwargs):
        self._init_(kwargs)
    def _parsekwargs_(self, kwargs):
        for k,v in self.kw.iteritems():
            kwargs.setdefault(k,v)
    def __call__(self, *args, **kwargs):
        self.fit(*args, **kwargs)
        return self.result

    def copy(self, **kwargs):
        kw = self.kw.copy()
        kw.update( kwargs)
        return self.__class__(**kw)

class Stats(_Fit_):
    def fit(self, x, y, xerr=None, yerr=None, bins=10, sens=0,
            freduces=[np.mean], align="mid"):
        self._parsekwargs_(kwargs)
        xrange, yrange = kwargs.get("xrange", (None,None)), kwargs.get("yrange", (None,None))
        size = len(x)
        if not hasattr(xerr, "__iter__"):
            if  not hasattr(yerr, "__iter__"):
                x,y = clip( (np.asarray(x),xrange), (np.asarray(y),yrange) )
            else:
                x,y,xerr  = clip( (np.asarray(x),xrange), (np.asarray(y),yrange), additional=[np.asarray(xerr)] )
        elif not hasattr(yerr, "__iter__"):
            x,y, xerr = clip( (np.asarray(x),xrange), (np.asarray(y),yrange), additional=[np.asarray(xerr)] )
        else:
            x,y,xerr, yerr = clip( (np.asarray(x),xrange), (np.asarray(y),yrange), additional=[np.asarray(xerr),np.asarray(yerr)] )

        if sens: x, y, xerr, yerr = y, x, yerr, xerr

        values, labels = (x, y) if sens else (y, x)

        if isinstance(bins, (int,long)):
            bins = np.linspace( labels.min(), labels.max(), bins )
        else:
            bins = np.array(bins)

        stats = [ np.array( [freduce(values[(labels>=b1) & (labels<b2)])  for b1,b2 in zip(bins[0:-1],bins[1:None])]) for freduce in freduces ]
        if align=="mid":
            bins = (bins[1:None]+bins[0:-1])/2.0
        elif align=="left":
            bins = bins[0,-1]
        elif align=="right":
            bins = bins[1,None]
        else:
            KeyError("align should be one of 'mid', 'left', 'right'")
        self.result = [bins, stats, sens]
        return bins, stats

    def get_model(self, x, statnum=0):
        if self.result is None:
            raise ValueError("no fit result recorded use .fit before")
        bins = self.result[0]



class Fit2d(_Fit_):
    dim = 1
    def fit(self, x,y,xerr=None, yerr=None, **kwargs):
        self._parsekwargs_(kwargs)
        dim, xrange, yrange = kwargs.get("dim",1), kwargs.get("xrange", (None,None)), kwargs.get("yrange", (None,None))

        size = len(x)
        if not hasattr(xerr, "__iter__"):
            if  not hasattr(yerr, "__iter__"):
                x,y = clip( ( np.asarray(x),xrange), (np.asarray(y),yrange) )
            else:
                x,y,yerr  = clip( (np.asarray(x),xrange), (np.asarray(y),yrange), additional=[np.asarray(yerr)] )
        elif not hasattr(yerr, "__iter__"):
            x,y, xerr = clip( (np.asarray(x),xrange), (np.asarray(y),yrange), additional=[np.asarray(xerr)] )
        else:
            x,y,xerr, yerr = clip( (np.asarray(x),xrange), (np.asarray(y),yrange), additional=[np.asarray(xerr),np.asarray(yerr)] )
        self.result = mpolyfit(np.array(x),np.array(y),dim)
        return self.result

    def get_model(self, x):
        if self.result is None:
            raise ValueError("no fit result recorded use .fit before")
        coef = self.result
        return get_polynome_model(x, coef)

    def get_residuals(self,x, y):
        if self.result is None:
            raise ValueError("no fit result recorded use .fit before")
        return y-self.get_model(x)


    def get_xmodel(self, xmin, xmax,  Npoints=None, space=np.linspace):
        if self.result is None:
            raise ValueError("no fit result recorded use .fit before")
        if Npoints is None:
            Npoints = 2 if len(self.result)<3 else 1000
        x = space(xmin, xmax, Npoints)
        return x,self.get_model(x)

    def get_text(self):
        if self.result is None:
            raise ValueError("no fit result recorded use .fit before")
        if "text" in self.kw:
            return self.kw["text"](self)
        return self._get_text(self) # _get_text is static

    @staticmethod
    def _get_text(fit):
        return coef2polynome(fit.result)

def coef2polynome(coef, fmt="%g", x="x"):
        out = ""
        dim = len(coef)
        for i,c in enumerate(coef):
            if (dim-1)==0:
                xs = ""
            elif (dim-1)==1:
                xs= x
            else:
                xs = "%s^%d"%(x,dim-1)
            out += ("%s"+fmt+" %s ")%( "-" if c<0.0 else "+" if i else "", np.abs(c), xs )
            dim -= 1
        return out


def cliptest(data, datarange=None):
    dmin, dmax = (None, None) if datarange is None else datarange
    test = np.ones(data.shape, dtype=np.bool)
    if issubclass(type(data),np.ma.MaskedArray):
        mask = data.mask
        test *= ~mask
    if dmin is not None:
        test *= data>=dmin
    if dmax is not None:
        test *= data<=dmax
    return test

def cliptest_values(data_range, *args, **kwargs):
    data, datarange = data_range
    test = cliptest(data, datarange)
    for data, datarange in args:
        test *= cliptest(data, datarange)
    return test

def clip(data_range, *args, **kwargs):
    additional = kwargs.pop("additional", [])
    if len(kwargs):
        raise KeyError("take only additional= as keyword argument")

    test = cliptest_values(data_range, *args)
    out = [ data_range[0][test]  ]
    for data, dr in args:
        out.append( data[test])
    for data in additional:
        out.append( data[test] if data is not None else None)
    return tuple(out)

class NotEnoughData(Exception):
    pass

def mpolyfit(x,y,dim):
    if dim==0: return y.mean()
    if len(x)<(dim+1):
        raise NotEnoughData("Not enought point to fit")

    xx = np.vstack([ pow(x,e) for e in np.arange(dim+1)[::-1]])
    w = np.linalg.lstsq( xx.T, y)
    return w[0]

def get_polynome_model(x, coef):
    y = np.zeros_like(x)
    y[:] = coef[-1]
    dim = len(coef)-1
    for i in range( dim):
        y += coef[i]*x**(dim-i)
    return y



AXES_DISPACH    = ("xlabel","ylabel","title", "xlim", "ylim")
FIGURE_DISPACH  = ("figsize", "dpi", "size_inches")
LINE_DISPACH    = ("fmt","marker", "alpha", "linestyle", "fillstyle", "markersize",
                   "drawstyle", "markerfacecolor", "mfc", "markeredgecolor","mec",
                   "markeredgewidth", "mew", "antialiased", "aa", "alpha")
XYFIT_FIDPACH   = ("xrange", "yrange", "dim", "fitter")

class XYPlot(_XYPlot_):
    ckw_plot = {}
    ckw_fit  = {}
    ckw_errorbar = {}
    ckw_fit     = {}
    ckw_plotfit = {"linestyle":"--"}
    ckw_plotresiduals = {}
    ckw_bar = {}

    ckw_xaverage = {"labelresult":True}
    ckw_yaverage = {"labelresult":True}

    ckw_xmedian = {"labelresult":True}
    ckw_ymedian = {"labelresult":True}
    ckw_ystd    = {"labelresult":True}
    ckw_xstd    = {"labelresult":True}
    ckw_xmedstat = {}
    ckw_ymedstat = {}
    ckw_xmeanstat = {}
    ckw_ymeanstat = {}
    ckw_xstdstat = {}
    ckw_ystdstat = {}
    ckw_statline = {}
    dispach_init = {
        ("color",)    :("plotfit", "plotresiduals", "errorbar", "plot"),
        LINE_DISPACH  :("plot","errorbar", "plotresiduals"),
        AXES_DISPACH  :("axes",),
        FIGURE_DISPACH:("figure",),
        XYFIT_FIDPACH: ("fit", ),
        ("fit",):("plotandfit",)
    }
    dispach_all = dispach_init.copy()
    dispach_all.update({
        tuple(list(XYFIT_FIDPACH)+list(LINE_DISPACH)+["fit","color"]):
        ("plotandfit",),
        ("nolegend",):("legend",),
        ("axes","figure"):("plotandfit", "axes", "legend"),
        ("kw_fit","kw_fitandplot", "kw_errorbar", "kw_plotfit"):("plotandfit",),
        all:("errorbar",)
    }
    )
    dispach_xallstat = {
        ("kw_xstdstat") :("xstdstat"),
        ("kw_xmeanstat"):("xmeanstat"),
        all:("xstdstat", "xmeanstat")
    }
    dispach_yallstat = {
        ("kw_ystdstat") :("ystdstat"),
        ("kw_ymeanstat"):("ymeanstat"),
        all:("ystdstat", "ymeanstat")
    }

    dispach_xmedstd = {
        all:("xmedian","xstd")
    }
    dispach_ymedstd = {
        all:("xmedian","xstd")
    }
    dispach_xaveragestd = {
        all:("xaverage","xstd")
    }
    dispach_yaveragestd = {
        all:("xaverage","xstd")
    }


    dispach_plotandfit = {
        XYFIT_FIDPACH:("fitandplot",),
        ("color",)  : ("fitandplot" , "errorbar"),
        LINE_DISPACH:("errorbar",),
        ("axes","figure")   :("fitandplot" , "errorbar"),
        ("kw_fit", "kw_plotfit"):("fitandplot",),
        all:("errorbar",)
    }

    dispach_fitandplot = {
        XYFIT_FIDPACH : ("fit",),
        ("color",)    : ("errorbar", "plotfit"),
        LINE_DISPACH  : ("errorbar",),
        ("axes","figure") : ("errorbar","plotfit"),
        #("kw_plotfit",)   : ("plotfit"),
        all:("plotfit",)
    }
    x    = None
    y    = None
    xerr = None
    yerr = None


    def stat(self, bins=10, sens=0, freduces=[np.mean], align="mid",**kwargs):
        y = self.substituteY(kwargs)
        x = self.substituteX(kwargs)
        values, labels = (x, y) if sens else (y, x)

        if isinstance(bins, (int,long)):
            bins = np.linspace( labels.min(), labels.max(), bins )
        else:
            bins = np.array(bins)

        stats = [ np.array( [freduce(values[(labels>=b1) & (labels<b2)])  for b1,b2 in zip(bins[0:-1],bins[1:None])]) for freduce in freduces ]
        if align=="mid":
            bins = (bins[1:None]+bins[0:-1])/2.0
        elif align=="left":
            bins = bins[0,-1]
        elif align=="right":
            bins = bins[1,None]
        else:
            KeyError("align should be one of 'mid', 'left', 'right'")
        return bins, stats

    def statlines(self, bins=10, sens=0, freduces=[np.mean, lambda d:d.mean()+d.std(), lambda d: d.mean() + d.std()], align="mid", **kwargs):
        self._parsekwargs_(kwargs, "statline")
        bins, stats = self.stat(bins=bins, sens=sens, freduces=freduces, align=align, **kwargs)
        output = []
        for stat in stats:
            if sens:
                output.append(plot( stat, bins, kwargs.pop("fmt", "-"), **kwargs))
            else:
                output.append(plot( bins, stat, kwargs.pop("fmt", "-"), **kwargs))
        return outputs

    def statline(self, bins=10, sens=0, freduce=np.mean, align="mid",  **kwargs):
        self._parsekwargs_(kwargs, "statline")
        axes = self.getKwAxes(kwargs)
        bins, stats = self.stat(bins=bins, sens=sens, freduces=[freduce], align=align, **kwargs)
        stat = stats[0]
        if sens:
            return plot( stat, bins, kwargs.pop("fmt", "-"),axes=axes,  **kwargs)
        else:
            return plot( bins, stat, kwargs.pop("fmt", "-"),axes=axes,  **kwargs)
    def xmedstat(self,  **kwargs):
        self._parsekwargs_(kwargs, "xmedstat")
        kwargs["sens"] = 1
        kwargs["freduce"] = np.median
        return self.statline(**kwargs)
    def ymedstat(self,  **kwargs):
        self._parsekwargs_(kwargs, "ymedstat")
        kwargs["sens"] = 0
        kwargs["freduce"] = np.median
        return self.statline(**kwargs)
    def xmeanstat(self,  **kwargs):
        self._parsekwargs_(kwargs, "xmeanstat")
        kwargs["sens"] = 1
        kwargs["freduce"] = np.mean
        return self.statline(**kwargs)
    def ymeanstat(self,  **kwargs):
        self._parsekwargs_(kwargs, "ymeanstat")
        kwargs["sens"] = 0
        kwargs["freduce"] = np.mean
        return self.statline(**kwargs)

    def xstdstat(self,  meanreduce=np.mean, **kwargs):
        self._parsekwargs_(kwargs, "xstdstat")
        kwargs["sens"] = 1
        kw1, kw2 = kwargs.copy(), kwargs.copy()
        kw1["freduce"] = lambda data: meanreduce(data)+np.std(data)
        kw2["freduce"] = lambda data: meanreduce(data)-np.std(data)
        return [self.statline(**kw1),self.statline(**kw2)]

    def ystdstat(self,  meanreduce=np.mean, **kwargs):
        self._parsekwargs_(kwargs, "ystdstat")
        kwargs["sens"] = 0
        kw1, kw2 = kwargs.copy(), kwargs.copy()
        kw1["freduce"] = lambda data: meanreduce(data)+np.std(data)
        kw2["freduce"] = lambda data: meanreduce(data)-np.std(data)
        return [self.statline(**kw1),self.statline(**kw2)]

    def xallstat(self, median=False, **kwargs):
        self._parseDispach_(kwargs, "xallstat")
        kw_xmeanstat =  kwargs.pop("kw_xmeanstat", {})
        kw_xstdstat  =  kwargs.pop("kw_xstdstat" , {})
        if median:
            kw_xstdstat["meanreduce"] = np.median
            return [self.xmedstat( **kw_xmeanstat), self.xstdstat(**kw_xstdstat)]
        return [self.xmeanstat( **kw_xmeanstat), self.xstdstat(**kw_xstdstat)]

    def yallstat(self, median=False, **kwargs):
        self._parseDispach_(kwargs, "yallstat")
        kw_ymeanstat =  kwargs.pop("kw_ymeanstat", {})
        kw_ystdstat  =  kwargs.pop("kw_ystdstat" , {})
        if median:
            kw_ystdstat["meanreduce"] = np.median
            return [self.ymedstat( **kw_ymeanstat), self.ystdstat(**kw_ystdstat)]
        return [self.ymeanstat( **kw_ymeanstat), self.ystdstat(**kw_ystdstat)]

    def xmedian(self, **kwargs):
        self._parsekwargs_(kwargs, "xmedian")

        axes = self.getKwAxes(kwargs)
        x = np.median(self.substituteX(kwargs))
        label = kwargs.pop("labelformat","Med(X) %.3e")%x if kwargs.pop("labelresult",False) else None
        return axvline(x=x , axes=axes, label=label, **kwargs)
    def ymedian(self, **kwargs):
        self._parsekwargs_(kwargs, "ymedian")
        axes = self.getKwAxes(kwargs)
        y = np.median(self.substituteY(kwargs))
        label = kwargs.pop("labelformat","Med(Y) %.3e")%y if kwargs.pop("labelresult",False) else None
        return axhline(y=y,axes=axes, label=label, **kwargs)
    def xaverage(self, **kwargs):
        self._parsekwargs_(kwargs, "xaverage")
        axes = self.getKwAxes(kwargs)
        x = np.mean(self.substituteX(kwargs))
        label = kwargs.pop("labelformat","Avg(X) %.3e")%x if kwargs.pop("labelresult",False) else None
        return axvline(x=x , axes=axes, label=label, **kwargs)
    def yaverage(self, **kwargs):
        self._parsekwargs_(kwargs, "yaverage")
        axes = self.getKwAxes(kwargs)
        y = np.average(self.substituteY(kwargs))
        label = kwargs.pop("labelformat","Avg(Y) %.3e")%y if kwargs.pop("labelresult",False) else None
        return axhline(y=y,axes=axes, label=label, **kwargs)
    def xstd(self, meanreduce=np.mean, **kwargs):
        self._parsekwargs_(kwargs, "xstd")
        axes = self.getKwAxes(kwargs)
        x = self.substituteX(kwargs)
        std = np.std(x)
        label = kwargs.pop("labelformat","Std(X) %.3e")%std if kwargs.pop("labelresult",False) else None
        avg = meanreduce(x)
        return [axvline(x=avg+std , axes=axes,label=label,**kwargs), axvline(x=avg-std , axes=axes,**kwargs)]

    def ystd(self, meanreduce=np.mean, **kwargs):
        self._parsekwargs_(kwargs, "ystd")
        axes = self.getKwAxes(kwargs)
        y = self.substituteY(kwargs)
        std   = np.std(y)
        label = kwargs.pop("labelformat" , "Std(Y) %.3e")%std if kwargs.pop("labelresult",False) else None
        avg = meanreduce(y)
        return [axhline(y=avg+std ,axes=axes,label=label,**kwargs), axhline(y=avg-std ,axes=axes, **kwargs)]


    def fitandplot(self, **kwargs):

        self._parseDispach_(kwargs, "fitandplot")
        kw_fit = kwargs.pop("kw_fit", {})
        try:
            self.fit(**kw_fit)
        except NotEnoughData:
            print "Warning not enough point to fit"
            return None

        kw_plotfit = kwargs.pop("kw_plotfit", {})
        return self.plotfit(**kw_plotfit)
    fitandplot.__doc__ = """ fit the point and plot result with plotfit.
    kw_fit dictionary is passedd to fit other keyword are passed to plotfit
    plotfit help:
    """+(_XYPlot_.plotfit.__doc__ or "")

    def plotandfit(self, **kwargs):
        """
        Plot the data fit it with the fitter and plot the fit.
        Return the result in a tuple of the return of
        (self.errorbar,self.plotfit,self.fit)
        """

        self._parseDispach_(kwargs, "plotandfit")
        kw_fitandplot  = kwargs.pop("kw_fitandplot", {})
        kw_errorbar    = kwargs.pop("kw_errorbar"  , {})

        er  = self.errorbar(**kw_errorbar)

        # set the same color to the point color
        if kwargs.get("fit",True):
            if not "color" in self.kw_plotfit:
                kw_fitandplot.setdefault("color", er.lines[0].get_color())
            pf = self.fitandplot(**kw_fitandplot)
            return (er,pf)
        return (er, None)

    def all(self, *args, **kwargs):
        """ Use the errorbar function to plot xy data"""
        output = []

        self._parseDispach_(kwargs, "all")
        #        self.plotandfit( kw_errorbar = kwargs.pop ("kw_errorbar", {}),
        #                         kw_fit      = kwargs.pop("kw_fit", {}),
        #                         kw_plotfit  = kwargs.pop("kw_plotfit", {}),
        #                         _noDispach_ = True
        #                     )
        output.append(self.plotandfit( **kwargs.pop("kw_plotandfit", {})))

        self.axes_set(   **kwargs.pop("kw_axes",  {})  )
        self.figure_set( **kwargs.pop("kw_figure",  {})  )
        self.subplots_adjust( **kwargs.pop("kw_subplots_adjust",  {})  )
        self.legend  ( **kwargs.pop("kw_legend", {}))
        #self._warningKw_(kwargs)



class XPlot(_XPlot_):
    ckw_acorr = {}
    ckw_hist  = {}
    ckw_imshow = {}
    dispach_init = {
        ("color",):("hist",),
        ("bins",): ("hist",),
        AXES_DISPACH:("axes",),
        FIGURE_DISPACH:("figure",)
    }
    x    = None
    xerr = None



class ImgPlot(XPlot):
    ckw_imshow   = {}
    ckw_colorbar = {}

    dispach_init =  XPlot.dispach_init.copy()
    dispach_init.update({
        ("vmin","vmax"):("imshow",),
        AXES_DISPACH   :("axes",),
        FIGURE_DISPACH:("figure",)
    })

    img    = None
    imgerr = None
    lastimgplot = None
    def setData(self, img=None, imgerr=None):
        self.setImg(img)
        self.setImgerr(imgerr)
    def setImg(self, img):
        self.img = img
    def setImgerr(self, imgerr):
        self.imgerr = imgerr
    def getImg(self, img=_void_):
        return self.getData("img", img)

    def getX(self, x=_void_):
        # for compatibility of XPlot functions
        # can be a problem if img contain colors
        img = self.getImg(img=x)
        if isinstance(img, np.ndarray):
            return img.flatten()
        return img

    def getXerr(self, xerr=_void_):
        # for compatibility of XPlot functions
        img = self.getImg(img=xerr)
        if isinstance(img, np.ndarray):
            return img.flatten()
        return img

    def getImgerr(self, imgerr=_void_):
        return self.getData("img", imgerr)

    def substituteImg(self, kw):
        return self.getImg(kw.pop("img", _void_))
    def substituteImgerr(self, kw):
        return self.getImgerr(kw.pop("img", _void_))

    def imshow(self, **kwargs):
        self._parsekwargs_(kwargs, "imshow")
        axes = self.getKwAxes(kwargs)
        img = self.substituteImg(kwargs)
        masknan = kwargs.pop("masknan", False)

        if not isinstance( img, np.ma.MaskedArray):
            img = np.asarray(img)
        if masknan:
            img = _masknan(img)

        self.lastimgplot = axes.imshow(img , **kwargs)
        return self.lastimgplot

    def colorbar(self,mappable=None, **kwargs):
        self._parsekwargs_(kwargs, "colorbar")
        if mappable is None and self.lastimgplot is not None:
            kwargs.setdefault("ax", self.lastimgplot.axes)
            return self.lastimgplot.axes.figure.colorbar( self.lastimgplot, **kwargs )
        else:
            return self.figure.colorbar( mappable, **kwargs )
    hist.__doc__ = _make_doc("hist", plt.hist)



class XYPlotData(XYPlot):
    """
    XYPlot that use data stored in a dictionary or any object with a __getitem__ method
    """
    dataKeys = None #if dataKey exists x,y,xerr and yerr are stored during setData by defaults
    def setData(self, data, x=None, y=None, xerr=None, yerr=None):
        self.setRootData(data)
        self.setX(x)
        self.setY(y)
        self.setXerr(xerr)
        self.setYerr(yerr)
    def setRootData(self, data):
        self.data = data
    def setFromDataKey(self, on, key=None):
        if key is None:
            if self.dataKeys:
                key = self.dataKeys[on]
        if key:
            setattr(self,on, self.data[key])
    def setX(self, x=None):
        self.setFromDataKey("x", x)
    def setY(self, y=None):
        self.setFromDataKey( "y", y)
    def setXerr(self, xerr=None):
        self.setFromDataKey("xerr", xerr)
    def setYerr(self, yerr=None):
        self.setFromDataKey( "yerr", yerr)

    def getData(self,k , datakey=_void_):
        if (datakey is not _void_) and (datakey is not None):
            return self.data[datakey]
        return getattr(self, k)

#############################################################
#
#   Collector of plots in list  or dict
#
#############################################################

class PlotList(list):
    loopclasses = (list,)
    def __init__(self, *args):
        super(PlotList, self).__init__(*args)
        for plot in self:
            if not isinstance(plot, _Plot_):
                raise ValueError("All element of the list must be a _Plot_ intsance")
    @staticmethod
    def _reform_args( args, it , size):
        return [a[it%size] if isinstance( a, self.loopclasses) else a for a in args]

        argsout = []
        for a in args:
            if isinstance( a, self.loopclasses):
                argsout.append( a[it%size] )
            else:
                argsout.append(a)


    def go(self, plotnames, *args, **kwargs):
        size = len(self)
        output = []
        for i,plot in enumerate(self):
            pname = plotnames[i%size] if  isinstance( plotnames, self.loopclasses) else plotnames
            iargs = [a[i%len(a)] if isinstance( a, self.loopclasses)
                     else a for a in args]
            ikwargs = {k:v[i%len(v)] if isinstance( v, self.loopclasses)
                       else v for k,v in kwargs.iteritems()}
            ##############
            #   take an eventual kw_PLOTNAME kewords and apply the loop sequence on
            #   its items.
            ##############
            kw_wargs = {k:v[i%len(v)] if isinstance( v, self.loopclasses)
                        else v for k,v in ikwargs.pop("kw_"+pname, {})}
            ### update the temporaly kwargs with the kw_wargs
            ikwargs.update(kw_wargs)
            output.append(getattr(plot, pname)(*iargs, **ikwargs))
        return output
    def go_(self, *args, **kwargs):
        self.go(*args, **kwargs)
        return self.go_

    loop  = go
    loop_ = go_

    def all(self, *args, **kwargs):
        return self.go("all", *args, **kwargs)

    def __getattr__(self, attr):
        if hasattr(self, attr):
            return super(PlotList,self).__getattribute__(attr)
        def tmp(*args, **kwargs):
            return [ getattr(plot, attr)(*args, **kwargs) for plot in self ]
        tmp.__doc__ = "tmpfunc of attribute '%s' in PlotList"
        return tmp

class PlotClassList(list):
        def __init__(self, *args, **kwargs):
            super(PlotClassList, self).__init__(*args)
            for plotclass in self:
                if not issubclass(plotclass, _Plot_):
                    raise ValueError("All element of the list must be a _Plot_ subclass")
            # set the default data kwargs
            self.data = kwargs
        def copy(self,**kwargs):
            return self.__class__( self, **self.data)

        def plot(self, **kwargs):
            datakwargs = self.data.copy()
            datakwargs.update( kwargs )
            output = []
            for Plot in self:
                output.append(Plot.newWithData(**datakwargs))
            return PlotList( output )

class PlotClassDict(dict):
        data = None
        loopclasses = (list,)
        def __init__(self, *args, **kwargs):
            super(PlotClassDict, self).__init__(*args, **kwargs)
            for plotclass in self.itervalues():
                if not issubclass(plotclass, _Plot_):
                    raise ValueError("All element of the list must be a _Plot_ subclass")
            # set the default data kwargs
        def setData(self, **kwargs):
            self.data = kwargs
            return self

        def copy(self,**kwargs):
            new = self.__class__( self)
            if self.data:
                newkwargs = self.data.copy()
                newkwargs.update(kwargs)
            else:
                newkwargs = kwargs
            new.setData( **newkwargs)
            return new

        def loop(self, pnames=None, **kwargs):
            pnames = pnames or self.keys()
            if isinstance(pnames, str):
                pnames = [pnames]
            size = len(pnames)
            output = []

            #####
            ## kw_something = { param:loopclass([val1,val2,val3,...])}
            ## must be tranformed to
            ## kw_something = loopclass( [ {param:val1},{param:val2}, ...] )
            ##
            for kw_plot, dictvals in kwargs.iteritems():
                if kw_plot[0:3] == "kw_":
                    newkwlist = [ {k:v[i%len(v)] for k,v in dictvals.iteritems() } for i in range(size) ]
                    kwargs[kw_plot] = self.loopclasses[0](newkwlist)

            for i,pname in enumerate(pnames):
                Plot = self[pname]
                ikwargs = {k:v[i%len(v)] if isinstance( v, self.loopclasses)
                           else v for k,v in kwargs.iteritems()}

                kw_wargs = {k:v[i%len(v)] if isinstance( v, self.loopclasses)
                            else v for k,v in ikwargs.pop("kw_"+pname, {})}
                ikwargs.update(kw_wargs)
                output.append(self.plot(pname , **ikwargs))
            return PlotList(output)

        def loopaxes( self, pnames, **kwargs):
            pnames = pnames or self.keys()
            if isinstance(pnames, str):
                pnames = [pnames]
            size = len(pnames)
            nx = int(np.sqrt(size))
            ny = int(np.ceil(size/float(nx)))
            axes = [(ny,nx,i+1) for i in range(size)]
            kwargs["axes"] = axes
            return self.loop(pnames, **kwargs)

        def plot(self, pname, **kwargs):
            datakwargs = self.data.copy()
            datakwargs.update( kwargs )
            output = {}
            return self[pname].newWithData(**datakwargs)

        def __getattr__(self,attr):
            if attr in self:
                def tmp(**kwargs):
                    allkwargs = self.data.copy()
                    allkwargs.update(kwargs)
                    return self[attr].newWithData(**allkwargs)
                return tmp
            return super(PlotClassDict, self).__getattribute__(attr)
