from .. import baseplot
from ..baseplot import DetPlot, DetXYPlot, DetImgPlot
from .. import plots
import numpy as np


class Jitter(DetXYPlot):
    kw_plot = {"fmt":"-"}

    def setData(self, detector=None, yx=0,
                space=False, frate=None, dt=None,
                timemin=None, timemax=None,
                fmin=None, fmax=None, **kwargs
                ):

        j = detector
        direction = "x" if yx else "y"
        if space is "fft":
            x, y = j.getFft(frate=frate,
                            fmin=fmin, fmax=fmax, **kwargs)
            y = y.apply(yx_idx=yx)

            self.axes_set(xlabel="F [Hz]",
                          ylabel="%s jitter fft" % direction
                          )
        elif space is "psd":
            x, y = j.getPsd(frate=frate,
                            fmin=fmin, fmax=fmax,
                            **kwargs)
            y = y.apply(yx_idx=yx)

            self.axes_set(xlabel="F [Hz]",
                          ylabel="%s psd [pixel/sqrt(Hz)]" % direction
                         )
        elif space == "direct" or not space:
            x, y = j.getDirectSpace(frate=frate,
                                    timemin=timemin, timemax=timemax,
                                    **kwargs
                                   )
            y = y.apply(yx_idx=yx)
            self.axes_set(xlabel="Time [s]",
                          ylabel="%s jitter [pixel]" % direction
                         )

        else:
            raise KeyError("space should be 'direct', 'fft' or 'psd'")

        self.x = x
        self.y = y

jitter = Jitter.newWithData
baseplot._Jitter_._addPlot("jitter", Jitter)
