from .. import baseplot
from ..baseplot import DetPlot, DetImgPlot
from .. import plots
import numpy as np

class StabilityFit(plots.Fit2d):
    dim = 1

stabilityfit = StabilityFit(dim=1)


class M2Temp(DetPlot):
    ckw_axes = dict(xlabel="Temp [k]", ylabel="$\sigma^2$ [$ADU^2$]")
    ckw_legend = dict(loc="lower left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=stabilityfit)
    dataKeys = {"x": ("getTemp", "nokw"),
                "y": ("getSigma2",),
                "xerr": (None,),
                "yerr": ("getSigma2error",)
                }
m2temp = M2Temp.newWithData
baseplot.Images._addPlot("m2temp", M2Temp)
baseplot.Images._addPlot("sigma2temp", M2Temp)


class SignalTemp(DetPlot):
    ckw_axes = dict(xlabel="Temp [k]", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=stabilityfit)
    dataKeys = {"x": ("getTemp", "nokw"),
                "y": ("getSignal",), "xerr": (None,),
                "yerr": ("getSignalerror",)
                }
signaltemp = SignalTemp.newWithData
baseplot.Images._addPlot("signaltemp", SignalTemp)


class Stability(DetPlot):
    ckw_axes = dict(xlabel="MJD", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=stabilityfit)
    dataKeys = {"y": ("getSignal",),
                "yerr": ("getSignalerror",)
                }

    def setX(self, **kwargs):
        dets = self.detector
        mjd = dets.key("MJD-OBS")
        minmjd = int(np.min(mjd))
        self.x = mjd-minmjd
        self.kw_axes["xlabel"] = "MJD -%d" % minmjd

stability = Stability.newWithData
baseplot.Images._addPlot("stability", Stability)


class Stability2(DetPlot):
    ckw_axes = dict(xlabel="MJD", ylabel="<S> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=stabilityfit)
    dataKeys = {}

    def setX(self, **kwargs):
        det = self.detector
        mjd = det.getMjd(**kwargs).transform(
                                             [("time", "file", "time")],
                                             True
                                             )
        minmjd = int(np.min(mjd))
        self.x = mjd-minmjd
        self.kw_axes["MJD"] = "MJD -%d" % minmjd

    def setY(self, **kwargs):
        det = self.detector
        normalize = kwargs.pop("normalize", True)
        darksection = kwargs.pop("darksection", None)
        signal = det[0].getData(**kwargs).transform(
                                                    [("time", "file", "time"),
                                                     ("pix", None)],
                                                    True
                                                    )
        if normalize:
            signal = signal - signal.mean(axis="time")
        if darksection:
            kwargs["section"] = darksection
            darks = det[0].getData(**kwargs)
            darks = darks.transform(
                                    [("time", "file", "time"),
                                     ("pix",None)],True
                                    ).mean(axis="pix")

            signal = signal - darks
        self.y = signal

    def setYerr(self, **kwargs):
        det = self.detector
        rms = det[1].getData(**kwargs).transform(
                                                 [("time", "file", "time"),
                                                  ("pix", None)],
                                                 True
                                                 )
        self.yerr = rms
baseplot.CubeImage._addPlot("stability", Stability2)
baseplot.Images._addPlot("stability2", Stability2)


class M2Stability(Stability):
    dataKeys = {"y": ("getSigma2",), "yerr": ("getSigma2error",)}
m2stability = M2Stability.newWithData
baseplot.Images._addPlot("m2stability", M2Stability)
baseplot.Images._addPlot("sigma2stability", M2Stability)

