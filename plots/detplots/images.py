from .. import baseplot
from ..baseplot import DetPlot, DetImgPlot
from .. import plots


class Signal(DetImgPlot):
    ckw_axes = dict(xlabel="Signal [ADU]")
    def setImg(self, **kwargs):
        self.img = self.detector.getSignal(**kwargs)
baseplot.Combined._addPlot("signal", Signal)


class M2(DetImgPlot):
    ckw_axes = dict(xlabel="Variance [ADU^2]")
    def setImg(self, **kwargs):
        self.img = self.detector.getSigma2(**kwargs)
baseplot.Combined._addPlot("m2", M2)
baseplot.Combined._addPlot("sigma2", M2)


class Std(DetImgPlot):
    def setImg(self, **kwargs):
        self.img = self.detector.getSigma(**kwargs)
baseplot.Combined._addPlot("std", Std)
baseplot.Combined._addPlot("sigma", Std)

