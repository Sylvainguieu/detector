from .. import baseplot
from ..baseplot import DetPlot, DetXYPlot, DetImgPlot
from .. import plots
import numpy as np


class GainFit(plots.Fit2d):
    dim = 1

    def get_text(self):
        coeff = self.result
        texts = []
        for c, expo in zip(coeff, range(len(coeff))[::-1]):
            if expo > 1:
                texts.append("%+4.2E P^%d" % (c, expo))
            elif expo == 1:
                texts.append("%+4.2E P" % (c))
            else:
                texts.append("%+4.2E" % (c))
        return " ".join(texts)
        return reduce(lambda t, ci: t + "%+4.2E.P^%d" % (ci[0], ci[1]),
                      zip(coeff, range(len(coeff))[::-1]), ""
                      )
gainfit = GainFit(dim=3)


class DetPlotPolar(DetXYPlot):
    def setX(self, **kwargs):
        polar_unit = kwargs.pop("polar_unit", "mv")
        kwargs.setdefault("freduce", np.nanmean)
        scale = {"mv": 1, "v": 1000.}[polar_unit]
        self.x = self.detector.get_polar()/scale
        self.kw_axes["xlabel"] = "Polar [%s]"%polar_unit

class GainMPolar(DetPlotPolar):
    """ Multiplicative Gain [no units] function to detector polarisation [mv]
    """
    ckw_axes = dict( ylabel="Gain")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)
    def setY(self, **kwargs):
        self.y = self.detector.getGainm(**kwargs)

gainmpolar = GainMPolar.newWithData
baseplot.LinearityList._addPlot("gainmpolar", GainMPolar)


class GainPolar(DetPlotPolar):
    """ Gain system [e-/ADU] function to detector polarisation [mv]"""
    ckw_axes = dict(ylabel="Gain System [e-/ADU]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)
    def setY(self, **kwargs):
        self.y = self.detector.getGain(**kwargs)

gainpolar = GainPolar.newWithData
baseplot.PtcList._addPlot("gainpolar", GainPolar)

class NoiseMinPolar(DetXYPlot):
    """ Min dit noise (std) [ADU]  function to detector polarisation [mV]"""
    ckw_axes = dict(xlabel="Polar [mv]", ylabel="Noise [ADU rms]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)
    def setX(self, **kwargs):
        self.x = self.detector.get_polarSet()
    def setY(self, **kwargs):
        self.y = self.detector.getMinDitSigma(**kwargs)

noiseminpolar = NoiseMinPolar.newWithData
baseplot.Carac._addPlot("noiseminpolar", NoiseMinPolar)


class NoisePolar(DetPlotPolar):
    """ Min dit noise (std) in ADU  function to polarisation """
    ckw_axes = dict(xlabel="Polar [mv]", ylabel="Noise [ADU rms]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)
    def setY(self, **kwargs):
        self.y = self.detector.getNoise(**kwargs)

noisepolar = NoisePolar.newWithData
baseplot.PtcList._addPlot("noisepolar", NoisePolar)


class eNoisePolar(DetPlotPolar):
    """ Min dit noise (std) in e- function to polarisation """
    ckw_axes = dict(xlabel="Polar [mv]", ylabel="Noise [e- rms]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)
    def setY(self, **kwargs):
        self.y = self.detector.getEnoise(**kwargs)

noisepolar = eNoisePolar.newWithData
baseplot.PtcList._addPlot("enoisepolar", eNoisePolar)


class MfPolar(DetPlotPolar):
    """ MxF function to polarisation """
    ckw_axes = dict(xlabel="Polar [mv]", ylabel="MxF")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)

    def setY(self, **kwargs):
        self.y = self.detector.getMf(**kwargs)

mfpolar = MfPolar.newWithData
baseplot.PtcList._addPlot("mfpolar", MfPolar)


class ExcessnoisePolar(DetPlotPolar):
    """ Excessnoise function to polarisation """
    ckw_axes = dict(xlabel="Polar [mv]", ylabel="F")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)
    def setY(self, **kwargs):
        self.y = self.detector.getExcessnoise(**kwargs)

excessnoisepolar = ExcessnoisePolar.newWithData
baseplot.PtcList._addPlot("excessnoisepolar", ExcessnoisePolar)


class eFluxPolar(DetPlotPolar):
    """ Flux in e-/ms function to polarisation [mv]"""
    ckw_axes = dict(xlabel="Polar [mv]", ylabel="Flux [e-/ms]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=gainfit)
    def setY(self, **kwargs):
        time_unit = kwargs.pop("time_unit", "ms")
        scale = {"ms": 1000., "s": 1.}[time_unit]
        self.y = self.detector.getEflux(**kwargs)/scale

efluxpolar = NoisePolar.newWithData
baseplot.PtcList._addPlot("efluxpolar", eFluxPolar)


####################################################
#
#  Images
#
####################################################

class Gain(DetImgPlot):
    ckw_axes = dict(xlabel="Gain Sys [e-/ADU]")

    def setImg(self, **kwargs):
        self.img = self.detector.getGain(**kwargs)
baseplot.Ptc._addPlot("gain", Gain)


class Noise2(DetImgPlot):
    """ square of noise as the result of PTC fit [ADU^2 rms]"""
    ckw_axes = dict(xlabel="Noise^2 [ADU^2 rms]")

    def setImg(self, **kwargs):
        self.img = self.detector.getNoise2(**kwargs)
baseplot.Ptc._addPlot("noise2", Noise2)


class Noise(DetImgPlot):
    ckw_axes = dict(xlabel="Noise [ADU rms]")

    def setImg(self, **kwargs):
        self.img = self.detector.getNoise(**kwargs)
baseplot.Ptc._addPlot("noisemin", Noise)


class NoiseMin(DetImgPlot):
    ckw_axes = dict(xlabel="Noise [ADU rms]")

    def setImg(self, **kwargs):
        self.img = self.detector.getMinDitSigma(**kwargs)
baseplot.Images._addPlot("noisemin", NoiseMin)


class eNoiseMin(DetImgPlot):
    """ The min dit std image in [e-]"""
    ckw_axes = dict(xlabel="Noise [e- rms]")

    def setImg(self, **kwargs):
        self.img = self.detector.getEnoise(**kwargs)
baseplot.Ptc._addPlot("enoisemin", eNoiseMin)


class Flux(DetImgPlot):
    """ Flux image in ADU """
    ckw_axes = dict(xlabel="Flux [ADU/{unit}]")

    def setImg(self, **kwargs):
        time_unit = kwargs.pop("time_unit", "ms")
        scale = {"ms": 1000., "s": 1.}[time_unit]
        self.kw_axes["xlabel"] = self.ckw_axes["xlabel"].format(unit=time_unit)
        self.img = self.detector.getFlux(**kwargs)/scale
baseplot.Linearity._addPlot("flux", Flux)


class eFlux(DetImgPlot):
    """ Flux image in e-"""
    ckw_axes = dict(title="Flux [e-/{unit}]")

    def setImg(self, **kwargs):
        time_unit = kwargs.pop("time_unit", "ms")
        scale = {"ms": 1000., "s": 1.}[time_unit]
        self.kw_axes["title"] = self.ckw_axes["title"].format(unit=time_unit)
        self.img = self.detector.getEflux(**kwargs)/scale
baseplot.PtcLinearity._addPlot("eflux", eFlux)


class Bias(DetImgPlot):
    ckw_axes = dict(xlabel="Bias [ADU]")

    def setImg(self, **kwargs):
        self.img = self.detector.getBias(**kwargs)
baseplot.Linearity._addPlot("bias", Bias)



