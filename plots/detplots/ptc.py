from .. import baseplot
from ..baseplot import DetPlot, DetXYPlot, DetImgPlot
from .. import plots
import numpy as np
"""
    def setX(self, **kwargs):
        self.x = self.detector.(**kwargs)
    def setY(self, **kwargs):
        self.y = self.detector.(**kwargs)
    def setXerr(self, **kwargs):
        self.xerr = self.detector.(**kwargs)
    def setYerr(self, **kwargs):
        self.yerr = self.detector.(**kwargs)

"""

class PtcFit(plots.Fit2d):
    def fit(self, signal, m2, signalErr=None, m2err=None, **kwargs):
        super(PtcFit, self).fit(signal, m2, signalErr, m2err, **kwargs)
        result = self.result
        whereMin = signal.argmin()

        minM2 = m2[whereMin]
        minM2err = m2err[whereMin] if hasattr(m2err, "__iter__") else None
        k = 1./result[0]

        floornoise = np.sqrt(minM2) * k
        floornoiseerror = 0.0 if minM2err is None else np.sqrt(minM2err)*k
        gain = k
        floornoiseptc2 = result[1]/k**2
        self.result = {"floornoise": floornoise,
                       "floornoiseerror": floornoiseerror,
                       "gain": gain,
                       "floornoiseptc2": floornoiseptc2,
                       "coef": result
                       }

    def get_model(self, x):
        if self.result is None:
            raise ValueError("no fit result recorded use .fit before")
        return plots.get_polynome_model(x, self.result["coef"])

    def get_text(self):
        msg = "gain= {gain:5.3f} e-/ADU,\
$noise_{{min}}$ = {floornoise:3.2f} e-+-{floornoiseerror:3.2f}"
        return msg.format(floornoiseptc=np.sqrt(self.result["floornoiseptc2"]),
                          **self.result
                          )


ptcfit = PtcFit(xrange=(0, 6000))

class Ptc(DetXYPlot):
    ckw_axes = dict(ylabel="$\sigma^2$ [$ADU^2$]", xlabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=ptcfit)

    def setX(self, **kwargs):
        self.x = self.detector.getSignal(**kwargs)
    def setY(self, **kwargs):
        self.y = self.detector.getSigma2(**kwargs)
    def setXerr(self, **kwargs):
        self.xerr = self.detector.getSignalerror(**kwargs)
    def setYerr(self, **kwargs):
        self.yerr = self.detector.getSigma2error(**kwargs)



ptc = Ptc.newWithData
baseplot.Images._addPlot("ptc", Ptc)
