from .. import baseplot
from ..baseplot import DetPlot, DetImgPlot, DetXYPlot
from .. import plots
import numpy as np
class LinearityFit(plots.Fit2d):
    dim = 1

    def get_text(self):
        coeff = self.result
        if self.dim == 1:
            return "%5.2f ADU/ms %+5.2f %s" % (coeff[0]/1000., coeff[1],
                                               self.kw["yrange"]
                                               )
        return "%s , %s" % (coeff, self.kw["yrange"])

linearityfit = LinearityFit(yrange=(0, 4000))




class Linearity(DetXYPlot):
    ckw_axes = dict(xlabel="DIT [s]", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=linearityfit)

    def setX(self, **kwargs):
        self.x = self.detector.get_dit()
    def setY(self, **kwargs):
        self.y = self.detector.getSignal(**kwargs)
    def setYerr(self, **kwargs):
        self.yerr = self.detector.getSignalerror(**kwargs)

linearity = Linearity.newWithData
baseplot.Images._addPlot("linearity", Linearity)


class SignalIllumination(DetXYPlot):
    ckw_axes = dict(xlabel="Illumination", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="upper left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=linearityfit, dim=3)

    def setX(self, **kwargs):
        base = kwargs.pop("base", False)
        if not base:
            self.x = self.detector.get_illumination(**kwargs)
        else:
            self.x = base**(self.detector.get_illumination(**kwargs))

    def setY(self, **kwargs):
        self.y = self.detector.getSignal(**kwargs)

    def setYerr(self, **kwargs):
        self.yerr = self.detector.getSignalerror(**kwargs)


signalillumination = SignalIllumination.newWithData
baseplot.Images._addPlot("signalillumination", SignalIllumination)


class M2Dit(DetXYPlot):
    """ Variance function to dit """
    ckw_axes = dict(xlabel="DIT [s]", ylabel="<S - Soff> [ADU]")
    ckw_legend = dict(loc="lower left", prop={"size": 8}, frameon=False,
                      labelspacing=0.1
                      )
    ckw_fit = dict(fitter=linearityfit)

    def setX(self, **kwargs):
        self.x = self.detector.get_dit()
    def setY(self, **kwargs):
        self.y = self.detector.getSigma2(**kwargs)
    def setYerr(self, **kwargs):
        self.yerr = self.detector.getSigma2error(**kwargs)

m2dit = M2Dit.newWithData
baseplot.Images._addPlot("sigma2dit", M2Dit)


