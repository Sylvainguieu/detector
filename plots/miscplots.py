from numpy import *
from .plots import *


def plot_gain_var(flats, sec=None, signalrange=(0, 4000)):
# remove the biases
    fs = flats.get_dit.choose((0.0001, None))
    shape = fs.getshape()[1:]
    dits = fs.get_dit.set()
    N = len(dits)
    data_dit = ndarray(tuple([N, 2]+list(shape)), dtype=float64)
    for i, dit in enumerate(dits):
        f = fs.get_dit.choose(dit)
        ptc = f.getPtc(signalrange=signalrange)
        gain = ptc.getGain()
        noise2 = ptc.getNoise2()

        data_dit[i, 0, :, :] = gain
        #gain     = 1./coeff[0]
        #noise2  = (coeff[1]*k**2)
        data_dit[i, 1, :, :] = noise2 / (gain**2)  # intercept of sig^2 axis


    # remove the darks
    fs = flats.get_illumination.choose((-9, None))
    shape = fs.getshape()[1:]
    illuminations = fs.get_illumination.set()
    N = len(illuminations)
    data_illum = ndarray(tuple([N, 2]+list(shape)), dtype=float64)
    for i, illumination in enumerate(illuminations):
        f = fs.get_illumination.choose(illumination)
        ptc = f.getPtc(signalrange=signalrange)
        gain = ptc.getGain()
        noise2 = ptc.getNoise2()
        data_illum[i, 0, :, :] = gain
        data_illum[i, 1, :, :] = noise2 / (gain**2)

    gain_dit = [nanmedian(d[(0,)+tuple(sec)]) for d in data_dit]
    gain_illum = [nanmedian(d[(0,)+tuple(sec)]) for d in data_illum]

    noise2_dit = [nanmedian(d[(1,)+tuple(sec)]) for d in data_dit]
    noise2_illum = [nanmedian(d[(1,)+tuple(sec)]) for d in data_illum]

    fdit = flats.get_dit.choose((0.0001, None)).get_illumination.choose((-9, None))
    fillum = flats.get_illumination.choose((-9, None)).get_dit.choose((0.0001, None))

    flux_dit_mean = [np.mean(f.getSignal(section=sec)) for f in fdit.get_dit.iter()]
    flux_illum_mean = [np.mean(f.getSignal(section=sec)) for f in fillum.get_illumination.iter()]

    flux_dit_max = [np.max(f.getSignal(section=sec)) for f in fdit.get_dit.iter()]
    flux_illum_max = [np.max(f.getSignal(section=sec)) for f in fillum.get_illumination.iter()]
    flux_dit_min = [np.min(f.getSignal(section=sec)) for f in fdit.get_dit.iter()]
    flux_illum_min = [np.min(f.getSignal(section=sec)) for f in fillum.get_illumination.iter()]

    fig, ax = subplots(3, 2, sharex="col")

    ax[0,0].plot(dits, gain_dit, "D", dits, gain_dit, ":")
    ax[0,1].plot(illuminations, gain_illum, "D",
                 illuminations, gain_illum, ":"
                 )
    ax[0,0].set(ylabel="Gain e-/ADU", xlim=(0.4/1000, 2.1/1000.))
    ax[0,1].set(ylabel="Gain e-/ADU", xlim=(-1, max(illuminations)+1))

    ax[1,0].plot(dits, noise2_dit, "D", dits, noise2_dit, ":")
    ax[1,1].plot(illuminations, noise2_illum, "D",
                 illuminations, noise2_illum, ":"
                 )
    ax[1,0].set(ylabel="$\sigma^2$ [ADU^2]", xlim=(0.4/1000, 2.1/1000.))
    ax[1,1].set(ylabel="$\sigma^2$ [ADU^2]", xlim=(-1, max(illuminations)+1))


    ax[2,0].vlines(dits, flux_dit_min,flux_dit_max)
    ax[2,1].vlines(illuminations, flux_illum_min, flux_illum_max)
    ax[2,0].plot(dits, flux_dit_mean, "D", dits, flux_dit_mean, ":")
    ax[2,1].plot(illuminations, flux_illum_mean, "D",illuminations, flux_illum_mean,":")
    ax[2,0].set( xlabel="DIT [s]", ylabel="Signal Range [ADU]")
    ax[2,1].set( xlabel="Illumination", ylabel="Signal Range [ADU]")
    if signalrange[1] is not None:
        ax[2,0].axhline(signalrange[1], color="red", linestyle=":")
        ax[2,1].axhline(signalrange[1], color="red", linestyle=":")
    if signalrange[0] is not None:
        ax[2,0].axhline(signalrange[0], color="red", linestyle=":")
        ax[2,1].axhline(signalrange[0], color="red", linestyle=":")
    fig.set_size_inches( (14,10))
    return fig
